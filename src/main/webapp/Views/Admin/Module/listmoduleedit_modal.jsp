<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="modal modal-insert-customer" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title">
                    <div class="content-title">
                        <h3 class="text-center">Cập nhật module</h3>
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <form id="form-edit-module" class="form-horizontal" novalidate method="POST" action="<c:url value='/Admin/Module/ListModule/Edit'/>">
                    <input type="hidden" name="id" value="${MODULE_EDIT.id}">
                    <div class="form-group">
                        <label class="control-label col-sm-3">Tên</label>
                        <div class="col-sm-9">
                            <input type="text" id="name" required name="name" value="${MODULE_EDIT.name}" class="form-control" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">Đường dẫn</label>
                        <div class="col-sm-9">
                            <input type="text" id="controller" required name="controller" value="${MODULE_EDIT.controller}" class="form-control" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">Lớp Icon</label>
                        <div class="col-sm-9">
                            <input type="text" id="icon" name="icon" class="form-control" value="${MODULE_EDIT.icon}" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">Lớp Css</label>
                        <div class="col-sm-9">
                            <input type="text" id="cssClass" name="cssClass" class="form-control" value="${MODULE_EDIT.cssClass}" placeholder="">
                        </div>
                    </div>
                    <div class="form-group text-center clearfix">
                        <button type="submit" class="btn btn-primary pull-right">Lưu</button>
                        <button type="reset" class="btn btn-danger pull-left">Nhập lại</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $('#form-edit-module').validate({
        submitHandler: function () {
            var data = JSON.stringify($('#form-edit-module').serializeObject());
            var url = $('#form-edit-module').attr('action');
            $.ajax({
                beforeSend: function (xhr) {
                    ajaxStartHandle();
                },
                url: url,
                type: 'POST',
                data: data,
                contentType: 'application/json',
                success: function (data) {
                    openMessage(data, function () {
                        reloadAjaxContent();
                    });
                }, error: function () {
                    reloadAjaxContent();
                },
                complete: function (jqXHR, textStatus) {
                    ajaxCompleteHandle();
                }
            });
        }
    });
</script>

