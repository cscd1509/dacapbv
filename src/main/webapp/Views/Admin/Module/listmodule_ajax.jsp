<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="module-role-list">
    <ul>
        <li>
            <label>
                <span class="module-collapse"><i class="far fa-minus-square"></i></span>
                <span>ROOT</span>
                <ul class="module-action">
                    <li><a class="btn-open-modal text-success" title="Thêm mới module con cho module này" controller="<c:url value="/Admin/Module/ListModule/ViewInsert/1"/>"><i class="fa fa-plus-circle"></i></a></li>
                </ul>
            </label>
            <ul>
                <c:forEach items="${MODULES}" var="MODULE"> 
                    <c:if test="${!MODULE.isDelete}">
                        <li>
                            <label>
                                <span class="module-collapse"><i class="far fa-minus-square"></i></span>
                                <span>${MODULE.name}</span>
                                <ul class="module-action">
                                    <li><a class="btn-open-modal text-success" controller="<c:url value="/Admin/Module/ListModule/ViewInsert/${MODULE.id}"/>"><i class="fa fa-plus-circle"></i></a></li>
                                    <li><a class="btn-open-modal text-primary" controller="<c:url value="/Admin/Module/ListModule/ViewEdit/${MODULE.id}"/>"><i class="fa fa-edit"></i></a></li>
                                    <li><a class="text-warning btn-send-ajax" controller="<c:url value="/Admin/Module/ListModule/ChangeStatus/${MODULE.id}"/>"><i class="fa ${MODULE.isShow?'fa-lock':'fa-unlock'}"></i></a></li>
                                    <li><a class="text-danger btn-send-ajax" controller="<c:url value="/Admin/Module/ListModule/Delete/${MODULE.id}"/>"><i class="fas fa-trash"></i></a></li>
                                </ul>
                            </label> 
                            <ul>                                                    
                                <c:forEach items="${MODULE.modules}" var="childrenModule">
                                    <c:if test="${!childrenModule.isDelete}">
                                        <li>
                                            <label>
                                                <span>${childrenModule.name}</span>                                                        
                                                <ul class="module-action">
                                                    <li><a class="btn-open-modal text-primary" controller="<c:url value="/Admin/Module/ListModule/ViewEdit/${childrenModule.id}"/>"><i class="fa fa-edit"></i></a></li>
                                                    <li><a class="text-warning btn-send-ajax" controller="<c:url value="/Admin/Module/ListModule/ChangeStatus/${childrenModule.id}"/>"><i class="fa ${childrenModule.isShow?'fa-lock':'fa-unlock'}"></i></a></li>
                                                    <li><a class="text-danger btn-send-ajax" controller="<c:url value="/Admin/Module/ListModule/Delete/${childrenModule.id}"/>"><i class="fas fa-trash"></i></a></li>
                                                </ul>
                                            </label>
                                        </li>                                                            
                                    </c:if>
                                </c:forEach>
                            </ul>
                        </li>
                    </c:if>
                </c:forEach>                                    
            </ul>
        </li>
    </ul>
</div>

<script>
    $('.module-collapse').on('click',function(){
        $(this).children('.far').toggleClass('fa-minus-square').toggleClass('fa-plus-square');
        $(this).parent('label').parent('li').children('ul').stop().toggle(300);
    });
</script>