<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="f" uri="/WEB-INF/tlds/functions.tld" %>
<div class="modal modal-insert-customer" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title">
                    <div class="content-title">
                        <h3 class="text-center">Thêm mới Module</h3>
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <form id="form-insert-module" class="form-horizontal" novalidate method="POST" action="<c:url value='/Admin/Module/ListModule/Insert'/>">
                    <div class="form-group">
                        <label class="control-label col-sm-3">Tên</label>
                        <div class="col-sm-9">
                            <input type="text" id="name" required name="name" class="form-control" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">Đường dẫn</label>
                        <div class="col-sm-9">
                            <input type="text" id="controller" required name="controller" class="form-control" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">Module cha</label>
                        <div class="col-sm-9">
                            <label class="control-label"><b class="bold-red">${MODULE_PARENT.name}</b></label>
                            <input type="hidden" class="form-control" name="module" value='${MODULE_PARENT.id}' readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">Lớp Icon</label>
                        <div class="col-sm-9">
                            <input type="text" id="icon" name="icon" class="form-control" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">Lớp Css</label>
                        <div class="col-sm-9">
                            <input type="text" id="cssClass" name="cssClass" class="form-control" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">Hiển thị</label>
                        <div class="col-sm-9">
                            <input type="checkbox" id="isShow" checked name="isShow">
                        </div>
                    </div>
                    <div class="form-group text-center clearfix">
                        <button type="submit" class="btn btn-primary pull-right">Lưu</button>
                        <button type="reset" class="btn btn-danger pull-left">Nhập lại</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $('#form-insert-module').validate({
        submitHandler: function () {
            var data = JSON.stringify($('#form-insert-module').serializeObject());
            var url = $('#form-insert-module').attr('action');
            $.ajax({
                beforeSend: function (xhr) {
                    ajaxStartHandle();
                },
                url: url,
                type: 'POST',
                data: data,
                contentType: 'application/json',
                success: function (data) {
                    openMessage(data, function () {
                        reloadAjaxContent();
                    });
                }, error: function () {
                    reloadAjaxContent();
                },
                complete: function (jqXHR, textStatus) {
                    ajaxCompleteHandle();
                }
            });
        }
    });
</script>
