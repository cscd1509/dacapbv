<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<input type="hidden" id="reloadController" value="<c:url value='/Admin/Module/ListModule/Ajax'/>">
<div class="content-title">
    <h3 class="text-center">Danh sách module</h3>
</div>
<div class="panel panel-default">
    <div class="ajax-content panel-body">
        <c:import url="/Views/Admin/Module/listmodule_ajax.jsp"/>
    </div>
</div><!-- end panel-->