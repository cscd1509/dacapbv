<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="f" uri="/WEB-INF/tlds/functions.tld" %>
<input type="hidden" id="reloadController" value="<c:url value='/Admin/Permission/ListTransfer/Ajax${f:buildPager(PAGER)}'/>">
<div class="content-title">
    <h3 class="text-center">Lịch sử chuyển tiền</h3>
</div>
<div class="panel panel-default">
    <div class="panel-heading">        
        <form class="form-inline text-right" id="pager-form" role="form" action="<c:url value="/Admin/Permission/ListTransfer"/>" method="GET">
            <div class="form-group">
                <label>Từ khóa</label>
                <input type="text" class="form-control" name="keyword" value="${param.keyword}" />
            </div>
            <div class="form-group">
                <label>Sắp xếp theo</label>
                <select name="orderColumn" class="form-control">
                    <option value="sendindAdminID" ${PAGER.orderColumn=="sendindAdminID"?"selected":""}>Người chuyển</option>
                    <option value="receivingAdminID" ${PAGER.orderColumn=="receivingAdminID"?"selected":""}>Người nhận</option>
                    <option value="amount" ${PAGER.orderColumn=="amount"?"selected":""}>Số tiền chuyển</option>
                    <option value="createdDate" ${PAGER.orderColumn=="createdDate"?"selected":""}>T/g giao dịch</option>
                </select>
            </div>
            <div class="form-group">
                <label>Thứ tự</label>
                <select name="asc" class="form-control">
                    <option value="true" ${PAGER.asc=="true"?"selected":""}>Tăng dần</option>
                    <option value="false" ${PAGER.asc=="false"?"selected":""}>Giảm dần</option>
                </select>
            </div>
            <input type="hidden" name="currentPage" value="${PAGER.currentPage}"/>
            <input type="hidden" name="displayPerPage" value="${PAGER.displayPerPage}"/>
            <button type="submit" class="btn btn-info">Lọc</button>
        </form>
    </div>
    <br/>
    <div class="clearfix">
        <a class="btn btn-primary btn-lg pull-right" data-toggle="modal" data-target="#modal-transfer">Nạp tiền <i class="fas fa-hand-holding-usd"></i></a>
    </div>
    <div class="ajax-content panel-body">   
        <c:import url="/Views/Admin/HistoryTransfer/listtransfer_ajax.jsp"/>
    </div>
</div>
<c:import url="/Views/Admin/HistoryTransfer/listtransfer_insert_ajax.jsp"/>