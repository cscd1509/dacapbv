<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="f" uri="/WEB-INF/tlds/functions.tld" %>
<c:if test="${f:size(ITEMS_LIST)==0}">
    <div class="alert alert-danger">
        Không tồn tại bản ghi nào!
    </div>
</c:if>
<c:if test="${f:size(ITEMS_LIST)!=0}">
    <table class="table table-condensed table-hover table-valign-midle table-grid-view table-align-center table-bordered">
        <thead>
            <tr>
                <th class="th-id" column="id">ID <span class="${PAGER.orderColumn=='id'?(PAGER.asc?'fa fa-caret-up':'fa fa-caret-down'):''}"></span></th>
                <th class="text-left" column="customerID">Username <span class="${PAGER.orderColumn=='customerID'?(PAGER.asc?'fa fa-caret-up':'fa fa-caret-down'):''}"></span></th>
                <th class="text-left" column="actionID">Title <span class="${PAGER.orderColumn=='actionID'?(PAGER.asc?'fa fa-caret-up':'fa fa-caret-down'):''}"></span></th>
                <th column="countryID">Category <span class="${PAGER.orderColumn=='countryID'?(PAGER.asc?'fa fa-caret-up':'fa fa-caret-down'):''}"></span></th>
                <th class="text-right" column="createdDate">Created Time <span class="${PAGER.orderColumn=='createdDate'?(PAGER.asc?'fa fa-caret-up':'fa fa-caret-down'):''}"></span></th>
                <th class="th-action external"></th>  
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${ITEMS_LIST}" var="ca">
                <tr>
                    <td>${ca.id}</td>
                    <td class="text-left">${ca.customerID.userName}</td>
                    <td class="text-left">${ca.title}</td>
                    <td>${ca.categoryID.actionName}</td>
                    <td class="text-right">${ca.createdDate}</td>
                    <td><a class="btn-open-modal btn btn-default btn-sm" controller="<c:url value='/Admin/Message/View/${ca.id}'/>"><i class="fa fa-eye"></i></a></td>
                </tr>
            </c:forEach>
        </tbody>
    </table><!-- end table -->
</c:if>
<c:if test="${PAGER.totalResult>0}">
    <div class="row">
        <div class="col-md-4 col-xs-12">
            <c:if test="${PAGER.totalResult>5}">
                <label class="control-label">Display: </label>
                <select class="form-control input-sm btn-change-display-per-page" controller="<c:url value='/Admin/Message/ListMessageNotRead?currentPage=1&orderColumn=id&asc=true&displayPerPage='/>">
                    <c:forEach begin="5" step="5" end="50" var="numb">
                        <option ${numb==PAGER.displayPerPage?'selected':''} value="${numb}">${numb}</option>
                    </c:forEach>
                </select>
            </c:if>
        </div>
        <div class="col-md-8 col-xs-12 text-right"> 
            <ul class="pagination pagination-sm">
                <li class="${PAGER.currentPage<=1?"hidden":""}">
                    <a href="<c:url value="/Admin/Message/ListMessageNotRead?currentPage=1&displayPerPage=${PAGER.displayPerPage}&orderColumn=${PAGER.orderColumn}&asc=${PAGER.asc}"/>"><i class="fa fa-angle-double-left"></i></a>
                </li>
                <li class="${PAGER.currentPage<=1?"hidden":""}">
                    <a href="<c:url value="/Admin/Message/ListMessageNotRead?currentPage=${PAGER.currentPage-1}&displayPerPage=${PAGER.displayPerPage}&orderColumn=${PAGER.orderColumn}&asc=${PAGER.asc}"/>"><i class="fa fa-angle-left"></i></a>
                </li>
                <c:if test="${PAGER.currentPage==2}">
                    <li>
                        <a href="<c:url value="/Admin/Message/ListMessageNotRead?currentPage=1&displayPerPage=${PAGER.displayPerPage}&orderColumn=${PAGER.orderColumn}&asc=${PAGER.asc}"/>">1</a>
                    </li>
                </c:if>
                <c:if test="${PAGER.currentPage>2}">
                    <c:forEach begin="${PAGER.currentPage-2}" end="${PAGER.currentPage-1}" var="page">
                        <li>
                            <a href="<c:url value="/Admin/Message/ListMessageNotRead?currentPage=${page}&displayPerPage=${PAGER.displayPerPage}&orderColumn=${PAGER.orderColumn}&asc=${PAGER.asc}"/>">${page}</a>
                        </li>
                    </c:forEach>
                </c:if>
                <li>
                    <a class="active" href="javascript:;">${PAGER.currentPage}</a>
                </li>                
                <c:if test="${PAGER.currentPage==PAGER.totalPage-1}">
                    <li>
                        <a href="<c:url value="/Admin/Message/ListMessageNotRead?currentPage=${PAGER.totalPage}&displayPerPage=${PAGER.displayPerPage}&orderColumn=${PAGER.orderColumn}&asc=${PAGER.asc}"/>">${PAGER.totalPage}</a>
                    </li>
                </c:if>
                <c:if test="${PAGER.currentPage<PAGER.totalPage-1}">
                    <c:forEach begin="${PAGER.currentPage+1}" end="${PAGER.currentPage+2}" var="page">
                        <li>
                            <a href="<c:url value="/Admin/Message/ListMessageNotRead?currentPage=${page}&displayPerPage=${PAGER.displayPerPage}&orderColumn=${PAGER.orderColumn}&asc=${PAGER.asc}"/>">${page}</a>
                        </li>
                    </c:forEach> 
                </c:if>              
                <li class="${PAGER.currentPage==PAGER.totalPage?"hidden":""}">
                    <a href="<c:url value="/Admin/Message/ListMessageNotRead?currentPage=${PAGER.currentPage+1}&displayPerPage=${PAGER.displayPerPage}&orderColumn=${PAGER.orderColumn}&asc=${PAGER.asc}"/>"><i class="fa fa-angle-right"></i></a>
                </li>
                <li class="${PAGER.currentPage==PAGER.totalPage?"hidden":""}">
                    <a href="<c:url value="/Admin/Message/ListMessageNotRead?currentPage=${PAGER.totalPage}&displayPerPage=${PAGER.displayPerPage}&orderColumn=${PAGER.orderColumn}&asc=${PAGER.asc}"/>"><i class="fa fa-angle-double-right"></i></a>
                </li>
            </ul> 
        </div>
    </div>
</c:if>