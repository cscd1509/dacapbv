<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="modal msg-detail-modal fade" id="myModal" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">  
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="defModalHead">${MESSAGE_SYSTEM.title}</h4>
            </div>
            <div class="modal-body">  
                <h5 style="margin-bottom: 15px">Sender: <label class="label-success label">${MESSAGE_SYSTEM.customerID.userName}</label></h5>
                <div style="margin-bottom: 15px">Category:  <label class="label-info label">${MESSAGE_SYSTEM.categoryID.actionName}</label></div>
                <div style="margin-bottom: 5px"><small><i class="fa fa-clock-o"></i> ${MESSAGE_SYSTEM.createdDate}</small></div>
                <hr/>
                <div class="content" style="line-height: 20px">
                    ${MESSAGE_SYSTEM.content}
                </div>
            </div>
            <div class="modal-footer clearfix">
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
    $('#myModal.msg-detail-modal').on('shown.bs.modal', function (e) {
        e.preventDefault();
        e.stopPropagation();
        reloadAjaxContent();
    });
</script>