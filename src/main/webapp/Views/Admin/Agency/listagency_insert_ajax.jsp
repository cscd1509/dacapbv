<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="f" uri="/WEB-INF/tlds/functions.tld" %>
<div class="modal modal-insert-customer" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title">
                    <div class="content-title">
                        <h3 class="text-center">Thêm mới đại lý</h3>
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="form-insert-admin" novalidate method="POST" action="<c:url value='/Admin/Permission/ListAgency/Insert'/>">
                    <div class="form-group">
                        <label for="name" class="control-label col-sm-3">Tên</label>
                        <div class="col-sm-9">
                            <input type="text" id="name" name="name" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group text-center clearfix">
                        <button type="submit" class="btn btn-primary pull-right">Lưu <i class="fa fa-check fa-right"></i></button>
                        <button type="reset" class="btn btn-danger pull-left">Nhập lại <i class="fa fa-times fa-right"></i></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $('#form-insert-admin').validate({
        submitHandler: function () {
            var customer = JSON.stringify($('#form-insert-admin').serializeObject());
            console.log(customer)
            var url = $('#form-insert-admin').attr('action');
            $.ajax({
                beforeSend: function (xhr) {
                    ajaxStartHandle();
                },
                url: url,
                type: 'POST',
                data: customer,
                contentType: 'application/json',
                success: function (data) {
                    openMessage(data, function () {
                        reloadAjaxContent();
                    });
                },
                complete: function (jqXHR, textStatus) {
                    ajaxCompleteHandle();
                }
            });
        }
    });
</script>