<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="f" uri="/WEB-INF/tlds/functions.tld" %>
<div class="modal modal-insert-customer" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title">
                    <div class="content-title">
                        <h3 class="text-center">Change Password</h3>
                    </div>
                </div>
            </div>
            <c:if test="${ADMIN_EDIT==null}">
                <c:set var="url" value="/Admin/Permission/ChangePassword" />
            </c:if>
            <c:if test="${ADMIN_EDIT!=null}">
                <c:set var="url" value="/Admin/Permission/ChangePassword/${ADMIN_EDIT}" />
            </c:if>
            <form id="form-change-password" class="form-horizontal" novalidate method="POST" action="<c:url value="${url}"/>">
                <div class="modal-body">
                    <c:if test="${ADMIN_EDIT==null}">
                        <div class="form-group">
                            <label class="control-label col-sm-3">Old Password</label>
                            <div class="col-sm-9">
                                <input type="password" required name="oldPass" class="form-control" />
                            </div>
                        </div>
                    </c:if>
                    <div class="form-group">
                        <label class="control-label col-sm-3">New Password</label>
                        <div class="col-sm-9">
                            <input type="password" required name="newPass" class="form-control" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer text-center">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <button type="reset" class="btn btn-danger">Clear Form</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $('#form-change-password').validate({
        submitHandler: function () {
            var url = $('#form-change-password').attr('action');
            $.ajax({
                beforeSend: function (xhr) {
                    ajaxStartHandle();
                },
                url: url,
                type: 'POST',
                data: $('#form-change-password').serializeObject(),
                success: function (data) {
                    clearTimeout(timeoutDisplayMessage);
                    if (typeof data !== "undefined") {
                        $('body').append(data);
                    }
                    $('#message').fadeIn(200, function () {
                        timeoutDisplayMessage = setTimeout(function () {
                            $('#message').remove();
                        }, 3000);
                    });
                },
                complete: function (jqXHR, textStatus) {
                    ajaxCompleteHandle();
                }
            });
        }
    });
</script>