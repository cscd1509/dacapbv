<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="f" uri="/WEB-INF/tlds/functions.tld" %>
<div class="modal modal-insert-customer" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title">
                    <div class="content-title">
                        <h3 class="text-center">Thêm mới QTV</h3>
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="form-insert-admin" novalidate method="POST" action="<c:url value='/Admin/Permission/ListAdmin/Insert'/>">
                    <div class="form-group">
                        <label for="name" class="control-label col-sm-3">Họ và tên</label>
                        <div class="col-sm-9">
                            <input type="text" id="name" name="name" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="userName" class="control-label col-sm-3">Tên đăng nhập</label>
                        <div class="col-sm-9">
                            <input type="text" id="userName" name="userName" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password" class="control-label col-sm-3">Mật khẩu</label>
                        <div class="col-sm-9">
                            <input type="password" id="password" name="password" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">Quyền</label>
                        <div class="col-sm-9">
                            <select class="form-control" required name="roleAdmID">
                                <option value="">Lựa chọn quyền</option>
                                <c:forEach items="${f:findAllAvailableRoleAdmin()}" var="role">
                                    <option value='${role.id}'>${role.name}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">Đại lý</label>
                        <div class="col-sm-9">
                            <select class="form-control" required name="agencyID">
                                <option value="">Lựa chọn đại lý</option>
                                <c:forEach items="${AVAIABLE_AGENCY}" var="item">
                                    <option value='${item.id}'>${item.name}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">Giới tính</label>
                        <div class="col-sm-9">
                            <select name="gender" class="form-control" required>
                                <option value="">Lựa chọn giới tính</option>
                                <option value="true">Nam</option>
                                <option value="false">Nữ</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="control-label col-sm-3">Email</label>
                        <div class="col-sm-9">
                            <input type="email" id="email" name="email" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="mobile" class="control-label col-sm-3">Số điện thoại</label>
                        <div class="col-sm-9">
                            <input type="text" id="mobile" name="mobile" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="taxCode" class="control-label col-sm-3">Mã số thuế</label>
                        <div class="col-sm-9">
                            <input type="text" id="taxCode" name="taxCode" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="billingAddress" class="control-label col-sm-3">Địa chỉ ngân hàng</label>
                        <div class="col-sm-9">
                            <input type="text" id="billingAddress" name="billingAddress" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="address" class="control-label col-sm-3">Địa chỉ</label>
                        <div class="col-sm-9">
                            <textarea name="address" id="address" rows="4" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="form-group text-center clearfix">
                        <button type="submit" class="btn btn-primary pull-right">Save <i class="fa fa-check fa-right"></i></button>
                        <button type="reset" class="btn btn-danger pull-left">Clear Form <i class="fa fa-times fa-right"></i></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $('#form-insert-admin').validate({
        submitHandler: function () {
            var customer = JSON.stringify($('#form-insert-admin').serializeObject());
            var url = $('#form-insert-admin').attr('action');
            $.ajax({
                beforeSend: function (xhr) {
                    ajaxStartHandle();
                },
                url: url,
                type: 'POST',
                data: customer,
                contentType: 'application/json',
                success: function (data) {
                    openMessage(data, function () {
                        reloadAjaxContent();
                    });
                },
                complete: function (jqXHR, textStatus) {
                    ajaxCompleteHandle();
                }
            });
        }
    });
</script>