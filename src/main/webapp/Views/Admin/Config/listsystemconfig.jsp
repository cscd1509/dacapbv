<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="f" uri="/WEB-INF/tlds/functions.tld" %>
<input type="hidden" id="reloadController" value="<c:url value='/Admin/Config/ListConfig/Ajax${f:buildPager(PAGER)}'/>">
<div class="content-title">
    <h3 class="text-center">List Administrators</h3>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <nav class="navbar navbar-default" role="navigation">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>                       
                    <a type="button" class="navbar-brand btn-reload-content" data-toggle="tooltip" data-placement="bottom" data-title="Tải lại nội dung hiển thị">
                        <i class="fas fa-sync-alt"></i>
                    </a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <div class="navbar-form navbar-left">
                        <div class="dropdown">
                            <a class="btn-sm btn btn-default btn-open-modal" data-toggle="dropdown" href="#">Sắp xếp theo <i class="fas fa-sort-amount-up fa-right"></i></a>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                                <li>                              
                                    <a href="<c:url value='/Admin/Config/ListConfig?currentPage=${PAGER.currentPage}&asc=${PAGER.asc}&displayPerPage=${PAGER.displayPerPage}&orderColumn=id'/>">ID</a>
                                </li>
                                <li>                              
                                    <a href="<c:url value='/Admin/Config/ListConfig?currentPage=${PAGER.currentPage}&asc=${PAGER.asc}&displayPerPage=${PAGER.displayPerPage}&orderColumn=name'/>">Name</a>
                                </li>
                                <li>                              
                                    <a href="<c:url value='/Admin/Config/ListConfig?currentPage=${PAGER.currentPage}&asc=${PAGER.asc}&displayPerPage=${PAGER.displayPerPage}&orderColumn=isTrue'/>">Status</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="navbar-form navbar-left">
                        <div class="dropdown">
                            <a class="btn-sm btn btn-default btn-open-modal" data-toggle="dropdown" href="#">Thứ tự <i class="fa fa-sort fa-right"></i></a>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                                <li>                              
                                    <a href="<c:url value='/Admin/Config/ListConfig?currentPage=${PAGER.currentPage}&displayPerPage=${PAGER.displayPerPage}&orderColumn=${PAGER.orderColumn}&asc=true'/>">Ascending</a>
                                </li>
                                <li>                              
                                    <a href="<c:url value='/Admin/Config/ListConfig?currentPage=${PAGER.currentPage}&displayPerPage=${PAGER.displayPerPage}&orderColumn=${PAGER.orderColumn}&asc=false'/>">Descending</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a class="btn-sm btn btn-default btn-open-modal" controller="<c:url value='/Admin/Config/ListConfig/ViewInsert'/>">Thêm mới <i class="fa fa-plus"></i></a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
    <div class="ajax-content panel-body">   
        <c:import url="/Views/Admin/Config/listsystemconfig_ajax.jsp"/>
    </div>
</div>