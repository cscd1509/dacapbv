<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="f" uri="/WEB-INF/tlds/functions.tld" %>
<div class="content-title">
    <h3 class="text-center">API Config</h3>
</div>
<div class="panel panel-default">
    <div class="panel-body">        
        <form class="form-horizontal" action="/Admin/Config/APIConfig/Update" id="form-update-api" novalidate>
            <div class="form-group">
                <label class="control-label col-sm-3 col-xs-12">API Key</label>
                <div class="col-sm-9 col-sx-12">
                    <input type="text" class="form-control" name="apikey" id="apikey" value="${API.key}" />
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3 col-xs-12">API Secret</label>
                <div class="col-sm-9 col-sx-12">
                    <input type="text" class="form-control" name="apisecret" required id="apisecret" value="${API.secret}" />
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9 col-sx-12">
                    <a class="btn btn-info" href="/Admin/Config/APIConfig/GetAccounts" required id="btn-get-accounts">Get Accounts</a>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3 col-xs-12">Wallet</label>
                <div class="col-sm-9 col-sx-12">
                    <select class="form-control" name="apiwallet" id="apiwallet" required>
                        <option value="">Select Wallet</option>
                        <c:forEach items="${API.accounts.data}" var="account">
                            <option value="${account.id}" ${account.id==API.selectedAccount.id?"selected":""}>${account.name} [${account.balance.amount} BTC]</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9 col-sx-12">
                    <button class="btn btn-primary pull-left">Save</button>
                    <button class="btn btn-danger pull-right">Reset</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    $('#btn-get-accounts').click(function (e) {
        e.preventDefault();
        var key = $('#apikey').val();
        var secret = $('#apisecret').val();
        if (!key.trim().length || !secret.trim().length) {
            alert("Input Key and Secret!");
            return;
        }
        var data = {"key": key, "secret": secret};
        var href = $(this).attr('href');
        $.ajax({
            beforeSend: function () {
                $('#apiwallet').html('<option>Waiting...</option>');
                $('#apiwallet').prop('disabled', true);
            },
            url: href,
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json',
            success: function (data) {
                $('#apiwallet').html('');
                var firtWallet = $('#apiwallet>option:first-child').clone();
                $('#apiwallet').append(firtWallet);
                $.each(data['data'], function (i, v) {
                    $('#apiwallet').append("<option value=" + v.id + ">" + v.name + " [" + v.balance.amount + " BTC]</option>");
                });
                $('#apiwallet').prop('disabled', false);
            }, error: function () {
                alert("error");
                $('#apiwallet').html('');
                $('#apiwallet').prop('disabled', false);
            }
        });
    });

    $('#form-update-api').validate({
        submitHandler: function () {
            var customer = JSON.stringify($('#form-update-api').serializeObject());
            var url = $('#form-update-api').attr('action');
            $.ajax({
                url: url,
                type: 'POST',
                data: customer,
                contentType: 'application/json',
                success: function (data) {
                    openMessageNotReload(data, function () {
                        location.reload();
                    });
                }
            });
        }
    });
</script>