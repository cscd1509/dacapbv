<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/tlds/functions.tld" prefix="f"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<c:set var="ADMIN_ID" value="${sessionScope['ADMIN']['ADMIN_ID']}"/>
<c:set var="ADMIN" value="${f:findAdminById(ADMIN_ID)}" scope="request"/>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Meta Keyword -->
        <meta name="keywords" content="">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <title>BV Admin Control Panel</title>
        <!--<link rel="shortcut icon" href='<c:url value="/Resources/img/web-icon.jpg"/>'>-->
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic&subset=latin,vietnamese' rel='stylesheet' type='text/css'/>
        <link rel="stylesheet" href='<c:url value="/Resources/shared/css/fontawesome-all.min.css"/>' type="text/css">
        <link rel="stylesheet" href='<c:url value="/Resources/css/themify-icons.css"/>' type="text/css">
        <link rel="stylesheet" href='<c:url value="/Resources/css/bootstrap.min.css"/>' type="text/css">
        <link rel="stylesheet" href='<c:url value="/Resources/css/reset-css.css"/>' type="text/css">
        <link rel="stylesheet" href='<c:url value="/Resources/css/message.css"/>' type="text/css">
        <link rel="stylesheet" href='<c:url value="/Resources/css/main_admin.css"/>' type="text/css">
        <link rel="stylesheet" href='<c:url value="/Resources/css/responsive_admin.css"/>' type="text/css">
        <link rel="stylesheet" href='<c:url value="/Resources/layout/home/css/daterangepicker.css"/>' type="text/css">
        <script src='<c:url value="/Resources/js/jquery-1.12.3.min.js"/>'></script>
        <script src="/ckeditor/ckeditor.js" type="text/javascript"></script>
        <script src="/ckeditor/config.js" type="text/javascript"></script>
        <script src="/ckfinder/ckfinder.js" type="text/javascript"></script>
        <script src='<c:url value="/Resources/js/bootstrap.min.js"/>'></script>
        <script src='<c:url value="/Resources/joli/js/plugins/jquery-validation/jquery.validate.js"/>'></script>
        <script src='<c:url value="/Resources/joli/js/plugins/jquery-validation/additional-methods.min.js"/>'></script>
        <script src="<c:url value="/Resources/js/jquery.cookie.js"/>" type="text/javascript"></script>
        <script src='<c:url value="/Resources/js/serialize.js"/>'></script>
        <script src='<c:url value="/Resources/joli/js/plugins/maskedinput/jquery.mask.min.js"/>'></script>
        <script src='<c:url value="/Resources/js/jquery.fileDownload.js"/>'></script>
        <script src='<c:url value="/Resources/layout/home/js/moment.min.js"/>'></script>
        <script src='<c:url value="/Resources/layout/home/js/daterangepicker.js"/>'></script>
        <script src='<c:url value="/Resources/js/main_admin.js"/>'></script>
    </head>
    <body data-request="0">
        <div class="progress progress-striped active" id="spinner">
            <div class="progress-bar"  role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                <span class="sr-only">0% Complete</span>
            </div>
        </div>
        <%@include file="/Views/ModuleAdmin/sidebar.jsp" %>
        <div id="container">
            <%@include file="/Views/ModuleAdmin/navigation.jsp" %>
            <div id="content">
                <t:insertAttribute name="Content" ignore="true" />
            </div> 
        </div>
    </body>
</html>