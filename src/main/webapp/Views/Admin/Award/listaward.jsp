<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="f" uri="/WEB-INF/tlds/functions.tld" %>
<input type="hidden" id="reloadController" value="/Admin/Award/ListAward/Ajax?currentPage=${PAGER.currentPage}&displayPerPage=${PAGER.displayPerPage}&orderColumn=${PAGER.orderColumn}&asc=${PAGER.asc}&keyword=${PAGER.keyword}&startDate=${param['startDate']}&endDate=${param['endDate']}&checkAwardID=${param['checkAwardID']}">
<div class="content-title">
    <h3 class="text-center">Danh sách thưởng</h3>
</div>
<div class="panel panel-default">
    <div class="panel-heading">        
        <form class="form-inline text-right" id="pager-form" role="form" action="<c:url value="/Admin/Award/ListAward"/>" method="GET">
            <div class="form-group">
                <label>Từ khóa</label>
                <input type="text" class="form-control" name="keyword" value="${param.keyword}" />
            </div>
            <div class="form-group">
                <select class="form-control" name="checkAwardID">
                    <option value="">Tất cả kiểu thưởng</option>
                    <c:forEach items="${f:getAvailableCheckAward()}" var="checkAward">
                        <option${param["checkAwardID"]==checkAward.id?' selected':''} value="${checkAward.id}">${checkAward.name}</option>
                    </c:forEach>
                </select>
            </div>
            <div class="form-group">
                <div class="input-group">                    
                    <span class="input-group-addon"><i class="ti-calendar"></i></span>
                    <input type="text" class="form-control" id="filterDateRange" autocomplete="off" value="" placeholder="Khoảng thời gian" />
                    <input type="hidden" id="startDate" name="startDate" value="${param["startDate"]}" />
                    <input type="hidden" id="endDate" name="endDate" value="${param["endDate"]}" />
                </div>
            </div>
            <div class="form-group">
                <label>Sắp xếp theo</label>
                <select name="orderColumn" class="form-control">
                    <option value="id" ${PAGER.orderColumn=="id"?"selected":""}>ID</option>
                    <option value="fullName" ${PAGER.orderColumn=="fullName"?"selected":""}>Họ và tên</option>
                    <option value="userName" ${PAGER.orderColumn=="userName"?"selected":""}>Tên đăng nhập</option>
                </select>
            </div>
            <div class="form-group">
                <label>Thứ tự</label>
                <select name="asc" class="form-control">
                    <option value="true" ${PAGER.asc=="true"?"selected":""}>Tăng dần</option>
                    <option value="false" ${PAGER.asc=="false"?"selected":""}>Giảm dần</option>
                </select>
            </div>
            <input type="hidden" name="currentPage" value="${PAGER.currentPage}"/>
            <input type="hidden" name="displayPerPage" value="${PAGER.displayPerPage}"/>
            <button type="submit" class="btn btn-info">Lọc</button>
        </form>
    </div>
    <br/>
    <div class="clearfix">
        <a href="javascript:void(0)" controller="<c:url value='/Admin/Award/ListAward/RunSystem'/>" class="btn btn-success btn-lg pull-left btn-send-ajax-confirm">Chạy hoa hồng</a>
        <a download href="<c:url value='/Admin/Award/ListAward/Export?orderColumn=${PAGER.orderColumn}&asc=${PAGER.asc}&keyword=${PAGER.keyword}&startDate=${param["startDate"]}&endDate=${param["endDate"]}&checkAwardID=${param["checkAwardID"]}'/>" class="btn btn-primary btn-lg pull-right">Xuất Excel</a>
    </div>
    <div class="ajax-content panel-body">   
        <c:import url="/Views/Admin/Award/listaward_ajax.jsp"/>
    </div>
</div>