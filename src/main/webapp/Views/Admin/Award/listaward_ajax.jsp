<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="f" uri="/WEB-INF/tlds/functions.tld" %>
<c:if test="${f:size(ITEMS_LIST)==0}">
    <div class="alert alert-danger">
        Không tồn tại bản ghi nào!
    </div>
</c:if>
<c:if test="${f:size(ITEMS_LIST)!=0}">
    <p><b>Tổng tiền:</b> <strong class="text-danger">${f:customFormatDecimal("###,###,###", PAGER.amount)} bv</strong></p>
    <br/>
    <table class="table table-bordered table-condensed table-hover table-valign-midle table-grid-view table-align-center">
        <thead>
            <tr>
                <th style="width: 1%;white-space: nowrap">ID <span class="${PAGER.orderColumn=='id'?(PAGER.asc?'fa fa-caret-up':'fa fa-caret-down'):''}"></span></th>
                <th>Người thụ hưởng <span class="${PAGER.orderColumn=='customerID.id'?(PAGER.asc?'fa fa-caret-up':'fa fa-caret-down'):''}"></span></th>
                <th>Từ <span class="${PAGER.orderColumn=='partnerID.id'?(PAGER.asc?'fa fa-caret-up':'fa fa-caret-down'):''}"></span></th>
                <th>Kiểu thưởng <span class="${PAGER.orderColumn=='checkAwardID.id'?(PAGER.asc?'fa fa-caret-up':'fa fa-caret-down'):''}"></span></th>
                <th>Số tiền <span class="${PAGER.orderColumn=='amount'?(PAGER.asc?'fa fa-caret-up':'fa fa-caret-down'):''}"></span></th>
                <th class="text-center">Khả dụng <span class="${PAGER.orderColumn=='amount'?(PAGER.asc?'fa fa-caret-up':'fa fa-caret-down'):''}"></span></th>
                <th class="text-right">Thời gian <span class="${PAGER.orderColumn=='createdDate'?(PAGER.asc?'fa fa-caret-up':'fa fa-caret-down'):''}"></span></th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${ITEMS_LIST}" var="item">
                <tr>
                    <td>${item.id}</td>
                    <td><label class="label label-success">${item.customerID.userName}</label></td>
                    <td><label class="label label-info">${item.partnerID.userName}</label></td>
                    <td>${item.checkAwardID.name}</td>
                    <td><label class="label label-danger">${f:customFormatDecimal("###,###,###", item.amount)} bv</label></td>
                    <td class="text-center" style="white-space: nowrap; width: 1%">
                        <c:choose>
                            <c:when test="${item.isAvailability}">
                                <b class="text-success">Khả dụng</b>
                            </c:when>
                            <c:otherwise>
                                <b class="text-danger">Không khả dụng</b>
                            </c:otherwise>
                        </c:choose>
                    </td>
                    <td class="text-right" style="white-space: nowrap; width: 1%">${f:customFormatDate("dd/MM/yyyy HH:mm:ss", item.createdDate)}</td>
                </tr>
            </c:forEach>
        </tbody>
    </table><!-- end table -->
</c:if>
<c:if test="${PAGER.totalResult>0}">
    <div class="row">
        <div class="col-md-4 col-xs-12">
            <c:if test="${PAGER.totalResult>5}">
                <label class="control-label">Display: </label>
                <select class="form-control input-sm btn-change-display-per-page" controller="<c:url value="/Admin/Award/ListAward?currentPage=1&orderColumn=id&asc=true&keyword=${PAGER.keyword}&startDate=${param['startDate']}&endDate=${param['endDate']}&checkAwardID=${param['checkAwardID']}&displayPerPage="/>">
                    <c:forEach begin="5" step="5" end="50" var="numb">
                        <option ${numb==PAGER.displayPerPage?'selected':''} value="${numb}">${numb}</option>
                    </c:forEach>
                </select>
            </c:if>
        </div>
        <div class="col-md-8 col-xs-12 text-right"> 
            <ul class="pagination pagination-sm">
                <li class="${PAGER.currentPage<=1?"hidden":""}">
                    <a href="<c:url value="/Admin/Award/ListAward?currentPage=1&displayPerPage=${PAGER.displayPerPage}&orderColumn=${PAGER.orderColumn}&asc=${PAGER.asc}&keyword=${PAGER.keyword}&startDate=${param['startDate']}&endDate=${param['endDate']}&checkAwardID=${param['checkAwardID']}"/>"><i class="fa fa-angle-double-left"></i></a>
                </li>
                <li class="${PAGER.currentPage<=1?"hidden":""}">
                    <a href="<c:url value="/Admin/Award/ListAward?currentPage=${PAGER.currentPage-1}&displayPerPage=${PAGER.displayPerPage}&orderColumn=${PAGER.orderColumn}&asc=${PAGER.asc}&keyword=${PAGER.keyword}&startDate=${param['startDate']}&endDate=${param['endDate']}&checkAwardID=${param['checkAwardID']}"/>"><i class="fa fa-angle-left"></i></a>
                </li>
                <c:if test="${PAGER.currentPage==2}">
                    <li>
                        <a href="<c:url value="/Admin/Award/ListAward?currentPage=1&displayPerPage=${PAGER.displayPerPage}&orderColumn=${PAGER.orderColumn}&asc=${PAGER.asc}&keyword=${PAGER.keyword}&startDate=${param['startDate']}&endDate=${param['endDate']}&checkAwardID=${param['checkAwardID']}"/>">1</a>
                    </li>
                </c:if>
                <c:if test="${PAGER.currentPage>2}">
                    <c:forEach begin="${PAGER.currentPage-2}" end="${PAGER.currentPage-1}" var="page">
                        <li>
                            <a href="<c:url value="/Admin/Award/ListAward?currentPage=${page}&displayPerPage=${PAGER.displayPerPage}&orderColumn=${PAGER.orderColumn}&asc=${PAGER.asc}&keyword=${PAGER.keyword}&startDate=${param['startDate']}&endDate=${param['endDate']}&checkAwardID=${param['checkAwardID']}"/>">${page}</a>
                        </li>
                    </c:forEach>
                </c:if>
                <li>
                    <a class="active" href="javascript:;">${PAGER.currentPage}</a>
                </li>                
                <c:if test="${PAGER.currentPage==PAGER.totalPage-1}">
                    <li>
                        <a href="<c:url value="/Admin/Award/ListAward?currentPage=${PAGER.totalPage}&displayPerPage=${PAGER.displayPerPage}&orderColumn=${PAGER.orderColumn}&asc=${PAGER.asc}&keyword=${PAGER.keyword}&startDate=${param['startDate']}&endDate=${param['endDate']}&checkAwardID=${param['checkAwardID']}"/>">${PAGER.totalPage}</a>
                    </li>
                </c:if>
                <c:if test="${PAGER.currentPage<PAGER.totalPage-1}">
                    <c:forEach begin="${PAGER.currentPage+1}" end="${PAGER.currentPage+2}" var="page">
                        <li>
                            <a href="<c:url value="/Admin/Award/ListAward?currentPage=${page}&displayPerPage=${PAGER.displayPerPage}&orderColumn=${PAGER.orderColumn}&asc=${PAGER.asc}&keyword=${PAGER.keyword}&startDate=${param['startDate']}&endDate=${param['endDate']}&checkAwardID=${param['checkAwardID']}"/>">${page}</a>
                        </li>
                    </c:forEach> 
                </c:if>              
                <li class="${PAGER.currentPage==PAGER.totalPage?"hidden":""}">
                    <a href="<c:url value="/Admin/Award/ListAward?currentPage=${PAGER.currentPage+1}&displayPerPage=${PAGER.displayPerPage}&orderColumn=${PAGER.orderColumn}&asc=${PAGER.asc}&keyword=${PAGER.keyword}&startDate=${param['startDate']}&endDate=${param['endDate']}&checkAwardID=${param['checkAwardID']}"/>"><i class="fa fa-angle-right"></i></a>
                </li>
                <li class="${PAGER.currentPage==PAGER.totalPage?"hidden":""}">
                    <a href="<c:url value="/Admin/Award/ListAward?currentPage=${PAGER.totalPage}&displayPerPage=${PAGER.displayPerPage}&orderColumn=${PAGER.orderColumn}&asc=${PAGER.asc}&keyword=${PAGER.keyword}&startDate=${param['startDate']}&endDate=${param['endDate']}&checkAwardID=${param['checkAwardID']}"/>"><i class="fa fa-angle-double-right"></i></a>
                </li>
            </ul> 
        </div>
    </div>
</c:if>