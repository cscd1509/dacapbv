<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="f" uri="/WEB-INF/tlds/functions.tld" %>
<!DOCTYPE html>
<html>
    <head>
        <!-- Mobile Specific Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Always force latest IE rendering engine -->
        <meta http-equiv="X-UA-Compatible" content="IE=9">
        <!-- Meta Keyword -->
        <meta name="keywords" content="">
        <!-- meta character set -->
        <meta charset="utf-8">
        <title>BV Admin Control Panel</title>
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic&subset=latin,vietnamese' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href='<c:url value="/Resources/css/font-awesome.min.css"/>' type="text/css">
        <link rel="stylesheet" href='<c:url value="/Resources/css/bootstrap.min.css"/>' type="text/css">
        <link rel="stylesheet" href='<c:url value="/Resources/css/reset-css.css"/>' type="text/css">
        <link rel="stylesheet" href='<c:url value="/Resources/css/login_admin.css"/>' type="text/css">
    </head>
    <body>     
        <div class="overlay"></div>
        <div id="container">
            <div class="panel signin">
                <div class="panel-heading">
                    <h1>BV</h1>
                    <h4>BV Login Page</h4>
                </div>
                <div class="panel-body">
                    <div class="or">v</div>
                    <form action="<c:url value="/Admin/Login" />" id="login-form" novalidate>
                        <div class="form-group login-input">
                            <i class="glyphicon glyphicon-user"></i>
                            <input type="text" name="username" class="form-control" placeholder="Enter Username">
                        </div>
                        <div class="form-group login-input">
                            <i class="glyphicon glyphicon-lock"></i>
                            <input type="password" name="password" class="form-control" placeholder="Enter Password">
                        </div>
                        <div class="form-group">
                            <div class="g-recaptcha" data-callback="recaptchaCallback" style="width: 100%" data-sitekey="${RE_CAPTCHA_KEY}"></div>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary btn-quirk btn-block btn-lg" disabled>Sign In</button>
                        </div>
                    </form>
                </div>
                <div class="panel-footer">
                    <h3>Copyright © 2016 BV - All Rights Reserved</h3>
                    <div>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-dribbble"></i></a>
                    </div>
                </div>
            </div>
        </div> 
        <style>
            #errorTxt{
                min-height: 0;
                border-radius: 4px;
                background-color: #E04B4A;
                margin-bottom: 10px;
            }
            #errorTxt>ul{
                font-size: 14px;
                margin-bottom: 0;
                padding-left: 20px;
            }
            #errorTxt>ul>li{
                margin: 10px;
                color: #fff;
                list-style: disc;
            }
        </style>
        <script src="https://www.google.com/recaptcha/api.js"></script>
        <script type="text/javascript" src='<c:url value="/Resources/js/jquery-1.12.3.min.js"/>'></script>
        <script type="text/javascript" src="<c:url value="/Resources/joli/js/plugins/jquery-validation/jquery.validate.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/Resources/joli/js/plugins/jquery-validation/additional-methods.min.js"/>"></script>
        <script type="text/javascript" src='<c:url value="/Resources/js/bootstrap.min.js"/>'></script>
        <script type="text/javascript" src='<c:url value="/Resources/js/serialize.js"/>'></script>
        <script type="text/javascript" src="<c:url value="/Resources/shared/js/noty/jquery.noty.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/Resources/shared/js/noty/layouts/bottomCenter.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/Resources/shared/js/noty/themes/default.js"/>"></script>
        <script>
            var alertInterval;
            function openAlert(data, callback) {
                $('#noty_topRight_layout_container').remove();
                clearInterval(alertInterval);
                $('body').append(data);
                if (typeof callback === 'function') {
                    callback();
                }
                alertInterval = setInterval(function () {
                    $('#noty_topRight_layout_container').remove();
                }, 5000);
            }
            function recaptchaCallback() {
                $('#login-form .btn-primary').removeAttr('disabled');
            }
        </script>
        <script>
            var validate = $("#login-form").validate({
                rules: {
                    username: {
                        required: true
                    },
                    password: {
                        required: true
                    },
                    captcha: {
                        required: true
                    }
                },
                messages: {
                    username: {
                        required: 'All fields are required!'
                    },
                    password: {
                        required: 'All fields are required!'
                    },
                    captcha: {
                        required: 'All fields are required!'
                    }
                },
                errorElement: 'li',
                errorLabelContainer: '#errorTxt>ul',
                submitHandler: function () {
                    var url = $('#login-form').attr('action');
                    $.ajax({
                        url: url,
                        beforeSend: function () {
                            $('#login-form .btn-primary').addClass('disabled');
                        },
                        type: "POST",
                        data: $('#login-form').serializeObject(),
                        success: function (data) {
                            if (!data.key) {
                                grecaptcha.reset();
                            }
                            openAlert(data.value);
                        },
                        error: function () {
                            $('#login-form .btn-primary').removeClass('disabled');
                            alert('Error. Try again later!');
                        }
                    });
                }
            });
        </script>
    </body>
</html>