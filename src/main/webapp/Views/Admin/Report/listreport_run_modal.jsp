<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="f" uri="/WEB-INF/tlds/functions.tld" %>
<div class="modal modal-insert-customer" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lgs">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <div class="modal-title">
                    <div class="content-title">
                        <h3 class="text-center">${SELECTED_REPORTER.name}</h3>
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered" id="table-list-payment">
                        <thead>
                            <tr>
                                <c:forEach items="${SELECTED_REPORTER.reporterQueryAliases}" var="alias">
                                    <th class="text-left">${alias.columnName}</th>
                                </c:forEach>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${ITEMS_LIST}" var="row">
                                <tr>
                                    <c:forEach items="${row}" var="cell">
                                        <td class="tableexport-string">${cell}</td>
                                    </c:forEach>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
                <script>
                    TableExport.prototype.charset = "charset=utf-8";
                    $('#table-list-payment').tableExport({
                        charset: 'charset=utf-8',
                        headers: true,
                        footers: false,
                        formats: ['xlsx', 'txt'],
                        filename: 'id',
                        bootstrap: true,
                        position: 'top',
                        ignoreRows: null,
                        ignoreCols: null,
                        ignoreCSS: '.tableexport-ignore',
                        emptyCSS: '.tableexport-empty',
                        trimWhitespace: false
                    });
                </script>
            </div>
        </div>
    </div>
</div>