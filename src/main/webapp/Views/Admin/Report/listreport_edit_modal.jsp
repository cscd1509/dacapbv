<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="f" uri="/WEB-INF/tlds/functions.tld" %>
<div class="modal modal-insert-customer" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <div class="modal-title">
                    <div class="content-title">
                        <h3 class="text-center">Sửa báo cáo</h3>
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <form id="form-insert-module" class="form-horizontal" novalidate method="POST" action="/Admin/Report/ListReport/Edit">
                    <input type="hidden" required name="id" class="form-control" value="${SELECTED_REPORTER.id}">
                    <div class="form-group">

                        <label class="control-label col-sm-3">Tên</label>
                        <div class="col-sm-9">
                            <input type="text" required name="name" class="form-control" value="${SELECTED_REPORTER.name}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">Câu lệnh</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" name="sQlQuery" rows="5">${SELECTED_REPORTER.sQlQuery}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3"></label>
                        <div class="col-sm-9">
                            <div class="checkbox">
                                <label><input type="checkbox" id="isActive" checked name="isActive" ${SELECTED_REPORTER.isActive?"checked":""}> Hiển thị</label>
                            </div>
                        </div>
                    </div>
                    <fieldset>
                        <legend class="text-center text-uppercase">Định danh trong báo cáo (Alias)</legend>
                        <div class="table-responsive">
                            <table class="table table-bordered table-valign-midle">                                
                                <tbody id="bill-detail">
                                    <tr>
                                        <td></td>
                                        <td>
                                            <input type="text" id="alias-columnName" class="form-control external"/>
                                        </td>
                                        <td>
                                            <input type="text" id="alias-name" class="form-control external"/>
                                        </td>
                                        <td>
                                            <select class="form-control external" id="alias-type">
                                                <option value="">-- Chọn kiểu dữ liệu --</option>
                                                <c:forEach items="${ALIAS_TYPE_ENUM}" var="entry">
                                                    <option value="${entry.name}">${entry.name}</option>
                                                </c:forEach>
                                            </select>
                                        </td>
                                        <td class="text-center"><a href="javascript:void(0)" id="btn-save-detail" class="fas fa-save text-success"></a></td>
                                    </tr>
                                    <tr>
                                        <th style="width:1%;" class="text-center">STT</th>
                                        <th class="text-center">Tên cột</th>
                                        <th class="text-center">Tên alias</th>
                                        <th class="text-center">Kiểu dữ liệu</th>
                                        <th style="width:1%;"></th>
                                    </tr>
                                    <c:set var="count" value="1"/>
                                    <c:forEach items="${SELECTED_REPORTER.reporterQueryAliases}" var="alias">
                                        <tr class="bill-detail" id="bill-detail-1" data-detail='{"columnName":"${alias.columnName}","aliasName":"${alias.aliasName}","aliasType":"${alias.aliasType}"}'>
                                            <td class="text-center">${count}</td>
                                            <td class="text-center">${alias.columnName}</td>
                                            <td class="text-center">${alias.aliasName}</td>
                                            <td class="text-center">${alias.aliasType}</td>
                                            <td class="text-center" style="white-space: nowrap;">
                                                <span class="detail-action action-edit text-info far fa-edit" style="margin-right: 3px;"></span>
                                                <span class="detail-action action-delete text-danger fas fa-trash-alt"></span>
                                            </td>
                                        </tr>                                        
                                        <c:set var="count" value="${count+1}"/>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </fieldset>
                    <br/>
                    <div class="form-group text-center clearfix">
                        <button type="reset" class="btn btn-danger pull-left">Nhập lại</button>
                        <button type="button" class="btn btn-success pull-right btn-submit">Lưu</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    var rowIndex = 0;
    var editRow;
    $(document).on('click', '#btn-save-detail', function () {
        var billDetail = document.getElementById('bill-detail');
        if (!$('#alias-columnName').val() || !$('#alias-name').val() || !$('#alias-type').val()) {
            return;
        }
        var row;
        var cellSTT;
        var cellColumnName;
        var cellAliasName;
        var cellAliasType;
        var cellAction;

        var columnName = $('#alias-columnName').val();
        var aliasName = $('#alias-name').val();
        var type = $('#alias-type').val();
        var typeName = $('#alias-type option:selected').html();
        if (editRow) {
            row = editRow;
            var cells = row.getElementsByTagName('td');
            cellSTT = cells[0];
            cellColumnName = cells[1];
            cellAliasName = cells[2];
            cellAliasType = cells[3];
            editRow = null;
        } else {
            row = billDetail.insertRow(rowIndex + 2);
            row.className = 'bill-detail';
            var rowID = 'bill-detail-' + (rowIndex + 1);
            row.id = rowID;
            cellSTT = row.insertCell(0);
            cellSTT.classList.add('text-center');
            cellSTT.innerHTML = rowIndex + 1;
            //
            cellColumnName = row.insertCell(1);
            cellColumnName.classList.add('text-center');
            //
            cellAliasName = row.insertCell(2);
            cellAliasName.classList.add('text-center');
            //
            cellAliasType = row.insertCell(3);
            cellAliasType.classList.add('text-center');
            //
            cellAction = row.insertCell(4);
            cellAction.classList.add('text-center');
            cellAction.style.whiteSpace = 'nowrap';

            var buttonEdit = document.createElement("span");
            buttonEdit.className += 'detail-action action-edit text-info far fa-edit';
            buttonEdit.style.marginRight = '3px';
            var buttonDelete = document.createElement("span");
            buttonDelete.className += 'detail-action action-delete text-danger fas fa-trash-alt';
            cellAction.appendChild(buttonEdit);
            cellAction.appendChild(buttonDelete);
            rowIndex++;
        }
        row.setAttribute('data-detail', '{"columnName":"' + columnName + '", "aliasName":"' + aliasName + '", "aliasType":"' + type + '"}');
        cellColumnName.innerHTML = columnName;
        cellAliasName.innerHTML = aliasName;
        cellAliasType.innerHTML = typeName;
        resetBillDetailInput();
    });
    function resetBillDetailInput() {
        $('#alias-columnName').val('');
        $('#alias-name').val('');
        $('#alias-type').val('');
    }
    $(document).on('click', '.detail-action', function () {
        if ($(this).hasClass('action-edit')) {
            editRow = $(this).parents('tr').get(0);
            var dataDetail = $(this).parents('tr').data('detail');
            var columnName = dataDetail['columnName'];
            var aliasName = dataDetail['aliasName'];
            var aliasType = dataDetail['aliasType'];
            $('#alias-columnName').val(columnName);
            $('#alias-name').val(aliasName);
            $('#alias-type option[value="' + aliasType + '"]').prop('selected', true);
        } else if ($(this).hasClass('action-delete')) {
            $(this).parents('tr').nextAll('tr').each(function () {
                var currentIndex = parseInt($(this).find('td:first-child').html());
                $(this).find('td:first-child').html(currentIndex - 1);
            });
            $(this).parents('tr').remove();
            rowIndex--;
        }
    });
    $(document).on('click', '.btn-submit', function () {
        var billdetails = [];
        $('.bill-detail').each(function (i, v) {
            var billDetail = $(this).data('detail');
            billdetails.push(billDetail);
        });
        var customerMap = $('#form-insert-module').serializeObject();
        customerMap['reporterQueryAliases'] = billdetails;
        var customer = JSON.stringify(customerMap);
        $('#form-insert-module').validate({
            submitHandler: function () {
                var url = $('#form-insert-module').attr('action');
                $.ajax({
                    beforeSend: function (xhr) {
                        ajaxStartHandle();
                    },
                    url: url,
                    type: 'POST',
                    data: customer,
                    contentType: 'application/json',
                    success: function (data) {
                        openMessage(data, function () {
                            reloadAjaxContent();
                        });
                    }, error: function () {
                        alert('Đã xảy ra lỗi. Vui lòng thử lại sau!');
                    },
                    complete: function (jqXHR, textStatus) {
                        ajaxCompleteHandle();
                    }
                });
            }
        });
    });
</script>