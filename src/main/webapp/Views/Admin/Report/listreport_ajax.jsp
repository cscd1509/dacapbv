<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="f" uri="/WEB-INF/tlds/functions.tld" %>
<div class="panel-body ${f:size(ITEMS_LIST)>0?"panel-body-table":""}">
    <c:choose>        
        <c:when test="${f:size(ITEMS_LIST)==0}">
            <div class="alert alert-danger">
                Không có bản ghi nào tồn tại!
            </div>
        </c:when>
        <c:otherwise>
            <table class="table table-condensed table-hover table-valign-midle">
                <thead>
                    <tr>
                        <th style="width: 1%;white-space: nowrap">STT <span class="${PAGER.orderColumn=='id'?(PAGER.asc?'fa fa-caret-up':'fa fa-caret-down'):''}"></span></th>
                        <th>Tên <span class="${PAGER.orderColumn=='name'?(PAGER.asc?'fa fa-caret-up':'fa fa-caret-down'):''}"></span></th>
                        <th>Query <span class="${PAGER.orderColumn=='sQlQuery'?(PAGER.asc?'fa fa-caret-up':'fa fa-caret-down'):''}"></span></th>
                        <th>Trạng thái <span class="${PAGER.orderColumn=='isActive'?(PAGER.asc?'fa fa-caret-up':'fa fa-caret-down'):''}"></span></th>
                        <th style="width: 1%"></th>  
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${ITEMS_LIST}" var="item">
                        <tr class="${item.isActive?'':'alert alert-danger'}">
                            <td class="td-align-center">${item.id}</td>
                            <td>${item.name}</td>
                            <td>${item.sQlQuery}</td>
                            <td>${item.isActive?'Bình thường':'Khóa'}</td>
                            <td>
                                <div class="dropdown">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cogs"></i></button>
                                    <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                        <li><a class="btn-open-modal" href="javascript:void(0)" controller="<c:url value='/Admin/Report/ListReport/Run/${item.id}'/>">Chạy thử</a></li>
                                        <!--<li><a class="btn-open-modal" href="javascript:void(0)" controller="<c:url value='/Admin/Report/ListReport/ViewEdit/${item.id}'/>">Sửa báo cáo</a></li>-->
                                        <li><a class="btn-send-ajax" href="javascript:void(0)" controller="<c:url value='/Admin/Report/ListReport/ChangeStatus/${item.id}'/>"> ${item.isActive?'Khóa':'Mở khóa'}</a></li>
                                        <li><a class="btn-send-ajax" href="javascript:void(0)" controller="<c:url value='/Admin/Report/ListReport/Delete/${item.id}'/>">Xóa loại báo cáo</a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table><!-- end table -->
        </c:otherwise>
    </c:choose>
</div>
<c:if test="${PAGER.totalResult>PAGER.displayPerPage}">
    <div class="panel-footer">        
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <c:if test="${PAGER.totalResult>5}">
                    <label class="control-label">Display: </label>
                    <select class="form-control input-sm btn-change-display-per-page" controller="<c:url value='/Admin/Report/ListReport?currentPage=1&orderColumn=id&asc=true&displayPerPage='/>">
                        <c:forEach begin="5" step="5" end="50" var="numb">
                            <option ${numb==PAGER.displayPerPage?'selected':''} value="${numb}">${numb}</option>
                        </c:forEach>
                    </select>
                </c:if>
            </div>
            <div class="col-md-8 col-xs-12 text-right"> 
                <ul class="pagination pagination-sm">
                    <li class="${PAGER.currentPage<=1?"hidden":""}">
                        <a href="<c:url value="/Admin/Report/ListReport?currentPage=1&displayPerPage=${PAGER.displayPerPage}&orderColumn=${PAGER.orderColumn}&asc=${PAGER.asc}"/>"><i class="fa fa-angle-double-left"></i></a>
                    </li>
                    <li class="${PAGER.currentPage<=1?"hidden":""}">
                        <a href="<c:url value="/Admin/Report/ListReport?currentPage=${PAGER.currentPage-1}&displayPerPage=${PAGER.displayPerPage}&orderColumn=${PAGER.orderColumn}&asc=${PAGER.asc}"/>"><i class="fa fa-angle-left"></i></a>
                    </li>
                    <c:if test="${PAGER.currentPage==2}">
                        <li>
                            <a href="<c:url value="/Admin/Report/ListReport?currentPage=1&displayPerPage=${PAGER.displayPerPage}&orderColumn=${PAGER.orderColumn}&asc=${PAGER.asc}"/>">1</a>
                        </li>
                    </c:if>
                    <c:if test="${PAGER.currentPage>2}">
                        <c:forEach begin="${PAGER.currentPage-2}" end="${PAGER.currentPage-1}" var="page">
                            <li>
                                <a href="<c:url value="/Admin/Report/ListReport?currentPage=${page}&displayPerPage=${PAGER.displayPerPage}&orderColumn=${PAGER.orderColumn}&asc=${PAGER.asc}"/>">${page}</a>
                            </li>
                        </c:forEach>
                    </c:if>
                    <li>
                        <a class="active" href="javascript:;">${PAGER.currentPage}</a>
                    </li>                
                    <c:if test="${PAGER.currentPage==PAGER.totalPage-1}">
                        <li>
                            <a href="<c:url value="/Admin/Report/ListReport?currentPage=${PAGER.totalPage}&displayPerPage=${PAGER.displayPerPage}&orderColumn=${PAGER.orderColumn}&asc=${PAGER.asc}"/>">${PAGER.totalPage}</a>
                        </li>
                    </c:if>
                    <c:if test="${PAGER.currentPage<PAGER.totalPage-1}">
                        <c:forEach begin="${PAGER.currentPage+1}" end="${PAGER.currentPage+2}" var="page">
                            <li>
                                <a href="<c:url value="/Admin/Report/ListReport?currentPage=${page}&displayPerPage=${PAGER.displayPerPage}&orderColumn=${PAGER.orderColumn}&asc=${PAGER.asc}"/>">${page}</a>
                            </li>
                        </c:forEach> 
                    </c:if>              
                    <li class="${PAGER.currentPage==PAGER.totalPage?"hidden":""}">
                        <a href="<c:url value="/Admin/Report/ListReport?currentPage=${PAGER.currentPage+1}&displayPerPage=${PAGER.displayPerPage}&orderColumn=${PAGER.orderColumn}&asc=${PAGER.asc}"/>"><i class="fa fa-angle-right"></i></a>
                    </li>
                    <li class="${PAGER.currentPage==PAGER.totalPage?"hidden":""}">
                        <a href="<c:url value="/Admin/Report/ListReport?currentPage=${PAGER.totalPage}&displayPerPage=${PAGER.displayPerPage}&orderColumn=${PAGER.orderColumn}&asc=${PAGER.asc}"/>"><i class="fa fa-angle-double-right"></i></a>
                    </li>
                </ul> 
            </div>
        </div>
    </div>
</c:if>