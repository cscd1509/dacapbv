<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="f" uri="/WEB-INF/tlds/functions.tld" %>
<link href="/Resources/shared/css/tableexport.min.css" rel="stylesheet" type="text/css"/>
<script src="/Resources/shared/js/tableexport/xlsx.core.min.js" type="text/javascript"></script>
<script src="/Resources/shared/js/tableexport/Blob.min.js" type="text/javascript"></script>
<script src="/Resources/shared/js/tableexport/FileSaver.min.js" type="text/javascript"></script>
<script src="/Resources/shared/js/tableexport/tableexport.min.js" type="text/javascript"></script>
<input type="hidden" id="reloadController" value="<c:url value='/Admin/Report/ListReport/Ajax${f:buildPager(PAGER)}'/>">
<div class="content-title">
    <h3 class="text-center">Quản lý báo cáo</h3>
</div>
<div class="panel panel-default">
    <div class="panel-heading">        
        <form class="form-inline text-right" id="pager-form" role="form" action="<c:url value="/Admin/Report/ListReport"/>" method="GET">
            <div class="form-group">
                <label>Từ khóa</label>
                <input type="text" class="form-control" name="keyword" value="${param.keyword}" />
            </div>
            <div class="form-group">
                <label>Sắp xếp theo</label>
                <select name="orderColumn" class="form-control">
                    <option value="id" ${PAGER.orderColumn=="id"?"selected":""}>ID</option>
                    <option value="fullName" ${PAGER.orderColumn=="name"?"selected":""}>Tên</option>
                </select>
            </div>
            <div class="form-group">
                <label>Thứ tự</label>
                <select name="asc" class="form-control">
                    <option value="true" ${PAGER.asc=="true"?"selected":""}>Tăng dần</option>
                    <option value="false" ${PAGER.asc=="false"?"selected":""}>Giảm dần</option>
                </select>
            </div>
            <input type="hidden" name="currentPage" value="${PAGER.currentPage}"/>
            <input type="hidden" name="displayPerPage" value="${PAGER.displayPerPage}"/>
            <button type="submit" class="btn btn-info">Lọc</button>
        </form>
    </div>
    <br/>
    <div class="clearfix"> 
        <a class="btn btn-primary btn-lg pull-right btn-open-modal" href="javascript:void(0)" controller="/Admin/Report/ListReport/ViewInsert">Thêm mới <i class="fas fa-hand-holding-usd"></i></a>
    </div>
    <div class="ajax-content panel-body">
        <c:import url="/Views/Admin/Report/listreport_ajax.jsp"/> 
    </div>
</div>