<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="f" uri="/WEB-INF/tlds/functions.tld" %>
<input type="hidden" id="reloadController" value="<c:url value='/Admin/Permission/ListRole/Ajax${f:buildPager(PAGER)}'/>">
<div class="content-title">
    <h3 class="text-center">Danh sách quyền</h3>
</div>
<div class="panel panel-default">
    <div class="panel-heading">        
        <form class="form-inline text-right" id="pager-form" role="form" action="<c:url value='/Admin/Permission/ListRole'/>" method="GET">
            <div class="form-group">
                <label>Sắp xếp theo</label>
                <select name="orderColumn" class="form-control">
                    <option value="id" ${PAGER.orderColumn=="id"?"selected":""}>ID</option>
                    <option value="name" ${PAGER.orderColumn=="name"?"selected":""}>Tên</option>
                    <option value="createdDate" ${PAGER.orderColumn=="createdDate"?"selected":""}>Ngày tạo</option>
                </select>
            </div>
            <div class="form-group">
                <label>Thứ tự</label>
                <select name="asc" class="form-control">
                    <option value="true" ${PAGER.asc=="true"?"selected":""}>Tăng dần</option>
                    <option value="false" ${PAGER.asc=="false"?"selected":""}>Giảm dần</option>
                </select>
            </div>
            <input type="hidden" name="currentPage" value="${PAGER.currentPage}"/>
            <input type="hidden" name="displayPerPage" value="${PAGER.displayPerPage}"/>
            <button type="submit" class="btn btn-info">Lọc</button>
        </form>
    </div>
    <br/>
    <div class="clearfix">
        <a class="btn btn-primary btn-lg pull-right" data-toggle="modal" data-target="#modal-insert-role">Thêm mới <i class="fas fa-hand-holding-usd"></i></a>
    </div>
    <div class="ajax-content panel-body">   
        <c:import url="/Views/Admin/AdminRole/listrole_ajax.jsp"/>
    </div>
</div>
<c:import url="/Views/Admin/AdminRole/listrole_insert_ajax.jsp"/>