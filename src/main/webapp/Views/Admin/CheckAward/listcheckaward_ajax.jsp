<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="f" uri="/WEB-INF/tlds/functions.tld" %>
<c:choose>
    <c:when test="${f:size(ITEMS_LIST)==0}">
        <div class="alert alert-danger">
            Không tồn tại bản ghi nào!
        </div>
    </c:when>
    <c:when test="${f:size(ITEMS_LIST)!=0}">
        <div class="table-responsive">
            <table class="table table-condensed table-bordered table-hover table-valign-midle table-grid-view table-align-center">
                <thead>
                    <tr>
                        <th class="text-left">ID <span class="${PAGER.orderColumn=='id'?(PAGER.asc?'fa fa-caret-up':'fa fa-caret-down'):''}"></span></th>
                        <th class="text-left">Tên <span class="${PAGER.orderColumn=='name'?(PAGER.asc?'fa fa-caret-up':'fa fa-caret-down'):''}"></span></th>
                        <th class="text-center" style="width: 300px">Phần trăm thưởng</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${ITEMS_LIST}" var="item">
                        <tr>
                            <td class="text-left">${item.id}</td>
                            <td class="text-left">${item.name}</td>
                            <td>
                                <form class="form-update-award" action="<c:url value='/Admin/Award/ListCheckAward/UpdateAward'/>">
                                    <div class="input-group">
                                        <input type="hidden" value="${item.id}" name="checkAwardID"/>
                                        <input type="number" class="form-control" name="newPrice" required min="0" max="100" value="${f:customFormatDecimal("0.########", item.percentAward)}">
                                        <span class="input-group-btn" style="vertical-align: top">
                                            <button class="btn btn-primary" type="submit">Lưu</button>
                                        </span>
                                    </div>
                                </form>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table><!-- end table -->
        </div>
        <c:if test="${PAGER.totalPage>1}">
            <div class="row">
                <div class="col-md-4 col-xs-12">
                    <c:if test="${PAGER.totalResult>5}">
                        <label class="control-label">Display: </label>
                        <select class="form-control input-sm btn-change-display-per-page" controller="<c:url value='/Admin/Award/ListCheckAward?currentPage=1&orderColumn=id&asc=true&displayPerPage='/>">
                            <c:forEach begin="5" step="5" end="50" var="numb">
                                <option ${numb==PAGER.displayPerPage?'selected':''} value="${numb}">${numb}</option>
                            </c:forEach>
                        </select>
                    </c:if>
                </div>
                <div class="col-md-8 col-xs-12 text-right"> 
                    <ul class="pagination pagination-sm">
                        <li class="${PAGER.currentPage<=1?"hidden":""}">
                            <a href="<c:url value="/Admin/Award/ListCheckAward?currentPage=1&displayPerPage=${PAGER.displayPerPage}&orderColumn=${PAGER.orderColumn}&asc=${PAGER.asc}"/>"><i class="fa fa-angle-double-left"></i></a>
                        </li>
                        <li class="${PAGER.currentPage<=1?"hidden":""}">
                            <a href="<c:url value="/Admin/Award/ListCheckAward?currentPage=${PAGER.currentPage-1}&displayPerPage=${PAGER.displayPerPage}&orderColumn=${PAGER.orderColumn}&asc=${PAGER.asc}"/>"><i class="fa fa-angle-left"></i></a>
                        </li>
                        <c:if test="${PAGER.currentPage==2}">
                            <li>
                                <a href="<c:url value="/Admin/Award/ListCheckAward?currentPage=1&displayPerPage=${PAGER.displayPerPage}&orderColumn=${PAGER.orderColumn}&asc=${PAGER.asc}"/>">1</a>
                            </li>
                        </c:if>
                        <c:if test="${PAGER.currentPage>2}">
                            <c:forEach begin="${PAGER.currentPage-2}" end="${PAGER.currentPage-1}" var="page">
                                <li>
                                    <a href="<c:url value="/Admin/Award/ListCheckAward?currentPage=${page}&displayPerPage=${PAGER.displayPerPage}&orderColumn=${PAGER.orderColumn}&asc=${PAGER.asc}"/>">${page}</a>
                                </li>
                            </c:forEach>
                        </c:if>
                        <li>
                            <a class="active" href="javascript:;">${PAGER.currentPage}</a>
                        </li>                
                        <c:if test="${PAGER.currentPage==PAGER.totalPage-1}">
                            <li>
                                <a href="<c:url value="/Admin/Award/ListCheckAward?currentPage=${PAGER.totalPage}&displayPerPage=${PAGER.displayPerPage}&orderColumn=${PAGER.orderColumn}&asc=${PAGER.asc}"/>">${PAGER.totalPage}</a>
                            </li>
                        </c:if>
                        <c:if test="${PAGER.currentPage<PAGER.totalPage-1}">
                            <c:forEach begin="${PAGER.currentPage+1}" end="${PAGER.currentPage+2}" var="page">
                                <li>
                                    <a href="<c:url value="/Admin/Award/ListCheckAward?currentPage=${page}&displayPerPage=${PAGER.displayPerPage}&orderColumn=${PAGER.orderColumn}&asc=${PAGER.asc}"/>">${page}</a>
                                </li>
                            </c:forEach> 
                        </c:if>              
                        <li class="${PAGER.currentPage==PAGER.totalPage?"hidden":""}">
                            <a href="<c:url value="/Admin/Award/ListCheckAward?currentPage=${PAGER.currentPage+1}&displayPerPage=${PAGER.displayPerPage}&orderColumn=${PAGER.orderColumn}&asc=${PAGER.asc}"/>"><i class="fa fa-angle-right"></i></a>
                        </li>
                        <li class="${PAGER.currentPage==PAGER.totalPage?"hidden":""}">
                            <a href="<c:url value="/Admin/Award/ListCheckAward?currentPage=${PAGER.totalPage}&displayPerPage=${PAGER.displayPerPage}&orderColumn=${PAGER.orderColumn}&asc=${PAGER.asc}"/>"><i class="fa fa-angle-double-right"></i></a>
                        </li>
                    </ul> 
                </div>
            </div>
        </c:if>
    </c:when>
</c:choose>
<script>
    $('.form-update-award').each(function (i, v) {
        $(v).validate({
            submitHandler: function () {
                var url = $(v).attr('action');
                $.ajax({
                    beforeSend: function (xhr) {
                        ajaxStartHandle();
                    },
                    url: url,
                    type: 'POST',
                    data: $(v).serializeObject(),
                    success: function (data) {
                        openMessage(data, function () {
                            reloadAjaxContent();
                        });
                    },
                    complete: function (jqXHR, textStatus) {
                        ajaxCompleteHandle();
                    }
                });
            }
        });
    });
</script>