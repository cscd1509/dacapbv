<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="f" uri="/WEB-INF/tlds/functions.tld" %>
<input type="hidden" id="reloadController" value="<c:url value='/Admin/Award/ListCheckAward/Ajax${f:buildPager(PAGER)}'/>">
<div class="content-title">
    <h3 class="text-center">Danh sách kiểu thưởng</h3>
</div>
<div class="panel panel-default">
    <div class="ajax-content panel-body">   
        <c:import url="/Views/Admin/CheckAward/listcheckaward_ajax.jsp"/>
    </div>
</div>
    