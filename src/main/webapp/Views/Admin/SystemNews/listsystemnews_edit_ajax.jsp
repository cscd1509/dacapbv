<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="f" uri="/WEB-INF/tlds/functions.tld" %>
<div class="modal modal-insert-customer" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title">
                    <div class="content-title">
                        <h3 class="text-center">Cập nhật tin tức</h3>
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="form-edit-news" novalidate method="POST" action="<c:url value='/Admin/Customer/ListSystemNews/Edit'/>">
                    <input type="hidden" name="id" value="${SELECTED_NEWS.id}"/>
                    <div class="form-group">
                        <label for="title-edit" class="control-label col-sm-3">Tiêu đề</label>
                        <div class="col-sm-9">
                            <input type="text" id="title-edit" name="title" maxlength="200" class="form-control" required value="${SELECTED_NEWS.title}" />
                            <p class="help-block">Tối đa 200 ký tự</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="urlAvatar-edit" class="control-label col-sm-3">Ảnh đại diện</label>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <input type="text" class="form-control" id="urlAvatar-edit" name="urlAvatar" value="${SELECTED_NEWS.urlAvatar}"/>
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-info" id="btn-finder-edit" data-target="#urlAvatar-edit" type="button">Upload</button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="shortDescription-edit" class="control-label col-sm-3">Mô tả ngắn</label>
                        <div class="col-sm-9">
                            <input type="text" id="shortDescription-edit" name="shortDescription" maxlength="4000" class="form-control" required value="${SELECTED_NEWS.shortDescription}" />
                            <p class="help-block">Tối đa 500 ký tự</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="customerUsername-edit" class="control-label col-sm-3">Nhánh được đọc</label>
                        <div class="col-sm-9">
                            <input type="text" id="customerUsername-edit" name="customerUsername" class="form-control" value="${SELECTED_NEWS.customerID==null?null:SELECTED_NEWS.customerID.userName}" />
                            <p class="help-block">Nhập tên đăng nhập, người được nhập và nhánh dưới có thể đọc, để trống nếu tất cả mọi người được đọc</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">Nội dung</label>
                        <div class="col-sm-9">
                            <textarea class="form-control ckeditor-content" name="content" maxlength="4000" id="ckeditor-edit" rows="7">${SELECTED_NEWS.content}</textarea>
                        </div>
                    </div>
                    <div class="form-group text-center clearfix">
                        <a href="javascript:void(0)" class="btn btn-primary pull-right">Lưu <i class="fa fa-check fa-right"></i></a>
                        <button type="reset" class="btn btn-danger pull-left">Nhập lại <i class="fa fa-times fa-right"></i></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).on('focusin', function (e) {
        e.stopImmediatePropagation();
    });
    CKEDITOR.replace('ckeditor-edit', {
        filebrowserBrowseUrl: '/ckfinder/ckfinder.html',
        filebrowserImageBrowseUrl: '/ckfinder/ckfinder.html?Type=Images',
        filebrowserFlashBrowseUrl: '/ckfinder/ckfinder.html?Type=Flash',
        filebrowserUploadUrl: '/ckfinder/core/connector/java/connector.java?command=QuickUpload&type=Files',
        filebrowserImageUploadUrl: '/ckfinder/core/connector/java/connector.java?command=QuickUpload&type=Images',
        filebrowserFlashUploadUrl: '/ckfinder/core/connector/java/connector.java?command=QuickUpload&type=Images',
        language: 'vi',
        height: 300
    });
    CKFinder.config.startupPath = "Images:/Contents/";
    $(document).on("click", "#btn-finder-edit", function () {
        var target = $(this).data('target');
        var finder = new CKFinder({
            filebrowserBrowseUrl: '/ckfinder/ckfinder.html',
            filebrowserImageBrowseUrl: '/ckfinder/ckfinder.html?Type=Images',
            filebrowserFlashBrowseUrl: '/ckfinder/ckfinder.html?Type=Flash',
            filebrowserUploadUrl: '/ckfinder/core/connector/java/connector.aspx?command=QuickUpload&type=Files',
            filebrowserImageUploadUrl: '/ckfinder/core/connector/java/connector.aspx?command=QuickUpload&type=Images',
            filebrowserFlashUploadUrl: '/ckfinder/core/connector/java/connector.java?command=QuickUpload&type=Images'
        });
        finder.selectActionFunction = function (fileUrl) {
            $(target).val(fileUrl);
        };
        finder.popup();
    });
    function CKupdate() {
        for (instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].updateElement();
        }
    }
</script>
<script>
    $('#form-edit-news .btn-primary').click(function () {
        CKupdate();
        $('#form-edit-news').submit();
    });
    $('#form-edit-news').validate({
        submitHandler: function () {
            var url = $('#form-edit-news').attr('action');
            $.ajax({
                beforeSend: function () {
                    ajaxStartHandle();
                },
                url: url,
                type: 'POST',
                data: $('#form-edit-news').serializeObject(),
                success: function (data) {
                    openMessage(data, function () {
                        reloadAjaxContent();
                    });
                },
                complete: function () {
                    ajaxCompleteHandle();
                }
            });
        }
    });
</script>