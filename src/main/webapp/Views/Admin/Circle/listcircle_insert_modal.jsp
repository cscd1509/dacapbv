<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="modal" id="modal-insert-circle" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title">
                    <h4 class="modal-title">Tạo chu kỳ mới</h4>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                </div>
            </div>                           
            <form id="form-insert-circle" confirm="true" class="form-horizontal" novalidate method="POST" action="<c:url value="/Admin/Award/ListCircle/Insert" />">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="count" class="control-label col-sm-3">Khoảng thời gian <i>(*)</i></label>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <input type="text" class="form-control external" name="dateRange" id="filterDateRange" value="" placeholder="Select time range" />
                                <span class="input-group-addon"><i class="ti-calendar"></i></span>
                            </div>
                            <input type="hidden" id="startDate" name="startDate" value="${param["startDate"]}" />
                            <input type="hidden" id="endDate" name="endDate" value="${param["endDate"]}" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer clearfix">
                    <button type="reset" class="btn btn-danger pull-left">Hủy <i class="fa fa-times fa-right"></i></button>
                    <button type="submit" class="btn btn-info pull-right">Gửi <i class="fa fa-check fa-right"></i></button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $('#form-insert-circle').validate({
        rules: {
            dateRange: {
                required: true
            }
        },
        submitHandler: function () {
            var formData = $('#form-insert-circle').serializeObject();
            var url = $('#form-insert-circle').attr('action');
            sendAjax(url, 'POST', formData, function (data) {
                openMessage(data);
            });
        }
    });
</script>