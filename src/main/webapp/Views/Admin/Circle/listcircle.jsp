<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="f" uri="/WEB-INF/tlds/functions.tld" %>
<link href="/Resources/shared/css/tableexport.min.css" rel="stylesheet" type="text/css"/>
<script src="/Resources/shared/js/tableexport/xlsx.core.min.js" type="text/javascript"></script>
<script src="/Resources/shared/js/tableexport/Blob.min.js" type="text/javascript"></script>
<script src="/Resources/shared/js/tableexport/FileSaver.min.js" type="text/javascript"></script>
<script src="/Resources/shared/js/tableexport/tableexport.min.js" type="text/javascript"></script>
<input type="hidden" id="reloadController" value="<c:url value='/Admin/Award/ListCircle/Ajax${f:buildPager(PAGER)}'/>">
<div class="content-title">
    <h3 class="text-center">Quản lý chu kỳ trả thưởng</h3>
</div>
<div class="panel panel-default">
    <div class="panel-heading">        
        <form class="form-inline text-right" id="pager-form" role="form" action="<c:url value="/Admin/Award/ListCircle"/>" method="GET">
            <div class="form-group">
                <label>Sắp xếp theo</label>
                <select name="orderColumn" class="form-control">
                    <option value="id" ${PAGER.orderColumn=="id"?"selected":""}>ID</option>
                    <option value="fromDate" ${PAGER.orderColumn=="fromDate"?"selected":""}>Từ ngày</option>
                    <option value="toDate" ${PAGER.orderColumn=="toDate"?"selected":""}>Đến ngày</option>
                    <option value="totalAmount" ${PAGER.orderColumn=="totalAmount"?"selected":""}>Tổng tiền</option>
                    <option value="isPayment" ${PAGER.orderColumn=="isPayment"?"selected":""}>Trạng thái</option>
                </select>
            </div>
            <div class="form-group">
                <label>Thứ tự</label>
                <select name="asc" class="form-control">
                    <option value="true" ${PAGER.asc=="true"?"selected":""}>Tăng dần</option>
                    <option value="false" ${PAGER.asc=="false"?"selected":""}>Giảm dần</option>
                </select>
            </div>
            <input type="hidden" name="currentPage" value="${PAGER.currentPage}"/>
            <input type="hidden" name="displayPerPage" value="${PAGER.displayPerPage}"/>
            <button type="submit" class="btn btn-info">Lọc</button>
        </form>
    </div>
    <br/>
    <div class="text-right">                
        <a class="btn-lg btn btn-primary" data-toggle="modal" data-target="#modal-insert-circle">Tạo mới <i class="fas fa-plus"></i></a>
    </div>
    <div class="ajax-content panel-body">   
        <c:import url="/Views/Admin/Circle/listcircle_ajax.jsp"/>
    </div>
</div>
<c:import url="/Views/Admin/Circle/listcircle_insert_modal.jsp"/>