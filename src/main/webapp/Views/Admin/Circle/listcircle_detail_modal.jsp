<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="f" uri="/WEB-INF/tlds/functions.tld" %>
<div class="modal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title">
                    <h4 class="modal-title">Danh sách thụ hưởng chu kỳ trả thưởng<br/>
                        từ ngày <b>${f:customFormatDate("dd/MM/yyyy", CIRCLE.fromDate)}</b> đến ngày <b>${f:customFormatDate("dd/MM/yyyy", CIRCLE.toDate)}</b></h4>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                </div>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered" id="table-list-payment">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Họ tên</th>
                                <th>Tên đăng nhập</th>
                                <th>Số ĐT</th>
                                <th>CMND</th>
                                <th>Ngân hàng</th>
                                <th>Chi nhánh</th>
                                <th>Số TK</th>
                                <th>Tổng tiền</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${RESULT}" var="item">
                                <tr>
                                    <td class="tableexport-string">${item.id}</td>
                                    <td class="tableexport-string" style="white-space: nowrap">${item.customerID.fullName}</td>
                                    <td class="tableexport-string">${item.customerID.userName}</td>
                                    <td class="tableexport-string">${item.customerID.mobile}</td>
                                    <td class="tableexport-string">${item.customerID.peoplesIdentity}</td>
                                    <td class="tableexport-string">${item.customerID.bankName}</td>
                                    <td class="tableexport-string">${item.customerID.bankAgency}</td>
                                    <td class="tableexport-string">${item.customerID.bankNumber}</td>
                                    <td class="tableexport-string"><label class="label label-danger">${f:customFormatDecimal("###,###,###", item.amount)}</label></td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
                <script>
                    TableExport.prototype.charset = "charset=utf-8";
                    $('#table-list-payment').tableExport({
                        charset: 'charset=utf-8',
                        headers: true,
                        footers: false,
                        formats: ['xlsx', 'txt'],
                        filename: 'id',
                        bootstrap: true,
                        position: 'bottom',
                        ignoreRows: null,
                        ignoreCols: null,
                        ignoreCSS: '.tableexport-ignore',
                        emptyCSS: '.tableexport-empty',
                        trimWhitespace: false
                    });
                </script>
            </div>
        </div>
    </div>
</div>