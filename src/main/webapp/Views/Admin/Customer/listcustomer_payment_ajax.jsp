<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="f" uri="/WEB-INF/tlds/functions.tld" %>
<div class="modal modal-insert-customer" id="payment-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title">
                    <div class="content-title">
                        <h3 class="text-center">Nạp tiền</h3>
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="form-insert-admin" novalidate method="POST" action="<c:url value='/Admin/Permission/List/Insert'/>">
                    <div class="form-group">
                        <label for="name" class="control-label col-sm-3">Số dư</label>
                        <div class="col-sm-9">
                            <label class="form-control-static text-danger">${f:customFormatDecimal("###,###,##0 bv", ADMIN.totalAmount)}</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">Người nhận</label>
                        <div class="col-sm-9">
                            <label class="form-control-static text-primary" id="customerUsername"></label>
                            <input type="hidden" readonly class="form-control" name="customerID" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="amount" class="control-label col-sm-3">Số tiền nạp</label>
                        <div class="col-sm-9">
                            <input type="number" id="amount" name="amount" min="0" value="0" class="form-control" required />
                        </div>
                    </div>
                    <div class="form-group text-center clearfix">
                        <button type="submit" class="btn btn-primary pull-right">Nạp <i class="fa fa-check fa-right"></i></button>
                        <button type="reset" class="btn btn-danger pull-left">Nhập lại <i class="fa fa-times fa-right"></i></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $('#form-insert-admin').validate({
        submitHandler: function () {
            var url = $('#form-insert-admin').attr('action');
            $.ajax({
                url: url,
                type: 'POST',
                data: $('#form-insert-admin').serializeObject(),
                success: function (data) {
                    openMessage(data, function () {
                        reloadAjaxContent();
                    });
                }
            });
        }
    });
</script>