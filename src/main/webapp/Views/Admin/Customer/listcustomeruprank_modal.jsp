<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="f" uri="/WEB-INF/tlds/functions.tld" %>
<div class="modal modal-insert-customer" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title">
                    <div class="content-title">
                        <h3 class="text-center">Điều chỉnh hạng</h3>
                    </div>
                </div>
            </div>
            <form id="form-edit-customer" class="form-horizontal" novalidate method="POST" action="<c:url value="/Admin/Customer/ListCustomer/UpRank"/>">
                <div class="modal-body">
                    <input type="hidden" name="cusID" value="${param['cusID']}">
                    <div class="form-group">
                        <label for="rankID" class="control-label col-sm-3">Cấp bậc</label>
                        <div class="col-sm-9">
                            <select name="rankID" class="form-control" required>
                                <option selected value="">Lựa chọn cấp bậc</option>
                                <c:forEach items="${RANK_NAMES}" var="item">
                                    <option value="${item.id}" ${param['curentRankID']==item.id?"selected":""}>${item.name}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer text-center">
                    <button type="submit" class="btn btn-primary">Lưu</button>
                    <button type="reset" class="btn btn-danger">Nhập lại</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $("#form-edit-customer").validate({
        submitHandler: function () {
            var url = $('#form-edit-customer').attr('action');
            var data = $('#form-edit-customer').serializeObject();
            $.ajax({
                beforeSend: function () {
                    ajaxStartHandle();
                    $('#form-edit-customer .btn-primary').html('Sending');
                    $('#form-edit-customer .btn-primary').addClass('disabled');
                },
                url: url,
                type: "POST",
                data: data,
                success: function (data) {
                    openMessage(data, function () {
                        reloadAjaxContent();
                    });
                }, error: function () {
                    alert("Error. Try again later!");
                }, complete: function () {
                    ajaxCompleteHandle();
                    $('#form-edit-customer .btn-primary').html('Save');
                    $('#form-edit-customer .btn-primary').removeClass('disabled');
                }
            });
        }
    });
</script>