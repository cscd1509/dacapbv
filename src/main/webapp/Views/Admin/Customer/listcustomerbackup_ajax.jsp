<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="f" uri="/WEB-INF/tlds/functions.tld" %>
<c:if test="${f:size(ITEMS_LIST)==0}">
    <div class="alert alert-danger">
        Không tồn tại bản ghi nào!
    </div>
</c:if>
<c:if test="${f:size(ITEMS_LIST)!=0}">
    <h4>Tổng số kết quả: <b>${PAGER.totalResult}</b></h4><br/>
    <div class="table-responsive">
        <table class="table table-bordered table-hover table-valign-midle table-grid-view table-align-center">
            <thead>
                <tr>
                    <th style="width: 1%"></th>
                    <th class="text-center" style="width: 1%">ID <span class="${PAGER.orderColumn=='id'?(PAGER.asc?'fa fa-caret-up':'fa fa-caret-down'):''}"></span></th>
                    <th class="text-left">Tên <span class="${PAGER.orderColumn=='fullName'?(PAGER.asc?'fa fa-caret-up':'fa fa-caret-down'):''}"></span></th>
                    <th class="text-left">Tên đăng nhập <span class="${PAGER.orderColumn=='userName'?(PAGER.asc?'fa fa-caret-up':'fa fa-caret-down'):''}"></span></th>
                    <th class="text-right">Nhánh trái <span class="${PAGER.orderColumn=='totalLeft'?(PAGER.asc?'fa fa-caret-up':'fa fa-caret-down'):''}"></span></th>
                    <th class="text-right">Nhánh phải <span class="${PAGER.orderColumn=='totalRight'?(PAGER.asc?'fa fa-caret-up':'fa fa-caret-down'):''}"></span></th>
                    <th class="text-center">Trạng thái <span class="${PAGER.orderColumn=='totalRight'?(PAGER.asc?'fa fa-caret-up':'fa fa-caret-down'):''}"></span></th>
                    <th style="width:1%"></th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${ITEMS_LIST}" var="c">
                    <tr>
                        <td>
                            <c:if test="${!c.isBackUp}">
                                <input type="checkbox" class="checkbox-not-backup" value="${c.id}"/>
                            </c:if>
                        </td>
                        <td class="text-center">${c.id}</td>
                        <td class="text-left">${c.fullName}</td>
                        <td class="text-left">${c.userName}</td>
                        <td class="text-right"><b class="text-danger">${f:customFormatDecimal("###,###,##0 bv", c.totalLeft)}</b></td>
                        <td class="text-right"><b class="text-danger">${f:customFormatDecimal("###,###,##0 bv", c.totalRight)}</b></td>
                        <td class="text-center" style="width: 1%;white-space: nowrap">
                            <c:choose>
                                <c:when test="${c.isBackUp}">
                                    <b class="text-success">Đã bảo lưu</b>
                                </c:when>
                                <c:otherwise>
                                    <b class="text-danger">Chưa bảo lưu</b>
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td>
                            <div class="btn-group btn-group-sm">
                                <button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown"><i class="ti-settings"></i></button>
                                <ul class="dropdown-menu dropdown-menu-right dropdown-menu-action" role="menu">
                                    <li>
                                        <a class="btn-send-ajax" controller="<c:url value="/Admin/Customer/ListCustomerBackUp/BackUp?listID=${c.id}"/>">Bảo lưu</a>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table><!-- end table -->
    </div>
</c:if>
<c:if test="${PAGER.totalPage>1}">
    <div class="row">
        <div class="col-md-4 col-xs-12">
            <c:if test="${PAGER.totalResult>5}">
                <label class="control-label">Display: </label>
                <select class="form-control input-sm btn-change-display-per-page" controller="<c:url value='/Admin/Customer/ListCustomerBackUp?currentPage=1&orderColumn=id&asc=true&displayPerPage='/>">
                    <c:forEach begin="5" step="5" end="50" var="numb">
                        <option ${numb==PAGER.displayPerPage?'selected':''} value="${numb}">${numb}</option>
                    </c:forEach>
                </select>
            </c:if>
        </div>
        <div class="col-md-8 col-xs-12 text-right"> 
            <ul class="pagination pagination-sm">
                <li class="${PAGER.currentPage<=1?"hidden":""}">
                    <a href="<c:url value="/Admin/Customer/ListCustomerBackUp?currentPage=1&displayPerPage=${PAGER.displayPerPage}&orderColumn=${PAGER.orderColumn}&isCheck=${PAGER.isCheck}&asc=${PAGER.asc}&isBackUp=${param.isBackUp}"/>"><i class="fa fa-angle-double-left"></i></a>
                </li>
                <li class="${PAGER.currentPage<=1?"hidden":""}">
                    <a href="<c:url value="/Admin/Customer/ListCustomerBackUp?currentPage=${PAGER.currentPage-1}&displayPerPage=${PAGER.displayPerPage}&orderColumn=${PAGER.orderColumn}&isCheck=${PAGER.isCheck}&asc=${PAGER.asc}&isBackUp=${param.isBackUp}"/>"><i class="fa fa-angle-left"></i></a>
                </li>
                <c:if test="${PAGER.currentPage==2}">
                    <li>
                        <a href="<c:url value="/Admin/Customer/ListCustomerBackUp?currentPage=1&displayPerPage=${PAGER.displayPerPage}&orderColumn=${PAGER.orderColumn}&isCheck=${PAGER.isCheck}&asc=${PAGER.asc}&isBackUp=${param.isBackUp}"/>">1</a>
                    </li>
                </c:if>
                <c:if test="${PAGER.currentPage>2}">
                    <c:forEach begin="${PAGER.currentPage-2}" end="${PAGER.currentPage-1}" var="page">
                        <li>
                            <a href="<c:url value="/Admin/Customer/ListCustomerBackUp?currentPage=${page}&displayPerPage=${PAGER.displayPerPage}&orderColumn=${PAGER.orderColumn}&isCheck=${PAGER.isCheck}&asc=${PAGER.asc}&isBackUp=${param.isBackUp}"/>">${page}</a>
                        </li>
                    </c:forEach>
                </c:if>
                <li>
                    <a class="active" href="javascript:;">${PAGER.currentPage}</a>
                </li>                
                <c:if test="${PAGER.currentPage==PAGER.totalPage-1}">
                    <li>
                        <a href="<c:url value="/Admin/Customer/ListCustomerBackUp?currentPage=${PAGER.totalPage}&displayPerPage=${PAGER.displayPerPage}&orderColumn=${PAGER.orderColumn}&isCheck=${PAGER.isCheck}&asc=${PAGER.asc}&isBackUp=${param.isBackUp}"/>">${PAGER.totalPage}</a>
                    </li>
                </c:if>
                <c:if test="${PAGER.currentPage<PAGER.totalPage-1}">
                    <c:forEach begin="${PAGER.currentPage+1}" end="${PAGER.currentPage+2}" var="page">
                        <li>
                            <a href="<c:url value="/Admin/Customer/ListCustomerBackUp?currentPage=${page}&displayPerPage=${PAGER.displayPerPage}&orderColumn=${PAGER.orderColumn}&isCheck=${PAGER.isCheck}&asc=${PAGER.asc}&isBackUp=${param.isBackUp}"/>">${page}</a>
                        </li>
                    </c:forEach> 
                </c:if>              
                <li class="${PAGER.currentPage==PAGER.totalPage?"hidden":""}">
                    <a href="<c:url value="/Admin/Customer/ListCustomerBackUp?currentPage=${PAGER.currentPage+1}&displayPerPage=${PAGER.displayPerPage}&orderColumn=${PAGER.orderColumn}&isCheck=${PAGER.isCheck}&asc=${PAGER.asc}&isBackUp=${param.isBackUp}"/>"><i class="fa fa-angle-right"></i></a>
                </li>
                <li class="${PAGER.currentPage==PAGER.totalPage?"hidden":""}">
                    <a href="<c:url value="/Admin/Customer/ListCustomerBackUp?currentPage=${PAGER.totalPage}&displayPerPage=${PAGER.displayPerPage}&orderColumn=${PAGER.orderColumn}&isCheck=${PAGER.isCheck}&asc=${PAGER.asc}&isBackUp=${param.isBackUp}"/>"><i class="fa fa-angle-double-right"></i></a>
                </li>
            </ul> 
        </div>
    </div>
</c:if>