<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="f" uri="/WEB-INF/tlds/functions.tld" %>
<input type="hidden" id="reloadController" value="<c:url value='/Admin/Customer/ListCustomer/Ajax${f:buildPager(PAGER)}'/>">
<div class="content-title">
    <h3 class="text-center">Danh sách thành viên</h3>
</div>
<div class="panel panel-default">
    <div class="panel-heading">        
        <form class="form-inline text-right" id="pager-form" role="form" action="<c:url value="/Admin/Customer/ListCustomer"/>" method="GET">
            <div class="form-group">
                <label>Từ khóa</label>
                <input type="text" class="form-control" name="keyword" value="${param.keyword}" />
            </div>
            <div class="form-group">
                <label>Cấp bậc</label>
                <select name="isCheck" class="form-control">
                    <option selected value="">Tất cả cấp bậc</option>
                    <c:forEach items="${RANK_NAMES}" var="item">
                        <option value="${item.id}" ${PAGER.isCheck==item.id?"selected":""}>${item.name}</option>
                    </c:forEach>
                </select>
            </div>
            <div class="form-group">
                <label>Sắp xếp theo</label>
                <select name="orderColumn" class="form-control">
                    <option value="id" ${PAGER.orderColumn=="id"?"selected":""}>ID</option>
                    <option value="fullName" ${PAGER.orderColumn=="fullName"?"selected":""}>Họ và tên</option>
                    <option value="userName" ${PAGER.orderColumn=="userName"?"selected":""}>Tên đăng nhập</option>
                </select>
            </div>
            <div class="form-group">
                <label>Thứ tự</label>
                <select name="asc" class="form-control">
                    <option value="true" ${PAGER.asc=="true"?"selected":""}>Tăng dần</option>
                    <option value="false" ${PAGER.asc=="false"?"selected":""}>Giảm dần</option>
                </select>
            </div>
            <button type="submit" class="btn btn-info">Lọc</button>
        </form>
    </div>
    <br/>
    <div class="text-right">
        <a download href="<c:url value='/Admin/Customer/ListCustomer/Export?&keyword=${PAGER.keyword}'/>" class="btn btn-primary btn-lg">Xuất Excel</a>
    </div>
    <div class="ajax-content panel-body">   
        <c:import url="/Views/Admin/Customer/listcustomer_ajax.jsp"/>
    </div>
</div>
<c:import url="/Views/Admin/Customer/listcustomer_payment_ajax.jsp"/>
<script>
    $(document).on('click', '.btn-payment', function () {
        var id = $(this).data('id');
        var username = $(this).data('username');
        $('#payment-modal input[name="customerID"]').val(id);
        $('#payment-modal #customerUsername').html(username);
        $('#payment-modal').modal('show');
    });
    $('.change-date').change(function () {
        window.location.href = ($(this).attr('controller') + $(this).val()).toString();
    });
</script>