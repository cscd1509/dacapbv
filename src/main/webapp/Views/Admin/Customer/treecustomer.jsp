<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="f" uri="/WEB-INF/tlds/functions.tld" %>
<input type="hidden" id="reloadController" value="<c:url value='/Admin/Customer/TreeCustomer/Reload'/>">
<div class="content-title">
    <h3 class="text-center">Cây hệ thống nhị phân</h3>
</div>
<div class="panel panel-default">
    <div class="panel-body">   
        <div class="ajax-content" id="distributor-view-tree">
            <c:import url="/Views/Admin/Customer/treecustomer_ajax.jsp"/>
        </div>
    </div>
</div><!-- end panel-->
<script>
    $(function () {
        $('[data-toggle="popover"]').popover({
            container: 'body',
            html: true,
            trigger: "hover"
        });
    });
    $(document).on('click touchstart', '#treeDiagram span[class|="ti"]', function () {
        $(this).toggleClass('ti-plus').toggleClass('ti-minus');
        $(this).parent().next('ul').toggle(0);
        var target = $(this).parents('.next-item');
        $(target).removeClass('next-item');
        var controller = $(this).attr('controller');
        sendAjax(controller, 'GET', null, function (data) {
            $(target).append(data);
            $('[data-toggle="popover"]').popover({
                container: 'body',
                html: true,
                trigger: "hover"
            });
        });
    });
</script>
<style>
    #distributor-view-tree ul{padding-left: 0px;}
    #distributor-view-tree ul li{border-left: 1px solid #ddd;display: block;font-size: 14px;list-style: square;margin-left: 5px;padding: 20px 0 0px 50px;position: relative;}
    #distributor-view-tree ul>li:last-child{border-left: none;}
    #distributor-view-tree ul>li:last-child>a:after{background-color: #ddd;content: "";cursor: default;height: 30px;left: -50px;position: absolute;top: -22px;width: 1px;}
    #treeDiagram>ul:first-child>li:last-child>a:after{display: none}
    #distributor-view-tree ul li a{position: relative;text-transform: capitalize;text-decoration: none;cursor: pointer;white-space: nowrap;letter-spacing: 2px;}
    #distributor-view-tree ul li a span{cursor: pointer;text-transform: uppercase;font-weight: 600;}
    #distributor-view-tree ul li a:before{background-color: #ddd;content: "";height: 1px;left: -50px;position: absolute;top: 7px;width: 43px;}
    #treeDiagram>ul:first-child>li:last-child>a:before{display: none}
    #distributor-view-tree>ul>li{border-left: none;}
    #distributor-view-tree>ul>li>a:before{display: none;}
    #distributor-view-tree>ul>li>a:after{display: none;}
    #distributor-view-tree ul li .end{padding-left: 10px;}
    #distributor-view-tree .hide-item>ul{display: none;}    
</style>