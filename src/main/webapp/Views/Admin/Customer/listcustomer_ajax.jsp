<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="f" uri="/WEB-INF/tlds/functions.tld" %>
<c:if test="${f:size(ITEMS_LIST)==0}">
    <div class="alert alert-danger">
        Không tồn tại bản ghi nào!
    </div>
</c:if>
<c:if test="${f:size(ITEMS_LIST)!=0}">
    <h4>Tổng số kết quả: <b>${PAGER.totalResult}</b></h4><br/>
    <div class="table-responsive">
        <table class="table table-bordered table-hover table-valign-midle table-grid-view table-align-center">
            <thead>
                <tr>
                    <th class="text-left">Tên <span class="${PAGER.orderColumn=='fullName'?(PAGER.asc?'fa fa-caret-up':'fa fa-caret-down'):''}"></span></th>
                    <th class="text-left">Tên đăng nhập <span class="${PAGER.orderColumn=='userName'?(PAGER.asc?'fa fa-caret-up':'fa fa-caret-down'):''}"></span></th>
                    <th class="text-center">Giới tính <span class="${PAGER.orderColumn=='gender'?(PAGER.asc?'fa fa-caret-up':'fa fa-caret-down'):''}"></span></th>
                    <th class="text-center">SĐT <span class="${PAGER.orderColumn=='mobile'?(PAGER.asc?'fa fa-caret-up':'fa fa-caret-down'):''}"></span></th>
                    <th class="text-right">Số dư <span class="${PAGER.orderColumn=='totalDeposit'?(PAGER.asc?'fa fa-caret-up':'fa fa-caret-down'):''}"></span></th>
                    <th class="text-center" style="width:1%;white-space: nowrap">Cấp bậc <span class="${PAGER.orderColumn=='rankCustomer.name'?(PAGER.asc?'fa fa-caret-up':'fa fa-caret-down'):''}"></span></th>
                    <th style="width:1%" class="text-center">#</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${ITEMS_LIST}" var="c">
                    <tr>
                        <td class="text-left">${c.fullName}</td>
                        <td class="text-left">${c.userName}</td>
                        <td class="text-center">${c.gender?"Nam":"Nữ"}</td>
                        <td class="text-center">${c.mobile}</td>
                        <td class="text-right"><b class="text-danger">${f:customFormatDecimal("###,###,##0 bv", c.totalDeposit)}</b></td>
                        <td class="text-center" style="width:1%;white-space: nowrap;background-color: rgba(0,0,0,.5)">
                            <c:choose>
                                <c:when test="${c.rankCustomer==null}">
                                    <b style="color: #fff">Chưa lên hạng</b>
                                </c:when>
                                <c:otherwise>
                                    <b style="color: ${c.rankCustomer.color}">${c.rankCustomer.name}</b>
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td style="white-space: nowrap">
                            <a class="btn-open-modal text-primary" data-toggle="tooltip" title="Sửa" controller="<c:url value="/Admin/Customer/ListCustomer/ViewEdit/${c.id}"/>"><i class="fa fa-edit"></i></a>
                            <a class="btn-open-modal text-success" data-toggle="tooltip" title="Điều chỉnh hạng" controller="<c:url value="/Admin/Customer/ListCustomer/ViewUpRank?cusID=${c.id}&curentRankID=${c.rankCustomer==null?'':c.rankCustomer.id}"/>"><i class="fa fa-level-up-alt"></i></a>
                            <a class="btn-send-ajax text-warning" data-toggle="tooltip" title="Reset mật khẩu" controller="<c:url value="/Admin/Customer/ListCustomer/ResetPassword/${c.id}"/>"><i class="fas fa-key"></i></a>
                            <a class="btn-send-ajax text-danger" data-toggle="tooltip" title="Khóa" controller="<c:url value="/Admin/Customer/ListCustomer/Block/${c.id}"/>"><i class="fas fa-lock"></i></a>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table><!-- end table -->
    </div>
</c:if>
<c:if test="${PAGER.totalPage>1}">
    <div class="row">
        <div class="col-md-4 col-xs-12">
            <c:if test="${PAGER.totalResult>5}">
                <label class="control-label">Display: </label>
                <select class="form-control input-sm btn-change-display-per-page" controller="<c:url value='/Admin/Customer/ListCustomerBackUp?currentPage=1&orderColumn=id&asc=true&displayPerPage='/>">
                    <c:forEach begin="5" step="5" end="50" var="numb">
                        <option ${numb==PAGER.displayPerPage?'selected':''} value="${numb}">${numb}</option>
                    </c:forEach>
                </select>
            </c:if>
        </div>
        <div class="col-md-8 col-xs-12 text-right"> 
            <ul class="pagination pagination-sm">
                <li class="${PAGER.currentPage<=1?"hidden":""}">
                    <a href="<c:url value="/Admin/Customer/ListCustomer?currentPage=1&displayPerPage=${PAGER.displayPerPage}&orderColumn=${PAGER.orderColumn}&isCheck=${PAGER.isCheck}&asc=${PAGER.asc}"/>"><i class="fa fa-angle-double-left"></i></a>
                </li>
                <li class="${PAGER.currentPage<=1?"hidden":""}">
                    <a href="<c:url value="/Admin/Customer/ListCustomer?currentPage=${PAGER.currentPage-1}&displayPerPage=${PAGER.displayPerPage}&orderColumn=${PAGER.orderColumn}&isCheck=${PAGER.isCheck}&asc=${PAGER.asc}"/>"><i class="fa fa-angle-left"></i></a>
                </li>
                <c:if test="${PAGER.currentPage==2}">
                    <li>
                        <a href="<c:url value="/Admin/Customer/ListCustomer?currentPage=1&displayPerPage=${PAGER.displayPerPage}&orderColumn=${PAGER.orderColumn}&isCheck=${PAGER.isCheck}&asc=${PAGER.asc}"/>">1</a>
                    </li>
                </c:if>
                <c:if test="${PAGER.currentPage>2}">
                    <c:forEach begin="${PAGER.currentPage-2}" end="${PAGER.currentPage-1}" var="page">
                        <li>
                            <a href="<c:url value="/Admin/Customer/ListCustomer?currentPage=${page}&displayPerPage=${PAGER.displayPerPage}&orderColumn=${PAGER.orderColumn}&isCheck=${PAGER.isCheck}&asc=${PAGER.asc}"/>">${page}</a>
                        </li>
                    </c:forEach>
                </c:if>
                <li>
                    <a class="active" href="javascript:;">${PAGER.currentPage}</a>
                </li>                
                <c:if test="${PAGER.currentPage==PAGER.totalPage-1}">
                    <li>
                        <a href="<c:url value="/Admin/Customer/ListCustomer?currentPage=${PAGER.totalPage}&displayPerPage=${PAGER.displayPerPage}&orderColumn=${PAGER.orderColumn}&isCheck=${PAGER.isCheck}&asc=${PAGER.asc}"/>">${PAGER.totalPage}</a>
                    </li>
                </c:if>
                <c:if test="${PAGER.currentPage<PAGER.totalPage-1}">
                    <c:forEach begin="${PAGER.currentPage+1}" end="${PAGER.currentPage+2}" var="page">
                        <li>
                            <a href="<c:url value="/Admin/Customer/ListCustomer?currentPage=${page}&displayPerPage=${PAGER.displayPerPage}&orderColumn=${PAGER.orderColumn}&isCheck=${PAGER.isCheck}&asc=${PAGER.asc}"/>">${page}</a>
                        </li>
                    </c:forEach> 
                </c:if>              
                <li class="${PAGER.currentPage==PAGER.totalPage?"hidden":""}">
                    <a href="<c:url value="/Admin/Customer/ListCustomer?currentPage=${PAGER.currentPage+1}&displayPerPage=${PAGER.displayPerPage}&orderColumn=${PAGER.orderColumn}&isCheck=${PAGER.isCheck}&asc=${PAGER.asc}"/>"><i class="fa fa-angle-right"></i></a>
                </li>
                <li class="${PAGER.currentPage==PAGER.totalPage?"hidden":""}">
                    <a href="<c:url value="/Admin/Customer/ListCustomer?currentPage=${PAGER.totalPage}&displayPerPage=${PAGER.displayPerPage}&orderColumn=${PAGER.orderColumn}&isCheck=${PAGER.isCheck}&asc=${PAGER.asc}"/>"><i class="fa fa-angle-double-right"></i></a>
                </li>
            </ul> 
        </div>
    </div>
</c:if>