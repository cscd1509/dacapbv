<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="f" uri="/WEB-INF/tlds/functions.tld" %>
<input type="hidden" id="reloadController" value="<c:url value='/Admin/Customer/ListCustomerBackUp/Ajax${f:buildPager(PAGER)}&isBackUp=${param.isBackUp}'/>">
<div class="content-title">
    <h3 class="text-center">Reset doanh số</h3>
</div>
<div class="panel panel-default">
    <div class="panel-heading">        
        <form class="form-inline text-right" id="pager-form" role="form" action="<c:url value="/Admin/Customer/ListCustomerBackUp"/>" method="GET">
            <div class="form-group">
                <label>Từ khóa</label>
                <input type="text" class="form-control" name="keyword" value="${param.keyword}" />
            </div>
            <div class="form-group">
                <label>Sắp xếp theo</label>
                <select name="orderColumn" class="form-control">
                    <option value="id" ${PAGER.orderColumn=="id"?"selected":""}>ID</option>
                    <option value="fullName" ${PAGER.orderColumn=="fullName"?"selected":""}>Họ và tên</option>
                    <option value="userName" ${PAGER.orderColumn=="userName"?"selected":""}>Tên đăng nhập</option>
                    <option value="isBackUp" ${PAGER.orderColumn=="isBackUp"?"selected":""}>Tên đăng nhập</option>
                </select>
            </div>
            <div class="form-group">
                <label>Bảo lưu</label>
                <select name="isBackUp" class="form-control">
                    <option ${param.isBackUp?"selected":""}>Tất cả</option>
                    <option value="true" ${param.isBackUp?"selected":""}>Đã bảo lưu</option>
                    <option value="false" ${param.isBackUp!=null&&!param.isBackUp?"selected":""}>Chưa bảo lưu</option>
                </select>
            </div>
            <div class="form-group">
                <label>Thứ tự</label>
                <select name="asc" class="form-control">
                    <option value="true" ${PAGER.asc=="true"?"selected":""}>Tăng dần</option>
                    <option value="false" ${PAGER.asc=="false"?"selected":""}>Giảm dần</option>
                </select>
            </div>
            <input type="hidden" name="currentPage" value="${PAGER.currentPage}"/>
            <input type="hidden" name="displayPerPage" value="${PAGER.displayPerPage}"/>
            <button type="submit" class="btn btn-info">Lọc</button>
        </form>
    </div>
    <br/>
    <div class="clearfix">
        <a class="btn btn-success text-uppercase pull-left" id="btn-backup-selected" href="javascript:void(0)">Reset những thành viên đã chọn <i class="fas fa-save"></i></a>       
        <a class="btn btn-primary text-uppercase pull-right" id="btn-backup-all" href="javascript:void(0)">Reset tất cả <i class="fas fa-save"></i></a>
    </div>
    <div class="ajax-content panel-body">   
        <c:import url="/Views/Admin/Customer/listcustomerbackup_ajax.jsp"/>
    </div>
</div>
<script>
    $('#btn-backup-all').click(function () {
        if (!confirm("Bạn chắc chắn muốn thực hiện hành động này?")) {
            return;
        }
        sendAjax('<c:url value="/Admin/Customer/ListCustomerBackUp/BackUp"/>', 'GET', null, function (data) {
            openMessage(data, function () {
                reloadAjaxContent();
            });
        });
    });
    $('#btn-backup-selected').click(function () {
        if (!$('.checkbox-not-backup:checked').length) {
            alert('Vui lòng chọn ít nhất một thành viên!');
            return;
        }
        if (!confirm("Bạn chắc chắn muốn thực hiện hành động này?")) {
            return;
        }
        var listID = [];
        $('.checkbox-not-backup:checked').each(function () {
            listID.push($(this).val());
        });
        console.log(listID.toString());
        sendAjax('<c:url value="/Admin/Customer/ListCustomerBackUp/BackUp"/>', 'GET', {"listID": listID.toString()}, function (data) {
            openMessage(data, function () {
                reloadAjaxContent();
            });
        });
    });
</script>