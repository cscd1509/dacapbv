<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="f" uri="/WEB-INF/tlds/functions.tld" %>
<input type="hidden" id="reloadController" value="<c:url value='/Admin/Customer/ListCustomerNotActivated/Ajax${f:buildPager(PAGER)}'/>">
<div class="content-title">
    <h3 class="text-center">Danh sách thành viên bị khóa</h3>
</div>
<div class="panel panel-default">
    <div class="panel-heading">        
        <form class="form-inline text-right" id="pager-form" role="form" action="<c:url value="/Admin/Customer/ListCustomerNotActivated"/>" method="GET">
            <div class="form-group">
                <label>Từ khóa</label>
                <input type="text" class="form-control" name="keyword" value="${param.keyword}" />
            </div>
            <div class="form-group">
                <label>Sắp xếp theo</label>
                <select name="orderColumn" class="form-control">
                    <option value="id" ${PAGER.orderColumn=="id"?"selected":""}>ID</option>
                    <option value="fullName" ${PAGER.orderColumn=="fullName"?"selected":""}>Họ và tên</option>
                    <option value="userName" ${PAGER.orderColumn=="userName"?"selected":""}>Tên đăng nhập</option>
                </select>
            </div>
            <div class="form-group">
                <label>Thứ tự</label>
                <select name="asc" class="form-control">
                    <option value="true" ${PAGER.asc=="true"?"selected":""}>Tăng dần</option>
                    <option value="false" ${PAGER.asc=="false"?"selected":""}>Giảm dần</option>
                </select>
            </div>
            <input type="hidden" name="currentPage" value="${PAGER.currentPage}"/>
            <input type="hidden" name="displayPerPage" value="${PAGER.displayPerPage}"/>
            <button type="submit" class="btn btn-info">Lọc</button>
        </form>
    </div>
    <div class="ajax-content panel-body">   
        <c:import url="/Views/Admin/Customer/listcustomernotactivated_ajax.jsp"/>
    </div>
</div>