<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="f" uri="/WEB-INF/tlds/functions.tld" %>
<div class="modal modal-insert-customer" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title">
                    <div class="content-title">
                        <h3 class="text-center">Chỉnh sửa người dùng</h3>
                    </div>
                </div>
            </div>
            <form id="form-edit-customer" class="form-horizontal" novalidate method="POST" action="<c:url value="/Admin/Customer/ListCustomer/Edit"/>">
                <div class="modal-body">
                    <input type="hidden" name="id" value="${CUSTOMER.id}">
                    <div class="form-group">
                        <label for="firstName" class="control-label col-sm-3">Họ và tên</label>
                        <div class="col-sm-9">
                            <input type="text" id="firstName" required name="fullName" value="${CUSTOMER.fullName}" class="form-control" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">Email</label>
                        <div class="col-sm-9">
                            <input type="email" id="moduleIcon" name="email" value="${CUSTOMER.email}" class="form-control" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="phoneCode" class="control-label col-sm-3">Số ĐT</label>
                        <div class="col-sm-9">
                            <input type="text" id="mobile" name="mobile" value="${CUSTOMER.mobile}" class="form-control" placeholder="">
                            <p class="help-block" style="margin-bottom: 0">Must include phone country code ext. E.x: +60123456789</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="peoplesIdentity" class="control-label col-sm-3">Số CMND/Passport No.</label>
                        <div class="col-sm-9">
                            <input type="text" id="peoplesIdentity" name="peoplesIdentity" value="${CUSTOMER.peoplesIdentity}" class="form-control" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="gender" class="control-label col-sm-3">Giới tính</label>
                        <div class="col-sm-9">
                            <select class="form-control" id="gender" name="gender">
                                <option value="">Lựa chọn giới tính</option>
                                <option value="1" ${CUSTOMER.gender?'selected':''}>Nam</option>
                                <option value="0" ${!CUSTOMER.gender?'selected':''}>Nữ</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="peoplesIdentity" class="control-label col-sm-3">Tên ngân hàng</label>
                        <div class="col-sm-9">
                            <input type="text" required name="bankName" value="${CUSTOMER.bankName}" class="form-control" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="peoplesIdentity" class="control-label col-sm-3">Số TK</label>
                        <div class="col-sm-9">
                            <input type="text" required name="bankNumber" value="${CUSTOMER.bankNumber}" class="form-control" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="peoplesIdentity" class="control-label col-sm-3">Chi nhánh</label>
                        <div class="col-sm-9">
                            <input type="text" required name="bankAgency" value="${CUSTOMER.bankAgency}" class="form-control" placeholder="">
                        </div>
                    </div>
                </div>
                <div class="modal-footer text-center">
                    <button type="submit" class="btn btn-primary">Lưu</button>
                    <button type="reset" class="btn btn-danger">Nhập lại</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $("#form-edit-customer").validate({
        rules: {
            fullName: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            mobile: {
                required: true
            },
            peoplesIdentity: {
                required: true
            },
            gender: {
                required: true
            }
        },
        submitHandler: function () {
            var url = $('#form-edit-customer').attr('action');
            var data = $('#form-edit-customer').serializeObject();
            $.ajax({
                beforeSend: function () {
                    ajaxStartHandle();
                    $('#form-edit-customer .btn-primary').html('Sending');
                    $('#form-edit-customer .btn-primary').addClass('disabled');
                },
                url: url,
                type: "POST",
                data: data,
                success: function (data) {
                    openMessage(data, function () {
                        reloadAjaxContent();
                    });
                }, error: function () {
                    alert("Error. Try again later!");
                }, complete: function () {
                    ajaxCompleteHandle();
                    $('#form-edit-customer .btn-primary').html('Save');
                    $('#form-edit-customer .btn-primary').removeClass('disabled');
                }
            });
        }
    });
</script>