<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" uri="http://tiles.apache.org/tags-tiles" %>
<div id="message" class="${MESSAGE.msgClass}">
    <button type="button" class="close" data-dismiss="modal">
        <span aria-hidden="true">×</span>
        <span class="sr-only">Close</span>
    </button>
    <div class="message-container">
        <div class="message-title">${MESSAGE.title}</div>
        <div class="message-body">${MESSAGE.msg}</div>
    </div>
</div>