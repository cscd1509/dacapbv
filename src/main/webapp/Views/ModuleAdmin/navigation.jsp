<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="f" uri="/WEB-INF/tlds/functions.tld" %>
<div id="header">
    <div class="pull-left header-btns">
        <ul class="clearfix">
            <li><a id="btn-expand-sidebar"><span class="glyphicon glyphicon-align-justify"></span></a></li>
        </ul>
    </div>
    <div class="pull-right header-btns">
        <ul class="clearfix">
            <li class="user-menu dropdown">
                <a data-toggle="dropdown" class="external">
                    <label style="white-space: nowrap">${ADMIN.name} - Số dư: <strong class="text-danger">${f:customFormatDecimal("###,###,##0", ADMIN.totalAmount)} đ</strong></label>
                </a>
                <ul class="dropdown-menu checkbox-persist center dropdown-menu-right" role="menu">
                    <li class="menu-arrow">
                        <div class="menu-arrow-up"></div>
                    </li>
                    <li class="dropdown-header">
                        ${ADMIN.userName} <span class="pull-right glyphicon glyphicon-user"></span>
                    </li>
                    <li>
                        <ul class="dropdown-items">
                            <li class="padding-none clearfix">
                                <div class="pull-left dropdown-edit"><i class="fa fa-edit"></i> <a class="btn-open-modal" controller="/Admin/Permission/ChangePassword">Edit Pass</a></div>
                                <div class="pull-right dropdown-signout"><i class="fa fa-sign-out-alt"></i> <a href='<c:url value="/Admin/Logout"/>'>Sign Out</a></div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>