<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="f" uri="/WEB-INF/tlds/functions.tld" %>
<div id="sidebar">
    <div class="logo">
        <a href="<c:url value="/Admin"/>" class="text-uppercase">
            <span>BV</span>
        </a>
    </div>
    <div class="scroll-bar">
        <ul class="nav sidebar-nav">
            <c:set var="MODULE_IN_ROLES" value="${f:filterModuleInRoleList(ADMIN.roleAdmID.moduleInRoles,1)}" />
            <c:set var="MODULE" value=""/>
            <c:forEach items="${MODULE_IN_ROLES}" var="MODULE_IN_ROLE"> 
                <c:set var="MODULE" value="${MODULE_IN_ROLE.moduleID}"/>
                <c:if test="${MODULE.isShow&&!MODULE.isDelete}">
                    <li class="${MODULE_IN_ROLE.moduleID.id==sessionScope['ADMIN']['MODULE_ACTIVE_PARENT']?'active':''}">
                        <a class="external">
                            <span class="fa ${MODULE_IN_ROLE.moduleID.icon} item-icon"></span>
                            <span class="sidebar-title">${MODULE_IN_ROLE.moduleID.name}</span>
                            <c:if test="${f:size(MODULE_IN_ROLE.moduleID.modules)>0}">
                                <span class="fa ${MODULE_IN_ROLE.moduleID.id==sessionScope['ADMIN']['MODULE_ACTIVE_PARENT']?'fa-angle-up':'fa-angle-down'} item-expand"></span>
                            </c:if>
                        </a> 
                        <ul>
                            <c:forEach items="${f:filterModuleInRoleList(ADMIN.roleAdmID.moduleInRoles,MODULE.id)}" var="childrenModuleInRole">
                                <c:set value="${childrenModuleInRole.moduleID}" var="childrenModule" />
                                <c:if test="${childrenModule.isShow&&!childrenModule.isDelete}">
                                    <li class="${childrenModule.id==sessionScope['ADMIN']['MODULE_ACTIVE']?'active':''}">
                                        <a class="${childrenModule.cssClass}" data-id="${childrenModule.id}" data-parent="${MODULE_IN_ROLE.moduleID.id}" href='<c:url value="/Admin${MODULE_IN_ROLE.moduleID.controller}${childrenModule.controller}"/>'>${childrenModule.name}</a>
                                    </li>
                                </c:if>
                            </c:forEach>
                        </ul>
                    </li>
                </c:if>
            </c:forEach>
        </ul>
    </div>
</div>
<script>
    $(document).on('click', '#sidebar .sidebar-nav>li>a.external', function (e) {
        e.preventDefault();
        if (!$(this).next('ul')) {
            return;
        }
        var parent = $(this).parent();
        if (!$(parent).hasClass('active')) {
            $(parent).addClass('active');
            $(this).children('.item-expand').removeClass('fa-angle-down').addClass('fa-angle-up');
        } else {
            $(parent).removeClass('active');
            $(this).children('.item-expand').removeClass('fa-angle-up').addClass('fa-angle-down');
        }
    });
    $('#sidebar ul.sidebar-nav>li>ul').each(function(i,v){
        if(!$(v).children().length){
            $(this).parent().remove();
        }
    });
</script>