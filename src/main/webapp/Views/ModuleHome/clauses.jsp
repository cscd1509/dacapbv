<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div class="modal fade" id="clauses-modal" role="dialog">
    <div class="modal-dialog modal-lg" style="position: absolute;top: 50%;left: 50%;margin: 0;transform: translate(-50%,-50%);
         -webkit-transform: translate(-50%,-50%);-moz-transform: translate(-50%,-50%);max-height: 100%;padding: 10px 0;">
        <div class="modal-content" style="border-radius: 0;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title text-uppercase" style="font-size:36px;">REGULATION</h4>
            </div>
            <div class="modal-body">

                    <h2 style="text-align: justify;">A. Introduction BV</h2>

                    <p style="text-align: justify;">A.1. These terms and conditions shall govern your use of our website: http://BV.com&nbsp;<br />
                        A.2. By using our website, you accept these terms and conditions in full; accordingly, if you disagree with these terms and conditions or any part of these terms and conditions, you must not use our website.<br />
                        A.3. If you register with our website or use any of our website services we will ask you to expressly agree to these terms and conditions.<br />
                        A.4. By using our website or agreeing to these terms and conditions, you warrant and represent to us that you are at least 18 years of age<br />
                        A.5. submit your in-employed, no one is the boss of anyone.</p>

                    <h2 style="text-align: justify;">B. Registration and accounts</h2>

                    <p style="text-align: justify;">B.1. To be eligible for an individual account on our website you must be at least 18 years of age.<br />
                        B.2. You may register for an account with our website by completing and submitting the account registration form.<br />
                        B.3. You must not allow any other person to use your account to access the website.<br />
                        B.4. You must notify us in writing immediately if you become aware of any unauthorized use of your account..<br />
                        B.5. You must not use any other person&#39;s account to access the website.</p>

                    <h2 style="text-align: justify;">C. Username and passwords</h2>

                    <p style="text-align: justify;">C.1. If you register for an account with our website, you will not be asked to provide us a username &nbsp;with password.<br />
                        C.2. You must keep your password confidential.<br />
                        C.3. You must notify us in writing immediately if you become aware of any disclosure of your password.<br />
                        C.4. You are responsible for any activity on our website arising out of any failure to keep your password confidential, and may be held liable for any losses arising out of such a failure.</p>

                    <h2 style="text-align: justify;">D. Cancellation and suspension of account</h2>

                    <p style="text-align: justify;">D.1. We may suspend your account at any time in our sole discretion without notice or explanation with the possibility of return on your funds.<br />
                        D.2. You can not cancel your account on our website.</p>

                    <h2 style="text-align: justify;">E. Breaches of these terms and conditions</h2>

                    <p style="text-align: justify;">E.1. Without prejudice to our other rights under these terms and conditions, if you breach these terms and conditions in any way, or if we reasonably suspect that you have breached these terms and conditions in any way, we may:</p>

                    <p style="margin-left: 40px; text-align: justify;"><span style="line-height: 20.8px;">- Send you one or more formal warnings.</span><br style="line-height: 20.8px;" />
                        <span style="line-height: 20.8px;">- Temporarily suspend your access to our website.</span><br style="line-height: 20.8px;" />
                        <span style="line-height: 20.8px;">- Permanently prohibit you from accessing our website.</span><br style="line-height: 20.8px;" />
                        <span style="line-height: 20.8px;">- Block computers using your IP address from accessing our website.</span><br style="line-height: 20.8px;" />
                        <span style="line-height: 20.8px;">- Contact any or all of your internet service providers and request that they block your access to our website.</span><br style="line-height: 20.8px;" />
                        <span style="line-height: 20.8px;">- Suspend or delete your account on our website.</span></p>

                    <p style="text-align: justify;"><span style="line-height: 20.8px;">E.2 Where we suspend or prohibit or block your access to our website or a part of our website, you must not take any action to circumvent such suspension or prohibition or blocking (including without limitation creating and/or using a different account).</span></p>

                    <h2 style="text-align: justify;">F. change</h2>

                    <p style="text-align: justify;">F.1. We may revise these terms and conditions from time to time.<br />
                        F.2. The revised terms and conditions shall apply to the use of our website from the date of publication of the revised terms and conditions on the website, and you hereby waive any right you may otherwise have to be notified of, or to consent to, revisions of these terms and conditions. We will not give you any written notice of any revision of these terms and conditions, and the revised terms and conditions will apply to the use of our website from the date that we give you such notice; if you do not agree to the revised terms and conditions you must stop using our website.<br />
                        F.3. If you have given your express agreement to these terms and conditions, we will ask for your express agreement to any revision of these terms and conditions; and if you do not give your express agreement to the revised terms and conditions within such period as we may specify, we will disable or delete your account on the website, and you must stop using the website.</p>

                    <h2 style="text-align: justify;">G. without ensure</h2>

                    <p style="text-align: justify;">G.1. There are no guarantees and promises! Neither explicit nor implicit.<br />
                        G.2. There are neither investments nor business! Participants help each other, sending each other BTC directly and without intermediaries. That&rsquo;s all! There&#39;s nothing more.<br />
                        G.3.There are no securities transactions, no relationship with the professional participants of the securities market.<br />
                        G.4. There are no rules. In principle! The only rule is no rules. At all! Even if you follow all of the instructions, you still may &quot;lose&quot;. &quot;Win&quot; might not be paid. Without any reasons or explanations.<br />
                        G.5. In general, you can lose all your money. Always remember about this and participate only with spare money. Or do not participate at all.</p>

                    <h2 style="text-align: justify;">H. Our details (BV)</h2>

                    <p style="text-align: justify;">H.1. This website is owned by Community of &nbsp;BV global, freedom from constraints.<br />
                        H.2. You can contact us by writing to the business address given above, by using our website contact form, by email to email.</p>

                    <h3 style="text-align: justify;"><a href="http://BV.com/">http://BV.com</a></h3>

                    <h3 style="text-align: justify;"><a href="mailto:support@BV.biz">E-mail: support@BV.biz</a></h3>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" style="background-color: #01859c; color: #fff;background-image: none;border: none;border-radius: 0">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<style>
    #faqs-modal .panel-heading{
        background-color: #01859c;
        background-image: none;
    }
    #faqs-modal .panel-title{
        font-size: 16px;
        text-transform: uppercase;
        color: #fff;
        font-weight: normal
    }
    #faqs-modal .panel-body{
        font-size: 14px;
        color: #000;
    }
</style>
<script>
    $('#clauses-modal .modal-dialog').mCustomScrollbar({axis: "y", autoHideScrollbar: true, scrollInertia: 20, advanced: {autoScrollOnFocus: false}});
</script>