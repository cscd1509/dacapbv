<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="/WEB-INF/tlds/functions.tld" prefix="f"%>
<div id="footer" class="color-white">
    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-xs-12">
                <h3>About us</h3>
                <c:set var="ABOUT" value="${f:findNewsEager(1)}"/>
                <div class="about-us" style="color: white;text-align: justify">
                    ${ABOUT.content}
                </div>
            </div>
            <div class="col-sm-4 col-xs-12">
                <h3>Recent News</h3>
                <ul>
                    <c:forEach items="${RECENT_NEWS}" var="news">
                        <li>
                            <a href="/NewsDetail/${news.id}" title="${news.name}" target="_blank">${news.name}</a>
                        </li>
                    </c:forEach>
                </ul>
            </div>
            <div class="col-sm-4 col-xs-12">
                <h3>How to contact?</h3>
                <ul>
                    <li>Contact with us 24h/day</li>
                    <li>
                        <a href="mailto:support@BV.com" class="btn btn-orange btn-transition btn-lg text-uppercase">
                            <span>support@BV.com&nbsp;&nbsp;&nbsp;<i class="fa fa-envelope"></i></span>
                            <span>support@BV.com&nbsp;&nbsp;&nbsp;<i class="fa fa-envelope"></i></span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div id="coppyright">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">
                Find and follow us: <i class="fa fa-facebook"></i> Facebook <i class="fa fa-twitter"></i> Twitter <i class="fa fa-youtube"></i> Youtube
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12 text-right">
                Copyright © 2016 BV - All Rights Reserved
            </div>
        </div>
    </div>
</div>