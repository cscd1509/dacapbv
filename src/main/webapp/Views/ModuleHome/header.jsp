<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="/WEB-INF/tlds/functions.tld" %>
<header class="clearfix header1">                 
    <div class="top-bar light_header">
    </div>
    <div class="navbar navbar-default navbar-top  light_header">
        <div class="container header-container abc">
            <div class="navbar-header" >
                <button type="button" class="navbar-toggle" data-toggle="collapse"
                        data-target="#navbar-collapse">
                    <i class="fa fa-bars"></i>
                </button>
                <a id="alogo" class="navbar-brand" href="/Home" title="BV" rel="home">
                    <img src="/Resources/img/logo-white.png" />
                </a>
            </div>
            <div class="navbar-collapse collapse" id="navbar-collapse">
                <div class="navbar-custom">
                    <div class="menu-top-menu-container">
                        <ul id="menu-top-menu" class="nav navbar-nav">
                            <li id="menu-item-12" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-12">
                                <a href="/Home" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-12">Home</a>
                            </li>
                            <li id="menu-item-12" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-12">
                                <a href="/NewsDetail/33" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-14">Introduction</a>
                            </li>
                            <!--                            <li id="menu-item-13" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-13">
                                                            <a href="#" data-toggle="modal" data-target="#clauses-modal" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-13">Clause<b class=""></b></a>
                                                        </li>
                                                        <li id="menu-item-161" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-161">
                                                            <a href="#" data-toggle="modal" data-target="#faqs-modal" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-161">Answers</a>
                                                        </li>-->
                            <li id="menu-item-81" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-81">
                                <a href="/News" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-81">News</a>
                            </li>
                        </ul>
                    </div>          
                </div>

                <div class="pull-right navbar-custom">
                    <a href="#" class="nav-search-btn">
                        <img src="/Resources/img/search_icon.png" />
                    </a>
                </div>
                <div class="pull-right navbar-custom">
                    <div class="menu-top-right-menu-container">
                        <ul id="menu-top-right-menu" class="nav navbar-nav topright-nav">
                            <li id="menu-item-72" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-72">
                                <a href="/Login" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-72">Login</a>
                            </li>
                            <li id="menu-item-73" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-73">
                                <a href="/Register" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-73">Sign Up</a>
                            </li>
                            <%--<li id="menu-item-74" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-74">
                                <div id="google_translate_element" style="padding: 30px 10px 6px 10px !important;"></div>
                            </li>--%>
                            <li>
                                <a href="javascript:void(0)" class="top-menu-selected-language" style="margin:0 !important" id="lang">
                                    <img src="/Resources/img/flag_England.png" alt=""/>&emsp;English
                                </a>                                
                                <ul class="top-menu-language-options">
                                    <li><a href="javascript:void(0)" data-lang="en" data-name="English" class="change-lang"><img src="/Resources/img/flag_England.png" alt=""/>&emsp;English</a></li>
                                    <li><a href="javascript:void(0)" data-lang="fr" data-name="French" class="change-lang"><img src="/Resources/img/flag_France.png" alt=""/>&emsp;French</a></li>
                                    <li><a href="javascript:void(0)" data-lang="es" data-text="Spanish" class="change-lang"><img src="/Resources/img/flag_Spain.png" alt=""/>&emsp;Spanish</a></li>
                                    <li><a href="javascript:void(0)" data-lang="zh-CN" data-text="Chinese" class="change-lang"><img src="/Resources/img/flag_China.png" alt=""/>&emsp;Chinese</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>                                   
                </div>
            </div>
        </div>
        <div class="btc-mobile-menu hide">
            <div class="menu-top-menu-for-mobile-english-container">
                <ul id="menu-top-menu-for-mobile-english" class="wpb-mobile-menu">
                    <li>
                        <a href="/Home">Home</a>
                    </li>
                    <li>
                        <a href="/Login">Login</a>
                    </li>
                    <li>
                        <a href="/Register">Sign Up</a>
                    </li>
                    <li>
                        <a href="/NewsDetail/33">Introduction</a>
                    </li>
                    <!--                    <li>
                                            <a href="#" data-toggle="modal" data-target="#clauses-modal">Clause<b class=""></b></a>
                                        </li>
                                        <li>
                                            <a href="#" data-toggle="modal" data-target="#faqs-modal">Answers</a>
                                        </li>-->
                    <li>
                        <a href="/News">News</a>
                    </li>  
                    <li>
                        <a href="javascript:void(0)" class="top-menu-selected-language" style="margin:0 !important" id="lang">
                            <img src="/Resources/img/flag_England.png" alt=""/>&emsp;English
                        </a>                                
                        <ul>
                            <li><a href="javascript:void(0)" data-lang="en" data-name="English" class="change-lang"><img src="/Resources/img/flag_England.png" alt=""/>&emsp;English</a></li>
                            <li><a href="javascript:void(0)" data-lang="fr" data-name="French" class="change-lang"><img src="/Resources/img/flag_France.png" alt=""/>&emsp;French</a></li>
                            <li><a href="javascript:void(0)" data-lang="es" data-text="Spanish" class="change-lang"><img src="/Resources/img/flag_Spain.png" alt=""/>&emsp;Spanish</a></li>
                            <li><a href="javascript:void(0)" data-lang="zh-CN" data-text="Chinese" class="change-lang"><img src="/Resources/img/flag_China.png" alt=""/>&emsp;Chinese</a></li>
                        </ul>
                    </li>
                </ul>
            </div>        
        </div>
        <div class="abcd"></div>
        <div class="pull-right navbar-custom mobile-menu"></div>
    </div>
</header>
<script type="text/javascript">
    function setCookie(b, h, c, f, e) {
        var a;
        if (c === 0) {
            a = "";
        } else {
            var g = new Date();
            g.setTime(g.getTime() + (c * 24 * 60 * 60 * 1000));
            a = "expires=" + g.toGMTString() + "; ";
        }
        var e = (typeof e === "undefined") ? "" : "; domain=" + e;
        document.cookie = b + "=" + h + "; " + a + "path=" + f + e;
    }

    function getCookie(d) {
        var b = d + "=";
        var a = document.cookie.split(";");
        for (var e = 0; e < a.length; e++) {
            var f = a[e].trim();
            if (f.indexOf(b) == 0) {
                return f.substring(b.length, f.length);
            }
        }
        return "";
    }

    //Google provides this function
    function googleTranslateElementInit() {
        new google.translate.TranslateElement({
            pageLanguage: "en",
            includedLanguages: "en,fr,es,zh-CN",
            layout: google.translate.TranslateElement.InlineLayout.SIMPLE,
            autoDisplay: false
        }, "google_translate_element");
    }

    //Using jQuery
    var googTrans = getCookie('googtrans');
    if (googTrans !== '' && googTrans !== '/en/en') {
        $('.top-menu-selected-language').html(getCookie('googtransName'));
    }
    $(document).ready(function () {
        $(".change-lang").on("click", function () {
            var googtrans = $(this).data('lang');
            var googtransName = $(this).html();
            setCookie("googtrans", "/en/" + googtrans, 0, "/", "BV.com");
            setCookie("googtrans", "/en/" + googtrans, 0, "/");
            setCookie("googtransName", googtransName, 0, "/", "BV.com");
            setCookie("googtransName", googtransName, 0, "/");
            location.reload();
        });

        if (googTrans !== '' && googTrans !== '/en/en') {
            $('.top-menu-selected-language').html(getCookie('googtransName'));
            downloadJSAtOnload();
        }
    });

    function downloadJSAtOnload() {
        var i;
        var paths = new Array(
                '//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit'
                );
        for (i in paths) {
            if (typeof paths[i] !== 'string') {
                console.log(typeof paths[i]);
                continue;
            }
            var element = document.createElement("script");
            element.src = paths[i];
            document.body.appendChild(element);
        }
    }
</script>
<div class="ticker-news" data="announcements-en">
    <div class="container">
        <span>INFORMATION ></span>
        <ul id="ticker">
            <c:set value="${f:getHomePosts(5)}" var="RECENT_NEWS" scope="request"/>
            <c:forEach items="${RECENT_NEWS}" var="news">
                <li>
                    <a href="/NewsDetail/${news.id}" title="${news.name}" target="_blank">${news.name}</a>
                </li>
            </c:forEach>
        </ul>
        <script type="text/javascript">
            var tickerIndex = 0;
            function customRotateTicker()
            {
                $('#ticker').find('li:eq(' + tickerIndex + ')').animate({top: '-34px'}, 500, function () {
                    $('#ticker').find('li:eq(' + tickerIndex + ')').css('top', '34px');
                });

                if (tickerIndex == ($('#ticker').find('li').length - 1))
                {
                    $('#ticker').find('li:eq(0)').css('top', '34px');
                    $('#ticker').find('li:eq(0)').animate({top: '0'}, 500, function () {
                        $('#ticker').find('li:gt(0)').css('top', '34px');
                    });
                    tickerIndex = 0;
                } else
                {
                    tickerIndex++;
                    $('#ticker').find('li:eq(' + tickerIndex + ')').animate({top: '0'}, 500);
                }
            }
            jQuery(document).ready(function () {
                /*jQuery('.ticker-news ul').innerfade({animationtype: 'slide', speed: 750 , timeout: 500});*/
                $('#ticker').find('li').each(function () {
                    $(this).css('top', '34px').css('position', 'absolute');
                });
                $('#ticker').find('li:first-child').css('top', '0');

                var sint = self.setInterval(customRotateTicker, 4000);
            });
        </script>
    </div>
</div>