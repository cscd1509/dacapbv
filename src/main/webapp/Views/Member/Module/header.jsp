<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/tlds/functions.tld" prefix="f"%>
<div id="menu-overlay" class="d-xl-none"></div>
<div id="mb-menu" class="d-xl-none">
    <a href="javascript:void(0)" id="close-menu" class="ti-close"></a>
    <ul class="list-unstyled">
        <li><a href="/System">Tổng tuyến kinh doanh</a></li>
        <li><a href="/Referral">Danh sách Cộng Tác Viên</a></li>
        <li><a href="javascript:void(0)" data-toggle="modal" data-target="#modal-affiliate">Giới thiệu tham gia</a></li>
        <!--        <li><a href="javascript:void(0)" data-toggle="modal" data-target="#modal-deposit">Đăng ký cộng tác viên</a></li>-->
        <li><a href="/HistoryAward">Thống kê thu nhập</a></li>
        <li><a href="/SystemNews">Tin nội bộ</a></li>
        <li class="divider"></li>
        <li><a href="/Profile">Hồ sơ cá nhân</a></li>
        <li><a href="/Security">Bảo mật</a></li>
        <li><a href="javascript:void(0)" data-toggle="modal" data-target="#modal-compose">Liên Hệ</a></li>
        <li class="divider"></li>
        <li><a href="/Signout" title="" class="dropdown-item"><i class="fas fa-sign-out-alt"></i>Thoát</a></li>
    </ul>
</div>
<header>
    <div class="row no-gutters align-items-center">
        <a href="javascript:void(0)" id="toggle-mb-menu" class="col col-auto d-flex d-xl-none justify-content-center align-items-center">
            <i class="ti-menu"></i>
        </a>
        <div class="col col-auto">
            <a class="logo d-block" href="/Dashboard">
                <img src="/Resources/shared/img/logo.png" alt="Logo"/> <span class="d-none d-sm-inline">System</span>
            </a>
        </div>
        <div class="row no-gutters align-items-center col">
            <div class="d-flex col justify-content-xl-start justify-content-end">
                <div class="active-account text-nowrap">
                    <div class="d-flex flex-column">
                        <small class="account-type-label">Thu Nhập</small>
                        <b class="d-block"><span class="account-type-amount">${f:customFormatDecimal("###,###,##0 bv", f:sumAwardByCusID(MEMBER.id))}</span></b>
                    </div>
                </div>
                <div class="active-account text-nowrap text-danger">
                    <div class="d-flex flex-column">
                        <small class="account-type-label">Năng động</small>
                        <b>${f:customFormatDecimal("###,###,##0 bv", MEMBER.totalAmount)}</b>
                    </div>
                </div>
                <div class="active-account text-nowrap text-success">
                    <div class="d-flex flex-column">
                        <small class="account-type-label">Đã Nạp</small>
                        <b>${f:customFormatDecimal("###,###,##0 bv", MEMBER.totalDeposit)}</b>
                    </div>
                </div>
            </div>
            <div class="dropdown user d-none d-xl-flex align-items-center col col-auto">
                <button class="btn btn-link text-uppercase d-flex align-items-center" data-toggle="dropdown">
                    <span class="badge badge-info" style="color:${MEMBER.rankCustomer.color}">${MEMBER.rankCustomer.name}</span>
                    ${MEMBER.fullName}&nbsp;<i class="fas fa-caret-down"></i>
                </button>
                <div class="dropdown-menu">
                    <a href="/Profile" title="" class="dropdown-item">
                        <i class="fas fa-user-circle"></i>Hồ Sơ Cá Nhân
                    </a>
                    <%--                    <a href="javascript:void(0)" data-toggle="modal" data-target="#modal-deposit" class="dropdown-item">
                                            <i class="far fa-credit-card"></i>Đăng ký đại lý
                                        </a>--%>
                    <a href="/Security" title="" class="dropdown-item">
                        <i class="fas fa-shield-alt"></i>Bảo Mật
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="/Signout" title="" class="dropdown-item">
                        <i class="fas fa-sign-out-alt"></i>Thoát
                    </a>
                </div>
            </div>
        </div>
    </div>
</header>
<script>
    $(function () {
        var pathname = location.pathname;
        if (pathname.split('/').length > 1) {
            var path = pathname.split('/')[1].toUpperCase();
            $('header .short-menu>li').each(function () {
                var href = $(this).children('a').attr('href').toUpperCase().split(/\//g)[1];
                if (href === path) {
                    $(this).addClass('active');
                }
            });
        }
    });
</script>