<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div class="left-sidebar">
    <ul class="list-unstyled menu d-flex flex-column justify-content-center justify-content-sm-start">
        <li>
            <a href="/System">
                <i class="fas fa-chart-bar"></i>
                <span>Tổng Tuyến<br/>Kinh Doanh</span>
            </a>
        </li>
        <li>
            <a href="/Referral">
                <i class="fas fa-users"></i>
                <span>Danh Sách<br/>CTV</span>
            </a>
        </li>
        <li>
            <a href="/Profile">
                <i class="fas fa-cubes"></i>
                <span>Hồ Sơ<br/>Cá Nhân</span>
            </a>
        </li>
        <li>
            <a href="javascript:void(0)" data-toggle="modal" data-target="#modal-affiliate">
                <i class="far fa-handshake"></i>
                <span>Giới Thiệu<br/>Tham Gia</span>
            </a>
        </li>
        <li>
            <a href="/HistoryAward">
                <i class="fas fa-trophy"></i>
                <span>Thống Kê<br/>Thu Nhập</span>
            </a>
        </li>
        <li>
            <a href="/SystemNews">
                <i class="far fa-newspaper"></i>
                <span>Tin<br/>Nội Bộ</span>
            </a>
        </li>
        <li>
            <a href="#" data-toggle="modal" data-target="#modal-compose">
                <i class="far fa-comment-alt"></i>
                <span>Liên Hệ</span>
            </a>
        </li>
    </ul>
</div>