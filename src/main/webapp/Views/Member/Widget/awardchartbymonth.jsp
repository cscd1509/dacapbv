<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/tlds/functions.tld" prefix="f"%>
<div class="table-responsive">
    <table class="table">
        <thead>
            <tr>
                <th class="text-center">Thời gian</th>
                <th class="text-center">Tổng tiền</th>
                <th class="text-center">Ngày tạo</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${HISTORY_PAYMENT}" var="item">
                <tr>
                    <td class="text-center">
                        Từ: ${f:customFormatDate("dd/MM/yyyy", item.circleWithdrawID.fromDate)}<br/>
                        Đến: ${f:customFormatDate("dd/MM/yyyy", item.circleWithdrawID.toDate)}
                    </td>
                    <td class="text-center"><span class="badge badge-danger">${f:customFormatDecimal("###,###,###", item.amount)}</span></td>
                    <td class="text-center">${f:customFormatDate("HH:mm:ss", item.circleWithdrawID.createdDate)}<br/>
                    ${f:customFormatDate("dd/MM/yyyy", item.circleWithdrawID.createdDate)}</td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</div>