<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/WEB-INF/tlds/functions.tld" prefix="f"%>
<div class="chart-holder" id="dashboard-donut-1" style="height: 235px;"></div>
<script>
    Morris.Donut({
        element: 'dashboard-donut-1',
        data: [
            {label: "Trực tiếp", value: ${AWARD_CHART_DATA[0]}},
            {label: "Gián tiếp", value: ${AWARD_CHART_DATA[1]}},
            {label: "Hệ thống", value: ${AWARD_CHART_DATA[2]}}
        ],
        labelColor: '#FFFFFF',
        colors: ['#33414E', '#1caf9a', '#FEA223'],
        resize: false
    });
</script>