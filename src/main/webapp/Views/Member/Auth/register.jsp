<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/tlds/functions.tld" prefix="f"%>
<c:set value="${f:getIntegerRandom()}" var="RESOURCES_VERSION" />
<!DOCTYPE html>
<html lang="en">
    <head>   
        <title>Đăng ký hệ thống BV</title>         
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="/Resources/shared/img/favicon.png" type="image/x-icon">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet"/>
        <link rel="stylesheet" type="text/css" href="<c:url value="/Resources/shared/css/bootstrap.min.css?v=${RESOURCES_VERSION}"/>"/>
        <link rel="stylesheet" type="text/css" href="<c:url value="/Resources/layout/member/css/login.css?v=${RESOURCES_VERSION}"/>"/>
        <script type="text/javascript" src="<c:url value="/Resources/shared/js/jquery-3.2.1.min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/Resources/shared/js/validationengine/jquery.validationEngine.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/Resources/shared/js/jquery-validation/jquery.validate.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/Resources/shared/js/jquery-validation/additional-methods.min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/Resources/shared/js/validationengine/languages/jquery.validationEngine-vi.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/Resources/shared/js/noty/jquery.noty.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/Resources/shared/js/noty/layouts/bottomCenter.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/Resources/shared/js/noty/themes/default.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/Resources/shared/js/serialize.js"/>"></script>
    </head>
    <body>
        <div id="login-container" class="d-flex align-items-center">
            <div class="login-box register-box">
                <div class="login-body">
                    <div class="login-title text-center"><strong>Đăng ký</strong> hệ thống BV</div>                        
                    <form method="post" action="<c:url value="/Register"/>" role="form" id="login-form">
                        <div class="form-group">
                            <label class="control-label">Họ và tên</label>
                            <input type="text" class="form-control" name="fullName"/>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Người giới thiệu</label>
                            <input type="text" class="form-control" name="refID" value="${REFERRAL}"/>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Tên đăng nhập</label>
                            <input type="text" class="form-control" name="userName" maxlength="15"/>
                            <div class="form-text text-muted">Chỉ chấp nhận ký tự abc và số (A-Z, a-z, 0-9)</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label">Mật khẩu</label>
                                    <input type="password" class="form-control" name="password" id="password"/>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label">&nbsp;</label>
                                    <input type="password" class="form-control external" name="repassword" placeholder="Nhập lại mật khẩu"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label">Địa chỉ email</label>
                                    <input type="email" class="form-control" name="email"/>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label">Số điện thoại</label>
                                    <input type="text" class="form-control" name="mobile"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label">Số CMND/Thẻ căn cước/Hộ chiếu</label>
                                    <input type="text" class="form-control" name="peoplesIdentity"/>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label">Giới tính</label>
                                    <select class="form-control" name="gender">
                                        <option value="1" selected>Nam</option>
                                        <option value="0">Nữ</option>
                                    </select> 
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label">Tên ngân hàng</label>
                                    <input type="text" class="form-control" name="bankName"/>
                                </div>
                            </div>
                            <div class="col-sm-4 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label">Số tài khoản</label>
                                    <input type="text" class="form-control" name="bankNumber"/>
                                </div>
                            </div>
                            <div class="col-sm-4 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label">Chi nhánh</label>
                                    <input type="text" class="form-control" name="bankAgency"/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-info btn-block btn-lg" id="btn-login" style="margin: 20px 0">Đăng ký</button> 
                        </div>
                        <div class="form-group row">
                            <div class="col-md-5">
                                <div class="login-subtitle">
                                    <a href="<c:url value="/ResetPassword"/>">Quên mật khẩu?</a>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="login-subtitle text-right">
                                    Bạn đã có tài khoản? <a href="<c:url value="/Login"/>">Đăng nhập!</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="login-footer">
                    <div class="pull-left">
                        Copyright © 2016 BV . All rights reserved.
                    </div>
                </div>
            </div>
        </div>
        <script>
            var validate = $("#login-form").validate({
                rules: {
                    fullName: {
                        required: true
                    },
                    password: {
                        required: true,
                        minlength: 6,
                        maxlength: 20
                    },
                    repassword: {
                        required: true,
                        equalTo: "#password"
                    },
                    userName: {
                        required: true,
                        matches: /^[A-Za-z0-9]{6,15}$/
                    },
                    refID: {
                        required: true
                    },
                    parentID: {
                        required: true
                    },
                    email: {
                        required: true,
                        email: true
                    }
                },
                submitHandler: function () {
                    var url = $('#login-form').attr('action');
                    $.ajax({
                        beforeSend: function () {
                            $('#btn-login').attr('disabled', true);
                        },
                        url: url,
                        type: "POST",
                        data: $('#login-form').serializeObject(),
                        success: function (data) {
                            openAlert(data);
                            $('#btn-login').attr('disabled', false);
                        },
                        error: function () {
                            openAlert(null, function () {
                                noty({text: 'Đã xảy ra lỗi. Vui lòng thử lại sau!', layout: 'bottomCenter', type: 'error'});
                            });
                            $('#btn-login').attr('disabled', false);
                        }
                    });
                }
            });
        </script>
    </body>
</html>