<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${sessionScope['MEMBER']!=null}">
    <c:redirect url="/Home"/>
</c:if>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Tạo lại mật khẩu hệ thống BV</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="/Resources/shared/img/favicon.png" type="image/x-icon">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet"/>
        <link rel="stylesheet" type="text/css" href="<c:url value="/Resources/shared/css/bootstrap.min.css?v=${RESOURCES_VERSION}"/>"/>
        <link rel="stylesheet" type="text/css" href="<c:url value="/Resources/layout/member/css/login.css?v=${RESOURCES_VERSION}"/>"/>
        <script type="text/javascript" src="<c:url value="/Resources/shared/js/jquery-3.2.1.min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/Resources/shared/js/validationengine/languages/jquery.validationEngine-en.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/Resources/shared/js/validationengine/jquery.validationEngine.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/Resources/shared/js/jquery-validation/jquery.validate.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/Resources/shared/js/jquery-validation/additional-methods.min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/Resources/shared/js/noty/jquery.noty.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/Resources/shared/js/noty/layouts/bottomCenter.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/Resources/shared/js/noty/themes/default.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/Resources/shared/js/serialize.js"/>"></script>
    </head>
    <body>        
        <div id="login-container" class="d-flex align-items-center">
            <div class="login-box animated fadeInDown">
                <div class="login-logo text-center">
                    <img src="/Resources/shared/img/logo.png" alt=""/>
                </div>
                <div class="login-body">
                    <div class="login-title text-center"><strong>Tạo lại</strong> mật khẩu</div>
                    <form action="<c:url value="/ResetPassword" />" class="form-horizontal" id="login-form" method="post">
                        <div class="form-group">
                            <input type="text" class="form-control" name="username" placeholder="Tên đăng nhập" />
                        </div>
                        <div class="form-group">                                        
                            <input type="email" class="form-control" name="email" placeholder="Địa chỉ email">
                        </div>
                        <button type="submit" class="btn btn-info btn-block" id="btn-login">Tạo lại</button>
                    </form>
                </div>
                <div class="login-footer">
                    <div class="pull-left">
                        Copyright © 2016 BV . All rights reserved.
                    </div>
                </div>
            </div>
        </div>
        <script>
            $("#login-form").validate({
                rules: {
                    username: {
                        required: true
                    },
                    password: {
                        required: true
                    }
                },
                submitHandler: function () {
                    var url = $('#login-form').attr('action');
                    $.ajax({
                        beforeSend: function () {
                            $('#btn-login').attr('disabled', true);
                        },
                        url: url,
                        type: "POST",
                        data: $('#login-form').serializeObject(),
                        success: function (data) {
                            openAlert(data);
                            $('#btn-login').attr('disabled', false);
                        },
                        error: function () {
                            openAlert(null, function () {
                                noty({text: 'Đã xảy ra lỗi. Vui lòng thử lại sau!', layout: 'bottomCenter', type: 'error'});
                            });
                            $('#btn-login').attr('disabled', false);
                        }
                    });
                }
            });
        </script>
    </body>
</html>