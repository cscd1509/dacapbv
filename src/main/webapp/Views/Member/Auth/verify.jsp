<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/tlds/functions.tld" prefix="f"%>
<c:set value="${f:getIntegerRandom()}" var="RESOURCES_VERSION" />
<c:if test="${sessionScope['TEMP_MEMBER']==null}">
    <c:redirect url="/Login"/>
</c:if>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>BV LOGIN PAGE</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="/Resources/shared/img/favicon.png" type="image/x-icon">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet"/>
        <link rel="stylesheet" type="text/css" href="<c:url value="/Resources/shared/css/bootstrap.min.css"/>"/>
        <link rel="stylesheet" type="text/css" href="<c:url value="/Resources/shared/css/themify-icons.css"/>"/>
        <link rel="stylesheet" type="text/css" href="<c:url value="/Resources/layout/member/css/login.css?v=${RESOURCES_VERSION}"/>"/>
        <script type="text/javascript" src="<c:url value="/Resources/shared/js/jquery-3.2.1.min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/Resources/shared/js/validationengine/languages/jquery.validationEngine-en.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/Resources/shared/js/validationengine/jquery.validationEngine.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/Resources/shared/js/jquery-validation/jquery.validate.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/Resources/shared/js/jquery-validation/additional-methods.min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/Resources/shared/js/noty/jquery.noty.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/Resources/shared/js/noty/layouts/bottomCenter.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/Resources/shared/js/noty/themes/default.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/Resources/shared/js/serialize.js"/>"></script>
    </head>
    <body>
        <div id="login-container" class="d-flex align-items-center">
            <div id="modal-2faconfirm" class="mr-auto ml-auto">
                <p class="text-center"><img src="/Resources/shared/img/logo.png" alt=""/></p>
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <form id="2faconfirm-form" role="form" novalidate action="/VerifyLogin">
                            <div class="modal-header">
                                <h5 class="modal-title">2-step Verification</h5>
                            </div>
                            <div class="modal-body">Please enter the verification code from your phone.</div>
                            <div class="modal-body bg-gray">
                                <div class="row">
                                    <div class="col col-auto">
                                        <i class="ti-mobile"></i>
                                    </div>
                                    <div class="col">
                                        <p>Enter the 2-step verification code provided by "Google&nbsp;Authenticator" to your smart phone</p>
                                        <p><input type="text" class="form-control" id="2FA-code" name="code" /></p>
                                        <small>More about 2-step authentication can be found in security.</small>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer d-flex justify-content-end">
                                <button type="submit" class="btn btn-primary btn-lg btn-save" id="btn-verify">Verify</button>
                            </div>
                        </form>

                        <script>
                            $("#2faconfirm-form").validate({
                                rules: {
                                    code: {
                                        required: true,
                                        number: true
                                    }
                                },
                                submitHandler: function () {
                                    var url = $("#2faconfirm-form").attr('action');
                                    $.ajax({
                                        beforeSend: function () {
                                            $('#btn-verify').attr('disabled', true);
                                        },
                                        url: url,
                                        type: "POST",
                                        data: $('#2faconfirm-form').serializeObject(),
                                        success: function (data) {
                                            openAlert(data.value);
                                        },
                                        error: function () {
                                            openAlert(null, function () {
                                                noty({text: 'Đã xảy ra lỗi. Vui lòng thử lại sau!', layout: 'bottomCenter', type: 'error'});
                                            });
                                        }, complete: function () {
                                            $('#btn-verify').attr('disabled', false);
                                        }
                                    });
                                }
                            });
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>