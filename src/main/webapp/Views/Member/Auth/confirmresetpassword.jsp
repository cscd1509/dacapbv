<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${sessionScope['MEMBER']!=null}">
    <c:redirect url="/Home"/>
</c:if>
<!DOCTYPE html>
<html lang="en" class="body-full-height">
    <head>
        <title>Tạo lại mật khẩu hệ thống BV</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="/Resources/shared/img/favicon.png" type="image/x-icon">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet"/>
        <link rel="stylesheet" type="text/css" href="<c:url value="/Resources/shared/css/bootstrap.min.css?v=${RESOURCES_VERSION}"/>"/>
        <link rel="stylesheet" type="text/css" href="<c:url value="/Resources/layout/member/css/login.css?v=${RESOURCES_VERSION}"/>"/>
        <script type="text/javascript" src="<c:url value="/Resources/shared/js/jquery-3.2.1.min.js"/>"></script>
    </head>
    <body>
        <div id="login-container" class="d-flex align-items-center">
            <div class="login-box animated fadeInDown">
                <div class="login-logo text-center">
                    <img src="/Resources/shared/img/logo.png" alt=""/>
                </div>
                <div class="login-body">
                    <c:if test="${RESULT_CONFIRM.key==0}">
                        <div class="login-title text-center text-danger">YÊU CẦU KHÔNG HỢP LỆ</div>
                    </c:if>
                    <c:if test="${RESULT_CONFIRM.key==1}">
                        <div class="login-title text-center">Mật khẩu mới của bạn là:</div>
                        <div class="form-group">
                            <input type="text" class="form-control new-password" readonly value="${RESULT_CONFIRM.value}" />
                        </div>
                    </c:if>
                    <div class="row align-items-center">
                        <div class="col-md-6">
                            <a class="btn btn-link btn-block" href="<c:url value="/ResetPassword"/>">Tạo lại mật khẩu lần nữa?</a>
                        </div>
                        <div class="col-md-6">
                            <a href="/Login" class="btn btn-info btn-block" id="btn-login">Đăng Nhập</a>
                        </div>
                    </div>
                </div>
                <div class="login-footer">
                    <div class="pull-left">
                        Copyright © 2016 BV . All rights reserved.
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>