<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/tlds/functions.tld" prefix="f"%>
<div class="modal fade" tabindex="-1" role="dialog" id="modal-transfer">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="POST" id="transfer-form" role="form" novalidate action="<c:url value="/Transaction/Transfer"/>">
                <div class="modal-header">
                    <h5 class="modal-title">Transfer</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
<!--                    <div class="alert alert-warning" role="alert">
                        You can transfer up to <b class="text-success">${f:customFormatDecimal("########.########", (TOTAL_REAL_COIN-0.01)<=0?0:TOTAL_REAL_COIN-0.01)}</b> <small>ETH</small>
                    </div>-->
                    <div class="form-group">
                        <label class="control-label">Amount</label> 
                        <input type="number" class="form-control" name="amount" value="0.01" />
                    </div>
                    <div class="form-group">
                        <label class="control-label">Receiver's Username</label> 
                        <input type="text" class="form-control" name="username"/>   
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-between">
                    <button type="reset" class="btn btn-danger btn-cancel">Cancel <span class="fa fa-times"></span></button>
                    <button type="submit" class="btn btn-success btn-save">Transfer <span class="fas fa-gift"></span></button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $("#transfer-form").validate({
        rules: {
            username: {
                required: true
            },
            amount: {
                required: true
            }
        },
        submitHandler: function () {
            var url = $('#transfer-form').attr('action');
            var data = $('#transfer-form').serializeObject();
            var needVerify = $('#withdraw-form').attr('data-needverify');
            if (needVerify==='true') {
                $('#modal-2faconfirm').modal('show');
                $('#btn-verify').off('click').click(function () {
                    if ($("#2faconfirm-form").validate().valid()) {
                        var code = $('#2FA-code').val();
                        data['code'] = code;
                        transfer(url, data);
                    }
                });
            } else {
                transfer(url, data);
            }

        }
    });
    function transfer(url, formData) {
        $.ajax({
            url: url,
            type: "POST",
            data: formData,
            success: function (data) {
                if (data.key === 1) {
                    location.reload();
                } else if (data.key === -1) {
                    openAlert(data.value);
                } else {
                    $('#modal-2faconfirm').modal('hide');
                    openAlert(data.value);
                }
            }, error: function () {
                openAlert(null, function () {
                    noty({text: 'Đã xảy ra lỗi. Vui lòng thử lại sau!', layout: 'bottomCenter', type: 'error'});
                });
            }
        });
    }
</script>