<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/tlds/functions.tld" prefix="f"%>
<div class="modal fade" tabindex="-1" role="dialog" id="modal-withdraw">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="POST" id="withdraw-form" role="form" novalidate action="<c:url value="/Transaction/Withdraw"/>">
                <div class="modal-header">
                    <h5 class="modal-title">Withdraw</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
<!--                    <div class="alert alert-warning" role="alert">
                        You can withdraw up to <b class="text-success">${f:customFormatDecimal("########.########", (TOTAL_REAL_COIN-0.01)<=0?0:TOTAL_REAL_COIN-0.01)}</b> ETH
                    </div>-->
                    <div class="form-group">
                        <label class="control-label">Amount</label> 
                        <input type="number" class="form-control" name="amount" value="0.01" />                                                    
                    </div>
                    <div class="form-group">
                        <label class="control-label">Wallet Address</label> 
                        <input type="text" class="form-control" name="address"/>                                                    
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-between">
                    <button type="reset" class="btn btn-danger btn-cancel">Cancel <span class="fa fa-times"></span></button>
                    <button type="submit" class="btn btn-success btn-save">Withdraw <span class="fas fa-download"></span></button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $("#withdraw-form").validate({
        rules: {
            address: {
                required: true
            },
            amount: {
                required: true
            }
        },
        submitHandler: function () {
            var url = $('#withdraw-form').attr('action');
            var data = $('#withdraw-form').serializeObject();
            var needVerify = $('#withdraw-form').attr('data-needverify');
            if (needVerify==='true') {
                $('#modal-2faconfirm').modal('show');
                $('#btn-verify').off('click').click(function () {
                    if ($("#2faconfirm-form").validate().valid()) {
                        var code = $('#2FA-code').val();
                        data['code'] = code;
                        withdraw(url, data);
                    }
                });
            } else {
                withdraw(url, data);
            }
        }
    });
    function withdraw(url, formData) {
        $.ajax({
            url: url,
            type: "POST",
            data: formData,
            success: function (data) {
                if (data.key === 1) {
                    location.reload();
                } else if (data.key === -1) {
                    openAlert(data.value);
                } else {
                    $('#modal-2faconfirm').modal('hide');
                    openAlert(data.value);
                }
            }, error: function () {
                openAlert(null, function () {
                    noty({text: 'Đã xảy ra lỗi. Vui lòng thử lại sau!', layout: 'bottomCenter', type: 'error'});
                });
            }
        });
    }
</script>