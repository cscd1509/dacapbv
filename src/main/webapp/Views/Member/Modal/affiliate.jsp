<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/tlds/functions.tld" prefix="f"%>
<div class="modal fade" tabindex="-1" role="dialog" id="modal-affiliate">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Giới Thiệu</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body"> 
                <div class="text-center">
                    <p>Bạn có thể giới thiệu người khác thông qua liên kết sau:</p>
                    <c:set var="req" value="${pageContext.request}" />
                    <c:url var="scheme" value="${req.scheme}"/>
                    <c:url var="serverName" value="${req.serverName}"/>
                    <c:set var="serverPortSpace" value="${req.serverPort==80||req.serverPort==443?'':':'}"/>
                    <c:set var="serverPort" value="${req.serverPort==80||req.serverPort==443?'':req.serverPort}"/>
                    <div class="affiliate-link">
                        <input type="text" class="form-control" id="affiliate-input" readonly value="${scheme}://${serverName}${serverPortSpace}${serverPort}/Register?referral=${MEMBER.userName}" />
                        <i class="far fa-clipboard btn-copy" data-target="#affiliate-input"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>