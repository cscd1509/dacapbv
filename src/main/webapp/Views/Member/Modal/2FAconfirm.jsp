<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/tlds/functions.tld" prefix="f"%>
<div class="modal fade" tabindex="-1" role="dialog" id="modal-2faconfirm">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="2faconfirm-form" role="form" novalidate>
                <div class="modal-header">
                    <h5 class="modal-title">2-step Verification</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">Please enter the verification code from your phone.</div>
                <div class="modal-body bg-gray">
                    <div class="row">
                        <div class="col col-auto">
                            <i class="ti-mobile"></i>
                        </div>
                        <div class="col">
                            <p>Enter the 2-step verification code provided by "Google&nbsp;Authenticator" to your smart phone</p>
                            <p><input type="text" class="form-control" id="2FA-code" name="code" /></p>
                            <small>More about 2-step authentication can be found in <a href="/Security" target="_blank" class="text-primary">security</a>.</small>
                        </div>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-end">
                    <a href="javascript:void(0)" class="btn btn-primary btn-lg btn-save" id="btn-verify">Verify</a>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $('#modal-2faconfirm').on('shown.bs.modal', function () {
        $('#2FA-code').trigger('focus');
    });
    $('#modal-2faconfirm').on('hidden.bs.modal', function () {
        $('#2FA-code').removeClass('error');
        $('#2FA-code-error').remove();
    });
    $("#2faconfirm-form").validate({
        rules: {
            code: {
                required: true,
                number: true
            }
        }
    });
</script>