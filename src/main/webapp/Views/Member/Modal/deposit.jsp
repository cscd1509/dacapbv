<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/tlds/functions.tld" prefix="f"%>
<div class="modal fade" tabindex="-1" role="dialog" id="modal-deposit">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Đăng ký đại lý</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <c:choose>
                    <c:when test="${MEMBER.rankCustomer!=0}">
                        <div class="text-center">
                            <p>
                                Bạn đang ở cấp bậc 
                                <c:choose>
                                    <c:when test="${MEMBER.rankCustomer==1}">
                                        <span class="badge badge-success">Đại lý</span>
                                    </c:when>
                                    <c:when test="${MEMBER.rankCustomer==2}">
                                        <span class="badge badge-warning">Đại lý khu vực</span>
                                    </c:when>
                                    <c:when test="${MEMBER.rankCustomer==3}">
                                        <span class="badge badge-info">Đại lý vùng</span>
                                    </c:when>
                                    <c:when test="${MEMBER.rankCustomer==4}">
                                        <span class="badge badge-danger">Tổng đại lý</span>
                                    </c:when>
                                    <c:otherwise>
                                        <span class="badge badge-dark">Thành viên mới</span>
                                    </c:otherwise>
                                </c:choose> trong hệ thống
                            </p>
                        </div>          
                    </c:when>
                    <c:otherwise>
                        <div class="text-center">
                            <p>Bạn chưa tham gia hệ thống<br/>Vui lòng sử dụng mã PIN đã được cấp để tham gia:</p>
                            <input type="text" class="form-control wallet-address" id="pinCode" />
                            <button class="btn btn-primary" id="btn-using-pin" style="border-radius: 20px;padding: 7px 25px;">Sử dụng</button>
                        </div>
                        <div>
                            <span class="text-danger"><b>Lưu ý:</b></span>
                            <ul>
                                <li>Bạn chỉ được sử dụng 1 mã PIN/ 1 tài khoản</li>
                                <li>Mệnh giá 1 mã PIN: ${f:customFormatDecimal('###,###,###', 12800000)} bv</li>
                            </ul>
                        </div>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </div>
    <script>
        $('#btn-using-pin').click(function () {
            var pinCode = $('#pinCode').val();
            if (!pinCode) {
                return;
            }
            $.ajax({
                url: '/Transaction/UsingPin',
                type: "POST",
                data: {'pinCode': pinCode},
                success: function (data) {
                    openAlert(data.value);
                }, error: function () {
                    openAlert(null, function () {
                        noty({text: 'Đã xảy ra lỗi. Vui lòng thử lại sau!', layout: 'bottomCenter', type: 'error'});
                    });
                }
            });
        });
    </script>
</div>