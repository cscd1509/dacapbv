<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/tlds/functions.tld" prefix="f"%>
<div class="modal fade" tabindex="-1" role="dialog" id="modal-compose">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form role="form" id="form-send-message" method="post" novalidate action="<c:url value="/Compose"/>">       
                <div class="modal-header">
                    <h5 class="modal-title">Đặt Câu Hỏi</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">      
                    <div class="form-group">
                        <label class="control-label">Loại Câu Hỏi</label>               
                        <select class="form-control" name="categoryID">
                            <option value="">-- Lựa chọn một --</option>
                            <c:forEach items="${f:getAllAvailableActiomSms()}" var="c">
                                <option value="${c.id}">${c.actionName}</option>
                            </c:forEach>                        
                        </select>
                    </div>                       
                    <div class="form-group">
                        <label class="control-label">Tiêu đề</label>
                        <input type="text" name="title" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Nội dung</label>
                        <textarea class="form-control" rows="5" name="content" maxlength="5000"></textarea>
                        <p class="form-text text-muted">Nội dung phải có ít nhất 10 ký tự và tối đa 5000 ký tự!</p>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-between">
                    <button type="reset" class="btn btn-danger"
                            onclick="$('#form-send-message .form-control.error').removeClass('error');$('#form-send-message label.error').remove()">Nhập lại</button>
                    <button type="button" class="btn btn-success" id="btn-send-message">Gửi</button>
                </div>
            </form>  
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $("#form-send-message").validate({
            rules: {
                title: {
                    required: true
                },
                categoryID: {
                    required: true
                },
                content: {
                    required: true,
                    minlength: 10,
                    maxlength: 5000
                }
            }
        });
        $('#btn-send-message').click(function () {
            if ($("#form-send-message").valid()) {
                var url = $('#form-send-message').attr('action');
                var data = $('#form-send-message').serializeObject();
                $.ajax({
                    beforeSend: function () {
                        $('#form-send-message .btn-primary').html('Seding');
                        $('#form-send-message .btn-primary').addClass('disabled');
                    },
                    url: url,
                    type: "POST",
                    data: data,
                    success: function (data) {
                        openAlert(data);
                        $('#form-send-message .btn-primary').html('Sedng');
                        $('#form-send-message .btn-primary').removeClass('disabled');
                    },
                    error: function () {
                        openAlert(null, function () {
                            noty({text: 'Đã xảy ra lỗi. Vui lòng thử lại sau!', layout: 'bottomCenter', type: 'error'});
                        });
                        $('#form-send-message .btn-primary').html('Send');
                        $('#form-send-message .btn-primary').removeClass('disabled');
                    }
                });
            }
        });
    });
</script>