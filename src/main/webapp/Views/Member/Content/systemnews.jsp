<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="f" uri="/WEB-INF/tlds/functions.tld" %>
<link href="<c:url value="/Resources/layout/member/css/system.css"/>" rel="stylesheet" type="text/css"/>
<h1>Tin nội bộ</h1>
<div class="history-trading">

    <form action="/SystemNews" method="GET" novalidate>
        <div class="form-group">
            <div class="input-group">
                <input type="text" class="form-control" value="${PAGER.keyword}" name="keyword" id="keyword" placeholder="Từ khóa">
                <div class="input-group-append">
                    <button class="btn btn-dark" type="button" id="button-addon2">Tìm kiếm</button>
                </div>
            </div>
        </div>
    </form>
    <div class="group text-right">
        Tổng bài đăng: <b class="text-danger">${PAGER.totalResult} bài</b>
    </div>
    <c:forEach items="${RESULTS}" var="item">
        <div class="item">
            <div class="bottom text-uppercase">
                <b class="text-info text-uppercase"><a href="/SystemNews/${item.id}">${item.title}</a></b>
            </div>
            <div class="top text-primary mt-1 mb-1">
                <i class="far fa-calendar-alt"></i> Ngày đăng: ${f:customFormatDate("dd/MM/yyyy", item.createdDate)}
            </div>
            <div class="bottom">
                ${item.shortDescription}
            </div>
        </div>
        <hr/>
    </c:forEach>
    <br/>
    <c:if test="${PAGER.totalPage>1}">
        <div class="d-flex justify-content-end">
            <ul class="pagination pagination-sm">
                <li class="${PAGER.currentPage<=1?"d-none":""}">
                    <a href="<c:url value="/Referral?currentPage=1&keyword=${PAGER.keyword}"/>"><i class="fa fa-angle-double-left"></i></a>
                </li>
                <li class="${PAGER.currentPage<=1?"d-none":""}">
                    <a href="<c:url value="/Referral?currentPage=${PAGER.currentPage-1}&keyword=${PAGER.keyword}"/>"><i class="fa fa-angle-left"></i></a>
                </li>
                <c:if test="${PAGER.currentPage==2}">
                    <li>
                        <a href="<c:url value="/Referral?currentPage=1&keyword=${PAGER.keyword}"/>">1</a>
                    </li>
                </c:if>
                <c:if test="${PAGER.currentPage>2}">
                    <c:forEach begin="${PAGER.currentPage-2}" end="${PAGER.currentPage-1}" var="page">
                        <li>
                            <a href="<c:url value="/Referral?currentPage=${page}&keyword=${PAGER.keyword}"/>">${page}</a>
                        </li>
                    </c:forEach>
                </c:if>
                <li>
                    <a class="active" href="javascript:;">${PAGER.currentPage}</a>
                </li>                
                <c:if test="${PAGER.currentPage==PAGER.totalPage-1}">
                    <li>
                        <a href="<c:url value="/Referral?currentPage=${PAGER.totalPage}&keyword=${PAGER.keyword}"/>">${PAGER.totalPage}</a>
                    </li>
                </c:if>
                <c:if test="${PAGER.currentPage<PAGER.totalPage-1}">
                    <c:forEach begin="${PAGER.currentPage+1}" end="${PAGER.currentPage+2}" var="page">
                        <li>
                            <a href="<c:url value="/Referral?currentPage=${page}&keyword=${PAGER.keyword}"/>">${page}</a>
                        </li>
                    </c:forEach> 
                </c:if>              
                <li class="${PAGER.currentPage==PAGER.totalPage?"d-none":""}">
                    <a href="<c:url value="/Referral?currentPage=${PAGER.currentPage+1}&keyword=${PAGER.keyword}"/>"><i class="fa fa-angle-right"></i></a>
                </li>
                <li class="${PAGER.currentPage==PAGER.totalPage?"d-none":""}">
                    <a href="<c:url value="/Referral?currentPage=${PAGER.totalPage}&keyword=${PAGER.keyword}"/>"><i class="fa fa-angle-double-right"></i></a>
                </li>
            </ul>
        </div>
    </c:if>
</div>