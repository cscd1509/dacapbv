<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/tlds/functions.tld" prefix="f"%>
<link href="<c:url value="/Resources/layout/member/css/trading.css?v=${RESOURCES_VERSION}"/>" rel="stylesheet" type="text/css"/>
<link href="<c:url value="/Resources/layout/member/css/star.css"/>" rel="stylesheet" type="text/css"/>
<div id="preloader">
    <div id="spinner"></div>
</div>
<div id="trade-action">
    <div class="row no-gutters">
        <div class="trade-info col d-flex align-items-center text-success">
            <small><i class="ti-user"></i>&nbsp;Đã giao dịch</small>
            <span class="coin-status d-flex align-items-center"><b>${BASIC_INFOMATION[0]['ValuesName']} bv</b></span>
        </div>
        <div class="w-100 d-lg-none"></div>
        <div class="trade-info col d-flex align-items-center text-warning">
            <small><i class="ti-user"></i>&nbsp;BV năm</small>
            <span class="d-flex align-items-end"><b>${BASIC_INFOMATION[1]['ValuesName']} bv</b></span>
        </div>
        <div class="w-100 d-lg-none"></div>
        <div class="trade-info col d-flex align-items-center text-info">
            <small><i class="ti-user"></i>&nbsp;BV tháng</small>
            <span class="d-flex align-items-end"><b>${BASIC_INFOMATION[2]['ValuesName']} bv</b></span>
        </div>
        <div class="w-100 d-lg-none"></div>
        <div class="trade-info col d-flex align-items-center text-danger">
            <small><i class="ti-user"></i>&nbsp;Cấp bậc</small>
            <span class="d-flex align-items-end"><b>${BASIC_INFOMATION[3]['ValuesName']}</b></span>
        </div>
    </div>
</div>
<div id="star-wrapper">
    <div id='stars'></div>
    <div id='stars2'></div>
    <div id='stars3'></div>
</div>
<div class="main-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3">
                <div class="widget widget-default widget-item-icon">
                    <div class="widget-item-left">
                        <span class="fas fa-sitemap"></span>
                    </div>
                    <div class="widget-data">
                        <div class="widget-int num-count">${BASIC_INFOMATION[8]['ValuesName']}</div>
                        <div class="widget-title"></div>
                        <div class="widget-subtitle">${BASIC_INFOMATION[8]['Name']}</div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="widget widget-default widget-item-icon">
                    <div class="widget-item-left">
                        <span class="fas fa-sitemap"></span>
                    </div>
                    <div class="widget-data">
                        <div class="widget-int num-count">${BASIC_INFOMATION[9]['ValuesName']}</div>
                        <div class="widget-title"></div>
                        <div class="widget-subtitle">${BASIC_INFOMATION[9]['Name']}</div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="widget widget-default widget-item-icon">
                    <div class="widget-item-left">
                        <span class="fas fa-users"></span>
                    </div>
                    <div class="widget-data">
                        <div class="widget-int num-count">${BASIC_INFOMATION[4]['ValuesName']}</div>
                        <div class="widget-title"></div>
                        <div class="widget-subtitle">${BASIC_INFOMATION[4]['Name']}</div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="widget widget-default widget-item-icon">
                    <div class="widget-item-left">
                        <span class="fas fa-users"></span>
                    </div>
                    <div class="widget-data">
                        <div class="widget-int num-count">${BASIC_INFOMATION[5]['ValuesName']}</div>
                        <div class="widget-title"></div>
                        <div class="widget-subtitle">${BASIC_INFOMATION[5]['Name']}</div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="widget widget-default widget-item-icon">
                    <div class="widget-item-left">
                        <span class="fas fa-link"></span>
                    </div>
                    <div class="widget-data">
                        <div class="widget-int num-count text-danger">${BASIC_INFOMATION[6]['ValuesName']}</div>
                        <div class="widget-title"></div>
                        <div class="widget-subtitle">${BASIC_INFOMATION[6]['Name']}</div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="widget widget-default widget-item-icon">
                    <div class="widget-item-left">
                        <span class="fas fa-link"></span>
                    </div>
                    <div class="widget-data">
                        <div class="widget-int num-count text-danger">${BASIC_INFOMATION[11]['ValuesName']}</div>
                        <div class="widget-title"></div>
                        <div class="widget-subtitle">${BASIC_INFOMATION[11]['Name']}</div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="widget widget-default widget-item-icon">
                    <div class="widget-item-left">
                        <span class="fas fa-link"></span>
                    </div>
                    <div class="widget-data">
                        <div class="widget-int num-count text-danger">${BASIC_INFOMATION[7]['ValuesName']}</div>
                        <div class="widget-title"></div>
                        <div class="widget-subtitle">${BASIC_INFOMATION[7]['Name']}</div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="widget widget-default widget-item-icon">
                    <div class="widget-item-left">
                        <span class="fas fa-calendar-alt"></span>
                    </div>
                    <div class="widget-data">
                        <div class="widget-int num-count">${BASIC_INFOMATION[12]['ValuesName']}</div>
                        <div class="widget-title"></div>
                        <div class="widget-subtitle">${BASIC_INFOMATION[12]['Name']}</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-sm-6 col-12">
                <div class="panel panel-default">
                    <div class="panel-heading ui-draggable-handle">
                        <div class="panel-title-box">
                            <h3>Lịch sử nhận tiền</h3>
                            <span>Danh sách 10 lần nhận tiền gần nhất</span>
                        </div>
                    </div>
                    <div class="panel-body panel-body-table">
                        <c:import url="/Views/Member/Widget/awardchartbymonth.jsp" />
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-12">
                <div class="panel panel-default">
                    <div class="panel-heading ui-draggable-handle">
                        <div class="panel-title-box">
                            <h3>Phân loại hoa hồng</h3>
                            <span>Biểu đồ phân loại hoa hồng</span>
                        </div>
                    </div>
                    <div class="panel-body padding-0">
                        <c:import url="/Views/Member/Widget/awardchartall.jsp" />
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-12">
                <!-- START PROJECTS BLOCK -->
                <div class="panel panel-default">
                    <div class="panel-heading ui-draggable-handle">
                        <div class="panel-title-box">
                            <h3>Nhánh dưới</h3>
                            <span>Top 5 thành viên cấp bậc cao nhất</span>
                        </div>
                    </div>
                    <div class="panel-body panel-body-table" style="height: 265px">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th width="40%">User</th>
                                        <th width="30%" class="text-center">Ngày đăng ký</th>
                                        <th width="30%" class="text-center">Cấp bậc</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${TOP5_CHILDREN}" var="child">
                                        <tr>
                                            <td><strong class="text-uppercase">${child.userName}</strong></td>
                                            <td class="text-center">${f:customFormatDate("dd/MM/yyyy", child.createdDate)}</td>
                                            <td class="text-center">                                                
                                                <span class="badge badge-info" style="color:${child.rankCustomer.color}">${child.rankCustomer.name}</span>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- END PROJECTS BLOCK -->
            </div>
            <div class="col-md-3 col-sm-6 col-12">
                <!-- START PROJECTS BLOCK -->
                <div class="panel panel-default">
                    <div class="panel-heading ui-draggable-handle">
                        <div class="panel-title-box">
                            <h3>Thăng hạng</h3>
                            <span>Lịch sử thăng hạng</span>
                        </div>
                    </div>
                    <div class="panel-body panel-body-table" style="height: 265px">
                        <c:choose>
                            <c:when test="${f:size(HISTORY_CHANGE_RANK)==0}">
                                <div class="mt-3 mb-3 pl-3 pr-3 text-center"><i class="text-danger">Bạn chưa thăng hặng</i></div>
                            </c:when>
                            <c:otherwise>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th class="text-left">Hạng</th>
                                                <th style="width: 1%;white-space: nowrap" class="text-right">Ngày thăng hạng</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach items="${HISTORY_CHANGE_RANK}" var="item">
                                                <tr style="color:${item.rankID.color}">
                                                    <td><strong class="text-uppercase">${item.rankID.name}</strong></td>
                                                    <td class="text-right">${f:customFormatDate("dd/MM/yyyy", item.createdDate)}</td>
                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                </div>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
                <!-- END PROJECTS BLOCK -->
            </div>
        </div>
    </div>
</div>