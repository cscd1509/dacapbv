<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="f" uri="/WEB-INF/tlds/functions.tld" %>
<link href="<c:url value="/Resources/layout/member/css/system.css"/>" rel="stylesheet" type="text/css"/>
<h1>Danh sách nhánh dưới</h1>
<div class="history-trading">
    <div class="row">
        <div class="col-12 col-sm-4">
            <div class="group text-center text-uppercase">
                Bộ lọc
            </div><br/>
            <form action="/Referral" method="GET" novalidate>
                <div class="form-group">
                    <label for="keyword">Từ khóa</label>
                    <input type="text" class="form-control" value="${PAGER.keyword}" name="keyword" id="keyword" placeholder="Họ tên hoặc tên đăng nhập">
                </div>
                <div class="form-group">
                    <label for="isCheck">Level tuyến dưới</label>
                    <input type="number" min="1" step="1" value="${PAGER.isCheck}" class="form-control" name="level" id="isCheck" placeholder="Đời F mấy. VD: F1, F2, F3...">
                </div>
                <div class="text-right">
                    <button type="submit" class="btn btn-danger text-uppercase">&emsp;Tìm&emsp;</button>
                </div>
            </form>
        </div>
        <div class="col-12 col-sm-8">
            <div class="group text-right">
                Tổng thành viên: <b class="text-danger">${PAGER.totalResult} Thành Viên</b>
            </div>
            <c:forEach items="${RESULTS}" var="item">
                <div class="item">
                    <div class="row no-gutters">
                        <div class="col-3">
                            <div class="top">
                                ${f:customFormatDate("dd/MM/yyyy",item.createdDate)}
                            </div>
                            <div class="bottom">                        
                                ${f:customFormatDate("HH:mm:ss",item.createdDate)}
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="top">
                                <span class="text-info text-uppercase">Nhánh F${item.level-MEMBER.level}</span>
                            </div>
                            <div class="bottom text-uppercase">
                                Người dùng: <b>${item.userName}</b>
                            </div>
                        </div>
                        <div class="col-5 text-right">
                            <div class="top">&nbsp;</div>
                            <div class="bottom text-uppercase">
                                <c:choose>
                                    <c:when test="${child.rankCustomer==1}">
                                        <span class="badge badge-success">Đại lý</span>
                                    </c:when>
                                    <c:when test="${child.rankCustomer==2}">
                                        <span class="badge badge-warning">Đại lý khu vực</span>
                                    </c:when>
                                    <c:when test="${child.rankCustomer==3}">
                                        <span class="badge badge-info">Đại lý vùng</span>
                                    </c:when>
                                    <c:when test="${child.rankCustomer==4}">
                                        <span class="badge badge-danger">Tổng đại lý</span>
                                    </c:when>
                                    <c:otherwise>
                                        <span class="badge badge-dark">Thành viên mới</span>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                    </div>
                </div>
            </c:forEach>
            <br/>
            <c:if test="${PAGER.totalResult>0}">
                <div class="d-flex justify-content-end">
                    <ul class="pagination pagination-sm">
                        <li class="${PAGER.currentPage<=1?"d-none":""}">
                            <a href="<c:url value="/Referral?currentPage=1&isCheck=${PAGER.isCheck}&keyword=${PAGER.keyword}"/>"><i class="fa fa-angle-double-left"></i></a>
                        </li>
                        <li class="${PAGER.currentPage<=1?"d-none":""}">
                            <a href="<c:url value="/Referral?currentPage=${PAGER.currentPage-1}&isCheck=${PAGER.isCheck}&keyword=${PAGER.keyword}"/>"><i class="fa fa-angle-left"></i></a>
                        </li>
                        <c:if test="${PAGER.currentPage==2}">
                            <li>
                                <a href="<c:url value="/Referral?currentPage=1&isCheck=${PAGER.isCheck}&keyword=${PAGER.keyword}"/>">1</a>
                            </li>
                        </c:if>
                        <c:if test="${PAGER.currentPage>2}">
                            <c:forEach begin="${PAGER.currentPage-2}" end="${PAGER.currentPage-1}" var="page">
                                <li>
                                    <a href="<c:url value="/Referral?currentPage=${page}&isCheck=${PAGER.isCheck}&keyword=${PAGER.keyword}"/>">${page}</a>
                                </li>
                            </c:forEach>
                        </c:if>
                        <li>
                            <a class="active" href="javascript:;">${PAGER.currentPage}</a>
                        </li>                
                        <c:if test="${PAGER.currentPage==PAGER.totalPage-1}">
                            <li>
                                <a href="<c:url value="/Referral?currentPage=${PAGER.totalPage}&isCheck=${PAGER.isCheck}&keyword=${PAGER.keyword}"/>">${PAGER.totalPage}</a>
                            </li>
                        </c:if>
                        <c:if test="${PAGER.currentPage<PAGER.totalPage-1}">
                            <c:forEach begin="${PAGER.currentPage+1}" end="${PAGER.currentPage+2}" var="page">
                                <li>
                                    <a href="<c:url value="/Referral?currentPage=${page}&isCheck=${PAGER.isCheck}&keyword=${PAGER.keyword}"/>">${page}</a>
                                </li>
                            </c:forEach> 
                        </c:if>              
                        <li class="${PAGER.currentPage==PAGER.totalPage?"d-none":""}">
                            <a href="<c:url value="/Referral?currentPage=${PAGER.currentPage+1}&isCheck=${PAGER.isCheck}&keyword=${PAGER.keyword}"/>"><i class="fa fa-angle-right"></i></a>
                        </li>
                        <li class="${PAGER.currentPage==PAGER.totalPage?"d-none":""}">
                            <a href="<c:url value="/Referral?currentPage=${PAGER.totalPage}&isCheck=${PAGER.isCheck}&keyword=${PAGER.keyword}"/>"><i class="fa fa-angle-double-right"></i></a>
                        </li>
                    </ul>
                </div>
            </c:if>
        </div>
    </div>
</div>