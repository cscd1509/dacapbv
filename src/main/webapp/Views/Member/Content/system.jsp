<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="f" uri="/WEB-INF/tlds/functions.tld" %>
<link href="<c:url value="/Resources/layout/member/css/system.css"/>" rel="stylesheet" type="text/css"/>
<h1>Cây hệ thống</h1>
<div id="distributor-view-tree">
    <div id="treeDiagram">
        ${TREE_CUSTOMER}
    </div>
</div>
<script>
    $(function () {
        $('[data-toggle="popover"]').popover({
            container: 'body',
            html: true,
            trigger: "hover"
        });
    });
    $(document).on('click touchstart', '#treeDiagram span[class|="ti"]', function () {
        $(this).toggleClass('ti-plus').toggleClass('ti-minus');
        $(this).parent().next('ul').toggle(0);
        var target = $(this).parents('.next-item');
        $(target).removeClass('next-item');
        var controller = $(this).attr('controller');
        sendAjax(controller, 'GET', null, function (data) {
            $(target).append(data);
            $('[data-toggle="popover"]').popover({
                container: 'body',
                html: true,
                trigger: "hover"
            });
        });
    });
    function sendAjax(url, type, data, handle) {
        $.ajax({
            url: url,
            type: type,
            data: data,
            success: function (data) {
                if (typeof handle !== "undefined") {
                    handle(data);
                }
            }
        });
    }
</script>