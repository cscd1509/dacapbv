<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/tlds/functions.tld" prefix="f"%>
<link rel="stylesheet" type="text/css" href="<c:url value="/Resources/layout/member/css/profile.css"/>"/>
<h1>Thông tin cá nhân</h1>
<div class="row">
    <div class="col-lg-4 col-md-6 col-sm-8 col-xs-10 col-12">
        <div class="user-info text-center">
            <div class="avatar">
                <img src="/Resources/shared/img/avatar-default.png" alt=""/>
            </div>
            <b>Tên đăng nhập: <span class="text-uppercase">${MEMBER.userName}</span></b>
        </div>
        <form method="POST" id="basic-info-form" role="form" novalidate action="<c:url value="/UpdateProfile"/>">
            <div class="form-edit">
                <div class="form-group">
                    <label class="control-label">Họ và tên</label> 
                    <input type="text" class="form-control" name="fullName" value="${MEMBER.fullName}" required>                                                    
                </div>
                <div class="form-group">
                    <label class="control-label">Địa chỉ email</label>                                                                                                                                                    
                    <input type="text" class="form-control" name="email" value="${MEMBER.email}" required>                                                    
                </div>
                <div class="form-group">
                    <label class="control-label">Số điện thoại</label>   
                    <input type="text" class="form-control" name="mobile" value="${MEMBER.mobile}" required>
                    <div class="form-text text-muted">Must include phone country code ext. E.x: +60123456789</div>
                </div>
                <div class="form-group">
                    <label class="control-label">Số CMND/Thẻ căn cước/Hộ chiếu</label>   
                    <input type="text" class="form-control" name="peoplesIdentity" value="${MEMBER.peoplesIdentity}" required>
                </div>
                <div class="form-group">  
                    <label class="control-label">Giới tính</label>   
                    <select class="form-control" name="gender" required>
                        <option value="">Lựa chọn giới tính</option>
                        <option value="1"${MEMBER.gender?" selected":""}>Nam</option>
                        <option value="0"${!MEMBER.gender?" selected":""}>Nữ</option>
                    </select> 
                </div>
                <div class="form-group">
                    <label class="control-label">Tên ngân hàng</label>
                    <input type="text" required name="bankName" value="${MEMBER.bankName}" class="form-control" placeholder="">
                </div>
                <div class="form-group">
                    <label class="control-label">Số TK</label>
                    <input type="text" required name="bankNumber" value="${MEMBER.bankNumber}" class="form-control" placeholder="">
                </div>
                <div class="form-group">
                    <label class="control-label">Chi nhánh</label>
                    <input type="text" required name="bankAgency" value="${MEMBER.bankAgency}" class="form-control" placeholder="">
                </div>
            </div>
            <div class="d-flex justify-content-between">
                <button type="reset" class="btn btn-danger btn-cancel">Nhập lại <span class="fa fa-times"></span></button>
                <button type="submit" class="btn btn-success btn-save">Cập nhật <span class="far fa-save"></span></button>
            </div>
        </form>
    </div>
</div>
<script>
    $("#basic-info-form").validate({
        submitHandler: function () {
            var url = $('#basic-info-form').attr('action');
            var data = $('#basic-info-form').serializeObject();
            var needVerify = $('#basic-info-form').attr('data-needverify');
            if (needVerify === 'true') {
                $('#modal-2faconfirm').modal('show');
                $('#btn-verify').off('click').click(function () {
                    if ($("#2faconfirm-form").validate().valid()) {
                        var code = $('#2FA-code').val();
                        data['code'] = code;
                        updateProfile(url, data);
                    }
                });
            } else {
                updateProfile(url, data);
            }
        }
    });
    function updateProfile(url, formData) {
        $.ajax({
            url: url,
            type: "POST",
            data: formData,
            success: function (data) {
                if (data.key === 1 || data.key === 0) {
                    $('#modal-2faconfirm').modal('hide');
                    openAlert(data.value);
                } else if (data.key === -1) {
                    openAlert(data.value);
                }
            }, error: function () {
                openAlert(null, function () {
                    noty({text: 'Đã xảy ra lỗi. Vui lòng thử lại sau!', layout: 'bottomCenter', type: 'error'});
                });
            }
        });
    }
</script>