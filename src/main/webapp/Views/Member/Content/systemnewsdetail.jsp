<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="f" uri="/WEB-INF/tlds/functions.tld" %>
<link href="<c:url value="/Resources/layout/member/css/system.css"/>" rel="stylesheet" type="text/css"/>
<h1>${SELECTED_NEWS.title}</h1>
<div class="history-trading">
    <div class="item mb-3">
        <div class="bottom">
            <b class="text-info"><i class="far fa-calendar-alt"></i> Ngày đăng: ${f:customFormatDate("dd/MM/yyyy", SELECTED_NEWS.createdDate)}</b>
        </div>
    </div>
    <div class="item mb-3">
        <div class="bottom text-wrap">
            ${SELECTED_NEWS.content}
        </div>
    </div>
    <div class="item">
        <div class="bottom text-right">
            <a href="/SystemNews"><i class="fas fa-arrow-left"></i> Quay lại</a>
        </div>
    </div>
</div>