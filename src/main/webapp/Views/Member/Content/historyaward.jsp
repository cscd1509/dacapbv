<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="/WEB-INF/tlds/functions.tld" %>
<link href="/Resources/layout/home/css/daterangepicker.css" rel="stylesheet" type="text/css"/>
<script src="/Resources/layout/home/js/moment.min.js" type="text/javascript"></script>
<script src="/Resources/layout/home/js/daterangepicker.js" type="text/javascript"></script>
<h1>Thống kê thu nhập</h1>
<div class="row">
    <div class="col-sm-6 col-12">
        <c:import url="/Views/Member/Widget/awardchartall.jsp" />
        <hr/>
        <h1>Thống kê theo tháng</h1>
        <div class="chart-holder" id="dashboard-bar-1"></div>
        <script>
            Morris.Bar({
                // ID of the element in which to draw the chart.
                element: 'dashboard-bar-1',
                // Chart data records -- each entry in this array corresponds to a point on
                // the chart.
                data: ${f:getJSON(AWARD_CHART_BAR)},
                // The name of the data record attribute that contains x-values.
                xkey: 'month',
                ykeys: ['value'],
                labels: ['Thưởng'],
                barRatio: 0.4,
                xLabelAngle: 35,
                hideHover: 'auto'
            });
        </script>

        <%--<br/>
        <c:import url="/Views/Member/Widget/awardchartbymonth.jsp" />--%>
    </div>
    <div class="col-sm-6 col-12">
        <div class="history-trading">
            <div class="group d-flex justify-content-between align-items-center">
                <span class="text-uppercase">Lịch sử nhận thưởng</span>
                <button class="btn btn-xs btn-dark" data-toggle="collapse" data-target="#award-filter">
                    Bộ lọc <span class="fas fa-sort-numeric-up"></span>
                </button>
            </div>
            <div class="collapse" id="award-filter">
                <hr class="mb-0 mt-0" style="border-top: 1px solid rgba(0,0,0,.7)"/>
                <div class="group">
                    <form class="form-inline" action="/HistoryAward" method="GET">
                        <div class="form-group mr-2 mb-1 mb-sm-0">
                            <label for="inputPassword2" class="sr-only">Kiểu thưởng</label>
                            <select class="form-control" name="checkAwardID">
                                <option ${PAGER.isCheck==null?'selected':''} value="">Tất cả</option>
                                <c:forEach items="${f:getAvailableCheckAward()}" var="item">
                                    <option ${item.id==PAGER.isCheck?'selected':''} value="${item.id}">${item.name}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="form-group mr-2 mb-1 mb-sm-0">
                            <label for="inputPassword2" class="sr-only">Khoảng thời gian</label>
                            <div class="input-group" id="filterRange">
                                <input type="text" class="form-control" readonly="" aria-label="Amount (to the nearest dollar)">
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="ti-calendar"></i>
                                    </span>
                                </div>
                            </div>
                            <input type="hidden" id="startDate" name="startDate" value="${param['startDate']}" />
                            <input type="hidden" id="endDate" name="endDate" value="${param['endDate']}" />
                            <script type="text/javascript">
                                var startDate = $('#startDate').val();
                                var endDate = $('#endDate').val();
                                $(function () {
                                    var datePickerOptions = {
                                        autoUpdateInput: false,
                                        maxDate: moment(),
                                        locale: {
                                            cancelLabel: 'Xóa',
                                            "format": "MM/DD/YYYY",
                                            "separator": " - ",
                                            "applyLabel": "Áp Dụng",
                                            "fromLabel": "Từ",
                                            "toLabel": "Đến",
                                            "customRangeLabel": "Tùy Chỉnh",
                                            "daysOfWeek": [
                                                "CN",
                                                "Th2",
                                                "Th3",
                                                "Th4",
                                                "Th5",
                                                "Th6",
                                                "Th7"
                                            ],
                                            "monthNames": [
                                                "Tháng 1",
                                                "Tháng 2",
                                                "Tháng 3",
                                                "Tháng 4",
                                                "Tháng 5",
                                                "Tháng 6",
                                                "Tháng 7",
                                                "Tháng 8",
                                                "Tháng 9",
                                                "Tháng 10",
                                                "Tháng 11",
                                                "Tháng 12"
                                            ],
                                            "firstDay": 1
                                        },
                                        ranges: {
                                            'Hôm Nay': [moment(), moment()],
                                            'Từ Hôm Qua': [moment().subtract(1, 'days'), moment()],
                                            '7 Ngày Qua': [moment().subtract(6, 'days'), moment()],
                                            '30 Ngày Qua': [moment().subtract(29, 'days'), moment()],
                                            'Tháng Này': [moment().startOf('month'), moment().endOf('month')],
                                            'Tháng Trước': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                                        }
                                    };
                                    if (startDate !== '' && endDate !== '') {
                                        datePickerOptions['startDate'] = moment(startDate, 'DD/MM/YYYY').toDate();
                                        datePickerOptions['endDate'] = moment(endDate, 'DD/MM/YYYY').toDate();
                                        $('#filterRange input').val(startDate + '-' + endDate);
                                    }
                                    $('#filterRange input').daterangepicker(datePickerOptions);
                                    $('#filterRange input').on('apply.daterangepicker', function (ev, picker) {
                                        $(this).val(picker.startDate.format('DD/MM/YYYY') + '-' + picker.endDate.format('DD/MM/YYYY'));
                                        $('#startDate').val(picker.startDate.format('DD/MM/YYYY'));
                                        $('#endDate').val(picker.endDate.format('DD/MM/YYYY'));
                                    });
                                    $('#filterRange input').on('cancel.daterangepicker', function (ev, picker) {
                                        $(this).val('');
                                        $('#startDate').val('');
                                        $('#endDate').val('');
                                    });
                                    $('.daterangepicker').click(function (e) {
                                        e.stopPropagation();
                                    });
                                });
//                                $('.btn-filter').click(function () {
//                                    var url = '/HistoryAward?';
//                                    var filterType = $('#filterType').val();
//                                    var startDate = $('#startDate').val();
//                                    var endDate = $('#endDate').val();
//                                    if (filterType !== '') {
//                                        url += 'checkAwardID=' + filterType + '&';
//                                    }
//                                    if (startDate !== '') {
//                                        url += 'startDate=' + startDate + '&';
//                                    }
//                                    if (endDate !== '') {
//                                        url += 'endDate=' + endDate + '&';
//                                    }
//                                    window.location.href = url;
//                                });
                            </script>
                        </div>
                        <div class="form-group mr-2 mb-1 mb-sm-0">
                            <label for="isAvailability" class="sr-only">Khả dụng</label>
                            <select class="form-control" name="isAvailability"> 
                                <option ${PAGER.isAvailability==-1?'selected':''} value="-1">Khả dụng và không khả dụng</option>
                                <option ${PAGER.isAvailability==1?'selected':''} value="1">Khả dụng</option>
                                <option ${PAGER.isAvailability==0?'selected':''} value="0">Không khả dụng</option>
                            </select>
                        </div> 
                        <button type="submit" class="btn btn-success" style="padding: 10px 20px;line-height: normal;height: auto;">Lọc</button>
                    </form>
                </div>
            </div>
            <hr class="mb-0 mt-0" style="border-top: 1px solid rgba(0,0,0,.7)"/>
            <div class="group text-right">
                Tổng tiền: <b class="text-danger">${f:customFormatDecimal("###,###,###", PAGER.amount)} bv</b>
            </div>
            <c:if test="${f:size(RESULTS)==0}">
                <p class="mt-2 text-danger"><i>Không tìm thấy dữ liệu nào</i></p>
            </c:if>
            <c:forEach items="${RESULTS}" var="award">
                <div class="item">
                    <div class="row no-gutters">
                        <div class="col-3">
                            <div class="top">
                                ${f:customFormatDate("dd/MM/yyyy",award.createdDate)}
                            </div>
                            <div class="bottom">                        
                                ${f:customFormatDate("HH:mm:ss",award.createdDate)}
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="top">
                                <span class="text-info text-uppercase">${award.checkAwardID.name}</span>
                            </div>
                            <div class="bottom text-uppercase">
                                Nhận từ: <b>${award.partnerID.userName}</b>
                            </div>
                        </div>
                        <div class="col-5 text-right">
                            <div class="top text-uppercase">
                                <c:choose>
                                    <c:when test="${award.isAvailability}">
                                        <b class="text-success">Khả dụng</b>
                                    </c:when>
                                    <c:otherwise>
                                        <b class="text-danger">Không khả dụng</b>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                            <div class="bottom text-uppercase">
                                <span class="text-success">${f:customFormatDecimal("###,###,###", award.amount)} <small>bv</small></span>
                            </div>
                        </div>
                    </div>
                </div>
                <hr/>
            </c:forEach>
            <br/>
            <c:if test="${PAGER.totalResult>0}">
                <div class="d-flex justify-content-end">
                    <ul class="pagination pagination-sm">
                        <li class="${PAGER.currentPage<=1?"d-none":""}">
                            <a href="<c:url value="/HistoryAward?currentPage=1&isCheck=${PAGER.isCheck}&startDate=${param['startDate']}&endDate=${param['endDate']}"/>"><i class="fa fa-angle-double-left"></i></a>
                        </li>
                        <li class="${PAGER.currentPage<=1?"d-none":""}">
                            <a href="<c:url value="/HistoryAward?currentPage=${PAGER.currentPage-1}&isCheck=${PAGER.isCheck}&startDate=${param['startDate']}&endDate=${param['endDate']}"/>"><i class="fa fa-angle-left"></i></a>
                        </li>
                        <c:if test="${PAGER.currentPage==2}">
                            <li>
                                <a href="<c:url value="/HistoryAward?currentPage=1&isCheck=${PAGER.isCheck}&startDate=${param['startDate']}&endDate=${param['endDate']}"/>">1</a>
                            </li>
                        </c:if>
                        <c:if test="${PAGER.currentPage>2}">
                            <c:forEach begin="${PAGER.currentPage-2}" end="${PAGER.currentPage-1}" var="page">
                                <li>
                                    <a href="<c:url value="/HistoryAward?currentPage=${page}&isCheck=${PAGER.isCheck}&startDate=${param['startDate']}&endDate=${param['endDate']}"/>">${page}</a>
                                </li>
                            </c:forEach>
                        </c:if>
                        <li>
                            <a class="active" href="javascript:;">${PAGER.currentPage}</a>
                        </li>                
                        <c:if test="${PAGER.currentPage==PAGER.totalPage-1}">
                            <li>
                                <a href="<c:url value="/HistoryAward?currentPage=${PAGER.totalPage}&isCheck=${PAGER.isCheck}&startDate=${param['startDate']}&endDate=${param['endDate']}"/>">${PAGER.totalPage}</a>
                            </li>
                        </c:if>
                        <c:if test="${PAGER.currentPage<PAGER.totalPage-1}">
                            <c:forEach begin="${PAGER.currentPage+1}" end="${PAGER.currentPage+2}" var="page">
                                <li>
                                    <a href="<c:url value="/HistoryAward?currentPage=${page}&isCheck=${PAGER.isCheck}&startDate=${param['startDate']}&endDate=${param['endDate']}"/>">${page}</a>
                                </li>
                            </c:forEach> 
                        </c:if>              
                        <li class="${PAGER.currentPage==PAGER.totalPage?"d-none":""}">
                            <a href="<c:url value="/HistoryAward?currentPage=${PAGER.currentPage+1}&isCheck=${PAGER.isCheck}&startDate=${param['startDate']}&endDate=${param['endDate']}"/>"><i class="fa fa-angle-right"></i></a>
                        </li>
                        <li class="${PAGER.currentPage==PAGER.totalPage?"d-none":""}">
                            <a href="<c:url value="/HistoryAward?currentPage=${PAGER.totalPage}&isCheck=${PAGER.isCheck}&startDate=${param['startDate']}&endDate=${param['endDate']}"/>"><i class="fa fa-angle-double-right"></i></a>
                        </li>
                    </ul>
                </div>
            </c:if>
        </div>
    </div>
</div>