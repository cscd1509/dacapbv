<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/tlds/functions.tld" prefix="f"%>
<c:if test="${sessionScope['MEMBER_DATA']==null}">
    <c:redirect url="/Home" />
</c:if>
<c:set value="${f:getIntegerRandom()}" var="RESOURCES_VERSION" scope="request" />
<c:set var="MEMBER" value="${sessionScope['MEMBER_DATA']['MEMBER']}" scope="request"/>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" class="no-js body-full-height">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="/Resources/shared/img/favicon.png" rel="shortcut icon" type="image/x-icon" />
        <meta http-equiv="content-language" content="en,us,uk,ml,sg,cn,vi" />
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
        <meta name="description" content="" />
        <meta name="keywords" content="BV,BV,Bitcoin,BTC,PH,GH,PHGH,PH/GH,PH-GH,PH GH" />
        <meta name="robots" content="" />
        <meta name='revisit-after' content='' />
        <meta property="og:image" content="" />
        <meta property="og:title" content="" />
        <meta property="og:description" content="" />
        <meta property="og:site_name" content="" />
        <meta property="og:url" content="" />
        <meta property="og:type" content="article" />
        <title>BV System</title>
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet"/>
        <link href="<c:url value="/Resources/shared/css/bootstrap.min.css"/>" rel="stylesheet"/>
        <link href="<c:url value="/Resources/shared/css/fontawesome-all.min.css"/>" rel="stylesheet"/>
        <link href="<c:url value="/Resources/shared/css/themify-icons.css"/>" rel="stylesheet"/>
        <link href="<c:url value="/Resources/shared/css/jquery.mCustomScrollbar.min.css"/>" rel="stylesheet"/>
        <link href="<c:url value="/Resources/shared/css/checkbox-switch.css"/>" rel="stylesheet"/>
        <link href="<c:url value="/Resources/layout/member/css/widget.css?buildNumber=${RESOURCES_VERSION}"/>" rel="stylesheet"/>
        <link href="<c:url value="/Resources/layout/member/css/main.css?buildNumber=${RESOURCES_VERSION}"/>" rel="stylesheet"/>
        <audio id="audio-alert" src="<c:url value="/Resources/joli/audio/alert.mp3"/>" preload="auto"></audio>
        <audio id="audio-fail" src="<c:url value="/Resources/joli/audio/fail.mp3"/>" preload="auto"></audio>
        <script type="text/javascript" src="<c:url value="/Resources/shared/js/jquery-3.2.1.min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/Resources/shared/js/popper.min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/Resources/shared/js/bootstrap.min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/Resources/shared/js/noty/jquery.noty.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/Resources/shared/js/noty/layouts/bottomCenter.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/Resources/shared/js/noty/themes/default.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/Resources/shared/js/jquery-mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/Resources/shared/js/jquery-validation/jquery.validate.js"/>"></script>
<!--        <script type="text/javascript" src="<c:url value="/Resources/shared/js/validationengine/jquery.validationEngine.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/Resources/shared/js/validationengine/languages/jquery.validationEngine-vi.js"/>"></script>-->
        <script type="text/javascript" src="<c:url value="/Resources/shared/js/jquery-validation/additional-methods.min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/Resources/shared/js/jquery-validation/localization/messages_vi.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/Resources/shared/js/morris/raphael-min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/Resources/shared/js/morris/morris.min.js"/>"></script>
    </head>
    <body>
        <div id="container" class="flex flex-column">
            <c:import url="/Views/Member/Module/header.jsp" />
            <div id="content" class="no-gutters">
                <c:import url="/Views/Member/Module/left-sidebar.jsp" />
                <div class="main-content">
                    <div id="sidebar-detail">                                    
                        <a class="close-sidebar ti-close"></a>
                        <div class="sidebar-content"></div>
                    </div>
                    <div id="sidebar-overlay"></div>
                    <t:insertAttribute name="Content"/>
                </div>
            </div>
        </div>
        <c:import url="/Views/Member/Modal/affiliate.jsp" />
        <c:import url="/Views/Member/Modal/composemessage.jsp" />
        <c:import url="/Views/Member/Modal/withdraw.jsp" />
        <c:import url="/Views/Member/Modal/transfer.jsp" />
        <script type="text/javascript" src="<c:url value="/Resources/shared/js/serialize.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/Resources/layout/member/js/main.js?buildNumber=${RESOURCES_VERSION}"/>"></script>
    </body>
</html>






