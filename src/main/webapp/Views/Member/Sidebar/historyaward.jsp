<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="/WEB-INF/tlds/functions.tld" %>
<div class="sidebar-header d-flex">
    <span class="title">History Award</span>
</div>
<div class="sidebar-body">
    <div class="history-trading">
        <c:set var="currentGroupDate" value="null"/>
        <c:forEach items="${HISTORY_AWARDS}" var="award">
            <c:set var="currentFormatDate" value="${f:customFormatDate('yyyy/MM/dd', award.createdDate)}"/>
            <c:if test="${!f:compareString(currentGroupDate,currentFormatDate)}">
                <c:set var="currentGroupDate" value="${currentFormatDate}"/>
                <div class="group text-center">${currentFormatDate}</div>
            </c:if>
            <div class="item">
                <div class="row no-gutters">
                    <div class="col-3">
                        <div class="top">
                            ${f:customFormatDate("yyyy/MM/dd",award.createdDate)}
                        </div>
                        <div class="bottom">                        
                            ${f:customFormatDate("HH:mm:ss",award.createdDate)}
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="top">
                            <span class="text-info text-uppercase">${award.checkAwardID.name}</span>
                        </div>
                        <div class="bottom text-uppercase">
                            From: ${award.partnerID.userName}
                        </div>
                    </div>
                    <div class="col-5 text-right">
                        <div class="top">&nbsp;</div>
                        <div class="bottom text-uppercase">
                            <span class="text-success">${award.amount} <small>ETH</small></span>
                        </div>
                    </div>
                </div>
            </div>
        </c:forEach>
    </div>
</div>