<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="/WEB-INF/tlds/functions.tld" %>
<div class="sidebar-header d-flex">
    <span class="title">History Deposit</span>
</div>
<div class="sidebar-body">
    <div class="history-trading">
        <c:forEach items="${HISTORY_DEPOSIT}" var="tran">
            <div class="item">
                <div class="row no-gutters">
                    <div class="col-3">
                        <div class="top">
                            ${f:customFormatDate("yyyy/MM/dd",tran.createdDate)}
                        </div>
                        <div class="bottom">                        
                            ${f:customFormatDate("HH:mm:ss",tran.createdDate)}
                        </div>
                    </div>
                    <div class="col-9">
                        <div class="top">  
                            <a class="text-info" target="_blank" href="https://live.blockcypher.com/btc/tx/${tran.transactionHash}">${tran.transactionHash}</a>
                        </div>
                        <div class="bottom text-uppercase">                      
                            <div class="d-flex text-success justify-content-between">
                                <span>Deposit</span>
                                <span><b>${f:customFormatDecimal("########.########", tran.amount)}</b>&nbsp;<small>ETH</small></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </c:forEach>
    </div>
</div>