<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="/WEB-INF/tlds/functions.tld" %>
<div class="sidebar-header d-flex">
    <span class="title">History Trading</span>
</div>
<div class="sidebar-body">
    <div class="history-trading">
        <c:set var="currentGroupDate" value="null"/>
        <c:forEach items="${TRADING_HISTORY}" var="trading">
            <c:set var="currentFormatDate" value="${f:customFormatDate('yyyy/MM/dd', trading.createdDate)}"/>
            <c:if test="${!f:compareString(currentGroupDate,currentFormatDate)}">
                <c:set var="currentGroupDate" value="${currentFormatDate}"/>
                <div class="group text-center">${currentFormatDate}</div>
            </c:if>
            <div class="item">
                <div class="row no-gutters">
                    <div class="col-3">
                        <div class="top">
                            ${f:customFormatDate("yyyy/MM/dd",trading.createdDate)}
                        </div>
                        <div class="bottom">                        
                            ${f:customFormatDate("HH:mm:ss",trading.createdDate)}
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="top">
                            <c:if test="${trading.direction==1}">
                                <span class="text-success"><i class="fas fa-caret-up"></i> Higher</span>
                            </c:if>
                            <c:if test="${trading.direction==-1}">
                                <span class="text-danger"><i class="fas fa-caret-down"></i> Lower</span>
                            </c:if>
                            /
                            <c:if test="${trading.blockDirection==1}">
                                <span class="text-success"><i class="fas fa-caret-up"></i> Higher</span>
                            </c:if>
                            <c:if test="${trading.blockDirection==-1}">
                                <span class="text-danger"><i class="fas fa-caret-down"></i> Lower</span>
                            </c:if>
                            <c:if test="${trading.blockDirection==0}">
                                <span class="text-warning"><i class="fas fa-sort"></i> Equal</span>
                            </c:if>
                        </div>
                        <div class="bottom text-uppercase">
                            ${trading.tradingCoinTypeID.name}
                        </div>
                    </div>
                    <div class="col-5 text-right">
                        <div class="top">                    
                            ${f:customFormatDecimal("########.########", trading.amount)} <small>ETH</small>
                        </div>
                        <div class="bottom text-uppercase">
                            <c:choose>
                                <c:when test="${trading.blockDirection==0}">
                                    <span class="text-warning">${f:customFormatDecimal("########.########",trading.amount*70/100)} <small>ETH</small></span>
                                </c:when>
                                <c:when test="${trading.direction==trading.blockDirection}">
                                    <span class="text-warning">${f:customFormatDecimal("########.########",trading.amount*180/100)} <small>ETH</small></span>
                                </c:when>
                                <c:when test="${trading.direction!=trading.blockDirection}">
                                    <span class="text-light">0.00 <small>ETH</small></span>
                                </c:when>
                            </c:choose>
                        </div>
                    </div>
                </div>
            </div>
        </c:forEach>
    </div>
</div>