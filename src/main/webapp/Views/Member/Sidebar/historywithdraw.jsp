<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="/WEB-INF/tlds/functions.tld" %>
<div class="sidebar-header d-flex">
    <span class="title">History Withdraw</span>
</div>
<div class="sidebar-body">
    <div class="history-trading">
        <c:forEach items="${HISTORY_WITHDRAW}" var="his">
            <div class="item">
                <div class="row no-gutters">
                    <div class="col-3">
                        <div class="top">
                            ${f:customFormatDate("yyyy/MM/dd",his.createdDate)}
                        </div>
                        <div class="bottom">                        
                            ${f:customFormatDate("HH:mm:ss",his.createdDate)}
                        </div>
                    </div>
                    <div class="col-9">
                        <div class="top">  
                            <a class="text-info" target="_blank" href="javascript:void(0)">${his.walletAddress}</a>
                        </div>
                        <div class="bottom text-uppercase">                      
                            <div class="d-flex text-success justify-content-between">
                                <span><b>${f:customFormatDecimal("########.########", his.amount)}</b>&nbsp;<small>ETH</small></span>
                                <c:choose>
                                    <c:when test="${!his.isConfirmed}">
                                        <span class="badge badge-dark">Pending</span>
                                    </c:when>
                                    <c:when test="${his.isConfirmed}">
                                        <span class="badge badge-success">Completed</span>
                                    </c:when>
                                </c:choose>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </c:forEach>
    </div>
</div>