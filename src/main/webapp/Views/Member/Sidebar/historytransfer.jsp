<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="/WEB-INF/tlds/functions.tld" %>
<div class="sidebar-header d-flex">
    <span class="title">History Withdraw</span>
</div>
<div class="sidebar-body">
    <div class="history-trading">        
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#sendTab">Sent</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#receiveTab">Received</a>
            </li>
        </ul>
        <div class="tab-content mt-0">
            <div class="tab-pane active" id="sendTab">
                <c:forEach items="${HISTORY_SEND}" var="his">
                    <div class="item">
                        <div class="top">
                            <div class="d-flex justify-content-between">
                                <span>${f:customFormatDate("yyyy/MM/dd HH:mm:ss",his.createdDate)}</span>
                                <span>Amount</span>
                            </div>
                        </div>
                        <div class="bottom">
                            <div class="d-flex justify-content-between">
                                <span class="text-info">Sent to: <b class="text-uppercase">${his.partnerID.userName}</b></span>
                                <span class="text-success"><b>${f:customFormatDecimal("########.########", his.amount)}</b>&nbsp;<small>ETH</small></span>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </div>
            <div class="tab-pane" id="receiveTab">
                <c:forEach items="${HISTORY_RECEIVE}" var="his">
                    <div class="item">
                        <div class="top">
                            <div class="d-flex justify-content-between">
                                <span>${f:customFormatDate("yyyy/MM/dd HH:mm:ss",his.createdDate)}</span>
                                <span>Amount</span>
                            </div>
                        </div>
                        <div class="bottom">
                            <div class="d-flex justify-content-between">
                                <span class="text-info">Received from: <b class="text-uppercase">${his.customerID.userName}</b></span>
                                <span class="text-success"><b>${f:customFormatDecimal("########.########", his.amount)}</b>&nbsp;<small>ETH</small></span>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </div>
        </div>
    </div>
</div>