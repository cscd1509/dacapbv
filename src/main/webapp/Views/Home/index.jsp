<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="t"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${sessionScope['MEMBER_DATA']!=null}">
    <c:redirect url="/Trading" />
</c:if>
<%@taglib uri="/WEB-INF/tlds/functions.tld" prefix="f"%>
<c:set value="${f:getIntegerRandom()}" var="RESOURCES_VERSION" />
<!DOCTYPE html>

<!--[if IE 9]><html lang="en" class="ie ie9"><![endif]-->
<html>
    <head>
        <meta charset="utf-8">
        <title>Washington Trade - Binary Option</title>
        <meta name="description" content="Ultimate Responsive HTML5 Landing Page" />
        <meta name="keywords" content="responsive html5 template, landing page, bootstrap 3.0, css, jquery" />
        <meta name="author" content="8Guild" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <!--Favicon-->
        <link rel="shortcut icon" href="Resources/shared/img/favicon.png" type="image/x-icon">
        <link rel="icon" href="Resources/shared/img/favicon.png" type="image/x-icon">
        <!--Google Fonts-->
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700' rel='stylesheet' type='text/css'>
        <!--Bootstrap 3.3.2-->
        <link href="Resources/layout/home/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <!--Icon Fonts-->
        <link href="Resources/layout/home/css/font-awesome.min.css" rel="stylesheet" media="screen">
        <link href="Resources/layout/home/css/icon-moon.css" rel="stylesheet" media="screen">
        <!--Animations-->
        <link href="Resources/layout/home/css/animate.css" rel="stylesheet" media="screen">
        <!--Theme Styles-->
        <link href="Resources/layout/home/css/theme-styles.css" rel="stylesheet" media="screen">
        <!--Color Schemes-->
        <link class="color-scheme" href="Resources/layout/home/css/color.css" rel="stylesheet" media="screen">
        <!--Modernizr-->
        <script src="Resources/layout/home/js/modernizr.custom.js"></script>
        <!--Adding Media Queries Support for IE8-->
        <!--[if lt IE 9]>
          <script src="Resources/layout/home/js/respond.min.js"></script>
          <script src="Resources/layout/home/js/excanvas.js"></script>
        <![endif]-->
    </head>

    <!--Body-->
    <body>
        <!--Page Preloading-->
        <div id="preloader"><div id="spinner"></div></div>

        <!--Navigation-->
        <nav class="side-navi animated">
            <ul>
                <li class="current"><a class="tooltipped scroll-up" href="#" data-toggle="tooltip" data-placement="left" title="Home"></a></li>
                <li><a class="tooltipped scroll" href="#features" data-toggle="tooltip" data-placement="left" title="Features"></a></li>
                <li><a class="tooltipped scroll" href="#apps" data-toggle="tooltip" data-placement="left" title="Apps"></a></li>
                <li><a class="tooltipped scroll" href="#testimonials" data-toggle="tooltip" data-placement="left" title="Testimonials"></a></li>
                <li><a class="tooltipped scroll" href="#faq" data-toggle="tooltip" data-placement="left" title="FAQ"></a></li>
                <li><a class="tooltipped scroll" href="#team" data-toggle="tooltip" data-placement="left" title="Team"></a></li>
                <li><a class="tooltipped scroll" href="#contacts" data-toggle="tooltip" data-placement="left" title="Contacts"></a></li>
                <li><a class="tooltipped scroll" href="#social" data-toggle="tooltip" data-placement="left" title="Social"></a></li>
            </ul>
        </nav>

        <!--Header (Hero Unit with Slider)-->
        <header class="hero">
            <div class="container">
                <!--Hero Slider-->
                <div class="row">
                    <div class="hero-slider">
                        <div class="slide first-slide">
                            <div class="col-lg-5 col-md-5 col-sm-5 animated">
                                <img style="margin-top:30px;" class="logo-image" src="Resources/layout/home/img/logo.png" alt="Ultima"/>
                                <h3>An account from 0.0 ETH</h3>
                                <p>You don’t need to save start-up capital: start earning without investing.<br/>
                                    Don’t deprive yourself of weekend earnings. Trade daily and earn more profit.</p>
                                <a class="btn btn-primary" href="/login">SIGN IN</a>
                            </div>
                            <div class="col-lg-7 col-md-7 col-sm-7 animated">
                                <img class="pull-right" src="Resources/layout/home/img/imac.png" width="689" height="659" alt="Components and Styles"/>
                            </div>
                        </div>
                        <div class="slide">
                            <div class="col-lg-5 col-md-5 col-sm-5 animated fadeInUp">
                                <img style="margin-top:30px;" class="logo-image" src="Resources/layout/home/img/logo.png" alt="Ultima"/>
                                <h3>Transactions start from 0.0001 ETH</h3>
                                <p>The minimum transaction cost is small enough that you won’t lose a big sum while you’re still learning how to trade.</p>
                                <a class="btn btn-primary" href="/login">SIGN IN</a>
                            </div>
                            <div class="col-lg-7 col-md-7 col-sm-7 animated fadeInRight">
                                <img class="iphone" src="Resources/layout/home/img/iphone.png" width="649" height="400" alt="Coming Soon Page"/>
                            </div>
                        </div>
                        <div class="slide">
                            <div class="col-lg-4 col-md-4 col-sm-4 animated fadeInDown">
                                <img style="margin-top:60px;" class="logo-image" src="Resources/layout/home/img/logo.png" alt="Ultima"/>
                                <h3>Uniquenon-stop trading</h3>
                                <p>Time is money. Don’t wait for a transaction to end: open several positions at the same time and continue trading.</p>
                                <a class="btn btn-primary" href="/login">SIGN IN</a>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 animated fadeInRight">
                                <div class="video">
                                    <img src="Resources/layout/home/img/macbook.png" width="782" height="422" alt=""/>
                                    <div class="vide-holder">
                                        <iframe src="https://www.youtube.com/embed/ymmopjFucZM" frameborder="0" allow="autoplay; encrypted-media" width="720" height="405"></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--Close Hero Slider-->

            </div>    	
        </header><!--Close Header-->

        <!--Features-->
        <section class="page-block" id="features">
            <div class="container">
                <div class="row page-header">
                    <h2>Features</h2>
                    <span>Simple and convenient</span>
                </div>
                <div class="row features">
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="feature-img"><img src="Resources/layout/home/img/feature1.png" width="124" height="120" alt="Editable colors"/></div>
                        <h3>Demo account</h3>
                        <p>You can always improve your trading skills on the demo account. When you're ready, switch to your real account.</p>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="feature-img"><img src="Resources/layout/home/img/feature2.png" width="131" height="131" alt="All you need for start"/></div>
                        <h3>Online support</h3>
                        <p>You can send a message anytime via chat and get feedback right away!</p>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="feature-img"><img src="Resources/layout/home/img/feature3.png" width="72" height="72" alt="Clean code"/></div>
                        <h3>Free training</h3>
                        <p>By studying the extensive knowledge base, you will be able to trade as profitably as possible.</p>
                    </div>
                </div>
            </div>
            <!--Tabs-->
            <div class="container-fluid">
                <div class="row">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs central">
                        <li class="active"><a href="#clean-code" data-toggle="tab">Advantage</a></li>
                        <li><a href="#more" data-toggle="tab">Security</a></li>
                    </ul>
                </div>
            </div>
            <!-- Tab panes -->
            <div class="container">
                <div class="row tab-content central">
                    <div class="tab-pane fade in active col-lg-12" id="clean-code">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 animated fadeInLeft">
                                <img src="Resources/layout/home/img/1.png" alt="Clean Code"/>
                            </div>
                            <div class="col-lg-6 col-md-6 animated fadeInRight">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h2>Advantage</h2>
                                        <p>BV is a client oriented company, creating new possibilities in the market of leading trading technologies.</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-1"><i class="feature-icon icon-checkmark"></i></div>
                                    <div class="col-lg-11">
                                        <p>A high-end trading platform with a wide range of financial assets.</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-1"><i class="feature-icon icon-checkmark"></i></div>
                                    <div class="col-lg-11">
                                        <p><b>Insurance up to $5,000,000</b>.</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-1"><i class="feature-icon icon-checkmark"></i></div>
                                    <div class="col-lg-11">
                                        <p>Some of the most advantageous trading terms and investment options on the market.</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-1"><i class="feature-icon icon-checkmark"></i></div>
                                    <div class="col-lg-11">
                                        <p>Analytical services for client trading.</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-1"><i class="feature-icon icon-checkmark"></i></div>
                                    <div class="col-lg-11">
                                        <p>Convenience for both the experienced and the novice trader.</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-1"><i class="feature-icon icon-checkmark"></i></div>
                                    <div class="col-lg-11">
                                        <p>A helpful section of high quality tutorials.</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-1"><i class="feature-icon icon-checkmark"></i></div>
                                    <div class="col-lg-11">
                                        <p>Efficient and highly professional client support staff.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade col-lg-12" id="more">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 animated fadeInLeft">
                                <img class="img-center" src="Resources/layout/home/img/3.png" alt="More Features"/>
                            </div>
                            <div class="col-lg-6 col-md-6 animated fadeInRight">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h2>Security</h2>
                                        <p class="double-space-bottom">All personal data provided to the Company by the Client is considered confidential and can be disclosed to third parties only with the consent of the Client.</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-2"><i class="feature-icon icon-shield"></i></div>
                                    <div class="col-lg-10">
                                        <p class="double-space-bottom">SSL Security</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-2"><i class="feature-icon icon-users"></i></div>
                                    <div class="col-lg-10">
                                        <p class="double-space-bottom">Quotes from leading agencies</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-2"><i class="feature-icon icon-credit"></i></div>
                                    <div class="col-lg-10">
                                        <p class="double-space-bottom">Coinbase's payment system API</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!--Mobile Apps-->
        <section class="page-block mobile-apps" id="apps">
            <div class="container">
                <div class="row page-header">
                    <h2>Mobile apps</h2>
                    <span>Earn money from anywhere!</span>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-4">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="row space-bottom">
                                    <div class="col-lg-9 col-md-9">
                                        <h3>Run on any device</h3>
                                    </div>
                                    <div class="col-lg-3 col-md-3">
                                        <img class="img-center" src="Resources/layout/home/img/apps-icon.png" alt="Mobile Apps"/>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12">
                                        <p class="text-justify">You can earn at anytime and anywhere with the BV app for iOS, android or windows phone. Stay in the know: instant information on transaction closings, promotions and tournaments.</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-10 col-lg-offset-1">
                                        <a class="btn btn-primary btn-block" href="#"><i class="icon-android"></i>Available on Google Play</a>
                                        <a class="btn btn-primary btn-block" href="#"><i class="icon-apple"></i>Available on App Store</a>
                                        <a class="btn btn-primary btn-block" href="#"><i class="icon-windows8"></i>Windows Marketplace</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-7 col-lg-offset-1 col-md-8">
                        <img class="double-space-top" src="Resources/layout/home/img/tablet.png" alt="Mobile Apps"/>
                    </div>
                </div>
            </div>
        </section>        

        <!--Testimonials-->
        <section class="page-block color" id="testimonials">
            <div class="container">
                <div class="row page-header">
                    <h2 class="text-light">User reviews</h2>
                </div>
                <div class="row">
                    <div id="testimonials-slider" class="testimonials-slider carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#testimonials-slider" data-slide-to="0" class="active"></li>
                            <li data-target="#testimonials-slider" data-slide-to="1"></li>
                            <li data-target="#testimonials-slider" data-slide-to="2"></li>
                        </ol>
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                            <div class="item active">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-lg-3 col-md-3 col-sm-4">
                                            <img class="img-circle img-center" src="Resources/layout/home/img/dirk.png" width="192" height="192" alt="Dexter Dirk"/>
                                        </div>
                                        <div class="col-lg-9 col-md-9 col-sm-8">
                                            <blockquote>
                                                <p class="text-light">Pretty cool! mad respect for the broker, there are no others like that, well, for successful trading, of course, you need experience and not to be greedy! Every day, little by little, today I wasn’t lucky, but tomorrow will work out for sure! In short, I want to say to BV, I'm with you!</p>
                                                <footer class="text-light">Dexter Dirk</footer>
                                            </blockquote>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-lg-3 col-md-3 col-sm-4">
                                            <img class="img-circle img-center" src="Resources/layout/home/img/roe.png" width="192" height="192" alt="Richard Roe"/>
                                        </div>
                                        <div class="col-lg-9 col-md-9 col-sm-8">
                                            <blockquote>
                                                <p class="text-light">Hello everyone, I am completely satisfied with BV. From 1.0 ETH, I got up to 15.0 ETH in 15 days, but because of my foolishness (greed) I spent my whole balance. I made a conclusion, next time I will be more intelligent.<br>P/s: I haven’t checked the conclusion personally, I didn’t have tim</p>
                                                <footer class="text-light">Richard Roe</footer>
                                            </blockquote>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-lg-3 col-md-3 col-sm-4">
                                            <img class="img-circle img-center" src="Resources/layout/home/img/doe.png" width="192" height="192" alt="Jonathan Doe"/>
                                        </div>
                                        <div class="col-lg-9 col-md-9 col-sm-8">
                                            <blockquote>
                                                <p class="text-light">For sure, everything is great with you - most importantly, you inspire confidence, which is definitely important... there’s an honest review...</p>
                                                <footer class="text-light">Jonathan Doe</footer>
                                            </blockquote>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!--FAQ-->
        <section class="page-block faq" id="faq">
            <div class="container">
                <div class="row page-header">
                    <h2>FAQ</h2>
                    <span>Popular Questions</span>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-4 col-sm-4">
                        <img class="img-center" src="Resources/layout/home/img/faq-icon.png" alt="FAQ"/>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8">
                        <div class="panel-group" id="accordion-faq">
                            <div class="panel">
                                <a class="panel-heading" data-toggle="collapse" data-parent="#accordion-faq" href="#collapse1">What is BV
                                    <div class="indicator"><div class="icon"><span class="hr-line"></span><span class="vr-line"></span></div></div>
                                </a>
                                <div id="collapse1" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        BV is a trading platform on which you can earn on rises or falls in currency exchange rates and prices for shares or commodities. The platform is designed to make trading a pleasant and profitable experience for you, regardless of your level of training.
                                    </div>
                                </div>
                            </div>
                            <div class="panel">
                                <a class="panel-heading collapsed" data-toggle="collapse" data-parent="#accordion-faq" href="#collapse2">
                                    I'm new and don't know anything about trading. Can I trade on the platform?
                                    <div class="indicator"><div class="icon"><span class="hr-line"></span><span class="vr-line"></span></div></div>
                                </a>
                                <div id="collapse2" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        Of course you can! Especially for you, we've developed training materials to help you start trading profitably as soon as possible. You will be helped by educational videos, strategies, informational email newsletters, and posts on our official social network accounts. In addition, you can always contact our support service via any communication channel you prefer.
                                    </div>
                                </div>
                            </div>
                            <div class="panel">
                                <a class="panel-heading collapsed" data-toggle="collapse" data-parent="#accordion-faq" href="#collapse3">
                                    Do I have to make a deposit right away to trade on the BV platform?
                                    <div class="indicator"><div class="icon"><span class="hr-line"></span><span class="vr-line"></span></div></div>
                                </a>
                                <div id="collapse3" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        You do not need to make a deposit right away. You can start by practicing on the training account, get used to the platform and fine tune a profitable new strategy, then go to the real account. You can also participate in free tournaments and earn real money for winning.
                                    </div>
                                </div>
                            </div>
                            <div class="panel">
                                <a class="panel-heading collapsed" data-toggle="collapse" data-parent="#accordion-faq" href="#collapse4">
                                    Why should I switch to the real account?
                                    <div class="indicator"><div class="icon"><span class="hr-line"></span><span class="vr-line"></span></div></div>
                                </a>
                                <div id="collapse3" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        When you trade on the real account, you get real profits. You will have access to a lot of privileges and additional opportunities, for example, participation in all tournaments and promotions on the platform, access to analytics, assistance from a personal manager, etc.
                                    </div>
                                </div>
                            </div>
                            <div class="panel">
                                <a class="panel-heading collapsed" data-toggle="collapse" data-parent="#accordion-faq" href="#collapse5">
                                    How do I switch to the real account?
                                    <div class="indicator"><div class="icon"><span class="hr-line"></span><span class="vr-line"></span></div></div>
                                </a>
                                <div id="collapse3" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        To switch to the real account, select the account type "Real" in the top panel of the platform. You will need to make a deposit to start trading on the real account. This can be done by click to "Deposit" button and follow the instructions.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--Team-->
        <section class="page-block team" id="team">
            <div class="container">
                <div class="row page-header">
                    <h2>Meet our team</h2>
                    <span>Subtext for header</span>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <img class="img-circle-thumbnail img-center" src="Resources/layout/home/img/dirk.jpg" alt="Dexter Dirk"/>
                        <h3>Dexter Dirk</h3>
                        <p>Senior designer</p>
                        <div class="social-networks">
                            <a class="icon-facebook tooltipped" href="#" title="Facebook"></a>
                            <a class="icon-instagram tooltipped" href="#" title="Instagram"></a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <img class="img-circle-thumbnail img-center" src="Resources/layout/home/img/roe.jpg" alt="Alexander Roe"/>
                        <h3>Alexander Roe</h3>
                        <p>Financial director</p>
                        <div class="social-networks">
                            <a class="icon-twitter tooltipped" href="#" title="Twitter"></a>
                            <a class="icon-google-plus tooltipped" href="#" title="Google+"></a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <img class="img-circle-thumbnail img-center" src="Resources/layout/home/img/davis.jpg" alt="Susanna Davis"/>
                        <h3>Susanna Davis</h3>
                        <p>Art director</p>
                        <div class="social-networks">
                            <a class="icon-youtube tooltipped" href="#" title="Youtube"></a>
                            <a class="icon-tumblr tooltipped" href="#" title="Tumblr"></a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <img class="img-circle-thumbnail img-center" src="Resources/layout/home/img/doe.jpg" alt="Jonathan Doe"/>
                        <h3>Jonathan Doe</h3>
                        <p>Technical support</p>
                        <div class="social-networks">
                            <a class="icon-facebook tooltipped" href="#" title="Facebook"></a>
                            <a class="icon-twitter tooltipped" href="#" title="Twitter"></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!--Contacts-->
        <section class="page-block contacts" id="contacts">
            <div class="container">
                <div class="row page-header">
                    <h2>Contacts</h2>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-4">
                        <div class="row space-bottom">
                            <div class="col-lg-9 col-md-9">
                                <h3>Privacy Policy</h3>
                            </div>
                            <div class="col-lg-3 col-md-3">
                                <img class="img-center" src="Resources/shared/img/wdc-icon.png" alt="About Us" height="70"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <p class="text-justify">The financial operations offered on this site may involve increased risk. By using the financial services and tools this site offers, you may suffer serious financial loss, or completely lose the funds in your guaranteed-trading account. Please evaluate all the financial risks and seek advice from an independent financial advisor before trading. BV is not responsible for any direct, indirect or consequential losses, or any other damages resulting from the user's actions on this site.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-lg-offset-1">
                        <div class="row space-bottom">
                            <div class="col-lg-8 col-md-8">
                                <h3>Adress</h3>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <img class="img-center" src="Resources/layout/home/img/map.png" alt="Adress"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <p>228 Cottage Ave, Cashmere, WA 98815, USA</p>
                            </div>
                        </div>
                        <div class="row space-bottom">
                            <div class="col-lg-8 col-md-8">
                                <h3>Contacts</h3>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <img class="img-center" src="Resources/layout/home/img/phone.png" alt="Contacts"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <p>business.BV@email.com<br/>support.BV@gmail.com</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-lg-offset-1">
                        <div class="row space-bottom">
                            <div class="col-lg-8 col-md-8">
                                <h3>Contact us</h3>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <img class="img-center" src="Resources/layout/home/img/plan.png" alt="Adress"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <form method="post" class="feedback-form ajax-form" role="form">
                                    <div class="form-group">
                                        <label class="sr-only" for="name1">Name</label>
                                        <input type="text" class="form-control" name="name" id="name1" placeholder="Name">
                                    </div>
                                    <div class="form-group">
                                        <label class="sr-only" for="email1">Email</label>
                                        <input type="email" class="form-control" name="email" id="email1" placeholder="Email">
                                    </div>
                                    <div class="form-group">
                                        <label class="sr-only" for="message">Message</label>
                                        <textarea class="form-control" name="message" id="message" rows="5" placeholder="Message"></textarea>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <!-- Validation Response -->
                                    <div class="response-holder"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!--Social Networks-->
        <section class="page-block" id="social">
            <div class="container">
                <div class="row page-header">
                    <h2>Find us on social networks</h2>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="social-networks">
                            <a class="icon-facebook tooltipped" href="#" title="Facebook"></a>
                            <a class="icon-instagram tooltipped" href="#" title="Instagram"></a>
                            <a class="icon-twitter tooltipped" href="#" title="Twitter"></a>
                            <a class="icon-google-plus tooltipped" href="#" title="Google+"></a>
                            <a class="icon-youtube tooltipped" href="#" title="Youtube"></a>
                            <a class="icon-tumblr tooltipped" href="#" title="Tumblr"></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!--Footer-->
        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="text-center text-light">Subscribe to our newsletter</h2>
                        <p class="text-light text-center">Sign up now to our newsletter and you'll be one of the first to know when the site is ready:</p>
                    </div>
                </div>
                <div class="row">

                    <!-- Begin MailChimp Signup Form -->
                    <form class="subscribe-form validate" action="" method="post" id="mc-embedded-subscribe-form" target="_blank" novalidate>
                        <div class="form-group col-lg-7 col-md-8 col-sm-8 col-lg-offset-1">
                            <label class="sr-only" for="mce-EMAIL">Email address</label>
                            <input type="email" name="EMAIL" class="form-control" id="mce-EMAIL" placeholder="Enter email" required>
                            <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                            <div style="position: absolute; left: -5000px;"><input type="text" name="b_168a366a98d3248fbc35c0b67_73d49e0d23" value=""></div>
                        </div>
                        <div class="form-group col-lg-3 col-md-4 col-sm-4"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="btn btn-primary btn-block"></div>
                    </form>
                    <!-- Close MailChimp Signup Form -->

                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <p class="copyright">&copy; 2016 Washington DC Trade. All Rights Reserved.</p>
                    </div>
                </div>
            </div>
        </footer>
        <div class="modal fade" id="notioce-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Notification</h4>
                    </div>
                    <div class="modal-body">
                        <h3>Dear investors</h3>
                        <p class="text-left">The BV project is available from June 08, 2018, close beta version 1.0 works fine on laptops/pc open beta devices expected on June 20, 2018.</p>
                        <p>
                            <b>Regard,</b><br />
                            <i>Wdc team</i>
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <!--Scroll To Top Button-->
        <div id="scroll-top" class="scroll-up"><i class="icon-arrow-up"></i></div>

        <!--Javascript (jQuery) Libraries and Plugins-->
        <script src="Resources/layout/home/js/jquery-1.11.2.min.js"></script>
        <script src="Resources/layout/home/js/jquery.easing.1.3.js"></script>
        <script src="Resources/layout/home/js/bootstrap.min.js"></script>
        <script src="Resources/layout/home/js/jquery.touchSwipe.min.js"></script>
        <script src="Resources/layout/home/js/jquery.placeholder.js"></script>
        <script src="Resources/layout/home/js/icheck.min.js"></script>
        <script src="Resources/layout/home/js/jquery.validate.min.js"></script>
        <script src="Resources/layout/home/js/jquery.fitvids.js"></script>
        <script src="Resources/layout/home/js/jquery.bxslider.min.js"></script>
        <script src="Resources/layout/home/js/waypoints.min.js"></script>
        <script src="Resources/layout/home/js/smoothscroll.js"></script>
        <script src="Resources/layout/home/js/mailer.js"></script>
        <script src="Resources/layout/home/js/landing1.js"></script>

        <script>
            $(window).load(function () {
                $('#spinner').fadeOut();
                $('#preloader').delay(300).fadeOut('slow', function () {
                    $('#notioce-modal').modal('show');
                });
                setTimeout(function () {
                    $('.first-slide div:first-child').addClass('fadeInDown');
                }, 100);
                setTimeout(function () {
                    $('.first-slide div:last-child').addClass('fadeInRight');
                }, 100);
                setTimeout(function () {
                    $('.side-navi').addClass('slideInRight');
                }, 100);
            }
            );
        </script>

    </body><!--Close Body-->
</html>
