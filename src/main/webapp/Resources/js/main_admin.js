$(document).on('click', '#btn-expand-sidebar', function (e) {
    e.stopPropagation();
    if ($('#sidebar').hasClass('small-sidebar')) {
        $('#sidebar').removeClass('small-sidebar');
        $('#container').removeClass('lg-container');
    } else {
        $('#sidebar').addClass('small-sidebar');
        $('#container').addClass('lg-container');
    }
});
$(document).on('click', '#hide-sidebar', function (e) {
    e.stopPropagation();
    if ($('#sidebar').hasClass('hidden-sidebar')) {
        $('#sidebar').removeClass('hidden-sidebar');
        $('#container').removeClass('sp-large-container');
    } else {
        $('#sidebar').addClass('hidden-sidebar');
        $('#container').addClass('sp-large-container');
    }
});
$('#fullscreen-toggle').on('click', function (e) {
    var element = document.documentElement;
    if (!$('body')
            .hasClass("full-screen")) {
        $('body')
                .addClass("full-screen");
        $('#fullscreen-toggle')
                .addClass("active");
        if (element.requestFullscreen) {
            element.requestFullscreen();
        } else if (element.mozRequestFullScreen) {
            element.mozRequestFullScreen();
        } else if (element.webkitRequestFullscreen) {
            element.webkitRequestFullscreen();
        } else if (element.msRequestFullscreen) {
            element.msRequestFullscreen();
        }
    } else {
        $('body')
                .removeClass("full-screen");
        $('#fullscreen-toggle')
                .removeClass("active");
        if (document.exitFullscreen) {
            document.exitFullscreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitExitFullscreen) {
            document.webkitExitFullscreen();
        }
    }
});
$(document).on('click', '.btn-send-ajax', function () {
    var url = $(this).attr('controller');
    if (url) {
        sendAjax(url, 'GET', null, function (data) {
            openMessage(data);
        });
    }
});
$(document).on('click', '.btn-send-ajax-confirm', function () {
    if (confirm("Bạn chắc chắn muốn thực hiện hành động này?")) {
        var url = $(this).attr('controller');
        if (url) {
            sendAjax(url, 'GET', null, function (data) {
                openMessage(data);
            });
        }
    }
});
var timeoutDisplayMessage;
function openMessage(data, callback) {
    clearTimeout(timeoutDisplayMessage);
    if (typeof data !== "undefined") {
        $('body').append(data);
    }
    if (typeof callback === 'function') {
        callback();
    }
    $('#message').fadeIn(200, function () {
        reloadAjaxContent();
    });
    timeoutDisplayMessage = setTimeout(function () {
        $('#message').remove();
    }, 3000);
}
$(document).on('click', '#message .close', function () {
    clearTimeout(timeoutDisplayMessage);
    $('#message').remove();
});
function sendAjax(url, type, data, handle) {
    $.ajax({
        beforeSend: function (xhr) {
            ajaxStartHandle();
        },
        url: url,
        type: type,
        data: data,
        success: function (data) {
            if (typeof handle !== "undefined") {
                handle(data);
            }
        }, complete: function (jqXHR, textStatus) {
            ajaxCompleteHandle();
        }
    });
}

function sendAjaxWithJsonObj(url, type, data, handle) {
    $.ajax({
        beforeSend: function (xhr) {
            ajaxStartHandle();
        },
        url: url,
        type: type,
        data: data,
        contentType: 'application/json',
        success: function (data) {
            if (typeof handle !== "undefined") {
                handle(data);
            }
        }, error: function () {
            alert('Error!');
        }, complete: function (jqXHR, textStatus) {
            ajaxCompleteHandle();
        }
    });
}
function reloadAjaxContent() {
    var url = $('#reloadController').val();
    $.ajax({
        url: url,
        type: 'GET',
        success: function (data) {
            $('.ajax-content').html(data);
        }
    });
}
$(document).on('change', '.btn-change-display-per-page', function () {
    window.location.href = $(this).attr('controller') + $(this).val();
});

$(document).on('click', '.btn-open-modal', function (e) {
    e.preventDefault();
    var controller = $(this).attr('controller');
    if (typeof controller !== "undefined") {
        sendAjax(controller, 'GET', null, function (data) {
            $('body').append(data);
            $('#myModal').modal({show: true});
        });
    }
});
$(document).on('hide.bs.modal', '#myModal', function () {
    $(this).remove();
});
$(document).on('click', '.dropdown-select', function (e) {
    e.stopPropagation();
});
$(document).ready(function () {
    $(document).on('change', '.module-role-list li>label input[type=checkbox]', function () {
        if ($(this).is(':checked')) {
            $(this).parents('label').parent('li').children('ul').find('input[type=checkbox]').prop('checked', true);
        } else {
            $(this).parents('label').parent('li').children('ul').find('input[type=checkbox]').prop('checked', false);
        }
    });

});
$(document).on('click', '.btn-change-permission', function (e) {
    e.preventDefault();
    var checkedBoxs;
    var data = [];
    checkedBoxs = $('.module-role-list').find('input[type="checkbox"]:not(.external):not(.select-all):checked');
    checkedBoxs.each(function (i, target) {
        data.push(parseInt($(target).val()));
    });
    var url = $('.change-permission-form').attr('action');
    if (typeof url !== 'undefined') {
        if (!confirm("Bạn chắc chắn muốn thực hiện hành động này?")) {
            return;
        }
        $.ajax({
            beforeSend: function (xhr) {
                ajaxStartHandle();
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
            },
            url: url,
            type: 'POST',
            data: JSON.stringify(data),
            success: function (data) {
                clearTimeout(timeoutDisplayMessage);
                if (typeof data !== "undefined") {
                    $('body').append(data);
                }
                if (typeof callback === 'function') {
                    callback();
                }
                $('#message').fadeIn(200);
                timeoutDisplayMessage = setTimeout(function () {
                    $('#message').remove();
                }, 3000);
            }, complete: function (jqXHR, textStatus) {
                ajaxCompleteHandle();
            }
        });
    }
});
$(document).ready(function () {
    $('.popover-dismiss').popover({
        trigger: 'hover',
        html: true,
        container: 'body'
    });
});
$('.date-mask').mask('00/00/0000', {selectOnFocus: true});

function openMessageNotReload(data, callback) {
    clearTimeout(timeoutDisplayMessage);
    if (typeof data !== "undefined") {
        $('body').append(data);
    }
    $('#message').fadeIn(200);
    timeoutDisplayMessage = setTimeout(function () {
        $('#message').remove();
        if (callback !== null) {
            callback();
        }
    }, 3000);
}


$(document).ready(function () {
    var startDate = $('#startDate').val();
    var endDate = $('#endDate').val();
    var datePickerOptions = {
        autoUpdateInput: false,
        maxDate: moment(),
        locale: {
            cancelLabel: 'Clear'
        },
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment()],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    };
    if (startDate !== '' && endDate !== '') {
        datePickerOptions['startDate'] = moment(startDate, 'DD/MM/YYYY').toDate();
        datePickerOptions['endDate'] = moment(endDate, 'DD/MM/YYYY').toDate();
        $('#filterDateRange').val(startDate + '-' + endDate);
    }
    $('#filterDateRange').daterangepicker(datePickerOptions);
    $('#filterDateRange').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY') + '-' + picker.endDate.format('DD/MM/YYYY'));
        $('#startDate').val(picker.startDate.format('DD/MM/YYYY'));
        $('#endDate').val(picker.endDate.format('DD/MM/YYYY'));
    });
    $('#filterDateRange').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
        $('#startDate').val('');
        $('#endDate').val('');
    });
    $('#filterDateRange').change(function () {
        if (!$(this).val()) {
            $('#startDate').val('');
            $('#endDate').val('');
        }
    });
    $('.daterangepicker').click(function (e) {
        e.stopPropagation();
    });
});
$('document').ready(function () {
    $('[data-toggle="tooltip"]').tooltip({container: 'body'});
    $(document).on('show.bs.dropdown', '.table-responsive', function () {
        $('.table-responsive').css("overflow", "inherit");
    });

    $(document).on('hide.bs.dropdown', '.table-responsive', function () {
        $('.table-responsive').css("overflow", "auto");
    });
});
function ajaxStartHandle() {
    var requestCount = parseInt($('body').data('request'));
    if (!requestCount) {
        $("body").addClass("sending");
        $('body #spinner').show(0, function () {
            $('body.sending #spinner .progress-bar').css('width', '100%');
        });
    }
    $('body').attr('data-request', requestCount + 1);
}
function ajaxCompleteHandle() {
    var requestCount = parseInt($('body').data('request'));
    $('body').attr('data-request', requestCount - 1);
    if (!requestCount) {
        $('body.sending #spinner .progress-bar').css('width', '0');
        $('body #spinner').hide(0);
        $("body").removeClass("sending");
    }
}
