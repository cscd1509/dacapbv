$.fn.serializeObject = function () {
    var o = {};
    $.each($(this).find('input:not(.external),select:not(.external),textarea:not(.external)'), function () {
        if (typeof $(this).attr('name') !== 'undefined') {
            var v;
            if ($(this).attr('type') === 'checkbox') {
                v = $(this).is(':checked');
                o[$(this).attr('name')] = v;
            } else if ($(this).attr('type') === 'radio') {
                if ($(this).is(':checked')) {
                    v = $(this).val();
                    o[$(this).attr('name')] = v;
                }
            } else if ($(this).val() === '') {
                v = null;
                o[$(this).attr('name')] = v;
            } else if ($(this).attr('data-json')) {
                v = {};
                v[$(this).attr('data-json')] = $(this).val();
                o[$(this).attr('name')] = v;
            } else {
                v = $(this).val();
                o[$(this).attr('name')] = v;
            }
        }
    });
    return o;
};

var alertInterval;
function openAlert(data, callback) {
    $('#noty_bottomCenter_layout_container').remove();
    clearInterval(alertInterval);
    $('body').append(data);
    if (typeof callback === 'function') {
        callback();
    }
    alertInterval = setInterval(function () {
        $('#noty_bottomCenter_layout_container').remove();
    }, 5000);
}