window.onload = function () {
    var activeCoinCode = $('#trade-action .coin-type .dropdown-menu .dropdown-item.active .code').html();
    var activeCoinName = $('#trade-action .coin-type .dropdown-menu .dropdown-item.active .name').html();
    $('#spinner').fadeOut(0);
    $('#preloader').delay(0).fadeOut('0', function () {

    });
    var globalTime;
    var dataPoints = [];
    var lastRate = 0;
    var chart;
    function initHistoryData() {
        $.ajax({
            url: "https://min-api.cryptocompare.com/data/histominute?fsym=" + activeCoinCode + "&tsym=USD&limit=15",
            method: "GET",
            async: false,
            success: function (data) {
                var value = data['Data'][0];
                var x = new Date(value['time'] * 1000);
                var open = value['open'];
                var close = value['close'];
                var low = open < close ? open : close;
                var hight = open >= close ? open : close;
                var y = [
                    open,
                    low,
                    hight,
                    close
                ];
                dataPoints.push({
                    x: x,
                    y: y
                });
                lastRate = close;
                var i = 0;
                while (i < data['Data'].length) {
                    var value = data['Data'][i];
                    x = new Date(value['time'] * 1000);
                    open = lastRate;
                    close = value['close'];
                    low = open < close ? open : close;
                    hight = open >= close ? open : close;
                    y = [
                        open,
                        low,
                        hight,
                        close
                    ];
                    dataPoints.push({
                        x: x, y: y
                    });
                    lastRate = close;
                    i++;
                }
            }
        });
    }

    function initGlobalTime() {
        $.ajax({
            url: "https://api.gdax.com/time",
            method: "GET",
            async: false,
            success: function (data) {
                globalTime = new Date(data['iso']);
            }
        });
    }
    function initChart() {
        chart = new CanvasJS.Chart("chartContainer", {
            theme: "dark2",
            colorSet: "colorSet3",
            backgroundColor: "transparent",
            animationEnabled: true,
            zoomEnabled: true,
            toolTip: {
                animationEnabled: true,
                fontColor: "#fff"
            },
            title: {
                text: activeCoinName + " Trading",
                verticalAlign: "center",
                horizontalAlign: "center",
                fontColor: "rgba(255,255,255,.3)"
            },
            axisX: {
                valueFormatString: "HH:mm:ss",
                labelFontSize: 9,
                labelFontFamily: "Roboto",
                labelFontWeight: "300",
                labelFontColor: "rgba(255,255,255,.3)",
                gridColor: "rgba(255,255,255,.1)",
                interval: 60,
                intervalType: "second",
                gridThickness: 1,
                includeZero: false,
                crosshair: {
                    color: "#696f83",
                    lineDashType: "solid",
                    enabled: true,
                    snapToDataPoint: false
                }
            },
            axisY: {
                labelFontSize: 11,
                labelFontFamily: "Roboto",
                labelFontWeight: "300",
                labelFontColor: "rgba(255,255,255,.3)",
                gridColor: "rgba(255,255,255,.1)",
                gridThickness: 1,
                includeZero: false,
//                stripLines: stripLines,
                crosshair: {
                    enabled: true,
                    color: "#696f83",
                    lineDashType: "solid",
                    snapToDataPoint: false,
                    labelFormatter: function (e) {
                        return CanvasJS.formatNumber(e.value, "###.000");
                    }
                }
            },
            data: [{
//                    toolTipContent: "<small style='color:rgba(255,255,255,.6);font-size:70%'>{x}:</small><br/><strong>{y}</strong> ETH",
                    type: "candlestick",
//                    indexLabel: "{y}",
                    xValueType: "dateTime",
                    connectNullData: true,
                    indexLabelFontSize: 10,
                    indexLabelFontColor: "#EEEEEE",
                    indexLabelPlacement: "inside",
                    yValueFormatString: "###,###,###.#######",
                    xValueFormatString: "YYYY/MM/DD HH:mm:ss",
                    markerColor: '#ffffff',
                    markerBorderThickness: 1,
                    markerBorderColor: "rgba(62,62,66,.3)",
                    dataPoints: dataPoints
                }]
        }
        );
        chart.render();
    }
    initGlobalTime();
    initHistoryData();
    initChart();
};