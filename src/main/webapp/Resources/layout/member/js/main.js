$('#content').css('padding-top', $('header').height());
$('[data-toggle="tooltip"]').tooltip({container: 'body'});

$('.btn-copy').click(function () {
    var $target = $($(this).data('target'));
    $target.select();
    document.execCommand("copy");
    openAlert(null, function () {
        noty({text: 'Coppied', layout: 'topRight', type: 'success'});
    });
});

$('header .active-account .dropdown-menu label').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    var accountCoinTypeID = $(this).find('input[type="radio"][name="account-type"]').val();
    $.get("/Trading/ChangeAccount?accountCoinTypeID=" + accountCoinTypeID, function (res) {
        if (res) {
            location.reload();
        }
    });
});

$('#toggle-mb-menu').click(function () {
    if ($('#mb-menu').hasClass('open')) {
        $('#menu-overlay').fadeOut(300);
        $('#mb-menu').removeClass('open');
    } else {
        $('#menu-overlay').fadeIn(300);
        $('#mb-menu').addClass('open');
    }
});
$('#menu-overlay,#close-menu').click(function (e) {
    e.stopPropagation();
    $('#menu-overlay').fadeOut(300);
    $('#mb-menu').removeClass('open');
});

$('[data-sidebar]').click(function (e) {
    e.preventDefault();
    var url = $(this).data('sidebar');
    $('#sidebar-detail').addClass('loading');
    if (!$('#sidebar-detail').hasClass('open')) {
        $('#sidebar-detail').addClass('open');
        $('#sidebar-overlay').addClass('open');
    }
    $.get(url, function (data) {
        $('#sidebar-detail .sidebar-content').html(data);
        $('#sidebar-detail .sidebar-body').mCustomScrollbar({theme: "light-3"});
    }).always(function () {
        $('#sidebar-detail').removeClass('loading');
    });
});
$('#sidebar-detail .close-sidebar').click(function (e) {
    e.preventDefault();
    $('#sidebar-detail').removeClass('open');
    $('#sidebar-overlay').removeClass('open');
    $('#sidebar-detail .sidebar-content').html('');
});

$('[data-action="sync"]').click(function () {
    if ($(this).hasClass('disabled')) {
        return;
    }
    $(this).addClass('active disabled');
    var $this = $(this);
    $.get("/Transaction/CheckDeposit", function (data) {
        if (data) {
            location.reload();
        }
    }).always(function () {
        $this.removeClass('active disabled');
    });
});

$('.modal').on('show.bs.modal', function () {
    var zIndex = 1040 + (10 * $('.modal:visible').length);
    $(this).css('z-index', zIndex);
    setTimeout(function () {
        $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
    }, 0);
});
$('.modal').on('hidden.bs.modal', function () {
    $('.modal:visible').length && $(document.body).addClass('modal-open');
    $(this).find('label.error').remove();
    $(this).find('.error').removeClass('error');
    $(this).find('form').trigger("reset");
});

$('#spinner').fadeOut(0);
$('#preloader').delay(0).fadeOut('slow');