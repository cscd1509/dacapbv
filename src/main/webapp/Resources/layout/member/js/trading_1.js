window.onload = function () {
    var activeCoinCode = $('#trade-action .coin-type .dropdown-menu .dropdown-item.active .code').html();
    var activeCoinName = $('#trade-action .coin-type .dropdown-menu .dropdown-item.active .name').html();
    var dataPoints = new Array();
    var currentviewPortIndex = 1;
    var tempDate = new Date();
    tempDate.setMinutes(tempDate.getMinutes() + 2);
    var stripLines = [{
            value: null,
            label: null,
            showOnTop: true,
            labelAlign: "center",
            color: "rgba(97,105,140,.7)",
            labelFontColor: "#fff",
            labelBackgroundColor: "#696f83",
            labelFormatter: function (e) {
                return CanvasJS.formatNumber(e.stripLine.value, "###.000");
            },
            thickness: 1.5,
            labelPlacement: "outside"
        }];
    var options = {
        theme: "dark2",
        colorSet: "colorSet3",
        backgroundColor: "transparent",
        animationEnabled: true,
        animationDuration: 1000,
        zoomEnabled: true,
        toolTip: {
            animationEnabled: true, //disable here
            fontColor: "#fff"
        },
        title: {
            text: activeCoinName + " Trading",
            verticalAlign: "center",
            horizontalAlign: "center",
            fontColor: "rgba(255,255,255,.3)"
        }, axisX: {
            valueFormatString: "HH:mm:ss",
            labelFontSize: 9,
            labelFontFamily: "Roboto",
            labelFontWeight: "300",
            labelFontColor: "rgba(255,255,255,.3)",
            gridColor: "rgba(255,255,255,.1)",
            interval: 30,
            intervalType: "second",
            gridThickness: 1,
            includeZero: false,
            crosshair: {
                color: "#696f83",
                lineDashType: "solid",
                enabled: true,
                snapToDataPoint: false
            }
        }, axisY: {
//            valueFormatString: "###.000",
            labelFontSize: 11,
            labelFontFamily: "Roboto",
            labelFontWeight: "300",
            labelFontColor: "rgba(255,255,255,.3)",
            gridColor: "rgba(255,255,255,.1)",
            gridThickness: 1,
            includeZero: false,
//            interval: .5,
            stripLines: stripLines,
            crosshair: {
                enabled: true,
                color: "#696f83",
                lineDashType: "solid",
                snapToDataPoint: false,
                labelFormatter: function (e) {
                    return CanvasJS.formatNumber(e.value, "###.000");
                }
            }
        }, data: [{
                toolTipContent: "<small style='color:rgba(255,255,255,.6);font-size:70%'>{x}:</small><br/><strong>{y}</strong> ETH",
                name: null,
		type: "waterfall",
                xValueType: "dateTime",
                markerSize: 0,
                connectNullData: true,
                yValueFormatString: "###.000",
                highlightEnabled: true,
                xValueFormatString: "YYYY/MM/DD HH:mm:ss ",
                showInLegend: false,
                markerColor: '#ffffff',
                markerBorderThickness: 1,
                markerBorderColor: "rgba(62,62,66,.3)",
                dataPoints: dataPoints,
                color: "rgba(46,50,60,.6)",
                fillOpacity: 1,
                lineColor: "rgba(182,201,243,.8)",
                lineThickness: 4
            }
        ]
    };
    var chart = new CanvasJS.Chart("chartContainer", options);
    generateData();
    var current;
    var currentRate;
    var chartDataInterval;
    var chartDataIntervalRunning = false;
    function generateData() {
        $.getJSON("https://api.gdax.com/time", function (time) {
            current = new Date(time['iso']);
            $.getJSON("https://min-api.cryptocompare.com/data/histominute?fsym=" + activeCoinCode + "&tsym=USD&limit=5", function (data) {
                var diff = 0;
                $.each(data['Data'], function (key, value) {
                    if (value['time'] * 1000 >= current.getTime()) {
                        return;
                    }
                    var time = new Date(value['time'] * 1000);
                    dataPoints.push({x: time, y: value['close']});
                    var cloneTime = new Date(value['time'] * 1000);
                    if (key < data['Data'].length - 1) {
                        diff = (data['Data'][key + 1]['close'] - value['close']) / 60;
                    }
                    var i = 5;
                    while (i < 60) {
                        cloneTime.setSeconds(i);
                        if (cloneTime.getTime() >= current.getTime()) {
                            return;
                        }
                        dataPoints.push({x: cloneTime.getTime(), y: value['close']});
                        i += 5;
                    }
                });
                dataPoints[dataPoints.length - 1]['markerSize'] = 7;
                stripLines[0]['value'] = dataPoints[dataPoints.length - 1]['y'];
                stripLines[0]['label'] = dataPoints[dataPoints.length - 1]['y'];
                if (dataPoints[dataPoints.length - 2]['y'] < dataPoints[dataPoints.length - 1]['y']) {
                    stripLines[0]['labelBackgroundColor'] = '#2cac40';
                } else if (dataPoints[dataPoints.length - 2]['y'] > dataPoints[dataPoints.length - 1]['y']) {
                    stripLines[0]['labelBackgroundColor'] = '#db4931';
                } else {
                    stripLines[0]['labelBackgroundColor'] = '#696f83';
                }
//                
                chart.options.axisX.minimum = dataPoints[currentviewPortIndex]['x'];
                chart.options.axisX.maximum = 180000 + dataPoints[dataPoints.length - 1]['x'];
//                chart.render();                
                currentviewPortIndex++;
                $('#spinner').fadeOut();
                $('#preloader').delay(300).fadeOut('slow', function () {
                    chart.render();
                    changeChartAction();
                    changeCallStatus();
                    changeSecondLabel();
                    current.setSeconds(current.getSeconds() + 1);
                    setInterval(timeOutData, 100);
                });
            });
        });
    }
    function changeSecondLabel() {
        var second = 60 - current.getSeconds() - 1;
        $('#second-countdown').html(second < 10 ? "0" + second : second);
        if (second < 30) {
            $('#second-countdown').removeClass('text-success').addClass('text-danger');
        } else {
            $('#second-countdown').removeClass('text-danger').addClass('text-success');
        }
    }
    function timeOutData() {
        if (current.getMilliseconds() % 5000===0) {
            $.ajax({
                url: "https://min-api.cryptocompare.com/data/price?fsym=" + activeCoinCode + "&tsyms=USD",
                method: "GET",
                async: false,
                success: function (data) {
                    currentRate = data['USD'];
                    delete dataPoints[dataPoints.length - 1]['markerSize'];
                    dataPoints.push({x: current.getTime(), y: currentRate, markerSize: 7});
                    stripLines[0]['value'] = currentRate;
                    stripLines[0]['label'] = currentRate;
                    if (dataPoints[dataPoints.length - 2]['y'] < dataPoints[dataPoints.length - 1]['y']) {
                        stripLines[0]['labelBackgroundColor'] = '#2cac40';
                    } else if (dataPoints[dataPoints.length - 2]['y'] > dataPoints[dataPoints.length - 1]['y']) {
                        stripLines[0]['labelBackgroundColor'] = '#db4931';
                    } else {
                        stripLines[0]['labelBackgroundColor'] = '#696f83';
                    }
                    chart.options.axisX.minimum = dataPoints[currentviewPortIndex]['x'];
                    chart.options.axisX.maximum = 180000 + dataPoints[dataPoints.length - 1]['x'];
                    currentviewPortIndex++;
                }
            });
            chart.render();
        }
        changeChartAction();
        changeCallStatus();
        changeSecondLabel();
        current.setMilliseconds(current.getMilliseconds() + 100);
        console.log(current);
//        setTimeout(timeOutData, 1000);
    }

    function changeChartAction() {
        $('.current-rate').html(Math.round(dataPoints[dataPoints.length - 1]['y'] * 1000) / 1000);
        if (dataPoints[dataPoints.length - 1]['y'] > dataPoints[dataPoints.length - 2]['y']) {
            $('.coin-status').removeClass('text-danger').addClass('text-success');
            $('.coin-status .fas').removeClass('d-none');
            $('.coin-status .fas').addClass('fa-long-arrow-alt-up').removeClass('fa-long-arrow-alt-down');
        } else if (dataPoints[dataPoints.length - 1]['y'] < dataPoints[dataPoints.length - 2]['y']) {
            $('.coin-status').removeClass('text-success').addClass('text-danger');
            $('.coin-status .fas').removeClass('d-none');
            $('.coin-status .fas').removeClass('fa-long-arrow-alt-up').addClass('fa-long-arrow-alt-down');
        } else {
            $('.coin-status .fas').addClass('d-none');
            $('.coin-status').removeClass('text-danger').removeClass('text-success');
        }
    }

    function changeCallStatus() {
        if (current.getSeconds() + 1 > 30) {
            $('#content .right-sidebar .call').addClass('disabled');
        } else {
            $('#content .right-sidebar .call').removeClass('disabled');
        }
    }
    $('#content .right-sidebar .call').click(function (e) {
        e.preventDefault();
        if ($(this).hasClass('disabled')) {
            return;
        }
    });
};
randomVote();
//setInterval()
function randomVote() {
    var higherVote = Math.round((Math.random() * 10 + 45) * 10) / 10;
    var lowerVote = 100 - higherVote;
    $('#higher-vote').css('width', higherVote + '%');
    $('#lower-vote').css('width', lowerVote + '%');
    $('#higher-percent').html(higherVote + '%');
    $('#lower-percent').html(lowerVote + '%');
    var randomTimeout = Math.floor(Math.random() * 2000) + 1000;
    setTimeout(randomVote, randomTimeout);
}

$('#trading-amount').keydown(function (e) {
// Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            (e.keyCode === 67 && (e.ctrlKey === true || e.metaKey === true)) ||
            (e.keyCode === 88 && (e.ctrlKey === true || e.metaKey === true)) ||
            (e.keyCode >= 35 && e.keyCode <= 39)) {
        return;
    }
// Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105) || $(this).val().length >= 6) {
        e.preventDefault();
    }
});
$('#trading-amount').keyup(function () {
    var min = parseFloat($('#trading-amount').attr('min'));
    if ($(this).val() < min) {
        $('#trading-amount').val(min);
    }
    var amount = parseFloat($(this).val());
    var profit = $('#trading-amount').data('profit');
    $('#content .right-sidebar .profit .money .receive').html((amount * 2 * profit / 100).toFixed(4));
});
function changeTradingAmount(diff) {
    var min = parseFloat($('#trading-amount').attr('min'));
    var amount = parseFloat($('#trading-amount').val());
    if (amount + diff < min) {
        amount = min;
    } else {
        amount = (amount + diff).toFixed(3);
    }
    $('#trading-amount').val(amount);
    var profit = $('#trading-amount').data('profit');
    $('#content .right-sidebar .profit .money .receive').html((amount * 2 * profit / 100).toFixed(4));
}
$('#content .right-sidebar .amount .bottom span.up').click(function () {
    var step = parseFloat($('#trading-amount').attr('step'));
    changeTradingAmount(step);
});
$('#content .right-sidebar .amount .bottom span.down').click(function () {
    var step = parseFloat($('#trading-amount').attr('step'));
    changeTradingAmount(-step);
});
$('.coin-search input').keyup(function () {
    var keyword = $(this).val().trim().toUpperCase();
    if (!keyword.length) {
        $('#trade-action .coin-type .dropdown-menu .dropdown-item.available-coin').removeClass('d-none');
    }
    $('#trade-action .coin-type .dropdown-menu .dropdown-item.available-coin').each(function () {
        var name = $(this).children('.name').html().toUpperCase();
        var code = $(this).children('.code').html().toUpperCase();
        if (name.indexOf(keyword) !== -1 || code.indexOf(keyword) !== -1) {
            $(this).removeClass('d-none');
        } else {
            $(this).addClass('d-none');
        }
    });
});