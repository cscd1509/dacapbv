var activeCoinID = $('#trade-action .coin-type .dropdown-menu .dropdown-item.active').data('id');
var activeCoinCode = $('#trade-action .coin-type .dropdown-menu .dropdown-item.active .code').html();
var activeCoinName = $('#trade-action .coin-type .dropdown-menu .dropdown-item.active .name').html();
var currentExchangeRate;
var globalTime;
var hasTrading = true;
var chart;
$(document).ready(function () {
    var dataPoints = [];
    var limitPerviewPort = Math.floor($('#content .main-content').width() / 2 / getSizeByDevice(19, 8));
//    console.log(limitPerviewPort);
    function initHistoryData() {
        getDataRate(1000, false, function (data) {
            var i = 0;
//            var lastClose;
            var lastMinute = new Date(data['Data'][data['Data'].length - 1]['time'] * 1000).getMinutes();
            lastMinute = lastMinute > 29 ? lastMinute - 30 : lastMinute;
            while (i < data['Data'].length) {
                var value = data['Data'][i];
                var x = new Date(value['time'] * 1000);
                var open = value['open'];
//                if (typeof lastClose !== 'undefined') {
//                    open = lastClose;
//                } else {
//                    open = value['open'];
//                }
                var close = value['close'];
                var low = value['low'];
                var high = value['high'];
                if (close < low) {
                    low = close;
                }
                if (close > high) {
                    high = close;
                }
                var color = genderPointColor(open, close);
                var y = [open, low, high, close];
                dataPoints.push({
                    x: x, y: y, color: color
                });
//                lastClose = value['close'];
                if (i >= data['Data'].length - lastMinute - 1) {
                    var direction = 'equal';
                    if (open < close) {
                        direction = 'up';
                    } else if (close < open) {
                        direction = 'down';
                    }
                    genderHistoryDot(lastMinute + 2 - (data['Data'].length - i), direction);
                }
                if (data['Data'].length) {

                }
                i++;
            }
            renderViewport(true);
        });
    }
    function getDirectionForHistoryDot(open, close) {
        var direction = 'equal';
        if (open < close) {
            direction = 'up';
        } else if (close < open) {
            direction = 'down';
        }
        return direction;
    }
    function genderHistoryDot(position, direction) {
        var $dot = $('#content .right-sidebar .history-rate>li:nth-child(' + position + ') .dot');
        $('#content .right-sidebar .history-rate>li .dot.current').removeClass('current');
        $dot.removeClass('up down equal').addClass(direction + ' active current');
    }
    function genderNewHistoryDot(direction) {
        var $dot = $('#content .right-sidebar .history-rate>li .dot.current');
        var currentIndex = $dot.index('#content .right-sidebar .history-rate>li .dot');
        if (currentIndex === 29) {
            currentIndex = 0;
            $('#content .right-sidebar .history-rate>li .dot').removeClass('active equal down up');
        } else {
            currentIndex++;
        }
        genderHistoryDot(currentIndex + 1, direction);
    }
    function genderCurrentHistoryDot(direction) {
        var $dot = $('#content .right-sidebar .history-rate>li .dot.current');
        $dot.removeClass('up down equal').addClass(direction);
    }
    function genderPointColor(open, close) {
        var color = pointColor;
        if (open < close) {
            color = risingColor;
        } else if (open > close) {
            color = fallingColor;
        }
        return color;
    }
    var maximum;
    var minimum;
    function renderViewport(first) {
        if (first) {
            var minIndex = 0;
            minIndex = dataPoints.length - limitPerviewPort - 1;
            minIndex = minIndex < 0 ? 0 : minIndex;
            minimum = new Date(dataPoints[minIndex]['x'].getTime());
//            minimum.setMinutes(minimum.getMinutes()-1);
            minimum.setSeconds(40);
            minimum.setMilliseconds(0);
            maximum = new Date(dataPoints[dataPoints.length - 1]['x'].getTime() + (60000 * 5));
            maximum.setSeconds(0);
            maximum.setMilliseconds(0);
        } else {
            minimum.setMinutes(minimum.getMinutes() + 1);
            maximum.setMinutes(maximum.getMinutes() + 1);
        }
    }

    var closing = false;
    function closeTrading(blockTime, blockDirection) {
        $.ajax({
            url: "/Trading/Close",
            method: "POST",
            data: {
                'blockTime': blockTime,
                'blockDirection': blockDirection
            },
            success: function (data) {
                $('.account-type-amount').html(Math.round(data * 1000000000000) / 1000000000000);
                hasTrading = false;
            }
        });
    }
    function intervalData() {
        renderSecondLabel();
        renderCallStatus();
        if (globalTime.getSeconds() === 0) {
//            destroyWorker();
            closing = true;
            $('#total-buy,#total-sell').html('0.00');
            getDataRate(1, true, function (data) {
//                console.log(JSON.stringify(data['Data'][data['Data'].length - 2]));
//                console.log(JSON.stringify(data['Data'][data['Data'].length - 1]));
                var value = data['Data'][1];
                var x = new Date(value['time'] * 1000);
                var before = data['Data'][0];
//                console.log(new Date(before['time'] * 1000) + ' - ' + globalTime);
                var last = dataPoints[dataPoints.length - 1];
                if (x.getTime() > last['x'].getTime()) {
                    if (hasTrading) {
                        var blockTime = before['time'] * 1000;
                        var blockDirection = 0;
                        if (before['close'] > before['open']) {
                            blockDirection = 1;
                        } else if (before['close'] < before['open']) {
                            blockDirection = -1;
                        }
                        setTimeout(closeTrading(blockTime, blockDirection), 50);
                    }
                    var beforeOpen = before['open'];
                    var beforeLow = before['low'];
                    var beforeHigh = before['high'];
                    var beforeClose = before['close'];
                    if (beforeClose < beforeLow) {
                        beforeLow = beforeClose;
                    }
                    if (beforeClose > beforeHigh) {
                        beforeHigh = beforeClose;
                    }
                    var color = genderPointColor(beforeOpen, beforeClose);
                    dataPoints[dataPoints.length - 1]['y'] = [beforeOpen, beforeLow, beforeHigh, beforeClose];
                    dataPoints[dataPoints.length - 1]['color'] = color;
                    var open = value['open'];
                    var low = value['low'];
                    var high = value['high'];
                    var close = value['close'];
                    color = genderPointColor(open, close);
                    dataPoints.push({
                        x: x, y: [open, low, high, close], color: color
                    });
                    renderStriplinesX();
                    renderStriplinesY();
                    renderChartAction();
                    renderViewport(false);
                    chart.render();
                    var beforeDirection = getDirectionForHistoryDot(beforeOpen, beforeClose);
                    var direction = getDirectionForHistoryDot(open, close);
                    genderCurrentHistoryDot(beforeDirection);
                    genderNewHistoryDot(direction);
                }
                closing = false;
            });
        } else if (!closing) {
            $.ajax({
                url: "https://min-api.cryptocompare.com/data/price?fsym=" + activeCoinCode + "&tsyms=USD&e=" + market,
                method: "GET",
                async: false,
                success: function (data) {
                    var close = currentExchangeRate = data['USD'];
                    var open = dataPoints[dataPoints.length - 1]['y'][0];
                    var beforeClose = dataPoints[dataPoints.length - 2]['y'][3];
                    if (close === open) {
                        beforeClose = dataPoints[dataPoints.length - 2]['y'][0];
                    }
                    var max = close;
                    var min = close;
                    if (close > beforeClose) {
                        min = (4 * beforeClose - close) / 3;
                    } else {
                        max = (4 * beforeClose - close) / 3;
                    }
                    close = Math.random() * (max - min) + min;
                    close = Math.round(close * 100) / 100;
                    var low = dataPoints[dataPoints.length - 1]['y'][1];
                    low = low > close ? close : low;
                    var high = dataPoints[dataPoints.length - 1]['y'][2];
                    high = high < close ? close : high;
                    dataPoints[dataPoints.length - 1]['y'][1] = low;
                    dataPoints[dataPoints.length - 1]['y'][2] = high;
                    dataPoints[dataPoints.length - 1]['y'][3] = close;
                    var color = genderPointColor(open, close);
                    dataPoints[dataPoints.length - 1]['color'] = color;
                    renderStriplinesX();
                    renderStriplinesY();
                    renderChartAction();
                    chart.render();
                    var direction = getDirectionForHistoryDot(open, close);
                    genderCurrentHistoryDot(direction);
                }
            });
        }
    }
    var market = 'Bitfinex';
    function getDataRate(limit, async, callback) {
        $.ajax({
            url: "https://min-api.cryptocompare.com/data/histominute",
            method: "GET",
            async: async,
            data: {
                fsym: activeCoinCode,
                tsym: 'USD',
                limit: limit,
                e: market
//                tryConversion: false
            },
            success: callback
        });
    }
    var w = null;
    function initTradingWorker() {
        if (typeof (Worker) !== "undefined") {
            // Check whether Web Worker has been created. If not, create a new Web Worker based on the Javascript file simple-timer.js
            if (w === null) {
                w = new Worker("/Resources/layout/member/js/trading-worker.js");
            }
            // Update timer div with output from Web Worker
            initGlobalTime();
            w.postMessage(globalTime);
            w.onmessage = function (e) {
                globalTime = e.data;
                intervalData();
            };
        } else {
            // Web workers are not supported by your browser
            alert("Sorry, your browser does not support. Please update your browser.");
        }
    }

    function renderSecondLabel() {
        var second = 60 - globalTime.getSeconds() - 1;
        $('#second-countdown').html(second < 10 ? "0" + second : second);
        if (second < 30) {
            $('#second-countdown').removeClass('text-success').addClass('text-danger');
        } else {
            $('#second-countdown').removeClass('text-danger').addClass('text-success');
        }
    }

    function renderCallStatus() {
        if (globalTime.getSeconds() >= 30) {
            $('#content .right-sidebar .call').addClass('disabled');
        } else {
            $('#content .right-sidebar .call').removeClass('disabled');
        }
    }

    function renderStriplinesY() {
        stripLinesY[0]['value'] = dataPoints[dataPoints.length - 1]['y'][3];
        stripLinesY[0]['label'] = dataPoints[dataPoints.length - 1]['y'][3];
        if (dataPoints[dataPoints.length - 2]['y'][3] < stripLinesY[0]['value']) {
            stripLinesY[0]['labelBackgroundColor'] = '#2cac40';
        } else if (dataPoints[dataPoints.length - 2]['y'][3] > stripLinesY[0]['value']) {
            stripLinesY[0]['labelBackgroundColor'] = '#db4931';
        } else {
            stripLinesY[0]['labelBackgroundColor'] = '#030304';
        }
    }

    function renderStriplinesX() {
        stripLinesX[0]['value'] = dataPoints[dataPoints.length - 1]['x'];
        stripLinesX[0]['label'] = globalTime;
    }

    function renderChartAction() {
        var currentRate = dataPoints[dataPoints.length - 1]['y'][3];
        $('.current-rate').html(currentRate);
        if (currentRate > dataPoints[dataPoints.length - 1]['y'][0]) {
            $('.coin-status').removeClass('text-danger').addClass('text-success');
            $('.coin-status .fas').removeClass('d-none');
            $('.coin-status .fas').addClass('fa-long-arrow-alt-up').removeClass('fa-long-arrow-alt-down');
        } else if (currentRate < dataPoints[dataPoints.length - 1]['y'][0]) {
            $('.coin-status').removeClass('text-success').addClass('text-danger');
            $('.coin-status .fas').removeClass('d-none');
            $('.coin-status .fas').removeClass('fa-long-arrow-alt-up').addClass('fa-long-arrow-alt-down');
        } else {
            $('.coin-status .fas').addClass('d-none');
            $('.coin-status').removeClass('text-danger').removeClass('text-success');
        }
    }

    function initGlobalTime() {
        $.ajax({
            url: "https://api.gdax.com/time",
            method: "GET",
            async: false,
            success: function (data) {
                globalTime = new Date(data['iso']);
                globalTime.setSeconds(globalTime.getSeconds() - 5);
                globalTime.setMilliseconds(0);
            }
        });
    }
    function getSizeByDevice(desktop, mobile) {
        return $(window).width() > 575 ? desktop : mobile;
    }
    var stripLinesY = [{
            value: null,
            label: null,
            labelAlign: "far",
            color: "rgba(255,255,255,1)",
            labelFontColor: "#fff",
            labelBackgroundColor: "#030304",
            labelFontFamily: 'Roboto',
            labelFontSize: 10,
            labelFormatter: function (e) {
                return CanvasJS.formatNumber(e.stripLine.value, "###.00");
            },
            thickness: 1,
//            labelPlacement: "outside"
        }];
    var stripLinesX = [{
            value: null,
            label: null,
            labelAlign: "far",
            color: "rgba(255,255,255,1)",
            labelFontColor: "#fff",
            labelFontSize: 10,
            labelBackgroundColor: "#030304",
            labelFormatter: function (e) {
                return CanvasJS.formatDate(e.stripLine.label, "HH:mm:ss");
            },
            thickness: 1,
            labelFontFamily: 'Roboto',
//            labelPlacement: "outside"
        }];
    var fallingColor = "#db4931";
    var risingColor = "#2cac40";
    var pointColor = "rgba(255,255,255,.6)";
    function initChart() {
        chart = new CanvasJS.Chart("chartContainer", {
            theme: "dark1",
            colorSet: "colorSet2",
            backgroundColor: "rgba(14, 19, 33, 0.76)",
            animationEnabled: true,
            zoomEnabled: getSizeByDevice(true, true),
            zoomType: "xy",
            toolTip: {
                enabled: getSizeByDevice(true, false),
                animationEnabled: true,
                fontColor: "#fff"
            },
            title: {
                text: activeCoinName + " Trading",
                verticalAlign: "center",
                horizontalAlign: "center",
                fontColor: "rgba(255,255,255,.3)"
            },
            axisX: {
                valueFormatString: "HH:mm",
                labelFontSize: getSizeByDevice(10, 11),
                labelFontFamily: "Roboto",
                labelFontWeight: "300",
                labelFontColor: "rgba(255,255,255,.5)",
                gridColor: "rgba(255,255,255,.1)",
                tickColor: "transparent",
                lineColor: "transparent",
                interval: getSizeByDevice(60, 300),
                intervalType: "second",
                gridThickness: 1,
                tickThickness: 0,
                tickLength: getSizeByDevice(15, 10),
                includeZero: false,
                viewportMinimum: minimum,
                maximum: maximum,
                stripLines: stripLinesX,
                crosshair: {
                    color: "#696f83",
                    lineDashType: "shortDashDot",
                    enabled: getSizeByDevice(true,false),
                    snapToDataPoint: false
                }
            },
            axisY: {
                labelFontSize: 10,
                labelFontFamily: "Roboto",
                labelFontWeight: "300",
                labelFontColor: "rgba(255,255,255,.5)",
                gridColor: "rgba(255,255,255,.1)",
                gridThickness: 1,
                includeZero: false,
                stripLines: stripLinesY,
                tickColor: "transparent",
                lineColor: "transparent",
                tickLength: getSizeByDevice(10, 5),
//                interlacedColor: "rgba(255,255,255,.02)",
                crosshair: {
                    enabled: getSizeByDevice(true,false),
                    color: "#696f83",
                    lineDashType: "shortDashDot",
                    snapToDataPoint: false,
                    labelFontSize: 9,
                    labelFormatter: function (e) {
                        return CanvasJS.formatNumber(e.value, "###.##");
                    }
                }
            },
            dataPointMaxWidth: getSizeByDevice(15, 7.5),
            data: [{
                    toolTipContent: "<small style='color:rgba(255,255,255,.6);font-size:70%'>{x}:</small>" +
                            "<br/>Open: <strong>{y[0]}</strong> USD" +
                            "<br/>High: <strong>{y[2]}</strong> USD" +
                            "<br/>Low: <strong>{y[1]}</strong> USD" +
                            "<br/>Close: <strong>{y[3]}</strong> USD",
                    type: "candlestick",
                    xValueType: "dateTime",
                    connectNullData: true,
                    yValueFormatString: "###,###,###.###",
                    xValueFormatString: "YYYY/MM/DD HH:mm:ss",
                    markerColor: '#ffffff',
                    markerBorderThickness: 1,
                    markerBorderColor: "rgba(62,62,66,.3)",
                    color: pointColor,
                    fillOpacity: 1,
                    fallingColor: fallingColor,
                    risingColor: risingColor,
                    lineThickness: 0,
                    lineColor: "red",
                    dataPoints: dataPoints
                }]
        });
        renderStriplinesX();
        renderStriplinesY();
        chart.render();
    }
    $('#content .right-sidebar .history-rate').html('<li><span class="dot"></span></li>'.times(30));
    initHistoryData();
    initChart();
//    initGlobalTime();
    initTradingWorker();
    $('#spinner').fadeOut(0);
    $('#preloader').delay(0).fadeOut('slow');
//    intervalDataArg = setInterval(intervalData, 1000);
});

randomVote();
function randomVote() {
    var higherVote = Math.round((Math.random() * 10 + 45) * 10) / 10;
    var lowerVote = 100 - higherVote;
    $('#higher-vote').css('width', higherVote + '%');
    $('#lower-vote').css('width', lowerVote + '%');
    $('#higher-percent').html(higherVote + '%');
    $('#lower-percent').html(lowerVote + '%');
    var randomTimeout = Math.floor(Math.random() * 2000) + 1000;
    setTimeout(randomVote, randomTimeout);
}

$('#trading-amount').keydown(function (e) {
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            (e.keyCode === 67 && (e.ctrlKey === true || e.metaKey === true)) ||
            (e.keyCode === 88 && (e.ctrlKey === true || e.metaKey === true)) ||
            (e.keyCode >= 35 && e.keyCode <= 39)) {
        return;
    }
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105) || $(this).val().length >= 6) {
        e.preventDefault();
    }
});
$('#trading-amount').keyup(function () {
    var min = parseFloat($('#trading-amount').attr('min'));
    if ($(this).val() < min) {
        $('#trading-amount').val(min);
    }
    var amount = parseFloat($(this).val());
    $('#content .right-sidebar .profit .money .receive').html((amount * 180 / 100).toFixed(4));
});
function changeTradingAmount(diff) {
    var min = parseFloat($('#trading-amount').attr('min'));
    var amount = parseFloat($('#trading-amount').val());
    if (amount + diff < min) {
        amount = min;
    } else {
        amount = (amount + diff).toFixed(3);
    }
    $('#trading-amount').val(amount);
    $('#content .right-sidebar .profit .money .receive').html((amount * 180 / 100).toFixed(4));
}
$('#content .right-sidebar .amount .bottom span.up').click(function () {
    var step = parseFloat($('#trading-amount').attr('step'));
    changeTradingAmount(step);
});
$('#content .right-sidebar .amount .bottom span.down').click(function () {
    var step = parseFloat($('#trading-amount').attr('step'));
    changeTradingAmount(-step);
});
$('.coin-search input').keyup(function () {
    var keyword = $(this).val().trim().toUpperCase();
    if (!keyword.length) {
        $('#trade-action .coin-type .dropdown-menu .dropdown-item.available-coin').removeClass('d-none');
    }
    $('#trade-action .coin-type .dropdown-menu .dropdown-item.available-coin').each(function () {
        var name = $(this).children('.name').html().toUpperCase();
        var code = $(this).children('.code').html().toUpperCase();
        if (name.indexOf(keyword) !== -1 || code.indexOf(keyword) !== -1) {
            $(this).removeClass('d-none');
        } else {
            $(this).addClass('d-none');
        }
    });
});

$('#content .right-sidebar .call').click(function (e) {
    e.preventDefault();
    if ($(this).hasClass('disabled') || globalTime.getSeconds() > 29) {
        return;
    }
    var amount = parseFloat($('#trading-amount').val());
    var min = parseFloat($('#trading-amount').attr('min'));
    if (amount < min) {
        return;
    }
    var direction = $(this).hasClass('call-up') ? 1 : -1;
    var data = {
        'amount': amount,
        'direction': direction,
        'time': globalTime.getTime()
    };
    $.ajax({
        url: "/Trading/Call",
        method: "POST",
        data: data,
        success: function (data) {
            openAlert(data.value);
            if (data.key) {
                switch (direction) {
                    case 1:
                    {
                        var totalBuy = amount + parseFloat($('#total-buy').html());
                        $('#total-buy').html(Math.round(totalBuy * 100000000) / 100000000);
                        break;
                    }
                    case -1:
                    {
                        var totalSell = amount + parseFloat($('#total-sell').html());
                        $('#total-sell').html(Math.round(totalSell * 100000000) / 100000000);
                        break;
                    }
                }
                $('.account-type-amount').html(Math.round(($('.account-type-amount').html() - amount) * 100000000) / 100000000);
                hasTrading = true;
            }
        },
        error: function () {
            openAlert(null, function () {
                noty({text: 'Have error. Try again later!', layout: 'topRight', type: 'error'});
            });
        }
    });
});
$('#content .right-sidebar .call').mouseenter(function () {
    if ($(this).hasClass('disabled')) {
        return;
    }
    if ($(this).hasClass('call-up')) {
        chart.options.backgroundColor = "rgba(44, 172, 64,.1)";
    } else {
        chart.options.backgroundColor = "rgba(219, 73, 49,.1)";
    }
    chart.render();
});
$('#content .right-sidebar .call').mouseleave(function () {
    chart.options.backgroundColor = "rgba(0,0,0,.8)";
    chart.render();
});

$('#trade-action .coin-type .dropdown-menu .dropdown-item.available-coin').click(function (e) {
    e.preventDefault();
    if ($(this).hasClass('active')) {
        return;
    }
    var currencyID = $(this).data('id');
    $.ajax({
        url: "/Trading/ChangeCurrency",
        method: "POST",
        data: {
            currency: currencyID
        },
        success: function (data) {
            if (data) {
                location.reload();
            }
        }
    });
});
String.prototype.times = function (n) {
    return Array.prototype.join.call({length: n + 1}, this);
};
//$(window).on("blur focus", function (e) {
//    var prevType = $(this).data("prevType");
//    if (prevType !== e.type && e.type === 'focus') {
//        location.reload();
//    }
//    $(this).data("prevType", e.type);
//});