var globalTime;
onmessage = (e) => {
    globalTime = e.data;
    setInterval(intervalGlobalTime, 1000);
};
function intervalGlobalTime() {
    globalTime.setSeconds(globalTime.getSeconds() + 1);
    postMessage(globalTime);
}