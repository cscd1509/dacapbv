package com.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "HistoryAward")
public class HistoryAward implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", insertable = false, updatable = false)
    private int id;

    @Column(name = "Name", insertable = false, updatable = false)
    private String name;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CreatedDate", insertable = false, updatable = false)
    private Date createdDate;

    @Column(name = "Amount", insertable = false, updatable = false)
    private BigDecimal amount;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CustomerID", updatable = false, insertable = false)
    private Customer customerID;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PartnerID", updatable = false, insertable = false)
    private Customer partnerID;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CheckAwardID", updatable = false, insertable = false)
    private CheckAward checkAwardID;

    @Column(name = "IsDeleted", insertable = false, updatable = false)
    private boolean isDeleted;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CircleMumber", updatable = false, insertable = false)
    private CircleWithdraw circleWithdrawID;
    
    @Column(name = "IsAvailability", insertable = false, updatable = false)
    private Boolean isAvailability;

    public HistoryAward() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Customer getCustomerID() {
        return customerID;
    }

    public void setCustomerID(Customer customerID) {
        this.customerID = customerID;
    }

    public Customer getPartnerID() {
        return partnerID;
    }

    public void setPartnerID(Customer partnerID) {
        this.partnerID = partnerID;
    }

    public CheckAward getCheckAwardID() {
        return checkAwardID;
    }

    public void setCheckAwardID(CheckAward checkAwardID) {
        this.checkAwardID = checkAwardID;
    }

    public boolean isIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public CircleWithdraw getCircleWithdrawID() {
        return circleWithdrawID;
    }

    public void setCircleWithdrawID(CircleWithdraw circleWithdrawID) {
        this.circleWithdrawID = circleWithdrawID;
    }

    public Boolean getIsAvailability() {
        return isAvailability;
    }

    public void setIsAvailability(Boolean isAvailability) {
        this.isAvailability = isAvailability;
    }    

}
