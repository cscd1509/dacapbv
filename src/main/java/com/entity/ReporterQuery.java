package com.entity;
// Generated Sep 23, 2018 2:57:32 PM by Hibernate Tools 4.3.1

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "ReporterQuery")
public class ReporterQuery implements java.io.Serializable {

    private int id;
    private String name;
    private String sQlQuery;
    private Boolean isDeleted;
    private Boolean isActive;
    private List<ReporterQueryAlias> reporterQueryAliases;

    public ReporterQuery() {
    }

    public ReporterQuery(int id) {
        this.id = id;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", insertable = false, updatable = false)
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "Name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "SQlQuery")
    public String getsQlQuery() {
        return sQlQuery;
    }

    public void setsQlQuery(String sQlQuery) {
        this.sQlQuery = sQlQuery;
    }

    @Column(name = "isDeleted", insertable = false, updatable = false)
    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    @Column(name = "IsActive")
    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    @Transient
    public List<ReporterQueryAlias> getReporterQueryAliases() {
        return reporterQueryAliases;
    }

    public void setReporterQueryAliases(List<ReporterQueryAlias> reporterQueryAliases) {
        this.reporterQueryAliases = reporterQueryAliases;
    }

}
