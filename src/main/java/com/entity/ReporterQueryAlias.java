package com.entity;
// Generated Sep 23, 2018 2:57:32 PM by Hibernate Tools 4.3.1

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ReporterQueryAlias")
public class ReporterQueryAlias implements java.io.Serializable {
    private int id;
    private String columnName;
    private String aliasName;
    private String aliasType;
    private ReporterQuery reporterQueryID;

    public ReporterQueryAlias() {
    }

    public ReporterQueryAlias(int id) {
        this.id = id;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", insertable = false, updatable = false)
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "ColumnName")
    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    @Column(name = "AliasName")
    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    @Column(name = "AliasType")
    public String getAliasType() {
        return aliasType;
    }

    public void setAliasType(String aliasType) {
        this.aliasType = aliasType;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ReporterQueryID")
    public ReporterQuery getReporterQueryID() {
        return reporterQueryID;
    }

    public void setReporterQueryID(ReporterQuery reporterQueryID) {
        this.reporterQueryID = reporterQueryID;
    }

}
