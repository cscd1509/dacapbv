package com.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "SystemConfig")
public class SystemConfig implements Serializable {

    @Id
    @Column(name = "ID", insertable = false, updatable = false)
    private int id;
    
    @Column(name = "Name")
    private String name;
    
    @Column(name = "IsTrue")
    private Boolean isTrue;
    
    @Column(name = "IsDelete")
    private boolean isDelete;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SystemConfig(int id) {
        this.id = id;
    }

    public SystemConfig() {
    }

    public boolean getIsTrue() {
        return isTrue;
    }

    public void setIsTrue(Boolean isTrue) {
        this.isTrue = isTrue;
    }

    public Boolean getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(boolean isDelete) {
        this.isDelete = isDelete;
    }
}
