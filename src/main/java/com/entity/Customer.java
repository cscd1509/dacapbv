package com.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "Customer")
public class Customer implements java.io.Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private int id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ParentID", insertable = false, updatable = false)
    private Customer parentID;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "RefID", insertable = false, updatable = false)
    private Customer refID;

    @Column(name = "FullName", insertable = false, updatable = false)
    private String fullName;

    @Column(name = "UserName", insertable = false, updatable = false)
    private String userName;

    @Column(name = "Email", insertable = false, updatable = false)
    private String email;

    @Column(name = "Gender", insertable = false, updatable = false)
    private Boolean gender;

    @Column(name = "Password", insertable = false, updatable = false)
    private String password;

    @Column(name = "PasswordTemp", insertable = false, updatable = true)
    private String passwordTemp;

    @Column(name = "Mobile", insertable = false, updatable = false)
    private String mobile;

    @Column(name = "PeoplesIdentity", insertable = false, updatable = false)
    private String peoplesIdentity;

    @Column(name = "BankName", insertable = false, updatable = false)
    private String bankName;

    @Column(name = "BankNumber", insertable = false, updatable = false)
    private String bankNumber;

    @Column(name = "BankAgency", insertable = false, updatable = false)
    private String bankAgency;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CreatedDate", length = 23, insertable = false, updatable = false)
    private Date createdDate;

    @Column(name = "IsActive", insertable = false)
    private Boolean isActive;

    @Column(name = "IsDeleted", insertable = false)
    private Boolean isDeleted;

    @Column(name = "ListCustomerId", insertable = false, updatable = false)
    private String listCustomerID;

    @Column(name = "ListCustomerRef", insertable = false, updatable = false)
    private String listCustomerRef;

    @Column(name = "level", insertable = false, updatable = false)
    private Integer level;

    @Column(name = "levecustomerRef", insertable = false, updatable = false)
    private Integer levelCustomerRef;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "RankCustomer", insertable = false, updatable = false)
    private RankName rankCustomer;

    @Column(name = "TotalAmount", insertable = false, updatable = false)
    private BigDecimal totalAmount;

    @Column(name = "TotalDeposit", insertable = false, updatable = false)
    private BigDecimal totalDeposit;

    @Column(name = "TotalLeft", insertable = false, updatable = false)
    private BigDecimal totalLeft;

    @Column(name = "TotalRight", insertable = false, updatable = false)
    private BigDecimal totalRight;

    @Column(name = "TotalBalance", insertable = false, updatable = false)
    private BigDecimal totalBalance;

    @Column(name = "SystemAmount", insertable = false, updatable = false)
    private BigDecimal systemAmount;

    @Column(name = "ReserveLeft", insertable = false, updatable = false)
    private BigDecimal reserveLeft;

    @Column(name = "ReserveRigh", insertable = false, updatable = false)
    private BigDecimal reserveRight;

    @Column(name = "TotalMoneyInMonth", insertable = false, updatable = false)
    private BigDecimal totalMoneyInMonth;

    @Column(name = "AwardsInMonth", insertable = false, updatable = false)
    private BigDecimal awardsInMonth;

    @Column(name = "IsBackUp", insertable = false, updatable = false)
    private Boolean isBackUp;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "parentID")
    private Set<Customer> customersForParentId = new HashSet<>(0);

    public Customer() {
    }

    public Customer(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Customer getParentID() {
        return parentID;
    }

    public void setParentID(Customer parentID) {
        this.parentID = parentID;
    }

    public Customer getRefID() {
        return refID;
    }

    public void setRefID(Customer refID) {
        this.refID = refID;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getGender() {
        return gender;
    }

    public void setGender(Boolean gender) {
        this.gender = gender;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordTemp() {
        return passwordTemp;
    }

    public void setPasswordTemp(String passwordTemp) {
        this.passwordTemp = passwordTemp;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPeoplesIdentity() {
        return peoplesIdentity;
    }

    public void setPeoplesIdentity(String peoplesIdentity) {
        this.peoplesIdentity = peoplesIdentity;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public RankName getRankCustomer() {
        return rankCustomer;
    }

    public void setRankCustomer(RankName rankCustomer) {
        this.rankCustomer = rankCustomer;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getListCustomerID() {
        return listCustomerID;
    }

    public void setListCustomerID(String listCustomerID) {
        this.listCustomerID = listCustomerID;
    }

    public Set<Customer> getCustomersForParentId() {
        return customersForParentId;
    }

    public void setCustomersForParentId(Set<Customer> customersForParentId) {
        this.customersForParentId = customersForParentId;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankNumber() {
        return bankNumber;
    }

    public void setBankNumber(String bankNumber) {
        this.bankNumber = bankNumber;
    }

    public String getBankAgency() {
        return bankAgency;
    }

    public void setBankAgency(String bankAgency) {
        this.bankAgency = bankAgency;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getListCustomerRef() {
        return listCustomerRef;
    }

    public void setListCustomerRef(String listCustomerRef) {
        this.listCustomerRef = listCustomerRef;
    }

    public BigDecimal getTotalDeposit() {
        return totalDeposit;
    }

    public void setTotalDeposit(BigDecimal totalDeposit) {
        this.totalDeposit = totalDeposit;
    }

    public Boolean getIsBackUp() {
        return isBackUp;
    }

    public void setIsBackUp(Boolean isBackUp) {
        this.isBackUp = isBackUp;
    }

    public BigDecimal getTotalLeft() {
        return totalLeft;
    }

    public void setTotalLeft(BigDecimal totalLeft) {
        this.totalLeft = totalLeft;
    }

    public BigDecimal getTotalRight() {
        return totalRight;
    }

    public void setTotalRight(BigDecimal totalRight) {
        this.totalRight = totalRight;
    }

    public BigDecimal getTotalBalance() {
        return totalBalance;
    }

    public void setTotalBalance(BigDecimal totalBalance) {
        this.totalBalance = totalBalance;
    }

    public BigDecimal getSystemAmount() {
        return systemAmount;
    }

    public void setSystemAmount(BigDecimal systemAmount) {
        this.systemAmount = systemAmount;
    }

    public BigDecimal getReserveLeft() {
        return reserveLeft;
    }

    public void setReserveLeft(BigDecimal reserveLeft) {
        this.reserveLeft = reserveLeft;
    }

    public BigDecimal getReserveRight() {
        return reserveRight;
    }

    public void setReserveRight(BigDecimal reserveRight) {
        this.reserveRight = reserveRight;
    }

    public BigDecimal getTotalMoneyInMonth() {
        return totalMoneyInMonth;
    }

    public void setTotalMoneyInMonth(BigDecimal totalMoneyInMonth) {
        this.totalMoneyInMonth = totalMoneyInMonth;
    }

    public BigDecimal getAwardsInMonth() {
        return awardsInMonth;
    }

    public void setAwardsInMonth(BigDecimal awardsInMonth) {
        this.awardsInMonth = awardsInMonth;
    }

    public Integer getLevelCustomerRef() {
        return levelCustomerRef;
    }

    public void setLevelCustomerRef(Integer levelCustomerRef) {
        this.levelCustomerRef = levelCustomerRef;
    }
    

}
