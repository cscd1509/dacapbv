package com.controller.user;

import com.bean.Alert;
import com.bean.MailTemplate;
import com.entity.Customer;
import com.facade.CustomerFacade;
import com.utils.CaptchaUtil;
import com.utils.CustomFunction;
import com.utils.MailUtils;
import com.utils.StringUtils;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.util.Pair;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("")
public class CustomerController {

    @RequestMapping(value = {"/Login"}, method = RequestMethod.GET)
    public String Login(HttpSession session) {
        session.removeAttribute("TEMP_MEMBER_ID");
        return "MemberLogin";
    }

    @RequestMapping(value = {"/Login"}, method = RequestMethod.POST, produces = "text/plain; charset=UTF-8")
    @ResponseBody
    public String Login(@RequestParam("username") String username,
            @RequestParam("password") String password, HttpSession session) {
        try {
            Customer cus = new CustomerFacade().checkLogin(username, password);
            if (cus == null) {
                return Alert.createErrorAlert("Sai tên đăng nhập hoặc mật khẩu!");
            } else if (!cus.getIsActive()) {
                return Alert.createErrorAlert("Tài khoản của bạn đã bị khóa!");
            } else {
                Map memberSession = new HashMap();
                memberSession.put("MEMBER", cus);
                session.setAttribute("MEMBER_DATA", memberSession);
                return Alert.createReloadPage();
            }
        } catch (Exception e) {
            return Alert.createErrorAlert("Đã xảy ra lỗi. Vui lòng thử lại sau!");
        }
    }

    @RequestMapping(value = {"/Register"}, method = RequestMethod.GET)
    public String Register(@RequestParam(value = "referral", required = false) String referral, ModelMap mm) {
        mm.put("REFERRAL", referral);
        return "MemberRegister";
    }

    @RequestMapping(value = {"/Register"}, method = RequestMethod.POST, produces = "text/plain; charset=UTF-8")
    @ResponseBody
    public String Register(@RequestParam("fullName") String fullName, @RequestParam(value = "refID") String refID,
            @RequestParam("password") String password, @RequestParam("email") String email,
            @RequestParam("mobile") String mobile, @RequestParam("peoplesIdentity") String peoplesIdentity,
            @RequestParam("bankName") String bankName,
            @RequestParam("bankNumber") String bankNumber,
            @RequestParam("bankAgency") String bankAgency, @RequestParam("gender") Boolean gender,
            @RequestParam("userName") String userName, HttpServletRequest request) {
        try {
            Pair<Integer, String> result = new CustomerFacade().createUser(
                    fullName, password, email, refID, mobile, gender, userName, peoplesIdentity, bankName, bankNumber, bankAgency
            );
            if (result.getKey() == 1) {
                return Alert.createSuccessAlert(result.getValue());
            } else {
                return Alert.createErrorAlert(result.getValue());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Alert.createErrorAlert("Đã xảy ra lỗi. Vui lòng thử lại sau!");
    }

    @RequestMapping(value = {"/ResetPassword"}, method = RequestMethod.GET)
    public String ResetPassword(ModelMap mm) {
        return "MemberResetPassword";
    }

    @RequestMapping(value = {"/ResetPassword"}, method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    @ResponseBody
    public String ResetPassword(@RequestParam("username") String username,
            @RequestParam("email") String email, HttpServletRequest request) {
        String result;
        try {
            Customer c = new CustomerFacade().findByUsername(username);
            if (!c.getEmail().equalsIgnoreCase(email)) {
                result = Alert.createErrorAlert("Tên đăng nhập hoặc email không chính xác!");
            } else if (!c.getIsActive()) {
                result = Alert.createErrorAlert("Tài khoản của bạn đang bị khóa!");
            } else {
                String newPassword = CaptchaUtil.generateCaptchaTextMethod2(9);
                c.setPasswordTemp(newPassword);
                MailUtils.sendMail(request.getServletContext().getRealPath("/WEB-INF/config.support.properties"),
                        request.getServletContext().getRealPath("/WEB-INF/config.support.account.properties"),
                        email, "BV: Tạo lại mật khẩu", MailTemplate.createRsPassTemp(
                                request.getRequestURL().toString().substring(0, request.getRequestURL().toString().indexOf(request.getRequestURI())),
                                c.getFullName(), c.getUserName(), newPassword));
                new CustomerFacade().edit(c);
                result = Alert.createSuccessAlert("Gửi yêu cầu tạo mật khẩu thành công! Vui lòng kiểm tra hòm thư để xác nhận lại!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            result = Alert.createErrorAlert("Đã xảy ra lỗi. Vui lòng thử lại sau!");
        }
        return result;
    }

//    @RequestMapping(value = {"/Enable2FA"}, method = RequestMethod.GET)
//    @ResponseBody
//    public Pair<Integer, Object> Enable2FA(HttpSession session, @RequestParam(value = "code", required = false) String code) {
//        Pair<Integer, Object> rs = new Pair(0, Alert.createErrorAlert("Đã xảy ra lỗi. Vui lòng thử lại sau!"));
//        try {
//            Map memberSession = (Map) session.getAttribute("MEMBER_DATA");
//            CustomerFacade cF = new CustomerFacade();
//            Customer cus = (Customer) memberSession.get("MEMBER");
//            if (cus.getEnable2FA()) {
//                final boolean verify = CustomFunction.verify(cus.getKey2FA(), code);
//                if (!verify) {
//                    return new Pair(-1, Alert.createErrorAlert("That code was invalid!"));
//                } else {
//                    rs = new Pair(1, Alert.createSuccessAlert("Disable 2-step veification successfully!"));
//                }
//            } else {
//                if (StringUtils.isEmpty(cus.getKey2FA())) {
//                    GoogleAuthenticator gAuth = new GoogleAuthenticator();
//                    final GoogleAuthenticatorKey key = gAuth.createCredentials();
//                    cus.setKey2FA(key.getKey());
//                    String qrUrl = GoogleAuthenticatorQRGenerator.getOtpAuthURL("BV", cus.getUserName(), key);
//                    cus.setQRCode2FA(qrUrl);
//                }
//                Map value = new HashMap();
//                value.put("msg", Alert.createSuccessAlert("Enable 2-step veification successfully!"));
//                value.put("qrcode", cus.getqRCode2FA());
//                rs = new Pair(2, value);
//            }
//            cus.setEnable2FA(!cus.getEnable2FA());
//            cF.edit(cus);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return rs;
//    }
    @RequestMapping(value = {"/ConfirmResetPassword/{userName}/{password}"}, method = RequestMethod.GET)
    public String ConfirmResetPassword(@PathVariable("userName") String userName, @PathVariable("password") String password,
            HttpServletRequest request, ModelMap mm) {
        Pair<Integer, String> result;
        try {
            Customer c = new CustomerFacade().findByUsername(userName);
            if (StringUtils.isEmpty(password) || StringUtils.isEmpty(c.getPasswordTemp()) || !CustomFunction.md5(c.getPasswordTemp()).equals(password)) {
                result = new Pair(0, "Yêu cầu không hợp lệ!");
            } else {
                result = new Pair(1, c.getPasswordTemp());
                c.setPassword(CustomFunction.md5(c.getPasswordTemp()));
                c.setPasswordTemp(null);
                new CustomerFacade().edit(c);
            }
        } catch (Exception e) {
            e.printStackTrace();
            result = new Pair(0, "Yêu cầu không hợp lệ!");
        }
        mm.put("RESULT_CONFIRM", result);
        return "MemberConfirmResetPassword";
    }

    @RequestMapping(value = {"/Signout"}, method = RequestMethod.GET)
    public void Signout(HttpSession session, HttpServletResponse response, HttpServletRequest request, @CookieValue(value = "LOCALE", defaultValue = "en") String locale) {
        session.removeAttribute("MEMBER_DATA");
        try {
            response.sendRedirect("/Home");
        } catch (IOException ex) {
            Logger.getLogger(CustomerController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @RequestMapping(value = {"/Security"}, method = RequestMethod.GET)
    public String Setting(HttpSession session, ModelMap mm) {
        return "MemberSecurity";
    }

    @RequestMapping(value = {"/Profile"}, method = RequestMethod.GET)
    public String Signout(HttpSession session, ModelMap mm) {
        return "MemberProfile";
    }

    @RequestMapping(value = {"/UpdateProfile"}, method = RequestMethod.POST)
    @ResponseBody
    public Pair<Integer, String> UpdateProfile(@RequestParam("fullName") String fullName,
            @RequestParam("email") String email, @RequestParam("mobile") String mobile,
            @RequestParam("peoplesIdentity") String peoplesIdentity,
            @RequestParam("bankName") String bankName,
            @RequestParam("bankNumber") String bankNumber,
            @RequestParam("bankAgency") String bankAgency,
            @RequestParam("gender") Boolean gender,
            @RequestParam(value = "code", required = false) String code, HttpSession session) {
        Map memberSession = (Map) session.getAttribute("MEMBER_DATA");
        Customer cus = (Customer) memberSession.get("MEMBER");
        Pair<Integer, String> result = new Pair(0, Alert.createErrorAlert("Đã xảy ra lỗi. Vui lòng thử lại sau!"));
        try {
            new CustomerFacade().updateProfile(fullName, email, mobile, peoplesIdentity, bankName, bankNumber, bankAgency, gender, cus.getId());
            cus.setFullName(fullName);
            cus.setEmail(email);
            cus.setMobile(mobile);
            cus.setPeoplesIdentity(peoplesIdentity);
            cus.setGender(gender);
            cus.setBankName(bankName);
            cus.setBankNumber(bankNumber);
            cus.setBankAgency(bankAgency);
            result = new Pair(1, Alert.createSuccessAlert("Cập nhật hồ sơ thành công!"));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    @RequestMapping(value = {"/UpdatePassword"}, method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    @ResponseBody
    public Pair<Integer, String> UpdatePassword(@RequestParam("oldPassword") String oldPassword,
            @RequestParam("newPassword") String newPassword, HttpSession session) {
        Map memberSession = (Map) session.getAttribute("MEMBER_DATA");
        Customer cus = (Customer) memberSession.get("MEMBER");
        oldPassword = CustomFunction.md5(oldPassword);
        newPassword = CustomFunction.md5(newPassword);
        Pair<Integer, String> result = new Pair(0, Alert.createErrorAlert("Đã xảy ra lỗi. Vui lòng thử lại sau!"));
        try {
            result = new CustomerFacade().updatePassword(oldPassword, newPassword, cus.getId());
            result = new Pair(result.getKey(), result.getKey() == 1 ? Alert.createSuccessAlert(result.getValue()) : Alert.createErrorAlert(result.getValue()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
