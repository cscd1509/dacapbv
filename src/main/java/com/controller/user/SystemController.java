package com.controller.user;

import com.bean.PagerPlus;
import com.entity.Customer;
import com.facade.CustomerFacade;
import com.utils.CustomFunction;
import java.util.Map;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("")
public class SystemController {

    @RequestMapping(value = "/System", method = RequestMethod.GET)
    public String System(HttpSession session, ModelMap mm) {
        Map memberSession = (Map) session.getAttribute("MEMBER_DATA");
        Customer cus = (Customer) memberSession.get("MEMBER");
        String tree = CustomFunction.buildTreeCustomer(new CustomerFacade().getTreeCustomer(cus.getId(), 3), cus.getParentID() == null ? null : cus.getParentID().getId(), "/System");
        mm.put("TREE_CUSTOMER", tree);
        return "MemberSystem";
    }

    @RequestMapping(value = "/System/Next/{id}/{fisrtId}", method = RequestMethod.GET,
            produces = "text/plain;charset=UTF-8")
    @ResponseBody
    public String nextTreeCustomerView(@PathVariable(value = "id") Integer id, @PathVariable(value = "fisrtId") Integer fisrtId) {
        String tree = CustomFunction.buildTreeCustomer(new CustomerFacade().getTreeCustomer(id, 3), fisrtId, "/System");
        return tree;
    }

    @RequestMapping(value = {"/Referral"}, method = RequestMethod.GET)
    public ModelAndView Referral(@RequestParam(value = "currentPage", required = false, defaultValue = "1") int currentPage,
            @RequestParam(value = "keyword", required = false) String keyword,
            @RequestParam(value = "level", required = false) Integer level, HttpSession session) {
        Map memberSession = (Map) session.getAttribute("MEMBER_DATA");
        Customer member = (Customer) memberSession.get("MEMBER");
        ModelMap mm = new ModelMap();
        PagerPlus pager = new PagerPlus();
        pager.setCurrentPage(currentPage);
        pager.setDisplayPerPage(10);
        pager.setAsc(false);
        pager.setOrderColumn("rankCustomer");
        pager.setKeyword(keyword);
        pager.setIsCheck(level);
        mm.put("RESULTS", new CustomerFacade().pagerReferral(pager, member));
        mm.put("PAGER", pager);
        return new ModelAndView("MemberReferral", mm);
    }
}
