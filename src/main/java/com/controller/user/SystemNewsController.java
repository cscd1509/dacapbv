package com.controller.user;

import com.bean.Pager;
import com.entity.Customer;
import com.facade.SystemNewsFacade;
import java.util.Map;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("")
public class SystemNewsController {

    @RequestMapping(value = {"/SystemNews"}, method = RequestMethod.GET)
    public ModelAndView Referral(@RequestParam(value = "currentPage", required = false, defaultValue = "1") int currentPage,
            @RequestParam(value = "keyword", required = false) String keyword, HttpSession session) {
        Map memberSession = (Map) session.getAttribute("MEMBER_DATA");
        Customer member = (Customer) memberSession.get("MEMBER");
        ModelMap mm = new ModelMap();
        Pager pager = new Pager();
        pager.setCurrentPage(currentPage);
        pager.setDisplayPerPage(10);
        pager.setAsc(false);
        pager.setOrderColumn("createdDate");
        pager.setKeyword(keyword);
        mm.put("RESULTS", new SystemNewsFacade().pager(pager, member));
        mm.put("PAGER", pager);
        return new ModelAndView("MemberSystemNews", mm);
    }

    @RequestMapping(value = {"/SystemNews/{id}"}, method = RequestMethod.GET)
    public ModelAndView Referral(@PathVariable(value = "id") int id, ModelMap mm, HttpSession session) {
        mm.put("SELECTED_NEWS", new SystemNewsFacade().find(id));
        return new ModelAndView("MemberSystemNewsDetail", mm);
    }
}
