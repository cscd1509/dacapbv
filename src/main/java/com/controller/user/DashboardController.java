package com.controller.user;

import com.bean.Pager;
import com.bean.PagerPlus;
import com.entity.Customer;
import com.facade.CustomerFacade;
import com.facade.HistoryAwardFacade;
import com.facade.HistoryChangeRankFacade;
import com.facade.HistoryPaymentFacade;
import java.util.Calendar;
import java.util.Map;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/Dashboard")
public class DashboardController {

    @RequestMapping(value = {""}, method = RequestMethod.GET)
    public String Dashboard(HttpSession session, ModelMap mm) {
        Map memberSession = (Map) session.getAttribute("MEMBER_DATA");
        Customer cus = (Customer) memberSession.get("MEMBER");
        Calendar c = Calendar.getInstance();
        mm.put("HOUR", c.get(Calendar.HOUR_OF_DAY));
        mm.put("MINUTE", c.get(Calendar.MINUTE));
        mm.put("SECOND", c.get(Calendar.SECOND));
        CustomerFacade cF = new CustomerFacade();
        PagerPlus pager = new PagerPlus();
        pager.setIsCheck(1);
        pager.setOrderColumn("createdDate");
        pager.setAsc(false);
        pager.setDisplayPerPage(5);
        Pager pagerChangeRank = new Pager();
        pagerChangeRank.setOrderColumn("createdDate");
        pagerChangeRank.setAsc(false);
        pagerChangeRank.setDisplayPerPage(5);
        mm.put("HISTORY_PAYMENT", new HistoryPaymentFacade().pager(pager, cus.getId()));
        mm.put("AWARD_CHART_DATA", new HistoryAwardFacade().getAwardChartData(cus.getId()));
        mm.put("HISTORY_CHANGE_RANK", new HistoryChangeRankFacade().pager(pagerChangeRank, cus.getId()));
        mm.put("BASIC_INFOMATION", cF.getBasicInfomation(cus.getId()));
        mm.put("TOP5_CHILDREN", cF.getTop5Children(cus.getId()));
        return "MemberDashboard";
    }
}
