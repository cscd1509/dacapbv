package com.controller.user;

import com.bean.PagerPlus;
import com.entity.Customer;
import com.facade.HistoryAwardFacade;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import javax.servlet.http.HttpSession;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("")
public class AwardController {

    @RequestMapping(value = {"/HistoryAward"}, method = RequestMethod.GET)
    public ModelAndView HistoryAward(@RequestParam(value = "currentPage", required = false, defaultValue = "1") int currentPage,
            @RequestParam(value = "checkAwardID", required = false) Integer checkAwardID,
            @RequestParam(value = "isAvailability", required = false, defaultValue = "-1") Integer isAvailability,
            @RequestParam(value = "startDate", required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") Date startDate,
            @RequestParam(value = "endDate", required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") Date endDate, HttpSession session) {
        Map memberSession = (Map) session.getAttribute("MEMBER_DATA");
        Customer member = (Customer) memberSession.get("MEMBER");
        ModelMap mm = new ModelMap();
        PagerPlus pager = new PagerPlus();
        pager.setCurrentPage(currentPage);
        pager.setDisplayPerPage(10);
        pager.setAsc(false);
        pager.setOrderColumn("createdDate");
        pager.setIsAvailability(isAvailability);
        if (startDate != null && endDate != null) {
            Calendar c = Calendar.getInstance();
            c.setTime(endDate);
            c.set(Calendar.HOUR_OF_DAY, 23);
            c.set(Calendar.MINUTE, 59);
            c.set(Calendar.SECOND, 59);
            c.set(Calendar.MILLISECOND, 9999);
            endDate = c.getTime();
            pager.setStartDate(startDate);
            pager.setEndDate(endDate);
        }
        pager.setIsCheck(checkAwardID);
        mm.put("RESULTS", new HistoryAwardFacade().pager(pager, member.getId()));
        mm.put("PAGER", pager);
        mm.put("AWARD_CHART_DATA", new HistoryAwardFacade().getAwardChartData(member.getId()));
        mm.put("AWARD_CHART_BAR", new HistoryAwardFacade().getAwardChartBar(member.getId(), Calendar.getInstance().get(Calendar.MONTH) + 1));
        return new ModelAndView("MemberHistoryAward", mm);
    }
}
