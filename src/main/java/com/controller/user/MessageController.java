package com.controller.user;

import com.bean.Alert;
import com.entity.Customer;
import com.entity.MessageCategory;
import com.entity.MessageSystem;
import com.facade.MessageSystemFacade;
import com.utils.StringUtils;
import java.util.Map;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "")
public class MessageController {

    @RequestMapping(value = {"/Compose"}, method = RequestMethod.POST)
    @ResponseBody
    public String ResetPassword(@RequestParam("categoryID") int categoryID, @RequestParam("title") String title,
            @RequestParam("content") String content, HttpSession session) {
        String result;
        Map memberSession = (Map) session.getAttribute("MEMBER_DATA");
        Customer cus = (Customer) memberSession.get("MEMBER");
        int countMsgPerDay = new MessageSystemFacade().countMessageInDay(cus.getId());
        if (countMsgPerDay >= 5) {
            result = Alert.createErrorAlert("The maximum number of messages is 5 messages per day!");
            return result;
        }
        if (StringUtils.isEmpty(content) || content.length() < 10 || content.length() > 5000) {
            result = Alert.createErrorAlert("Content must be at least 10 characters and max 5000 characters!");
            return result;
        }
        try {
            MessageSystem ms = new MessageSystem();
            MessageCategory cat = new MessageCategory();
            cat.setId(categoryID);
            ms.setCategoryID(cat);
            ms.setTitle(StringUtils.escapeHtmlEntity(title));
            ms.setContent(StringUtils.escapeHtmlEntity(content));
            ms.setCustomerID(new Customer(cus.getId()));
            new MessageSystemFacade().create(ms);
            result = Alert.createSuccessAlert("Send message successfully!");
        } catch (Exception e) {
            e.printStackTrace();
            result = Alert.createErrorAlert("Send message failed!");
        }
        return result;
    }
}
