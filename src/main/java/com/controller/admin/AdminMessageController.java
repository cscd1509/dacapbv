package com.controller.admin;

import com.bean.Message;
import com.bean.PagerPlus;
import com.entity.MessageSystem;
import com.facade.MessageSystemFacade;
import com.utils.LogUtils;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/Admin/Message")
public class AdminMessageController {

    @RequestMapping(value = "/ListMessageNotRead", method = RequestMethod.GET)
    public String ListMessageNotRead(@RequestParam(value = "currentPage", required = false, defaultValue = "1") int currentPage,
            @RequestParam(value = "displayPerPage", required = false, defaultValue = "10") int displayPerPage,
            @RequestParam(value = "orderColumn", required = false, defaultValue = "createdDate") String orderColmn,
            @RequestParam(value = "asc", required = false, defaultValue = "false") boolean asc, ModelMap mm) {
        PagerPlus pager = new PagerPlus();
        pager.setAsc(asc);
        pager.setCurrentPage(currentPage);
        pager.setDisplayPerPage(displayPerPage);
        pager.setOrderColumn(orderColmn);
        List result = new MessageSystemFacade().pagerNotRead(pager, false);
        mm.put("ITEMS_LIST", result);
        mm.put("PAGER", pager);
        return "AdminListMessageNotRead";
    }

    @RequestMapping(value = "/ListMessageNotRead/Ajax", method = RequestMethod.GET)
    public String ListMessageNotReadAjax(@RequestParam(value = "currentPage", required = false, defaultValue = "1") int currentPage,
            @RequestParam(value = "displayPerPage", required = false, defaultValue = "10") int displayPerPage,
            @RequestParam(value = "orderColumn", required = false, defaultValue = "createdDate") String orderColmn,
            @RequestParam(value = "asc", required = false, defaultValue = "false") boolean asc, ModelMap mm) {
        PagerPlus pager = new PagerPlus();
        pager.setAsc(asc);
        pager.setCurrentPage(currentPage);
        pager.setDisplayPerPage(displayPerPage);
        pager.setOrderColumn(orderColmn);
        List result = new MessageSystemFacade().pagerNotRead(pager, false);
        mm.put("ITEMS_LIST", result);
        mm.put("PAGER", pager);
        return "Ajax.AdminListMessageNotRead";
    }

    @RequestMapping(value = "/ListMessageRead", method = RequestMethod.GET)
    public String ListMessageRead(@RequestParam(value = "currentPage", required = false, defaultValue = "1") int currentPage,
            @RequestParam(value = "displayPerPage", required = false, defaultValue = "10") int displayPerPage,
            @RequestParam(value = "orderColumn", required = false, defaultValue = "createdDate") String orderColmn,
            @RequestParam(value = "asc", required = false, defaultValue = "false") boolean asc, ModelMap mm) {
        PagerPlus pager = new PagerPlus();
        pager.setAsc(asc);
        pager.setCurrentPage(currentPage);
        pager.setDisplayPerPage(displayPerPage);
        pager.setOrderColumn(orderColmn);
        List result = new MessageSystemFacade().pagerNotRead(pager, true);
        mm.put("ITEMS_LIST", result);
        mm.put("PAGER", pager);
        return "AdminListMessageRead";
    }

    @RequestMapping(value = "/ListMessageRead/Ajax", method = RequestMethod.GET)
    public String ListMessageReadAjax(@RequestParam(value = "currentPage", required = false, defaultValue = "1") int currentPage,
            @RequestParam(value = "displayPerPage", required = false, defaultValue = "10") int displayPerPage,
            @RequestParam(value = "orderColumn", required = false, defaultValue = "createdDate") String orderColmn,
            @RequestParam(value = "asc", required = false, defaultValue = "false") boolean asc, ModelMap mm) {
        PagerPlus pager = new PagerPlus();
        pager.setAsc(asc);
        pager.setCurrentPage(currentPage);
        pager.setDisplayPerPage(displayPerPage);
        pager.setOrderColumn(orderColmn);
        List result = new MessageSystemFacade().pagerNotRead(pager, true);
        mm.put("ITEMS_LIST", result);
        mm.put("PAGER", pager);
        return "Ajax.AdminListMessageRead";
    }

    @RequestMapping(value = "/View/{mId}", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView ViewMessage(@PathVariable(value = "mId") int mId, ModelMap mm, HttpSession session) {
        MessageSystem ms = (MessageSystem) new MessageSystemFacade().findEager(mId);
        ms.setIsCheck(true);
        Map adminSession = (Map) session.getAttribute("ADMIN");
        int myId = (int) adminSession.get("ADMIN_ID");
        try {
            new MessageSystemFacade().edit(ms);
            LogUtils.logs(myId, LogUtils.ACTION_UPDATE, 1084, "Checked message id:'" + mId + ", successfully!");
        } catch (Exception ex) {
            ex.printStackTrace();
            LogUtils.logs(myId, LogUtils.ACTION_UPDATE, 1084, "Checked message id:'" + mId + ", successfully!");
        }
        mm.put("MESSAGE_SYSTEM", ms);
        return new ModelAndView("Ajax.AdminMessageDetail");
    }

    @RequestMapping(value = "/ListMessageRead/Delete/{mId}", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView DeleteMessage(@PathVariable(value = "mId") int mId, ModelMap mm, HttpSession session) {
        MessageSystem ms = (MessageSystem) new MessageSystemFacade().find(mId);
        ms.setIsDelete(true);
        Message msg;
        Map adminSession = (Map) session.getAttribute("ADMIN");
        int myId = (int) adminSession.get("ADMIN_ID");
        try {
            new MessageSystemFacade().edit(ms);
            LogUtils.logs(myId, LogUtils.ACTION_DELETE, 1084, "Delete message id:'" + mId + ", successfully!");
            msg = new Message("Success", "Delete message successfully!", Message.SUCCESS_MESSAGE_CLASS);
        } catch (Exception ex) {
            ex.printStackTrace();
            LogUtils.logs(myId, LogUtils.ACTION_DELETE, 1084, "Delete message id:'" + mId + ", failed!");
            msg = new Message("Error", "Đã xảy ra lỗi. Vui lòng thử lại sau!", Message.ERROR_MESSAGE_CLASS);
        }
        mm.put("MESSAGE", msg);
        return new ModelAndView("MessageLayout");
    }
}
