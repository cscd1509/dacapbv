package com.controller.admin;

import com.bean.ExcelFile;
import com.bean.Message;
import com.bean.Pager;
import com.facade.PinSystemFacade;
import com.utils.LogUtils;
import java.util.List;
import java.util.Map;
import javafx.util.Pair;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/Admin/PinSystem")
public class AdminPinSystemController {

    @RequestMapping(value = "/ListPinSystemNotUsed", method = RequestMethod.GET)
    public String ListPinSystemNotUsed(@RequestParam(value = "currentPage", required = false, defaultValue = "1") int currentPage,
            @RequestParam(value = "displayPerPage", required = false, defaultValue = "10") int displayPerPage,
            @RequestParam(value = "orderColumn", required = false, defaultValue = "id") String orderColmn,
            @RequestParam(value = "asc", required = false, defaultValue = "false") boolean asc, ModelMap mm) {
        Pager pager = new Pager();
        pager.setAsc(asc);
        pager.setCurrentPage(currentPage);
        pager.setDisplayPerPage(displayPerPage);
        pager.setOrderColumn(orderColmn);
        List result = new PinSystemFacade().pager(pager, false);
        mm.put("ITEMS_LIST", result);
        mm.put("PAGER", pager);
        return "AdminListPinSystemNotUsed";
    }

    @RequestMapping(value = "/ListPinSystemNotUsed/Ajax", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView ListPinSystemNotUsedAjax(@RequestParam(value = "currentPage", required = false, defaultValue = "1") int currentPage,
            @RequestParam(value = "displayPerPage", required = false, defaultValue = "10") int displayPerPage,
            @RequestParam(value = "orderColumn", required = false, defaultValue = "id") String orderColmn,
            @RequestParam(value = "asc", required = false, defaultValue = "false") boolean asc, ModelMap mm) {
        Pager pager = new Pager();
        pager.setAsc(asc);
        pager.setCurrentPage(currentPage);
        pager.setDisplayPerPage(displayPerPage);
        pager.setOrderColumn(orderColmn);
        List result = new PinSystemFacade().pager(pager, false);
        mm.put("ITEMS_LIST", result);
        mm.put("PAGER", pager);
        return new ModelAndView("Ajax.AdminListPinSystemNotUsed");
    }

    @RequestMapping(value = "/ListPinSystemNotUsed/Export", method = RequestMethod.GET)
    public ModelAndView exportComissionDistributorView(
            @RequestParam(value = "displayPerPage", required = false, defaultValue = "0") int displayPerPage,
            @RequestParam(value = "orderColumn", required = false, defaultValue = "id") String orderColmn,
            @RequestParam(value = "asc", required = false, defaultValue = "false") boolean asc, HttpSession session) {
        Pager pager = new Pager();
        pager.setAsc(asc);
        pager.setCurrentPage(1);
        pager.setDisplayPerPage(displayPerPage);
        pager.setOrderColumn(orderColmn);
        ExcelFile file = new ExcelFile();
        new PinSystemFacade().exportNotUsed(pager, file);
        return new ModelAndView("ExcelView", "myModel", file);
    }

    @RequestMapping(value = "/Insert", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView InsertPinSys(ModelMap mm) {
        return new ModelAndView("Ajax.AdminListPinSystemInsertModal");
    }

    @RequestMapping(value = "/Insert", method = RequestMethod.POST)
    @ResponseBody
    public ModelAndView InsertPinSys(@RequestParam(value = "count") int count, HttpSession session, ModelMap mm) {
        Map adminSession = (Map) session.getAttribute("ADMIN");
        int adId = (int) adminSession.get("ADMIN_ID");
        Pair<Integer, String> result;
        try {
            result = new PinSystemFacade().insertPinSys(count);
            Message msg = new Message();
            msg.setMsg(result.getValue());
            if (result.getKey() == 1) {
                msg.setTitle("Thành công");
                msg.setMsgClass(Message.SUCCESS_MESSAGE_CLASS);
                LogUtils.logs(adId, LogUtils.ACTION_ADD, 68, "Insert " + count + " PIN success!");
            } else {
                msg.setTitle("Thất bại");
                msg.setMsgClass(Message.ERROR_MESSAGE_CLASS);
                LogUtils.logs(adId, LogUtils.ACTION_ADD, 68, "Insert " + count + " PIN failed!");
            }
            mm.put("MESSAGE", msg);
        } catch (Exception e) {
            e.printStackTrace();
            LogUtils.logs(adId, LogUtils.ACTION_ADD, 68, "Insert " + count + " PIN failed!");
        }
        return new ModelAndView("MessageLayout");
    }

    @RequestMapping(value = "/ListPinSystemUsed", method = RequestMethod.GET)
    public String ListPinSystemUsed(@RequestParam(value = "currentPage", required = false, defaultValue = "1") int currentPage,
            @RequestParam(value = "displayPerPage", required = false, defaultValue = "10") int displayPerPage,
            @RequestParam(value = "orderColumn", required = false, defaultValue = "id") String orderColmn,
            @RequestParam(value = "asc", required = false, defaultValue = "true") boolean asc,
            @RequestParam(value = "keyword", required = false, defaultValue = "") String keyword, ModelMap mm) {
        Pager pager = new Pager();
        pager.setAsc(asc);
        pager.setCurrentPage(currentPage);
        pager.setDisplayPerPage(displayPerPage);
        pager.setOrderColumn(orderColmn);
        pager.setKeyword(keyword);
        List result = new PinSystemFacade().pager(pager, true);
        mm.put("ITEMS_LIST", result);
        mm.put("PAGER", pager);
        return "AdminListPinSystemUsed";
    }

    @RequestMapping(value = "/ListPinSystemUsed/Ajax", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView ListPinSystemUsedAjax(@RequestParam(value = "currentPage", required = false, defaultValue = "1") int currentPage,
            @RequestParam(value = "displayPerPage", required = false, defaultValue = "10") int displayPerPage,
            @RequestParam(value = "orderColumn", required = false, defaultValue = "id") String orderColmn,
            @RequestParam(value = "asc", required = false, defaultValue = "true") boolean asc,
            @RequestParam(value = "keyword", required = false, defaultValue = "") String keyword, ModelMap mm) {
        Pager pager = new Pager();
        pager.setAsc(asc);
        pager.setCurrentPage(currentPage);
        pager.setDisplayPerPage(displayPerPage);
        pager.setOrderColumn(orderColmn);
        pager.setKeyword(keyword);
        List result = new PinSystemFacade().pager(pager, true);
        mm.put("ITEMS_LIST", result);
        mm.put("PAGER", pager);
        return new ModelAndView("Ajax.AdminListPinSystemUsed");
    }
}
