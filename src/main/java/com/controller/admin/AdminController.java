package com.controller.admin;

import com.bean.Alert;
import com.entity.Admin;
import com.facade.AdminFacade;
import com.utils.CaptchaUtil;
import com.utils.ConfigUtils;
import com.utils.StringUtils;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import javafx.util.Pair;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/Admin")
public class AdminController {

    @RequestMapping(value = {""}, method = RequestMethod.GET)
    public String getHomeView(ModelMap mm, HttpServletResponse response) {
        Cookie cookie = new Cookie("MODULE_ACTIVE", "");
        cookie.setMaxAge(0);
        cookie.setPath("/");
        response.addCookie(cookie);
        cookie = new Cookie("MODULE_ACTIVE_PARENT", "");
        cookie.setMaxAge(0);
        cookie.setPath("/");
        response.addCookie(cookie);
        return "AdminIndex";
    }

    @RequestMapping(value = "/Login", method = RequestMethod.GET)
    public String getLoginView(HttpServletRequest request, ModelMap mm) {
        String reCaptchaPath = request.getServletContext().getRealPath("/WEB-INF/config.reCaptcha.properties");
        Properties props = ConfigUtils.getInstance().getProperties(reCaptchaPath);
        String captchaKey = props.getProperty("key");
        mm.put("RE_CAPTCHA_KEY", captchaKey);
        return "AdminLogin";
    }

    @RequestMapping(value = "/Login", method = RequestMethod.POST)
    @ResponseBody
    public Pair<Integer, String> login(@RequestParam(value = "username") String username,
            @RequestParam(value = "password") String password, @RequestParam("g-recaptcha-response") String recaptchaResponse,
            ModelMap mm, HttpServletRequest request) {
        HttpSession session = request.getSession();
        String reCaptchaPath = session.getServletContext().getRealPath("/WEB-INF/config.reCaptcha.properties");
        Properties props = ConfigUtils.getInstance().getProperties(reCaptchaPath);
        String secret = props.getProperty("secret");
        CaptchaUtil cUtil = new CaptchaUtil();
        boolean verifyCaptcha = cUtil.verifyReCaptcha(secret, recaptchaResponse);
        if (!verifyCaptcha) {
            return new Pair(0, Alert.createErrorAlert("Captcha không đúng!"));
        }
        Pair<Integer, String> result;
        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password)) {
            result = new Pair(0, Alert.createErrorAlert("Vui lòng nhập tất cả các trường!"));
            return result;
        }
        try {
            Admin admin = new AdminFacade().checkLogin(username, password);
            if (admin == null) {
                result = new Pair(0, Alert.createErrorAlert("Sai tên đăng nhập hoặc mật khẩu!"));
                return result;
            }
            if (!admin.getIsActive()) {
                result = new Pair(0, Alert.createErrorAlert("Tài khoản của bạn đã bị khóa!"));
                return result;
            }
            Map adminSession = new HashMap();
            adminSession.put("ADMIN_ID", admin.getId());
            session.setAttribute("ADMIN", adminSession);
            result = new Pair(1, Alert.createRedirectPage("/Admin"));
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            result = new Pair(0, Alert.createErrorAlert("Đã xảy ra lỗi. Vui lòng thử lại sau!"));
            return result;
        }
    }

    @RequestMapping(value = "/Logout", method = RequestMethod.GET)
    public void logout(HttpServletRequest request, HttpServletResponse response) {
        try {
            request.getSession().removeAttribute("ADMIN");
            Cookie cookie = new Cookie("MODULE_ACTIVE", "");
            cookie.setMaxAge(0);
            cookie.setPath("/");
            response.addCookie(cookie);
            cookie = new Cookie("MODULE_ACTIVE_PARENT", "");
            cookie.setMaxAge(0);
            cookie.setPath("/");
            response.addCookie(cookie);
            response.sendRedirect("/Admin/Login");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
