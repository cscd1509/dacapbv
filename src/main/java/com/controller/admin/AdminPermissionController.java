package com.controller.admin;

import com.bean.Message;
import com.bean.Pager;
import com.entity.Admin;
import com.entity.Agency;
import com.entity.ModuleInRole;
import com.entity.RoleAdmin;
import com.facade.AdminFacade;
import com.facade.AgencyFacade;
import com.facade.HistoryTransferFacade;
import com.facade.ModuleFacade;
import com.facade.ModuleInRoleFacade;
import com.facade.RoleAdminFacade;
import com.utils.LogUtils;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javafx.util.Pair;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/Admin/Permission")
public class AdminPermissionController {

    @RequestMapping(value = "/ListRole", method = RequestMethod.GET)
    public String ListRole(@RequestParam(value = "currentPage", required = false, defaultValue = "1") int currentPage,
            @RequestParam(value = "displayPerPage", required = false, defaultValue = "10") int displayPerPage,
            @RequestParam(value = "orderColumn", required = false, defaultValue = "id") String orderColmn,
            @RequestParam(value = "asc", required = false, defaultValue = "true") boolean asc, ModelMap mm) {
        Pager pager = new Pager();
        pager.setAsc(asc);
        pager.setCurrentPage(currentPage);
        pager.setDisplayPerPage(displayPerPage);
        pager.setOrderColumn(orderColmn);
        List result = new RoleAdminFacade().pager(pager);
        mm.put("ITEMS_LIST", result);
        mm.put("PAGER", pager);
        return "AdminListRole";
    }

    @RequestMapping(value = "/ListRole/Ajax", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView ListRoleAjax(@RequestParam(value = "currentPage", required = false, defaultValue = "1") int currentPage,
            @RequestParam(value = "displayPerPage", required = false, defaultValue = "10") int displayPerPage,
            @RequestParam(value = "orderColumn", required = false, defaultValue = "id") String orderColmn,
            @RequestParam(value = "asc", required = false, defaultValue = "true") boolean asc, ModelMap mm) {
        Pager pager = new Pager();
        pager.setAsc(asc);
        pager.setCurrentPage(currentPage);
        pager.setDisplayPerPage(displayPerPage);
        pager.setOrderColumn(orderColmn);
        List result = new RoleAdminFacade().pager(pager);
        mm.put("ITEMS_LIST", result);
        mm.put("PAGER", pager);
        return new ModelAndView("Ajax.AdminListRole");
    }

    @RequestMapping(value = "/ListRole/ViewChangePermission/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView ChangePermission(@PathVariable Integer id, ModelMap mm, HttpSession session) {
        RoleAdmin roleAdmin = new RoleAdminFacade().find(id);
        List<ModuleInRole> moduleInRoles = roleAdmin.getModuleInRoles();
        mm.put("MODULE_IN_ROLES_CHECK", moduleInRoles);
        mm.put("MODULES", new ModuleFacade().findAllModuleByLevel(1));
        mm.put("CURRENT_ROLE", id);
        return new ModelAndView("Ajax.AdminListRoleChangePermision");
    }

    @RequestMapping(value = {"/ListRole/ChangePermission/{id}"}, method = RequestMethod.POST)
    @ResponseBody
    public ModelAndView ChangePermission(@PathVariable(value = "id") Integer id, @RequestBody int[] ids,
            ModelMap mm, HttpSession session) {
        Message msg;
        Map adminSession = (Map) session.getAttribute("ADMIN");
        int myId = (int) adminSession.get("ADMIN_ID");
        try {
            new ModuleInRoleFacade().create(id, ids);
            LogUtils.logs(myId, LogUtils.ACTION_UPDATE, 9, "Update role's permission successfully");
            msg = new Message("Success", "Update role's permission successfully", Message.SUCCESS_MESSAGE_CLASS);
            mm.put("MESSAGE", msg);
            return new ModelAndView("MessageLayout");
        } catch (Exception e) {
            e.printStackTrace();
            LogUtils.logs(myId, LogUtils.ACTION_UPDATE, 9, "Update role's permission failed!");
            msg = new Message("Error", "Update role's permission failed", Message.ERROR_MESSAGE_CLASS);
            mm.put("MESSAGE", msg);
            return new ModelAndView("MessageLayout");
        }
    }

    @RequestMapping(value = {"/ListRole/Delete/{id}"}, method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView DeleteRole(@PathVariable(value = "id") Integer id,
            ModelMap mm, HttpSession session) {
        Message msg;
        Map adminSession = (Map) session.getAttribute("ADMIN");
        int myId = (int) adminSession.get("ADMIN_ID");
        try {
            new RoleAdminFacade().delete(id);
            LogUtils.logs(myId, LogUtils.ACTION_UPDATE, 9, "Delete role:" + id + " successfully");
            msg = new Message("Success", "Delete role successfully", Message.SUCCESS_MESSAGE_CLASS);
            mm.put("MESSAGE", msg);
            return new ModelAndView("MessageLayout");
        } catch (Exception e) {
            e.printStackTrace();
            LogUtils.logs(myId, LogUtils.ACTION_UPDATE, 9, "Delete role:" + id + " failed!");
            msg = new Message("Error", "Delete role failed", Message.ERROR_MESSAGE_CLASS);
            mm.put("MESSAGE", msg);
            return new ModelAndView("MessageLayout");
        }
    }

    @RequestMapping(value = "/ListRole/ViewInsert", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView ListRoleViewInsert() {
        return new ModelAndView("Ajax.AdminListRoleInsertModal");
    }

    @RequestMapping(value = "/ListRole/Insert", method = RequestMethod.POST)
    @ResponseBody
    public ModelAndView InsertRoleAdmin(@RequestParam(value = "name") String name, ModelMap mm, HttpSession session) {
        Message msg;
        Map adminSession = (Map) session.getAttribute("ADMIN");
        int myId = (int) adminSession.get("ADMIN_ID");
        try {
            RoleAdmin roleAdmin = new RoleAdmin();
            roleAdmin.setName(name);
            new RoleAdminFacade().create(roleAdmin);
            LogUtils.logs(myId, LogUtils.ACTION_UPDATE, 9, "Create role's permission successfully");
            msg = new Message("Success", "Create role's permission successfully", Message.SUCCESS_MESSAGE_CLASS);
            mm.put("MESSAGE", msg);
            return new ModelAndView("MessageLayout");
        } catch (Exception e) {
            e.printStackTrace();
            LogUtils.logs(myId, LogUtils.ACTION_UPDATE, 9, "Create role's permission failed!");
            msg = new Message("Error", "Create role's permission failed", Message.ERROR_MESSAGE_CLASS);
            mm.put("MESSAGE", msg);
            return new ModelAndView("MessageLayout");
        }
    }

    @RequestMapping(value = "/ListAdmin", method = RequestMethod.GET)
    public String ListAdmin(@RequestParam(value = "currentPage", required = false, defaultValue = "1") int currentPage,
            @RequestParam(value = "displayPerPage", required = false, defaultValue = "10") int displayPerPage,
            @RequestParam(value = "orderColumn", required = false, defaultValue = "id") String orderColmn,
            @RequestParam(value = "asc", required = false, defaultValue = "true") boolean asc, ModelMap mm) {
        Pager pager = new Pager();
        pager.setAsc(asc);
        pager.setCurrentPage(currentPage);
        pager.setDisplayPerPage(displayPerPage);
        pager.setOrderColumn(orderColmn);
        List result = new AdminFacade().pager(pager);
        mm.put("ITEMS_LIST", result);
        mm.put("PAGER", pager);
        return "AdminListAdmin";
    }

    @RequestMapping(value = "/ListAdmin/Ajax", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView ListAdminAjax(@RequestParam(value = "currentPage", required = false, defaultValue = "1") int currentPage,
            @RequestParam(value = "displayPerPage", required = false, defaultValue = "10") int displayPerPage,
            @RequestParam(value = "orderColumn", required = false, defaultValue = "id") String orderColmn,
            @RequestParam(value = "asc", required = false, defaultValue = "true") boolean asc, ModelMap mm) {
        Pager pager = new Pager();
        pager.setAsc(asc);
        pager.setCurrentPage(currentPage);
        pager.setDisplayPerPage(displayPerPage);
        pager.setOrderColumn(orderColmn);
        List result = new AdminFacade().pager(pager);
        mm.put("ITEMS_LIST", result);
        mm.put("PAGER", pager);
        return new ModelAndView("Ajax.AdminListAdmin");
    }

    @RequestMapping(value = "/ListAdmin/Delete/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView delete(@PathVariable Integer id, ModelMap mm, HttpSession session) {
        Message msg;
        Map adminSession = (Map) session.getAttribute("ADMIN");
        int myId = (int) adminSession.get("ADMIN_ID");
        if (Objects.equals(myId, id)) {
            msg = new Message("Error", "You can't do this action!", Message.ERROR_MESSAGE_CLASS);
            LogUtils.logs(myId, LogUtils.ACTION_DELETE, 9, "Delete Admin:'" + myId + ", Error: You can't do this action!");
            mm.put("MESSAGE", msg);
            return new ModelAndView("MessageLayout");
        }
        try {
            new AdminFacade().delete(id);
            LogUtils.logs(myId, LogUtils.ACTION_DELETE, 9, "Delete admin id:'" + myId + ", Successfully!");
            msg = new Message("Success", "Delete admin successfully", Message.SUCCESS_MESSAGE_CLASS);
            mm.put("MESSAGE", msg);
            return new ModelAndView("MessageLayout");
        } catch (Exception e) {
            msg = new Message("Error", "You can't do this action!", Message.ERROR_MESSAGE_CLASS);
            mm.put("MESSAGE", msg);
            LogUtils.logs(myId, LogUtils.ACTION_DELETE, 9, "Delete admin id:'" + myId + ", Error");
            return new ModelAndView("MessageLayout");
        }
    }

    @RequestMapping(value = "/ListAdmin/Block/{id}/{status}", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView block(@PathVariable Integer id, @PathVariable Boolean status,
            ModelMap mm, HttpSession session) {
        Message msg;
        Map adminSession = (Map) session.getAttribute("ADMIN");
        int myId = (int) adminSession.get("ADMIN_ID");
        if (Objects.equals(myId, id)) {
            msg = new Message("Error", "You can't do this action!", Message.ERROR_MESSAGE_CLASS);
            LogUtils.logs(myId, LogUtils.ACTION_UPDATE, 9, "Block Admin:'" + myId + ", Error: You can't do this action!");
            mm.put("MESSAGE", msg);
            return new ModelAndView("MessageLayout");
        }
        try {
            new AdminFacade().block(id, status);
            LogUtils.logs(myId, LogUtils.ACTION_UPDATE, 9, "Block admin id:'" + myId + ", Successfully!");
            msg = new Message("Success", "Block admin successfully", Message.SUCCESS_MESSAGE_CLASS);
            mm.put("MESSAGE", msg);
            return new ModelAndView("MessageLayout");
        } catch (Exception e) {
            msg = new Message("Error", "You can't do this action!", Message.ERROR_MESSAGE_CLASS);
            mm.put("MESSAGE", msg);
            LogUtils.logs(myId, LogUtils.ACTION_UPDATE, 9, "Block admin id:'" + myId + ", Error");
            return new ModelAndView("MessageLayout");
        }
    }

    @RequestMapping(value = "/ListAdmin/ViewInsert", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView ListAdminViewInsert(HttpSession session, ModelMap mm) {
        mm.put("AVAIABLE_AGENCY", new AgencyFacade().findAllAvaiableAgency());
        return new ModelAndView("Ajax.ListAdminViewInsert");
    }

    @RequestMapping(value = "/ListAdmin/Insert", method = RequestMethod.POST)
    @ResponseBody
    public ModelAndView register(@RequestBody Map admin, ModelMap mm, HttpSession session) {
        Message msg;
        Map adminSession = (Map) session.getAttribute("ADMIN");
        int myId = (int) adminSession.get("ADMIN_ID");
        int result;
        try {
            result = new AdminFacade().create(admin);
        } catch (Exception e) {
            e.printStackTrace();
            LogUtils.logs(myId, LogUtils.ACTION_ADD, 9, "Create admin id:'" + myId + ", failed!");
            msg = new Message("Error", "Đã xảy ra lỗi. Vui lòng thử lại sau!", Message.ERROR_MESSAGE_CLASS);
            mm.put("MESSAGE", msg);
            return new ModelAndView("MessageLayout");
        }
        switch (result) {
            case 1: {
                LogUtils.logs(myId, LogUtils.ACTION_ADD, 9, "Create admin id:'" + myId + ", failed!");
                msg = new Message("Error", "Đã xảy ra lỗi. Vui lòng thử lại sau!", Message.ERROR_MESSAGE_CLASS);
                mm.put("MESSAGE", msg);
                break;
            }
            case 2: {
                LogUtils.logs(myId, LogUtils.ACTION_ADD, 9, "Create admin id:'" + myId + ", failed!");
                msg = new Message("Error", "All field are required", Message.ERROR_MESSAGE_CLASS);
                mm.put("MESSAGE", msg);
                break;
            }
            case 3: {
                LogUtils.logs(myId, LogUtils.ACTION_ADD, 9, "Create admin id:'" + myId + ", failed!");
                msg = new Message("Error", "Username existed", Message.ERROR_MESSAGE_CLASS);
                mm.put("MESSAGE", msg);
                break;
            }
            case 4: {
                LogUtils.logs(myId, LogUtils.ACTION_ADD, 9, "Create admin id:'" + myId + ", successfully!");
                msg = new Message("Success", "Create admin successfully", Message.SUCCESS_MESSAGE_CLASS);
                mm.put("MESSAGE", msg);
                break;
            }
            default: {
                LogUtils.logs(myId, LogUtils.ACTION_ADD, 9, "Create admin id:'" + myId + ", failed!");
                msg = new Message("Error", "Đã xảy ra lỗi. Vui lòng thử lại sau!", Message.ERROR_MESSAGE_CLASS);
                mm.put("MESSAGE", msg);
                break;
            }
        }
        return new ModelAndView("MessageLayout");
    }

    @RequestMapping(value = "/ChangePassword/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView ViewChangePassword(@PathVariable(value = "id") int id, ModelMap mm) {
        mm.put("ADMIN_EDIT", id);
        return new ModelAndView("Ajax.ChangePassword");
    }

    @RequestMapping(value = "/ChangePassword", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView ViewChangePassword(ModelMap mm) {
        return new ModelAndView("Ajax.ChangePassword");
    }

    @RequestMapping(value = "/ChangePassword/{id}", method = RequestMethod.POST)
    @ResponseBody
    public ModelAndView ChangePassword(@PathVariable(value = "id") int id, @RequestParam(value = "newPass") String newPass, HttpSession session, ModelMap mm) {
        Message msg;
        Map adminSession = (Map) session.getAttribute("ADMIN");
        int myId = (int) adminSession.get("ADMIN_ID");
        try {
            int rs = new AdminFacade().changePassword(null, newPass, id);
            if (rs == 1) {
                LogUtils.logs(myId, LogUtils.ACTION_UPDATE, 9, "Change password id:" + id + " successfully");
                msg = new Message("Success", "Change password successfully", Message.SUCCESS_MESSAGE_CLASS);
                mm.put("MESSAGE", msg);
                return new ModelAndView("MessageLayout");
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            e.printStackTrace();
            LogUtils.logs(myId, LogUtils.ACTION_UPDATE, 9, "Change password id:" + id + " failed!");
            msg = new Message("Error", "Change password failed", Message.ERROR_MESSAGE_CLASS);
            mm.put("MESSAGE", msg);
            return new ModelAndView("MessageLayout");
        }
    }

    @RequestMapping(value = "/ChangePassword", method = RequestMethod.POST)
    @ResponseBody
    public ModelAndView ChangePassword(@RequestParam(value = "oldPass") String oldPass,
            @RequestParam(value = "newPass") String newPass, HttpSession session, ModelMap mm) {
        Message msg;
        Map adminSession = (Map) session.getAttribute("ADMIN");
        int myId = (int) adminSession.get("ADMIN_ID");
        try {
            int rs = new AdminFacade().changePassword(oldPass, newPass, myId);
            if (rs == 1) {
                LogUtils.logs(myId, LogUtils.ACTION_UPDATE, 9, "Change password id:" + myId + " successfully (self)!");
                msg = new Message("Success", "Change password successfully", Message.SUCCESS_MESSAGE_CLASS);
                mm.put("MESSAGE", msg);
            } else {
                LogUtils.logs(myId, LogUtils.ACTION_UPDATE, 9, "Change password id:" + myId + " failed (self)!");
                msg = new Message("Error", "Invalid Old Password", Message.ERROR_MESSAGE_CLASS);
                mm.put("MESSAGE", msg);
            }
            return new ModelAndView("MessageLayout");
        } catch (Exception e) {
            e.printStackTrace();
            LogUtils.logs(myId, LogUtils.ACTION_UPDATE, 9, "Change password id:" + myId + " failed (self)!");
            msg = new Message("Error", "Change password failed", Message.ERROR_MESSAGE_CLASS);
            mm.put("MESSAGE", msg);
            return new ModelAndView("MessageLayout");
        }
    }

    @RequestMapping(value = "/ListAgency", method = RequestMethod.GET)
    public String ListAgency(@RequestParam(value = "currentPage", required = false, defaultValue = "1") int currentPage,
            @RequestParam(value = "displayPerPage", required = false, defaultValue = "10") int displayPerPage,
            @RequestParam(value = "orderColumn", required = false, defaultValue = "id") String orderColmn,
            @RequestParam(value = "asc", required = false, defaultValue = "true") boolean asc, ModelMap mm) {
        Pager pager = new Pager();
        pager.setAsc(asc);
        pager.setCurrentPage(currentPage);
        pager.setDisplayPerPage(displayPerPage);
        pager.setOrderColumn(orderColmn);
        List result = new AgencyFacade().pager(pager);
        mm.put("ITEMS_LIST", result);
        mm.put("PAGER", pager);
        return "AdminListAgency";
    }

    @RequestMapping(value = "/ListAgency/Ajax", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView ListAgencyAjax(@RequestParam(value = "currentPage", required = false, defaultValue = "1") int currentPage,
            @RequestParam(value = "displayPerPage", required = false, defaultValue = "10") int displayPerPage,
            @RequestParam(value = "orderColumn", required = false, defaultValue = "id") String orderColmn,
            @RequestParam(value = "asc", required = false, defaultValue = "true") boolean asc, ModelMap mm) {
        Pager pager = new Pager();
        pager.setAsc(asc);
        pager.setCurrentPage(currentPage);
        pager.setDisplayPerPage(displayPerPage);
        pager.setOrderColumn(orderColmn);
        List result = new AgencyFacade().pager(pager);
        mm.put("ITEMS_LIST", result);
        mm.put("PAGER", pager);
        return new ModelAndView("Ajax.AdminListAgency");
    }

    @RequestMapping(value = "/ListAgency/Delete/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView deleteAgency(@PathVariable Integer id, ModelMap mm, HttpSession session) {
        Message msg;
        Map adminSession = (Map) session.getAttribute("ADMIN");
        int myId = (int) adminSession.get("ADMIN_ID");
        try {
            new AgencyFacade().delete(id);
            LogUtils.logs(myId, LogUtils.ACTION_DELETE, 9, "Xóa đại lý id:'" + myId + ", thành công!");
            msg = new Message("Thành công", "Xóa đại lý thành công", Message.SUCCESS_MESSAGE_CLASS);
            mm.put("MESSAGE", msg);
            return new ModelAndView("MessageLayout");
        } catch (Exception e) {
            msg = new Message("Thất bại", "Xóa đại lý không thành công!", Message.ERROR_MESSAGE_CLASS);
            mm.put("MESSAGE", msg);
            LogUtils.logs(myId, LogUtils.ACTION_DELETE, 9, "Delete admin id:'" + myId + ", Error");
            return new ModelAndView("MessageLayout");
        }
    }

    @RequestMapping(value = "/ListAgency/Block/{id}/{status}", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView blockAgency(@PathVariable Integer id, @PathVariable Boolean status,
            ModelMap mm, HttpSession session) {
        Message msg;
        Map adminSession = (Map) session.getAttribute("ADMIN");
        int myId = (int) adminSession.get("ADMIN_ID");
        try {
            new AgencyFacade().block(id, status);
            LogUtils.logs(myId, LogUtils.ACTION_UPDATE, 9, "Ẩn đại lý id:'" + myId + ", thành công!");
            msg = new Message("Thành công", "Ẩn đại lý thành công", Message.SUCCESS_MESSAGE_CLASS);
            mm.put("MESSAGE", msg);
            return new ModelAndView("MessageLayout");
        } catch (Exception e) {
            msg = new Message("Thất bại", "Ẩn đại lý không thành công!", Message.ERROR_MESSAGE_CLASS);
            mm.put("MESSAGE", msg);
            LogUtils.logs(myId, LogUtils.ACTION_UPDATE, 9, "Ẩn đại lý id:'" + myId + ", thất bại");
            return new ModelAndView("MessageLayout");
        }
    }

    @RequestMapping(value = "/ListAgency/ViewInsert", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView ListAgencyViewInsert(HttpSession session) {
        return new ModelAndView("Ajax.ListAgencyViewInsert");
    }

    @RequestMapping(value = "/ListAgency/Insert", method = RequestMethod.POST)
    @ResponseBody
    public ModelAndView CreateAgency(@RequestBody Agency agency, ModelMap mm, HttpSession session) {
        Message msg;
        Map adminSession = (Map) session.getAttribute("ADMIN");
        int myId = (int) adminSession.get("ADMIN_ID");
        try {
            agency.setCreatedAdminID(new Admin(myId));
            new AgencyFacade().create(agency);
            LogUtils.logs(myId, LogUtils.ACTION_ADD, 9, "Tạo đại lý thành công, id:" + agency.getId());
            msg = new Message("Thành công", "Tạo đại lý thành công!", Message.SUCCESS_MESSAGE_CLASS);
            mm.put("MESSAGE", msg);
        } catch (Exception e) {
            e.printStackTrace();
            LogUtils.logs(myId, LogUtils.ACTION_ADD, 9, "Tạo đại lý không thành công!");
            msg = new Message("Thất bại", "Đã xảy ra lỗi. Vui lòng thử lại sau!", Message.ERROR_MESSAGE_CLASS);
            mm.put("MESSAGE", msg);
            return new ModelAndView("MessageLayout");
        }
        return new ModelAndView("MessageLayout");
    }

    @RequestMapping(value = "/ListTransfer", method = RequestMethod.GET)
    public String ListTransfer(@RequestParam(value = "currentPage", required = false, defaultValue = "1") int currentPage,
            @RequestParam(value = "displayPerPage", required = false, defaultValue = "10") int displayPerPage,
            @RequestParam(value = "orderColumn", required = false, defaultValue = "createdDate") String orderColmn,
            @RequestParam(value = "asc", required = false, defaultValue = "false") boolean asc,
            @RequestParam(value = "keyword", required = false, defaultValue = "") String keyword, ModelMap mm, HttpSession session) {
        Map adminSession = (Map) session.getAttribute("ADMIN");
        int myId = (int) adminSession.get("ADMIN_ID");
        Pager pager = new Pager();
        pager.setAsc(asc);
        pager.setCurrentPage(currentPage);
        pager.setDisplayPerPage(displayPerPage);
        pager.setOrderColumn(orderColmn);
        pager.setKeyword(keyword);
        List result = new HistoryTransferFacade().pager(pager, myId);
        mm.put("ITEMS_LIST", result);
        mm.put("PAGER", pager);
        return "AdminListTransfer";
    }

    @RequestMapping(value = "/ListTransfer/Ajax", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView ListTransferAjax(@RequestParam(value = "currentPage", required = false, defaultValue = "1") int currentPage,
            @RequestParam(value = "displayPerPage", required = false, defaultValue = "10") int displayPerPage,
            @RequestParam(value = "orderColumn", required = false, defaultValue = "createdDate") String orderColmn,
            @RequestParam(value = "asc", required = false, defaultValue = "false") boolean asc,
            @RequestParam(value = "keyword", required = false, defaultValue = "") String keyword, ModelMap mm, HttpSession session) {
        Map adminSession = (Map) session.getAttribute("ADMIN");
        int myId = (int) adminSession.get("ADMIN_ID");
        Pager pager = new Pager();
        pager.setAsc(asc);
        pager.setCurrentPage(currentPage);
        pager.setDisplayPerPage(displayPerPage);
        pager.setOrderColumn(orderColmn);
        pager.setKeyword(keyword);
        List result = new HistoryTransferFacade().pager(pager, myId);
        mm.put("ITEMS_LIST", result);
        mm.put("PAGER", pager);
        return new ModelAndView("Ajax.AdminListTransfer");
    }

    @RequestMapping(value = "/ListTransfer/Insert", method = RequestMethod.POST)
    @ResponseBody
    public ModelAndView CreateTransfer(@RequestParam(value = "receivingAdminUsername") String receivingAdminUsername,
            @RequestParam(value = "amount") BigDecimal amount, ModelMap mm, HttpSession session) {
        Message msg;
        Map adminSession = (Map) session.getAttribute("ADMIN");
        int myId = (int) adminSession.get("ADMIN_ID");
        Pair<Integer, String> result;
        try {
            result = new HistoryTransferFacade().transfer(myId, receivingAdminUsername, amount);
            if (result.getKey() == 1) {
                LogUtils.logs(myId, LogUtils.ACTION_ADD, 9, "Chuyển tiền thành công cho " + receivingAdminUsername + " , " + amount + "đ");
                msg = new Message("Thành công", result.getValue(), Message.SUCCESS_MESSAGE_CLASS);
            } else {
                LogUtils.logs(myId, LogUtils.ACTION_ADD, 9, "Chuyển tiền thất bại cho " + receivingAdminUsername + " , " + amount + "đ");
                msg = new Message("Thất bại", result.getValue(), Message.ERROR_MESSAGE_CLASS);
            }
            mm.put("MESSAGE", msg);
        } catch (Exception e) {
            e.printStackTrace();
            LogUtils.logs(myId, LogUtils.ACTION_ADD, 9, "Chuyển tiền thất bại cho " + receivingAdminUsername + " , " + amount + "đ, " + e.getMessage());
            msg = new Message("Thất bại", "Đã xảy ra lỗi. Vui lòng thử lại sau!", Message.ERROR_MESSAGE_CLASS);
            mm.put("MESSAGE", msg);
        }
        return new ModelAndView("MessageLayout");
    }
}
