package com.controller.admin;

import com.bean.ExcelFile;
import com.bean.Message;
import com.bean.Pager;
import com.bean.PagerPlus;
import com.entity.Customer;
import com.entity.RankName;
import com.entity.SystemNews;
import com.facade.CustomerFacade;
import com.facade.HistoryDepositFacade;
import com.facade.RankNameFacade;
import com.facade.SystemNewsFacade;
import com.utils.CustomFunction;
import com.utils.LogUtils;
import com.utils.StringUtils;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import javafx.util.Pair;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/Admin/Customer")
public class AdminCustomerController {

    @RequestMapping(value = "/TreeCustomer", method = RequestMethod.GET)
    public ModelAndView getTreeCustomerView(ModelMap mm, HttpSession session) {
        String tree = CustomFunction.buildTreeCustomer(new CustomerFacade().getTreeCustomer(1, 3), null, "/Admin/Customer/TreeCustomer");
        mm.put("TREE_CUSTOMER", tree);
        return new ModelAndView("AdminTreeCustomer");
    }

    @RequestMapping(value = "/TreeCustomer/Reload", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView reloadTreeCustomerView(ModelMap mm, HttpSession session) {
        String tree = CustomFunction.buildTreeCustomer(new CustomerFacade().getTreeCustomer(1, 3), null, "/Admin/Customer/TreeCustomer");
        mm.put("TREE_CUSTOMER", tree);
        return new ModelAndView("Ajax.AdminTreeCustomer");
    }

    @RequestMapping(value = "/TreeCustomer/Next/{id}/{fisrtId}", method = RequestMethod.GET,
            produces = "text/plain;charset=UTF-8")
    @ResponseBody
    public String nextTreeCustomerView(@PathVariable(value = "id") Integer id, @PathVariable(value = "fisrtId") Integer fisrtId) {
        String tree = CustomFunction.buildTreeCustomer(new CustomerFacade().getTreeCustomer(id, 4), fisrtId, "/Admin/Customer/TreeCustomer");
        return tree;
    }

    @RequestMapping(value = "/ListCustomer", method = RequestMethod.GET)
    public String ListCustomer(@RequestParam(value = "currentPage", required = false, defaultValue = "1") int currentPage,
            @RequestParam(value = "displayPerPage", required = false, defaultValue = "10") int displayPerPage,
            @RequestParam(value = "orderColumn", required = false, defaultValue = "id") String orderColmn,
            @RequestParam(value = "asc", required = false, defaultValue = "true") boolean asc,
            @RequestParam(value = "isCheck", required = false) Integer isCheck,
            @RequestParam(value = "keyword", required = false, defaultValue = "") String keyword, ModelMap mm) {
        PagerPlus pager = new PagerPlus();
        pager.setAsc(asc);
        pager.setCurrentPage(currentPage);
        pager.setDisplayPerPage(displayPerPage);
        pager.setOrderColumn(orderColmn);
        pager.setKeyword(keyword);
        pager.setIsCheck(isCheck);
        List result = new CustomerFacade().pager(pager);
        mm.put("RANK_NAMES", new RankNameFacade().findAllAvaiable());
        mm.put("ITEMS_LIST", result);
        mm.put("PAGER", pager);
        return "AdminListCustomer";
    }

    @RequestMapping(value = "/ListCustomer/Ajax", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView ListCustomerAjax(@RequestParam(value = "currentPage", required = false, defaultValue = "1") int currentPage,
            @RequestParam(value = "displayPerPage", required = false, defaultValue = "10") int displayPerPage,
            @RequestParam(value = "orderColumn", required = false, defaultValue = "id") String orderColmn,
            @RequestParam(value = "asc", required = false, defaultValue = "true") boolean asc,
            @RequestParam(value = "isCheck", required = false) Integer isCheck,
            @RequestParam(value = "keyword", required = false, defaultValue = "") String keyword, ModelMap mm) {
        PagerPlus pager = new PagerPlus();
        pager.setAsc(asc);
        pager.setCurrentPage(currentPage);
        pager.setDisplayPerPage(displayPerPage);
        pager.setOrderColumn(orderColmn);
        pager.setKeyword(keyword);
        pager.setIsCheck(isCheck);
        List result = new CustomerFacade().pager(pager);
        mm.put("ITEMS_LIST", result);
        mm.put("PAGER", pager);
        mm.put("RANK_NAMES", new RankNameFacade().findAllAvaiable());
        return new ModelAndView("Ajax.AdminListCustomer");
    }

    @RequestMapping(value = "/ListCustomer/Export", method = RequestMethod.GET)
    public ModelAndView exportListCustomerView(
            @RequestParam(value = "orderColumn", required = false, defaultValue = "id") String orderColmn,
            @RequestParam(value = "keyword", required = false) String keyword,
            @RequestParam(value = "asc", required = false, defaultValue = "false") boolean asc, HttpSession session) {
        Pager pager = new Pager();
        pager.setAsc(asc);
        pager.setOrderColumn(orderColmn);
        pager.setKeyword(keyword);
        ExcelFile file = new ExcelFile();
        new CustomerFacade().export(pager, file);
        return new ModelAndView("ExcelView", "myModel", file);
    }

    @RequestMapping(value = "/ListCustomer/ViewEdit/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView ViewInsert(ModelMap mm, @PathVariable(value = "id") int id) {
        mm.put("CUSTOMER", new CustomerFacade().findEager(id));
        return new ModelAndView("Ajax.AdminListCustomerViewEdit");
    }

    @RequestMapping(value = "/ListCustomer/Edit", method = RequestMethod.POST)
    @ResponseBody
    public ModelAndView Edit(@RequestParam("id") int id, @RequestParam("fullName") String fullName,
            @RequestParam("email") String email, @RequestParam("mobile") String mobile,
            @RequestParam("peoplesIdentity") String peoplesIdentity,
            @RequestParam("gender") boolean gender,
            @RequestParam("bankName") String bankName,
            @RequestParam("bankNumber") String bankNumber,
            @RequestParam("bankAgency") String bankAgency, HttpSession session, ModelMap mm) {
        Message msg;
        Map adminSession = (Map) session.getAttribute("ADMIN");
        int myId = (int) adminSession.get("ADMIN_ID");
        int result = new CustomerFacade().edit(fullName, email, mobile, peoplesIdentity, bankName, bankNumber, bankAgency, gender, id);
        switch (result) {
            case 1: {
                LogUtils.logs(myId, LogUtils.ACTION_ADD, 1075, "Cập nhật thành viên id:'" + id + ", thất bại!");
                msg = new Message("Thất bại", "Bạn không có quyền thực hiện hành động này!", Message.ERROR_MESSAGE_CLASS);
                mm.put("MESSAGE", msg);
                break;
            }
            case 2: {
                LogUtils.logs(myId, LogUtils.ACTION_ADD, 1075, "Cập nhật thành viên id:'" + id + ", thất bại!");
                msg = new Message("Thất bại", "Quốc gia không đúng!", Message.ERROR_MESSAGE_CLASS);
                mm.put("MESSAGE", msg);
                break;
            }
            case 3: {
                LogUtils.logs(myId, LogUtils.ACTION_ADD, 1075, "Cập nhật thành viên id:'" + id + ", thất bại!");
                msg = new Message("Thất bại", "Mã điện thoại không đúng!", Message.ERROR_MESSAGE_CLASS);
                mm.put("MESSAGE", msg);
                break;
            }
            case 4: {
                LogUtils.logs(myId, LogUtils.ACTION_ADD, 1075, "Cập nhật thành viên id:'" + id + ", thành công!");
                msg = new Message("Thành công", "Cập nhật thành viên thành công!", Message.SUCCESS_MESSAGE_CLASS);
                mm.put("MESSAGE", msg);
                break;
            }
            default: {
                LogUtils.logs(myId, LogUtils.ACTION_ADD, 1075, "Edit customer id:'" + id + ", failed!");
                msg = new Message("Thất bại", "Đã xảy ra lỗi. Vui lòng thử lại sau!", Message.ERROR_MESSAGE_CLASS);
                mm.put("MESSAGE", msg);
                break;
            }
        }
        return new ModelAndView("MessageLayout");
    }

    @RequestMapping(value = "/ListCustomer/ViewUpRank", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView ViewUpRank(ModelMap mm) {
        mm.put("RANK_NAMES", new RankNameFacade().findAllAvaiable());
        return new ModelAndView("Ajax.AdminListCustomerViewUpRank");
    }

    @RequestMapping(value = "/ListCustomer/UpRank", method = RequestMethod.POST)
    @ResponseBody
    public ModelAndView UpRank(@RequestParam("cusID") int cusID, @RequestParam("rankID") int rankID, HttpSession session, ModelMap mm) {
        Message msg;
        Map adminSession = (Map) session.getAttribute("ADMIN");
        int myId = (int) adminSession.get("ADMIN_ID");
        Pair<Integer, String> result;
        try {
            result = new CustomerFacade().upRank(cusID, rankID);
            if (result.getKey() == 1) {
                LogUtils.logs(myId, LogUtils.ACTION_ADD, 9, "Điều chỉnh hạng thành công, id: " + cusID + " lên hạng: " + rankID);
                msg = new Message("Thành công", result.getValue(), Message.SUCCESS_MESSAGE_CLASS);
            } else {
                LogUtils.logs(myId, LogUtils.ACTION_ADD, 9, "Điều chỉnh hạng thất bại, id: " + cusID + " lên hạng: " + rankID);
                msg = new Message("Thất bại", result.getValue(), Message.ERROR_MESSAGE_CLASS);
            }
            mm.put("MESSAGE", msg);
        } catch (Exception e) {
            e.printStackTrace();
            LogUtils.logs(myId, LogUtils.ACTION_ADD, 9, "Điều chỉnh hạng thất bại, id: " + cusID + " lên hạng: " + rankID + ", " + e.getMessage());
            msg = new Message("Thất bại", "Đã xảy ra lỗi, Vui lòng thử lại sau!", Message.ERROR_MESSAGE_CLASS);
            mm.put("MESSAGE", msg);
        }
        return new ModelAndView("MessageLayout");
    }

    @RequestMapping(value = {"/ListCustomer/ResetPassword/{id}"}, method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView ResetPassword(@PathVariable(value = "id") Integer id,
            ModelMap mm, HttpSession session) {
        Message msg;
        Map adminSession = (Map) session.getAttribute("ADMIN");
        int myId = (int) adminSession.get("ADMIN_ID");
        Customer c = (Customer) new CustomerFacade().find(id);
        c.setPassword(CustomFunction.md5("123456"));
        try {
            new CustomerFacade().edit(c);
            LogUtils.logs(myId, LogUtils.ACTION_UPDATE, 9, c.getIsActive() ? "Reset password Customer id:" + id + " successfully" : "Reset password Customer id:" + id + ", successfully");
            msg = new Message("Thành công", "Reset mật khẩu về 123456 thành công", Message.SUCCESS_MESSAGE_CLASS);
            mm.put("MESSAGE", msg);
            return new ModelAndView("MessageLayout");
        } catch (Exception e) {
            e.printStackTrace();
            LogUtils.logs(myId, LogUtils.ACTION_UPDATE, 9, c.getIsActive() ? "Reset password Customer id:" + id + " successfully" : "Reset password Customer id:" + id + ", failed");
            msg = new Message("Thất bại", "Đã xảy ra lỗi. Vui lòng thử lại sau!", Message.ERROR_MESSAGE_CLASS);
            mm.put("MESSAGE", msg);
            return new ModelAndView("MessageLayout");
        }
    }

    @RequestMapping(value = {"/ListCustomer/Block/{id}"}, method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView DeleteRole(@PathVariable(value = "id") Integer id,
            ModelMap mm, HttpSession session) {
        Message msg;
        Map adminSession = (Map) session.getAttribute("ADMIN");
        int myId = (int) adminSession.get("ADMIN_ID");
        Customer c = (Customer) new CustomerFacade().find(id);
        c.setIsActive(!c.getIsActive());
        try {
            new CustomerFacade().edit(c);
            LogUtils.logs(myId, LogUtils.ACTION_UPDATE, 9, c.getIsActive() ? "Hiện thành viên id:" + id + " thành công" : "Ẩn thành viên id:" + id + ", thành công");
            msg = new Message("Thành công", c.getIsActive() ? "Hiện thành viên thành công!" : "Ẩn thành viên thành công!", Message.SUCCESS_MESSAGE_CLASS);
            mm.put("MESSAGE", msg);
            return new ModelAndView("MessageLayout");
        } catch (Exception e) {
            e.printStackTrace();
            LogUtils.logs(myId, LogUtils.ACTION_UPDATE, 9, c.getIsActive() ? "Hiện thành viên id:" + id + " thất bại" : "Ẩn thành viên id:" + id + ", thất bại");
            msg = new Message("Thất bại", "Đã xảy ra lỗi. Vui lòng thử lại sau!", Message.ERROR_MESSAGE_CLASS);
            mm.put("MESSAGE", msg);
            return new ModelAndView("MessageLayout");
        }
    }

    @RequestMapping(value = "/ListCustomerNotActivated", method = RequestMethod.GET)
    public String ListCustomerNotActivated(@RequestParam(value = "currentPage", required = false, defaultValue = "1") int currentPage,
            @RequestParam(value = "displayPerPage", required = false, defaultValue = "10") int displayPerPage,
            @RequestParam(value = "orderColumn", required = false, defaultValue = "id") String orderColmn,
            @RequestParam(value = "asc", required = false, defaultValue = "true") boolean asc,
            @RequestParam(value = "keyword", required = false, defaultValue = "") String keyword, ModelMap mm) {
        Pager pager = new Pager();
        pager.setAsc(asc);
        pager.setCurrentPage(currentPage);
        pager.setDisplayPerPage(displayPerPage);
        pager.setOrderColumn(orderColmn);
        pager.setKeyword(keyword);
        List result = new CustomerFacade().pagerNotActivated(pager);
        mm.put("ITEMS_LIST", result);
        mm.put("PAGER", pager);
        return "AdminListCustomerNotActivated";
    }

    @RequestMapping(value = "/ListCustomerNotActivated/Ajax", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView ListCustomerNotActivatedAjax(@RequestParam(value = "currentPage", required = false, defaultValue = "1") int currentPage,
            @RequestParam(value = "displayPerPage", required = false, defaultValue = "10") int displayPerPage,
            @RequestParam(value = "orderColumn", required = false, defaultValue = "id") String orderColmn,
            @RequestParam(value = "asc", required = false, defaultValue = "true") boolean asc,
            @RequestParam(value = "keyword", required = false, defaultValue = "") String keyword, ModelMap mm) {
        Pager pager = new Pager();
        pager.setAsc(asc);
        pager.setCurrentPage(currentPage);
        pager.setDisplayPerPage(displayPerPage);
        pager.setOrderColumn(orderColmn);
        pager.setKeyword(keyword);
        List result = new CustomerFacade().pagerNotActivated(pager);
        mm.put("ITEMS_LIST", result);
        mm.put("PAGER", pager);
        return new ModelAndView("Ajax.AdminListCustomerNotActivated");
    }

    @RequestMapping(value = "/ListCustomerBackUp", method = RequestMethod.GET)
    public String ListCustomerBackup(@RequestParam(value = "currentPage", required = false, defaultValue = "1") int currentPage,
            @RequestParam(value = "displayPerPage", required = false, defaultValue = "10") int displayPerPage,
            @RequestParam(value = "orderColumn", required = false, defaultValue = "id") String orderColmn,
            @RequestParam(value = "asc", required = false, defaultValue = "true") boolean asc,
            @RequestParam(value = "keyword", required = false, defaultValue = "") String keyword,
            @RequestParam(value = "isBackUp", required = false) Boolean isBackUp, ModelMap mm) {
        PagerPlus pager = new PagerPlus();
        pager.setAsc(asc);
        pager.setCurrentPage(currentPage);
        pager.setDisplayPerPage(displayPerPage);
        pager.setOrderColumn(orderColmn);
        pager.setKeyword(keyword);
        List result = new CustomerFacade().pagerBackup(pager, isBackUp);
        mm.put("ITEMS_LIST", result);
        mm.put("PAGER", pager);
        return "AdminListCustomerBackUp";
    }

    @RequestMapping(value = "/ListCustomerBackUp/Ajax", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView ListCustomerBackUpAjax(@RequestParam(value = "currentPage", required = false, defaultValue = "1") int currentPage,
            @RequestParam(value = "displayPerPage", required = false, defaultValue = "10") int displayPerPage,
            @RequestParam(value = "orderColumn", required = false, defaultValue = "id") String orderColmn,
            @RequestParam(value = "asc", required = false, defaultValue = "true") boolean asc,
            @RequestParam(value = "keyword", required = false, defaultValue = "") String keyword,
            @RequestParam(value = "isBackUp", required = false) Boolean isBackUp, ModelMap mm) {
        PagerPlus pager = new PagerPlus();
        pager.setAsc(asc);
        pager.setCurrentPage(currentPage);
        pager.setDisplayPerPage(displayPerPage);
        pager.setOrderColumn(orderColmn);
        pager.setKeyword(keyword);
        List result = new CustomerFacade().pagerBackup(pager, isBackUp);
        mm.put("ITEMS_LIST", result);
        mm.put("PAGER", pager);
        return new ModelAndView("Ajax.AdminListCustomerBackUp");
    }

    @RequestMapping(value = "/ListCustomerBackUp/BackUp", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView CreateSystemNews(@RequestParam(value = "listID", required = false) String listID, ModelMap mm, HttpSession session) {
        Message msg;
        Map adminSession = (Map) session.getAttribute("ADMIN");
        int myId = (int) adminSession.get("ADMIN_ID");
        Pair<Integer, String> result;
        try {
            result = new CustomerFacade().backUp(listID, myId);
            if (result.getKey() == 1) {
                LogUtils.logs(myId, LogUtils.ACTION_ADD, 9, "Bảo lưu người dùng thành công, " + (StringUtils.isEmpty(listID) ? "tất cả" : listID));
                msg = new Message("Thành công", "Bảo lưu người dùng thành công!", Message.SUCCESS_MESSAGE_CLASS);
            } else {
                LogUtils.logs(myId, LogUtils.ACTION_ADD, 9, "Bảo lưu người dùng thất bại, " + (StringUtils.isEmpty(listID) ? "tất cả" : listID));
                msg = new Message("Thất bại", "Bảo lưu người dùng thất bại!", Message.ERROR_MESSAGE_CLASS);
            }
            mm.put("MESSAGE", msg);
        } catch (Exception e) {
            e.printStackTrace();
            LogUtils.logs(myId, LogUtils.ACTION_ADD, 9, "Bảo lưu người dùng thất bại, " + e.getMessage());
            msg = new Message("Thất bại", "Đã xảy ra lỗi, Vui lòng thử lại sau!", Message.ERROR_MESSAGE_CLASS);
            mm.put("MESSAGE", msg);
        }
        return new ModelAndView("MessageLayout");
    }

    @RequestMapping(value = "/ListDeposit", method = RequestMethod.GET)
    public String ListDeposit(@RequestParam(value = "currentPage", required = false, defaultValue = "1") int currentPage,
            @RequestParam(value = "displayPerPage", required = false, defaultValue = "10") int displayPerPage,
            @RequestParam(value = "orderColumn", required = false, defaultValue = "createdDate") String orderColmn,
            @RequestParam(value = "asc", required = false, defaultValue = "false") boolean asc,
            @RequestParam(value = "keyword", required = false, defaultValue = "") String keyword, ModelMap mm, HttpSession session) {
        Map adminSession = (Map) session.getAttribute("ADMIN");
        int myId = (int) adminSession.get("ADMIN_ID");
        Pager pager = new Pager();
        pager.setAsc(asc);
        pager.setCurrentPage(currentPage);
        pager.setDisplayPerPage(displayPerPage);
        pager.setOrderColumn(orderColmn);
        pager.setKeyword(keyword);
        List result = new HistoryDepositFacade().pager(pager, myId);
        mm.put("ITEMS_LIST", result);
        mm.put("PAGER", pager);
        return "AdminListDeposit";
    }

    @RequestMapping(value = "/ListDeposit/Ajax", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView ListDepositAjax(@RequestParam(value = "currentPage", required = false, defaultValue = "1") int currentPage,
            @RequestParam(value = "displayPerPage", required = false, defaultValue = "10") int displayPerPage,
            @RequestParam(value = "orderColumn", required = false, defaultValue = "createdDate") String orderColmn,
            @RequestParam(value = "asc", required = false, defaultValue = "false") boolean asc,
            @RequestParam(value = "keyword", required = false, defaultValue = "") String keyword, ModelMap mm, HttpSession session) {
        Map adminSession = (Map) session.getAttribute("ADMIN");
        int myId = (int) adminSession.get("ADMIN_ID");
        Pager pager = new Pager();
        pager.setAsc(asc);
        pager.setCurrentPage(currentPage);
        pager.setDisplayPerPage(displayPerPage);
        pager.setOrderColumn(orderColmn);
        pager.setKeyword(keyword);
        List result = new HistoryDepositFacade().pager(pager, myId);
        mm.put("ITEMS_LIST", result);
        mm.put("PAGER", pager);
        return new ModelAndView("Ajax.AdminListDeposit");
    }

    @RequestMapping(value = "/ListDeposit/Insert", method = RequestMethod.POST)
    @ResponseBody
    public ModelAndView CreateDeposit(@RequestParam(value = "receivingCustomerUsername") String receivingCustomerUsername,
            @RequestParam(value = "amount") BigDecimal amount, @RequestParam(value = "discountPercent") BigDecimal discountPercent,
            ModelMap mm, HttpSession session) {
        Message msg;
        Map adminSession = (Map) session.getAttribute("ADMIN");
        int myId = (int) adminSession.get("ADMIN_ID");
        Pair<Integer, String> result;
        try {
            result = new HistoryDepositFacade().deposit(myId, receivingCustomerUsername, amount, discountPercent);
            if (result.getKey() == 1) {
                LogUtils.logs(myId, LogUtils.ACTION_ADD, 9, "Nạp tiền thành công cho " + receivingCustomerUsername + " , " + amount + "đ");
                msg = new Message("Thành công", result.getValue(), Message.SUCCESS_MESSAGE_CLASS);
            } else {
                LogUtils.logs(myId, LogUtils.ACTION_ADD, 9, "Nạp tiền thất bại cho " + receivingCustomerUsername + " , " + amount + "đ");
                msg = new Message("Thất bại", result.getValue(), Message.ERROR_MESSAGE_CLASS);
            }
            mm.put("MESSAGE", msg);
        } catch (Exception e) {
            e.printStackTrace();
            LogUtils.logs(myId, LogUtils.ACTION_ADD, 9, "Nạp tiền thất bại cho " + receivingCustomerUsername + " , " + amount + "đ, " + e.getMessage());
            msg = new Message("Thất bại", "Đã xảy ra lỗi. Vui lòng thử lại sau!", Message.ERROR_MESSAGE_CLASS);
            mm.put("MESSAGE", msg);
        }
        return new ModelAndView("MessageLayout");
    }

    @RequestMapping(value = "/ListSystemNews", method = RequestMethod.GET)
    public String ListSystemNews(@RequestParam(value = "currentPage", required = false, defaultValue = "1") int currentPage,
            @RequestParam(value = "displayPerPage", required = false, defaultValue = "10") int displayPerPage,
            @RequestParam(value = "orderColumn", required = false, defaultValue = "id") String orderColmn,
            @RequestParam(value = "asc", required = false, defaultValue = "true") boolean asc,
            @RequestParam(value = "keyword", required = false, defaultValue = "") String keyword, ModelMap mm) {
        Pager pager = new Pager();
        pager.setAsc(asc);
        pager.setCurrentPage(currentPage);
        pager.setDisplayPerPage(displayPerPage);
        pager.setOrderColumn(orderColmn);
        pager.setKeyword(keyword);
        List result = new SystemNewsFacade().pager(pager, null);
        mm.put("ITEMS_LIST", result);
        mm.put("PAGER", pager);
        return "AdminListSystemNews";
    }

    @RequestMapping(value = "/ListSystemNews/Ajax", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView ListSystemNewsAjax(@RequestParam(value = "currentPage", required = false, defaultValue = "1") int currentPage,
            @RequestParam(value = "displayPerPage", required = false, defaultValue = "10") int displayPerPage,
            @RequestParam(value = "orderColumn", required = false, defaultValue = "id") String orderColmn,
            @RequestParam(value = "asc", required = false, defaultValue = "true") boolean asc,
            @RequestParam(value = "keyword", required = false, defaultValue = "") String keyword, ModelMap mm) {
        Pager pager = new Pager();
        pager.setAsc(asc);
        pager.setCurrentPage(currentPage);
        pager.setDisplayPerPage(displayPerPage);
        pager.setOrderColumn(orderColmn);
        pager.setKeyword(keyword);
        List result = new SystemNewsFacade().pager(pager, null);
        mm.put("ITEMS_LIST", result);
        mm.put("PAGER", pager);
        return new ModelAndView("Ajax.AdminListSystemNews");
    }

    @RequestMapping(value = "/ListSystemNews/Insert", method = RequestMethod.POST)
    @ResponseBody
    public ModelAndView CreateSystemNews(@RequestParam(value = "customerUsername") String customerUsername,
            @RequestParam(value = "title") String title, @RequestParam(value = "urlAvatar") String urlAvatar,
            @RequestParam(value = "shortDescription") String shortDescription, @RequestParam(value = "content") String content,
            ModelMap mm, HttpSession session) {
        Message msg;
        Map adminSession = (Map) session.getAttribute("ADMIN");
        int myId = (int) adminSession.get("ADMIN_ID");
        Pair<Integer, String> result;
        try {
            result = new SystemNewsFacade().create(myId, customerUsername, title, urlAvatar, shortDescription, content);
            if (result.getKey() == 1) {
                LogUtils.logs(myId, LogUtils.ACTION_ADD, 9, "Tạo tin tức thành công");
                msg = new Message("Thành công", result.getValue(), Message.SUCCESS_MESSAGE_CLASS);
            } else {
                LogUtils.logs(myId, LogUtils.ACTION_ADD, 9, "Tạo tin tức thất bại, " + result.getValue());
                msg = new Message("Thất bại", result.getValue(), Message.ERROR_MESSAGE_CLASS);
            }
            mm.put("MESSAGE", msg);
        } catch (Exception e) {
            e.printStackTrace();
            LogUtils.logs(myId, LogUtils.ACTION_ADD, 9, "Tạo tin tức thất bại, " + e.getMessage());
            msg = new Message("Thất bại", "Đã xảy ra lỗi, Vui lòng thử lại sau!", Message.ERROR_MESSAGE_CLASS);
            mm.put("MESSAGE", msg);
        }
        return new ModelAndView("MessageLayout");
    }

    @RequestMapping(value = "/ListSystemNews/ViewEdit/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView ViewEditSystemNewsAjax(@PathVariable(value = "id") int id, ModelMap mm) {
        mm.put("SELECTED_NEWS", new SystemNewsFacade().findEager(id));
        return new ModelAndView("Ajax.AdminListSystemNewsEditView");
    }

    @RequestMapping(value = "/ListSystemNews/Edit", method = RequestMethod.POST)
    @ResponseBody
    public ModelAndView EditSystemNews(@RequestParam(value = "id") int id, @RequestParam(value = "customerUsername") String customerUsername,
            @RequestParam(value = "title") String title, @RequestParam(value = "urlAvatar") String urlAvatar,
            @RequestParam(value = "shortDescription") String shortDescription, @RequestParam(value = "content") String content,
            ModelMap mm, HttpSession session) {
        Message msg;
        Map adminSession = (Map) session.getAttribute("ADMIN");
        int myId = (int) adminSession.get("ADMIN_ID");
        Pair<Integer, String> result;
        try {
            result = new SystemNewsFacade().edit(id, customerUsername, title, urlAvatar, shortDescription, content);
            if (result.getKey() == 1) {
                LogUtils.logs(myId, LogUtils.ACTION_ADD, 9, "Cập nhật tin tức thành công, " + id);
                msg = new Message("Thành công", result.getValue(), Message.SUCCESS_MESSAGE_CLASS);
            } else {
                LogUtils.logs(myId, LogUtils.ACTION_ADD, 9, "Cập nhật tin tức thất bại, " + result.getValue() + " ,id: " + id);
                msg = new Message("Thất bại", result.getValue(), Message.ERROR_MESSAGE_CLASS);
            }
            mm.put("MESSAGE", msg);
        } catch (Exception e) {
            e.printStackTrace();
            LogUtils.logs(myId, LogUtils.ACTION_ADD, 9, "Cập nhật tin tức thất bại, " + e.getMessage() + " ,id: " + id);
            msg = new Message("Thất bại", "Đã xảy ra lỗi, Vui lòng thử lại sau!", Message.ERROR_MESSAGE_CLASS);
            mm.put("MESSAGE", msg);
        }
        return new ModelAndView("MessageLayout");
    }

    @RequestMapping(value = {"/ListSystemNews/Block/{id}/{status}"}, method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView BlockSystemNews(@PathVariable(value = "id") Integer id, @PathVariable(value = "status") boolean status,
            ModelMap mm, HttpSession session) {
        Message msg;
        Map adminSession = (Map) session.getAttribute("ADMIN");
        int myId = (int) adminSession.get("ADMIN_ID");
        try {
            SystemNewsFacade sNFacade = new SystemNewsFacade();
            SystemNews news = sNFacade.findEager(id);
            news.setIsShow(status);
            sNFacade.edit(news);
            LogUtils.logs(myId, LogUtils.ACTION_UPDATE, 9, status ? "Hiện tin tức id:" + id + " thành công" : "Ẩn tin tức id:" + id + ", thành công");
            msg = new Message("Thành công", news.getIsShow() ? "Hiện tin tức thành công!" : "Ẩn tin tức thành công!", Message.SUCCESS_MESSAGE_CLASS);
            mm.put("MESSAGE", msg);
            return new ModelAndView("MessageLayout");
        } catch (Exception e) {
            e.printStackTrace();
            LogUtils.logs(myId, LogUtils.ACTION_UPDATE, 9, (status ? "Hiện tin tức id:" + id + " thất bại" : "Ẩn tin tức id:" + id + ", thất bại") + ", " + e.getMessage());
            msg = new Message("Thất bại", "Đã xảy ra lỗi. Vui lòng thử lại sau!", Message.ERROR_MESSAGE_CLASS);
            mm.put("MESSAGE", msg);
            return new ModelAndView("MessageLayout");
        }
    }

    @RequestMapping(value = {"/ListSystemNews/Delete/{id}"}, method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView DeleteSystemNews(@PathVariable(value = "id") Integer id,
            ModelMap mm, HttpSession session) {
        Message msg;
        Map adminSession = (Map) session.getAttribute("ADMIN");
        int myId = (int) adminSession.get("ADMIN_ID");
        try {
            SystemNewsFacade sNFacade = new SystemNewsFacade();
            int result = sNFacade.delete(id);
            if (result > 0) {
                LogUtils.logs(myId, LogUtils.ACTION_UPDATE, 9, "Xóa tin tức id:" + id + ", thành công");
                msg = new Message("Thành công", "Xóa tin tức thành công!", Message.SUCCESS_MESSAGE_CLASS);
                mm.put("MESSAGE", msg);
                return new ModelAndView("MessageLayout");
            }
            throw new Exception();
        } catch (Exception e) {
            e.printStackTrace();
            LogUtils.logs(myId, LogUtils.ACTION_UPDATE, 9, "Xóa tin tức thất bại, " + e.getMessage());
            msg = new Message("Thất bại", "Đã xảy ra lỗi. Vui lòng thử lại sau!", Message.ERROR_MESSAGE_CLASS);
            mm.put("MESSAGE", msg);
            return new ModelAndView("MessageLayout");
        }
    }
}
