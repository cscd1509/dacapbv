package com.controller.admin;

import com.bean.Message;
import com.bean.Pager;
import com.entity.SystemConfig;
import com.facade.SystemConfigFacade;
import com.utils.LogUtils;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/Admin/Config")
public class AdminSystemConfigController {

    @RequestMapping(value = "/ListConfig", method = RequestMethod.GET)
    public String ListConfig(@RequestParam(value = "currentPage", required = false, defaultValue = "1") int currentPage,
            @RequestParam(value = "displayPerPage", required = false, defaultValue = "10") int displayPerPage,
            @RequestParam(value = "orderColumn", required = false, defaultValue = "id") String orderColmn,
            @RequestParam(value = "keyword", required = false, defaultValue = "") String keyword,
            @RequestParam(value = "asc", required = false, defaultValue = "true") boolean asc, ModelMap mm) {
        Pager pager = new Pager();
        pager.setAsc(asc);
        pager.setCurrentPage(currentPage);
        pager.setDisplayPerPage(displayPerPage);
        pager.setKeyword(keyword);
        pager.setOrderColumn(orderColmn);
        List result = new SystemConfigFacade().pager(pager);
        mm.put("ITEMS_LIST", result);
        mm.put("PAGER", pager);
        return "AdminListConfig";
    }

    @RequestMapping(value = "/ListConfig/Ajax", method = RequestMethod.GET)
    public String ListConfigAjax(@RequestParam(value = "currentPage", required = false, defaultValue = "1") int currentPage,
            @RequestParam(value = "displayPerPage", required = false, defaultValue = "10") int displayPerPage,
            @RequestParam(value = "orderColumn", required = false, defaultValue = "id") String orderColmn,
            @RequestParam(value = "keyword", required = false, defaultValue = "") String keyword,
            @RequestParam(value = "asc", required = false, defaultValue = "true") boolean asc, ModelMap mm) {
        Pager pager = new Pager();
        pager.setAsc(asc);
        pager.setCurrentPage(currentPage);
        pager.setDisplayPerPage(displayPerPage);
        pager.setKeyword(keyword);
        pager.setOrderColumn(orderColmn);
        List result = new SystemConfigFacade().pager(pager);
        mm.put("ITEMS_LIST", result);
        mm.put("PAGER", pager);
        return "Ajax.AdminListConfig";
    }

    @RequestMapping(value = {"/ListConfig/ChangeStatus/{id}"}, method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView ChangePermission(@PathVariable(value = "id") Integer id,
            ModelMap mm, HttpSession session, HttpServletRequest request) {
        Message msg;
        Map adminSession = (Map) session.getAttribute("ADMIN");
        int myId = (int) adminSession.get("ADMIN_ID");
        SystemConfig s = (SystemConfig) new SystemConfigFacade().find(id);
        try {
            s.setIsTrue(!s.getIsTrue());
            new SystemConfigFacade().edit(s);
            Map<String, Boolean> configs = (Map) request.getServletContext().getAttribute("SYSTEM_CONFIG");
            configs.put(String.valueOf(s.getId()), s.getIsTrue());
            request.getServletContext().setAttribute("SYSTEM_CONFIG", configs);
            LogUtils.logs(myId, LogUtils.ACTION_UPDATE, 9, "Update config, id:" + id + " to " + s.getIsTrue() + " successfully");
            msg = new Message("Success", "Update config successfully", Message.SUCCESS_MESSAGE_CLASS);
            mm.put("MESSAGE", msg);
            return new ModelAndView("MessageLayout");
        } catch (Exception e) {
            e.printStackTrace();
            LogUtils.logs(myId, LogUtils.ACTION_UPDATE, 9, "Update config, id:" + id + " to " + s.getIsTrue() + " successfully");
            msg = new Message("Error", "Update config failed", Message.ERROR_MESSAGE_CLASS);
            mm.put("MESSAGE", msg);
            return new ModelAndView("MessageLayout");
        }
    }
}
