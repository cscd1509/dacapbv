package com.controller.admin;

import com.bean.ExcelFile;
import com.bean.Message;
import com.bean.Pager;
import com.bean.PagerPlus;
import com.facade.CheckAwardFacade;
import com.facade.CircleWithdrawFacade;
import com.facade.HistoryAwardFacade;
import com.utils.LogUtils;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javafx.util.Pair;
import javax.servlet.http.HttpSession;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/Admin/Award")
public class AdminAwardController {

    @RequestMapping(value = "/ListAward", method = RequestMethod.GET)
    public String ListAward(@RequestParam(value = "currentPage", required = false, defaultValue = "1") int currentPage,
            @RequestParam(value = "displayPerPage", required = false, defaultValue = "10") int displayPerPage,
            @RequestParam(value = "orderColumn", required = false, defaultValue = "createdDate") String orderColmn,
            @RequestParam(value = "asc", required = false, defaultValue = "false") boolean asc,
            @RequestParam(value = "startDate", required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") Date startDate,
            @RequestParam(value = "endDate", required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") Date endDate,
            @RequestParam(value = "checkAwardID", required = false) Integer checkAwardID,
            @RequestParam(value = "keyword", required = false) String keyword, ModelMap mm) {
        PagerPlus pager = new PagerPlus();
        pager.setAsc(asc);
        pager.setCurrentPage(currentPage);
        pager.setDisplayPerPage(displayPerPage);
        pager.setOrderColumn(orderColmn);
        pager.setKeyword(keyword);
        pager.setIsCheck(checkAwardID);
        if (startDate != null && endDate != null) {
            Calendar c = Calendar.getInstance();
            c.setTime(endDate);
            c.set(Calendar.HOUR_OF_DAY, 23);
            c.set(Calendar.MINUTE, 59);
            c.set(Calendar.SECOND, 59);
            c.set(Calendar.MILLISECOND, 9999);
            endDate = c.getTime();
            pager.setStartDate(startDate);
            pager.setEndDate(endDate);
        }
        List result = new HistoryAwardFacade().pager(pager, null);
        mm.put("ITEMS_LIST", result);
        mm.put("PAGER", pager);
        return "AdminListAward";
    }

    @RequestMapping(value = "/ListAward/Ajax", method = RequestMethod.GET)
    public String ListAwardAjax(@RequestParam(value = "currentPage", required = false, defaultValue = "1") int currentPage,
            @RequestParam(value = "displayPerPage", required = false, defaultValue = "10") int displayPerPage,
            @RequestParam(value = "orderColumn", required = false, defaultValue = "createdDate") String orderColmn,
            @RequestParam(value = "asc", required = false, defaultValue = "false") boolean asc,
            @RequestParam(value = "startDate", required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") Date startDate,
            @RequestParam(value = "endDate", required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") Date endDate,
            @RequestParam(value = "checkAwardID", required = false) Integer checkAwardID,
            @RequestParam(value = "keyword", required = false) String keyword, ModelMap mm) {
        PagerPlus pager = new PagerPlus();
        pager.setAsc(asc);
        pager.setCurrentPage(currentPage);
        pager.setDisplayPerPage(displayPerPage);
        pager.setOrderColumn(orderColmn);
        pager.setKeyword(keyword);
        pager.setIsCheck(checkAwardID);
        if (startDate != null && endDate != null) {
            Calendar c = Calendar.getInstance();
            c.setTime(endDate);
            c.set(Calendar.HOUR_OF_DAY, 23);
            c.set(Calendar.MINUTE, 59);
            c.set(Calendar.SECOND, 59);
            c.set(Calendar.MILLISECOND, 9999);
            endDate = c.getTime();
            pager.setStartDate(startDate);
            pager.setEndDate(endDate);
        }
        List result = new HistoryAwardFacade().pager(pager, null);
        mm.put("ITEMS_LIST", result);
        mm.put("PAGER", pager);
        return "Ajax.AdminListAward";
    }

    @RequestMapping(value = "/ListAward/RunSystem", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView RunSystem(ModelMap mm, HttpSession session) {
        Message msg;
        Map adminSession = (Map) session.getAttribute("ADMIN");
        int myId = (int) adminSession.get("ADMIN_ID");
        Pair<Integer, String> result;
        try {
            result = new HistoryAwardFacade().runSystem();
            if (result.getKey() == 1) {
                LogUtils.logs(myId, LogUtils.ACTION_ADD, 9, result.getValue());
                msg = new Message("Thành công", result.getValue(), Message.SUCCESS_MESSAGE_CLASS);
            } else {
                LogUtils.logs(myId, LogUtils.ACTION_ADD, 9, result.getValue());
                msg = new Message("Thất bại", result.getValue(), Message.ERROR_MESSAGE_CLASS);
            }
            mm.put("MESSAGE", msg);
        } catch (Exception e) {
            e.printStackTrace();
            LogUtils.logs(myId, LogUtils.ACTION_ADD, 9, "Chạy hoa hồng thất bại!");
            msg = new Message("Thất bại", "Đã xảy ra lỗi. Vui lòng thử lại sau!", Message.ERROR_MESSAGE_CLASS);
            mm.put("MESSAGE", msg);
        }
        return new ModelAndView("MessageLayout");
    }

    @RequestMapping(value = "/ListAward/Export", method = RequestMethod.GET)
    public ModelAndView exportListAwardView(
            @RequestParam(value = "orderColumn", required = false, defaultValue = "id") String orderColmn,
            @RequestParam(value = "startDate", required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") Date startDate,
            @RequestParam(value = "endDate", required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") Date endDate,
            @RequestParam(value = "checkAwardID", required = false) Integer checkAwardID,
            @RequestParam(value = "keyword", required = false) String keyword,
            @RequestParam(value = "asc", required = false, defaultValue = "false") boolean asc, HttpSession session) {
        PagerPlus pager = new PagerPlus();
        pager.setAsc(asc);
        pager.setOrderColumn(orderColmn);
        pager.setKeyword(keyword);
        pager.setIsCheck(checkAwardID);
        if (startDate != null && endDate != null) {
            Calendar c = Calendar.getInstance();
            c.setTime(endDate);
            c.set(Calendar.HOUR_OF_DAY, 23);
            c.set(Calendar.MINUTE, 59);
            c.set(Calendar.SECOND, 59);
            c.set(Calendar.MILLISECOND, 9999);
            endDate = c.getTime();
            pager.setStartDate(startDate);
            pager.setEndDate(endDate);
        }
        ExcelFile file = new ExcelFile();
        new HistoryAwardFacade().exportAward(pager, file);
        return new ModelAndView("ExcelView", "myModel", file);
    }

    @RequestMapping(value = "/ListCircle", method = RequestMethod.GET)
    public String ListCircle(@RequestParam(value = "currentPage", required = false, defaultValue = "1") int currentPage,
            @RequestParam(value = "displayPerPage", required = false, defaultValue = "10") int displayPerPage,
            @RequestParam(value = "orderColumn", required = false, defaultValue = "createdDate") String orderColmn,
            @RequestParam(value = "asc", required = false, defaultValue = "false") boolean asc,
            @RequestParam(value = "keyword", required = false) String keyword, ModelMap mm) {
        Pager pager = new PagerPlus();
        pager.setAsc(asc);
        pager.setCurrentPage(currentPage);
        pager.setDisplayPerPage(displayPerPage);
        pager.setOrderColumn(orderColmn);
        pager.setKeyword(keyword);
        List result = new CircleWithdrawFacade().pager(pager);
        mm.put("ITEMS_LIST", result);
        mm.put("PAGER", pager);
        return "AdminListCircle";
    }

    @RequestMapping(value = "/ListCircle/Ajax", method = RequestMethod.GET)
    public String ListCircleAjax(@RequestParam(value = "currentPage", required = false, defaultValue = "1") int currentPage,
            @RequestParam(value = "displayPerPage", required = false, defaultValue = "10") int displayPerPage,
            @RequestParam(value = "orderColumn", required = false, defaultValue = "createdDate") String orderColmn,
            @RequestParam(value = "asc", required = false, defaultValue = "false") boolean asc,
            @RequestParam(value = "keyword", required = false) String keyword, ModelMap mm) {
        Pager pager = new PagerPlus();
        pager.setAsc(asc);
        pager.setCurrentPage(currentPage);
        pager.setDisplayPerPage(displayPerPage);
        pager.setOrderColumn(orderColmn);
        pager.setKeyword(keyword);
        List result = new CircleWithdrawFacade().pager(pager);
        mm.put("ITEMS_LIST", result);
        mm.put("PAGER", pager);
        return "Ajax.AdminListCircle";
    }

    @RequestMapping(value = "/ListCircle/Detail/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView ViewInsert(ModelMap mm, @PathVariable(value = "id") int id) {
        CircleWithdrawFacade cwFacade = new CircleWithdrawFacade();
        mm.put("CIRCLE", cwFacade.find(id));
        mm.put("RESULT", cwFacade.circleDetail(id, null));
        return new ModelAndView("Ajax.AdminListCircleDetail");
    }

    @RequestMapping(value = "/ListCircle/Paid/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView Paid(@PathVariable(value = "id") int id, HttpSession session, ModelMap mm) {
        Map adminSession = (Map) session.getAttribute("ADMIN");
        int adId = (int) adminSession.get("ADMIN_ID");
        Pair<Integer, String> result;
        try {
            CircleWithdrawFacade cwFacade = new CircleWithdrawFacade();
            result = cwFacade.paid(id);
            Message msg = new Message();
            msg.setMsg(result.getValue());
            if (result.getKey() == 1) {
                msg.setTitle("Thành công");
                msg.setMsgClass(Message.SUCCESS_MESSAGE_CLASS);
                LogUtils.logs(adId, LogUtils.ACTION_ADD, 68, "Paid circle " + id + " success!");
            } else {
                msg.setTitle("Thất bại");
                msg.setMsgClass(Message.ERROR_MESSAGE_CLASS);
                LogUtils.logs(adId, LogUtils.ACTION_ADD, 68, "Paid circle " + id + " failed!");
            }
            mm.put("MESSAGE", msg);
        } catch (Exception e) {
            e.printStackTrace();
            LogUtils.logs(adId, LogUtils.ACTION_ADD, 68, "Paid circle " + id + " failed!");
        }
        return new ModelAndView("MessageLayout");
    }

    @RequestMapping(value = "/ListCircle/Delete/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView Delete(@PathVariable(value = "id") int id, HttpSession session, ModelMap mm) {
        Map adminSession = (Map) session.getAttribute("ADMIN");
        int adId = (int) adminSession.get("ADMIN_ID");
        Pair<Integer, String> result;
        try {
            CircleWithdrawFacade cwFacade = new CircleWithdrawFacade();
            result = cwFacade.delete(id);
            Message msg = new Message();
            msg.setMsg(result.getValue());
            if (result.getKey() == 1) {
                msg.setTitle("Thành công");
                msg.setMsgClass(Message.SUCCESS_MESSAGE_CLASS);
                LogUtils.logs(adId, LogUtils.ACTION_ADD, 68, "Delete circle " + id + " success!");
            } else {
                msg.setTitle("Thất bại");
                msg.setMsgClass(Message.ERROR_MESSAGE_CLASS);
                LogUtils.logs(adId, LogUtils.ACTION_ADD, 68, "Delete circle " + id + " failed!");
            }
            mm.put("MESSAGE", msg);
        } catch (Exception e) {
            e.printStackTrace();
            LogUtils.logs(adId, LogUtils.ACTION_ADD, 68, "Delete circle " + id + " failed!");
        }
        return new ModelAndView("MessageLayout");
    }

    @RequestMapping(value = "/ListCircle/Insert", method = RequestMethod.POST)
    @ResponseBody
    public ModelAndView Delete(@RequestParam(value = "startDate", required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") Date startDate,
            @RequestParam(value = "endDate", required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") Date endDate, HttpSession session, ModelMap mm) {
        Map adminSession = (Map) session.getAttribute("ADMIN");
        int adId = (int) adminSession.get("ADMIN_ID");
        Pair<Integer, String> result;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        try {
            if (startDate != null && endDate != null) {
                Calendar c = Calendar.getInstance();
                c.setTime(endDate);
                c.set(Calendar.HOUR_OF_DAY, 23);
                c.set(Calendar.MINUTE, 59);
                c.set(Calendar.SECOND, 59);
                c.set(Calendar.MILLISECOND, 9999);
                endDate = c.getTime();
            }
            CircleWithdrawFacade cwFacade = new CircleWithdrawFacade();
            result = cwFacade.createCircleWithdraw(startDate, endDate);
            Message msg = new Message();
            msg.setMsg(result.getValue());
            if (result.getKey() == 1) {
                msg.setTitle("Thành công");
                msg.setMsgClass(Message.SUCCESS_MESSAGE_CLASS);
                LogUtils.logs(adId, LogUtils.ACTION_ADD, 68, "Insert circle from " + sdf.format(startDate) + " to " + sdf.format(endDate) + " success!");
            } else {
                msg.setTitle("Thất bại");
                msg.setMsgClass(Message.ERROR_MESSAGE_CLASS);
                LogUtils.logs(adId, LogUtils.ACTION_ADD, 68, "Insert circle from " + sdf.format(startDate) + " to " + sdf.format(endDate) + " failed!");
            }
            mm.put("MESSAGE", msg);
        } catch (Exception e) {
            e.printStackTrace();
            LogUtils.logs(adId, LogUtils.ACTION_ADD, 68, "Insert circle from " + sdf.format(startDate) + " to " + sdf.format(endDate) + " failed!");
        }
        return new ModelAndView("MessageLayout");
    }

    @RequestMapping(value = "/ListCheckAward", method = RequestMethod.GET)
    public String ListCheckAward(@RequestParam(value = "currentPage", required = false, defaultValue = "1") int currentPage,
            @RequestParam(value = "displayPerPage", required = false, defaultValue = "10") int displayPerPage,
            @RequestParam(value = "orderColumn", required = false, defaultValue = "id") String orderColmn,
            @RequestParam(value = "asc", required = false, defaultValue = "true") boolean asc,
            @RequestParam(value = "keyword", required = false, defaultValue = "") String keyword, ModelMap mm) {
        Pager pager = new Pager();
        pager.setAsc(asc);
        pager.setCurrentPage(currentPage);
        pager.setDisplayPerPage(displayPerPage);
        pager.setOrderColumn(orderColmn);
        pager.setKeyword(keyword);
        List result = new CheckAwardFacade().pager(pager);
        mm.put("ITEMS_LIST", result);
        mm.put("PAGER", pager);
        return "AdminListCheckAward";
    }

    @RequestMapping(value = "/ListCheckAward/Ajax", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView ListCheckAwardAjax(@RequestParam(value = "currentPage", required = false, defaultValue = "1") int currentPage,
            @RequestParam(value = "displayPerPage", required = false, defaultValue = "10") int displayPerPage,
            @RequestParam(value = "orderColumn", required = false, defaultValue = "id") String orderColmn,
            @RequestParam(value = "asc", required = false, defaultValue = "true") boolean asc,
            @RequestParam(value = "keyword", required = false, defaultValue = "") String keyword, ModelMap mm) {
        Pager pager = new Pager();
        pager.setAsc(asc);
        pager.setCurrentPage(currentPage);
        pager.setDisplayPerPage(displayPerPage);
        pager.setOrderColumn(orderColmn);
        pager.setKeyword(keyword);
        List result = new CheckAwardFacade().pager(pager);
        mm.put("ITEMS_LIST", result);
        mm.put("PAGER", pager);
        return new ModelAndView("Ajax.AdminListCheckAward");
    }

    @RequestMapping(value = "/ListCheckAward/UpdateAward", method = RequestMethod.POST)
    @ResponseBody
    public ModelAndView CreateCheckAward(@RequestParam(value = "checkAwardID") int checkAwardID,
            @RequestParam(value = "newPrice") BigDecimal newPrice, ModelMap mm, HttpSession session) {
        Message msg;
        Map adminSession = (Map) session.getAttribute("ADMIN");
        int myId = (int) adminSession.get("ADMIN_ID");
        Pair<Integer, String> result;
        try {
            result = new CheckAwardFacade().updateAward(myId, newPrice);
            if (result.getKey() == 1) {
                LogUtils.logs(myId, LogUtils.ACTION_ADD, 9, "Cập nhật bảng giá thành công, id: " + checkAwardID + " , price: " + newPrice);
                msg = new Message("Thành công", "Cập nhật bảng giá thành công!", Message.SUCCESS_MESSAGE_CLASS);
            } else {
                LogUtils.logs(myId, LogUtils.ACTION_ADD, 9, "Cập nhật bảng giá thất bại, id: " + checkAwardID + " , price: " + newPrice);
                msg = new Message("Thất bại", "Cập nhật bảng giá thất bại!", Message.ERROR_MESSAGE_CLASS);
            }
            mm.put("MESSAGE", msg);
        } catch (Exception e) {
            e.printStackTrace();
            LogUtils.logs(myId, LogUtils.ACTION_ADD, 9, "Cập nhật bảng giá thất bại, id: " + checkAwardID + " , price: " + newPrice + ", " + e.getMessage());
            msg = new Message("Thất bại", "Đã xảy ra lỗi. Vui lòng thử lại sau!", Message.ERROR_MESSAGE_CLASS);
            mm.put("MESSAGE", msg);
        }
        return new ModelAndView("MessageLayout");
    }
}
