/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controller.admin;

import com.bean.Message;
import com.bean.Pager;
import com.entity.ReporterQuery;
import com.facade.CustomerFacade;
import com.facade.ReporterQueryAliasFacade;
import com.facade.ReporterQueryFacade;
import com.utils.AliasType;
import com.utils.LogUtils;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

/**
 *
 * @author dell
 */
@Controller
@RequestMapping(value = "/Admin/Report")
public class AdminReportController {

    @RequestMapping(value = "/ListReport", method = RequestMethod.GET)
    public String ListReport(@RequestParam(value = "currentPage", required = false, defaultValue = "1") int currentPage,
            @RequestParam(value = "displayPerPage", required = false, defaultValue = "10") int displayPerPage,
            @RequestParam(value = "orderColumn", required = false, defaultValue = "id") String orderColmn,
            @RequestParam(value = "asc", required = false, defaultValue = "false") boolean asc,
            @RequestParam(value = "keyword", required = false, defaultValue = "") String keyword, ModelMap mm, HttpSession session) {
        Pager pager = new Pager();
        pager.setAsc(asc);
        pager.setCurrentPage(currentPage);
        pager.setDisplayPerPage(displayPerPage);
        pager.setOrderColumn(orderColmn);
        pager.setKeyword(keyword);
        List result = new ReporterQueryFacade().pager(pager, null);
        mm.put("ITEMS_LIST", result);
        mm.put("PAGER", pager);
        mm.put("ALIAS_TYPE_ENUM", AliasType.values());
        return "AdminListReport";
    }

    @RequestMapping(value = "/ListReport/Ajax", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView ListReportAjax(@RequestParam(value = "currentPage", required = false, defaultValue = "1") int currentPage,
            @RequestParam(value = "displayPerPage", required = false, defaultValue = "10") int displayPerPage,
            @RequestParam(value = "orderColumn", required = false, defaultValue = "id") String orderColmn,
            @RequestParam(value = "asc", required = false, defaultValue = "false") boolean asc,
            @RequestParam(value = "keyword", required = false, defaultValue = "") String keyword, ModelMap mm, HttpSession session) {
        Pager pager = new Pager();
        pager.setAsc(asc);
        pager.setCurrentPage(currentPage);
        pager.setDisplayPerPage(displayPerPage);
        pager.setOrderColumn(orderColmn);
        pager.setKeyword(keyword);
        List result = new ReporterQueryFacade().pager(pager, null);
        mm.put("ITEMS_LIST", result);
        mm.put("PAGER", pager);
        return new ModelAndView("Ajax.AdminListReport");
    }

    @RequestMapping(value = "/ListReport/ViewInsert", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView ListReportViewInsert(ModelMap mm) {
        mm.put("ALIAS_TYPE_ENUM", AliasType.values());
        return new ModelAndView("Ajax.AdminListReportInsertModal");
    }

    @RequestMapping(value = "/ListReport/Insert", method = RequestMethod.POST)
    @ResponseBody
    public ModelAndView InsertReporter(@RequestBody ReporterQuery reporter, HttpSession session, ModelMap mm) {
        Message msg;
        Map adminSession = (Map) session.getAttribute("ADMIN");
        int myId = (int) adminSession.get("ADMIN_ID");
        try {
            new ReporterQueryFacade().create(reporter);
            LogUtils.logs(myId, LogUtils.ACTION_UPDATE, 9, "Tạo báo cáo thành công: " + reporter.getId());
            msg = new Message("Thành công", "Tạo báo cáo thành công!", Message.SUCCESS_MESSAGE_CLASS);
        } catch (Exception e) {
            e.printStackTrace();
            LogUtils.logs(myId, LogUtils.ACTION_UPDATE, 9, "Tạo báo cáo thất bại");
            msg = new Message("Thất bại", "Đã xảy ra lỗi. Vui lòng thử lại sau!", Message.ERROR_MESSAGE_CLASS);
        }
        mm.put("MESSAGE", msg);
        return new ModelAndView("MessageLayout");
    }

    @RequestMapping(value = "/ListReport/ViewEdit/{reporterID}", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView ListReportViewEdit(@PathVariable("reporterID") int reporterID, ModelMap mm) {
        ReporterQuery reporter = (ReporterQuery) new ReporterQueryFacade().find(reporterID);
        reporter.setReporterQueryAliases(new ReporterQueryAliasFacade().findAllAliasByReportID(reporterID));
        mm.put("SELECTED_REPORTER", reporter);
        mm.put("ALIAS_TYPE_ENUM", AliasType.values());
        return new ModelAndView("Ajax.AdminListReportEditModal");
    }

    @RequestMapping(value = "/ListReport/Edit", method = RequestMethod.POST)
    @ResponseBody
    public ModelAndView EditReporter(@RequestBody ReporterQuery reporter, HttpSession session, ModelMap mm) {
        Message msg;
        Map adminSession = (Map) session.getAttribute("ADMIN");
        int myId = (int) adminSession.get("ADMIN_ID");
        try {
            new ReporterQueryFacade().edit(reporter);
            LogUtils.logs(myId, LogUtils.ACTION_UPDATE, 9, "Cập nhật báo cáo thành công: " + reporter.getId());
            msg = new Message("Thành công", "Cập nhật báo cáo thành công", Message.SUCCESS_MESSAGE_CLASS);
        } catch (Exception e) {
            e.printStackTrace();
            LogUtils.logs(myId, LogUtils.ACTION_UPDATE, 9, "Cập nhật báo cáo thất bại: " + reporter.getId());
            msg = new Message("Thất bại", "Đã xảy ra lỗi. Vui lòng thử lại sau", Message.ERROR_MESSAGE_CLASS);
        }
        mm.put("MESSAGE", msg);
        return new ModelAndView("MessageLayout");
    }

    @RequestMapping(value = "/ListReport/Run/{reporterID}", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView ListReportViewRun(@PathVariable("reporterID") int reporterID, ModelMap mm, HttpSession session) {
        ReporterQuery reporter = (ReporterQuery) new ReporterQueryFacade().find(reporterID);
        reporter.setReporterQueryAliases(new ReporterQueryAliasFacade().findAllAliasByReportID(reporterID));
        mm.put("SELECTED_REPORTER", reporter);
        mm.put("ITEMS_LIST", new ReporterQueryFacade().run(reporter));
        return new ModelAndView("Ajax.AdminListReportRunModal");

    }

    @RequestMapping(value = "/ListReport/ChangeStatus/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView changeStatusReporter(@PathVariable(value = "id") Integer id, ModelMap mm, HttpSession session) {
        Message msg;
        Map adminSession = (Map) session.getAttribute("ADMIN");
        int myId = (int) adminSession.get("ADMIN_ID");
        ReporterQuery reporter = (ReporterQuery) new ReporterQueryFacade().find(id);
        try {
            reporter.setIsActive(!reporter.getIsActive());
            new CustomerFacade().edit(reporter);
            LogUtils.logs(myId, LogUtils.ACTION_UPDATE, 9, (reporter.getIsActive() ? "Hiện" : "Ẩn") + " báo cáo thành công: " + id);
            msg = new Message("Thành công", "Cập nhật báo cáo thành công", Message.SUCCESS_MESSAGE_CLASS);
        } catch (Exception e) {
            e.printStackTrace();
            LogUtils.logs(myId, LogUtils.ACTION_UPDATE, 9, (reporter.getIsActive() ? "Hiện" : "Ẩn") + " báo cáo thất bại: " + id);
            msg = new Message("Thất bại", "Đã xảy ra lỗi. Vui lòng thử lại sau!", Message.ERROR_MESSAGE_CLASS);
        }
        mm.put("MESSAGE", msg);
        return new ModelAndView("MessageLayout");
    }

    @RequestMapping(value = {"/ListReport/Delete/{id}"}, method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView DeleteReporterQuery(@PathVariable(value = "id") Integer id,
            ModelMap mm, HttpSession session) {
        Message msg;
        Map adminSession = (Map) session.getAttribute("ADMIN");
        int myId = (int) adminSession.get("ADMIN_ID");
        try {
            new ReporterQueryFacade().delete(id);
            LogUtils.logs(myId, LogUtils.ACTION_UPDATE, 9, "Xóa báo cáo thành công: " + id);
            msg = new Message("Thành công", "Xóa báo cáo thành công", Message.SUCCESS_MESSAGE_CLASS);
        } catch (Exception e) {
            e.printStackTrace();
            LogUtils.logs(myId, LogUtils.ACTION_UPDATE, 9, "Xóa báo cáo thất bại: " + id);
            msg = new Message("Thất bại", "Đã xảy ra lỗi. Vui lòng thử lại sau!", Message.ERROR_MESSAGE_CLASS);
        }
        mm.put("MESSAGE", msg);
        return new ModelAndView("MessageLayout");
    }
}
