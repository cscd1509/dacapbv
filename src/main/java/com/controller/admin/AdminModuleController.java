package com.controller.admin;

import com.bean.Message;
import com.entity.Module;
import com.facade.AdminFacade;
import com.facade.ModuleFacade;
import com.utils.LogUtils;
import java.util.Map;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/Admin/Module")
public class AdminModuleController {

    //ListModule    
    @RequestMapping(value = "/ListModule", method = RequestMethod.GET)
    public String getDefaultListModuleView(ModelMap mm, HttpSession session) {
        mm.put("MODULES", new ModuleFacade().findAllModuleByLevel(1));
        return "AdminListModule";
    }

    //ListModuleAjax  
    @RequestMapping(value = "/ListModule/Ajax", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView getDefaultListModuleAjaxView(ModelMap mm, HttpSession session) {
        mm.put("MODULES", new ModuleFacade().findAllModuleByLevel(1));
        return new ModelAndView("Ajax.AdminListModule");
    }

    @RequestMapping(value = "/ListModule/ViewInsert/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView getListModuleInsertView(@PathVariable(value = "id") Integer id, ModelMap mm, HttpSession session) {
        Module module = (Module) new ModuleFacade().find(id);
        mm.put("MODULE_PARENT", module);
        return new ModelAndView("Ajax.AdminListModuleViewInsert");
    }

    @RequestMapping(value = "/ListModule/Insert", method = RequestMethod.POST)
    @ResponseBody
    public ModelAndView insertModule(@RequestBody Map module, ModelMap mm, HttpSession session) {
        Message msg;
        Map adminSession = (Map) session.getAttribute("ADMIN");
        int admId = (int) adminSession.get("ADMIN_ID");
        try {
            Integer role = new AdminFacade().getAdminRoleByAdminId(admId);
            if (role != 1) {
                LogUtils.logs(admId, LogUtils.ACTION_ADD, 56, "Thêm mới module thất bại, không có quyền");
                msg = new Message("Thất bại", "Thêm mới module thất bại", Message.ERROR_MESSAGE_CLASS);
                mm.put("MESSAGE", msg);
                return new ModelAndView("MessageLayout");
            }
            new ModuleFacade().create(module);
            LogUtils.logs(admId, LogUtils.ACTION_ADD, 56, "Thêm mới module thành công");
            msg = new Message("Thành công", "Thêm mới module thành công!", Message.SUCCESS_MESSAGE_CLASS);
        } catch (Exception e) {
            e.printStackTrace();
            LogUtils.logs(admId, LogUtils.ACTION_ADD, 56, "Thêm mới module thất bại");
            msg = new Message("Thất bại", "Thêm mới module thất bại!", Message.ERROR_MESSAGE_CLASS);
        }
        mm.put("MESSAGE", msg);
        return new ModelAndView("MessageLayout");
    }

    @RequestMapping(value = "/ListModule/ViewEdit/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView getListModuleEditView(@PathVariable(value = "id") Integer id, ModelMap mm, HttpSession session) {
        Module module = (Module) new ModuleFacade().find(id);
        mm.put("MODULE_EDIT", module);
        return new ModelAndView("Ajax.AdminListModuleViewEdit");
    }

    @RequestMapping(value = "/ListModule/Edit", method = RequestMethod.POST)
    @ResponseBody
    public ModelAndView editModule(@RequestBody Map module, ModelMap mm, HttpSession session) {
        Message msg;
        Map adminSession = (Map) session.getAttribute("ADMIN");
        int admId = (int) adminSession.get("ADMIN_ID");
        try {
            Integer role = new AdminFacade().getAdminRoleByAdminId(admId);
            if (role != 1) {
                LogUtils.logs(admId, LogUtils.ACTION_ADD, 56, "Cập nhật module thất bại, không có quyền");
                msg = new Message("Thất bại", "Cập nhật module thất bại!", Message.ERROR_MESSAGE_CLASS);
                mm.put("MESSAGE", msg);
                return new ModelAndView("MessageLayout");
            }
            new ModuleFacade().edit(module);
            LogUtils.logs(admId, LogUtils.ACTION_ADD, 56, "Cập nhật module thành công");
            msg = new Message("Thành công", "Cập nhật module thành công!", Message.SUCCESS_MESSAGE_CLASS);
        } catch (Exception e) {
            e.printStackTrace();
            LogUtils.logs(admId, LogUtils.ACTION_ADD, 56, "Cập nhật module thất bại, " + e.getMessage());
            msg = new Message("Thất bại", "Cập nhật module thất bại!", Message.ERROR_MESSAGE_CLASS);
        }
        mm.put("MESSAGE", msg);
        return new ModelAndView("MessageLayout");
    }

    @RequestMapping(value = "/ListModule/Delete/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView deleteCustomer(@PathVariable(value = "id") Integer id, ModelMap mm, HttpSession session) {
        Message msg;
        Map adminSession = (Map) session.getAttribute("ADMIN");
        int admId = (int) adminSession.get("ADMIN_ID");
        try {
            Integer role = new AdminFacade().getAdminRoleByAdminId(admId);
            if (role != 1) {
                LogUtils.logs(admId, LogUtils.ACTION_UPDATE, 56, "Xóa module thất bại, không có quyền: " + id);
                msg = new Message("Thất bại", "Xóa module thất bại!", Message.ERROR_MESSAGE_CLASS);
                mm.put("MESSAGE", msg);
                return new ModelAndView("MessageLayout");
            }
            Module module = (Module) new ModuleFacade().find(id);
            module.setIsDelete(true);
            new ModuleFacade().edit(module);
            LogUtils.logs(admId, LogUtils.ACTION_DELETE, 56, "Xóa module thành công: " + id);
            msg = new Message("Thành công", "Delete module successfully!", Message.SUCCESS_MESSAGE_CLASS);
        } catch (Exception e) {
            e.printStackTrace();
            LogUtils.logs(admId, LogUtils.ACTION_DELETE, 56, "Xóa module thất bại: " + id + ", " + e.getMessage());
            msg = new Message("Thất bại", "Xóa module thất bại!", Message.ERROR_MESSAGE_CLASS);
        }
        mm.put("MESSAGE", msg);
        return new ModelAndView("MessageLayout");
    }

    @RequestMapping(value = "/ListModule/ChangeStatus/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView changeStatusCustomer(@PathVariable(value = "id") Integer id, ModelMap mm, HttpSession session) {
        Message msg;
        Map adminSession = (Map) session.getAttribute("ADMIN");
        int admId = (int) adminSession.get("ADMIN_ID");
        try {
            Integer role = new AdminFacade().getAdminRoleByAdminId(admId);
            if (role != 1) {
                LogUtils.logs(admId, LogUtils.ACTION_ADD, 56, "Cập nhật module thất bại, không có quyền");
                msg = new Message("Thất bại", "Cập nhật module thất bại!", Message.ERROR_MESSAGE_CLASS);
                mm.put("MESSAGE", msg);
                return new ModelAndView("MessageLayout");
            }
            Module module = (Module) new ModuleFacade().find(id);
            module.setIsShow(!module.getIsShow());
            new ModuleFacade().edit(module);
            LogUtils.logs(admId, LogUtils.ACTION_ADD, 56, "Cập nhật module thành công");
            msg = new Message("Thành công", "Cập nhật module thành công!", Message.SUCCESS_MESSAGE_CLASS);
        } catch (Exception e) {
            e.printStackTrace();
            LogUtils.logs(admId, LogUtils.ACTION_ADD, 56, "Cập nhật module thất bại, " + e.getMessage());
            msg = new Message("Thất bại", "Cập nhật module thất bại!", Message.ERROR_MESSAGE_CLASS);
        }
        mm.put("MESSAGE", msg);
        return new ModelAndView("MessageLayout");
    }
}
