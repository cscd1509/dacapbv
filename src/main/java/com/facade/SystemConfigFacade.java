package com.facade;

import com.bean.Pager;
import com.entity.SystemConfig;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

public class SystemConfigFacade extends AbstractFacade {

    public SystemConfigFacade() {
        super(SystemConfig.class);
    }

    public Map<String, Boolean> getAllAvailable() {
        Session session = null;
        Map<String, Boolean> map = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Criteria cr = session.createCriteria(entityClass);
                cr.add(Restrictions.eq("isDelete", false));
                List<SystemConfig> list = cr.list();
                if (list != null && list.size() > 0) {
                    map = new HashMap();
                    for (SystemConfig s : list) {
                        map.put(String.valueOf(s.getId()), s.getIsTrue());
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return map;
    }

    public List<SystemConfig> pager(Pager pager) {
        Session session = null;
        List<SystemConfig> list = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Criteria cr = session.createCriteria(SystemConfig.class);
                cr.add(Restrictions.eq("isDelete", false));
                cr.setProjection(Projections.rowCount());
                pager.setTotalResult(((Long) cr.uniqueResult()).intValue());
                cr = session.createCriteria(SystemConfig.class);
                cr.add(Restrictions.eq("isDelete", false));
                cr.setFirstResult(pager.getFirstResult());
                cr.setMaxResults(pager.getDisplayPerPage());
                cr.addOrder(pager.getAsc() ? Order.asc(pager.getOrderColumn()) : Order.desc(pager.getOrderColumn()));
                list = cr.list();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return list;
    }
}
