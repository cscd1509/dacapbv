package com.facade;

import com.bean.ExcelFile;
import com.bean.Pager;
import com.bean.PagerPlus;
import com.entity.HistoryAward;
import com.entity.PinSystem;
import com.utils.StringUtils;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javafx.util.Pair;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.hibernate.transform.Transformers;
import org.hibernate.type.BigDecimalType;
import org.hibernate.type.DateType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;

public class HistoryAwardFacade extends AbstractFacade {

    public HistoryAwardFacade() {
        super(HistoryAward.class);
    }

    public List<HistoryAward> pager(PagerPlus pager, Integer customerID) {
        Session session = null;
        List<HistoryAward> rs = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Criteria cr = session.createCriteria(HistoryAward.class, "h");
                cr.createAlias("customerID", "customerID", JoinType.LEFT_OUTER_JOIN);
                cr.createAlias("partnerID", "partnerID", JoinType.LEFT_OUTER_JOIN);
                cr.createAlias("checkAwardID", "checkAwardID", JoinType.LEFT_OUTER_JOIN);
                Conjunction conj = Restrictions.conjunction();
                conj.add(Restrictions.eq("isDeleted", false));
                if (customerID != null) {
                    conj.add(Restrictions.eq("customerID.id", customerID));
                }
                if (!StringUtils.isEmpty(pager.getKeyword())) {
                    conj.add(Restrictions.eq("customerID.userName", pager.getKeyword()));
                }
                if (pager.getIsCheck() != null) {
                    conj.add(Restrictions.eq("checkAwardID.id", pager.getIsCheck()));
                }
                if (pager.getIsAvailability() != null) {
                    switch (pager.getIsAvailability()) {
                        case 0: {
                            conj.add(Restrictions.eq("isAvailability", false));
                            break;
                        }
                        case 1: {
                            conj.add(Restrictions.eq("isAvailability", true));
                            break;
                        }
                    }
                }
                if (pager.getStartDate() != null || pager.getEndDate() != null) {
                    if (pager.getStartDate() == null) {
                        Calendar c = Calendar.getInstance();
                        c.set(Calendar.YEAR, c.get(Calendar.YEAR) - 100);
                        pager.setStartDate(c.getTime());
                    }
                    if (pager.getEndDate() == null) {
                        Calendar c = Calendar.getInstance();
                        c.set(Calendar.YEAR, c.get(Calendar.YEAR) + 100);
                        pager.setEndDate(c.getTime());
                    }
                    conj.add(Restrictions.between("createdDate", pager.getStartDate(), pager.getEndDate()));
                }
                cr.add(conj);
                cr.setProjection(Projections.sum("amount"));
                pager.setAmount((BigDecimal) cr.uniqueResult());
                cr.setProjection(Projections.rowCount());
                pager.setTotalResult(((Long) cr.uniqueResult()).intValue());
                cr = session.createCriteria(HistoryAward.class, "h");
                cr.createAlias("customerID", "customerID", JoinType.LEFT_OUTER_JOIN);
                cr.createAlias("partnerID", "partnerID", JoinType.LEFT_OUTER_JOIN);
                cr.createAlias("checkAwardID", "checkAwardID", JoinType.LEFT_OUTER_JOIN);
                cr.setFetchMode("customerID", FetchMode.JOIN);
                cr.setFetchMode("partnerID", FetchMode.JOIN);
                cr.setFetchMode("checkAwardID", FetchMode.JOIN);
                cr.add(conj);
                cr.setFirstResult(pager.getFirstResult());
                cr.setMaxResults(pager.getDisplayPerPage());
                cr.addOrder(pager.getAsc() ? Order.asc(pager.getOrderColumn()) : Order.desc(pager.getOrderColumn()));
                rs = cr.list();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return rs;
    }

    public void exportAward(PagerPlus pager, ExcelFile file) {
        Session session = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                String queryStr = "select cus.UserName as Customer,part.UserName as Partner,ca.Name as CheckAward, ha.Amount, case when ha.IsAvailability=1 then N'Khả dụng' else N'Không khả dụng' end as Availability,ha.CreatedDate "
                        + "from HistoryAward ha left join Customer cus on ha.CustomerID=cus.ID left join Customer part on ha.PartnerID=part.id left join CheckAward ca on ha.CheckAwardID=ca.ID where ha.IsDeleted=0 ";

                if (!StringUtils.isEmpty(pager.getKeyword())) {
                    queryStr += " cus.userName like '%" + pager.getKeyword() + "%' or part.userName like '%" + pager.getKeyword() + "%'";
                }
                if (pager.getIsCheck() != null) {
                    queryStr += " ca.id=" + pager.getIsCheck();
                }
                if (pager.getIsAvailability() != null) {
                    queryStr += " ha.isAvailability=" + pager.getIsAvailability();
                }
                if (pager.getStartDate() != null || pager.getEndDate() != null) {
                    if (pager.getStartDate() == null) {
                        Calendar c = Calendar.getInstance();
                        c.set(Calendar.YEAR, c.get(Calendar.YEAR) - 100);
                        pager.setStartDate(c.getTime());
                    }
                    if (pager.getEndDate() == null) {
                        Calendar c = Calendar.getInstance();
                        c.set(Calendar.YEAR, c.get(Calendar.YEAR) + 100);
                        pager.setEndDate(c.getTime());
                    }
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    queryStr += " ha.createdDate between '" + sdf.format(pager.getStartDate()) + "' and '" + sdf.format(pager.getEndDate()) + "'";
                }
                Query q = session.createSQLQuery(queryStr)
                        .addScalar("Customer", StringType.INSTANCE)
                        .addScalar("Partner", StringType.INSTANCE)
                        .addScalar("CheckAward", StringType.INSTANCE)
                        .addScalar("Amount", BigDecimalType.INSTANCE)
                        .addScalar("Availability", StringType.INSTANCE)
                        .addScalar("CreatedDate", DateType.INSTANCE);
                file.setContents(q.list());
                List<String> header = new ArrayList();
                header.add("Người thụ hưởng");
                header.add("Từ");
                header.add("Kiểu thưởng");
                header.add("Số tiền");
                header.add("Khả dụng");
                header.add("Thời gian");
                file.setTitles(header);
                file.setFileName("Dach sach thuong");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
    }

    public BigDecimal sumAwardByCusID(int cusID) {
        Session session = null;
        BigDecimal rs = BigDecimal.ZERO;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Query q = session.createSQLQuery("select ISNULL(SUM(Amount),0) from HistoryAward where CustomerID=:cusID");
                q.setParameter("cusID", cusID);
                rs = (BigDecimal) q.uniqueResult();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return rs;
    }

    public Object[] getAwardChartData(int cusID) {
        Session session = null;
        Object[] rs = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Query q = session.createSQLQuery("HistoryAwardChartData :cusID")
                        .addScalar("Total1", BigDecimalType.INSTANCE)
                        .addScalar("Total2", BigDecimalType.INSTANCE)
                        .addScalar("Total3", BigDecimalType.INSTANCE);
                q.setParameter("cusID", cusID);
                rs = (Object[]) q.uniqueResult();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return rs;
    }

    public List getAwardChartBar(int cusID, int year) {
        Session session = null;
        List rs = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Query q = session.createSQLQuery("HistoryAwardBarData :cusID,:year")
                        .addScalar("month", StringType.INSTANCE)
                        .addScalar("value", BigDecimalType.INSTANCE);
                q.setParameter("cusID", cusID)
                        .setParameter("year", year);
                q.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
                rs = (List) q.list();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return rs;
    }
    public Pair<Integer, String> runSystem() throws Exception {
        Transaction trans = null;
        Session session = null;
        Pair<Integer, String> result = new Pair(0, "Đã xảy ra lỗi. Vui lòng thực hiện lại sau!");
        try {
            session = com.facade.HibernateConfiguration.getInstance().openSession();
            trans = session.beginTransaction();
            Query q = session.createSQLQuery("Run_system")
                    .addScalar("Result", IntegerType.INSTANCE).addScalar("Msg", StringType.INSTANCE);
            Object[] row = (Object[]) q.uniqueResult();
            result = new Pair<>((Integer) row[0], (String) row[1]);
            trans.commit();
        } catch (HibernateException e) {
            try {
                if (trans != null) {
                    trans.rollback();
                }
            } catch (Exception ex) {
                throw ex;
            }
            e.printStackTrace();
            throw e;
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return result;
    }

}
