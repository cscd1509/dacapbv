package com.facade;

import com.bean.ExcelFile;
import com.bean.Pager;
import com.entity.PinSystem;
import com.utils.StringUtils;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javafx.util.Pair;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;

public class PinSystemFacade extends AbstractFacade {

    public PinSystemFacade() {
        super(PinSystem.class);
    }

    public List pager(Pager pager, boolean isOwned) {
        Session session = null;
        List list = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Criteria cr = session.createCriteria(PinSystem.class);
                cr.createAlias("customerID", "customerID", JoinType.LEFT_OUTER_JOIN);
                Conjunction conj = Restrictions.conjunction();
                if (isOwned) {
                    conj.add(Restrictions.isNotNull("customerID"));
                } else {
                    conj.add(Restrictions.isNull("customerID"));
                }
                if (!StringUtils.isEmpty(pager.getKeyword())) {
                    conj.add(
                            Restrictions.or(
                                    Restrictions.like("customerID.userName", "%" + pager.getKeyword() + "%"),
                                    Restrictions.like("pinCode", "%" + pager.getKeyword() + "%")
                            )
                    );
                }
                cr.add(conj);
                cr.setProjection(Projections.rowCount());
                pager.setTotalResult(((Long) cr.uniqueResult()).intValue());
                cr = session.createCriteria(PinSystem.class);
                cr.createAlias("customerID", "customerID", JoinType.LEFT_OUTER_JOIN);
                cr.add(conj);
                cr.setFirstResult(pager.getFirstResult());
                cr.setMaxResults(pager.getDisplayPerPage());
                cr.addOrder(pager.getAsc() ? Order.asc(pager.getOrderColumn()) : Order.desc(pager.getOrderColumn()));
                list = cr.list();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return list;
    }

    public void exportNotUsed(Pager pager, ExcelFile file) {
        Session session = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Criteria cr = session.createCriteria(PinSystem.class);
                cr.add(Restrictions.isNull("customerID"));
                cr.setProjection(Projections.rowCount());
                cr.setFirstResult(pager.getFirstResult());
                if (pager.getDisplayPerPage() > 0) {
                    cr.setMaxResults(pager.getDisplayPerPage());
                }
                cr.addOrder(pager.getAsc() ? Order.asc(pager.getOrderColumn()) : Order.desc(pager.getOrderColumn()));
                cr.setProjection(Projections.projectionList()
                        .add(Projections.property("id"), "id")
                        .add(Projections.property("pinCode"), "pinCode")
                        .add(Projections.property("createdDate"), "createdDate"));
                file.setContents(cr.list());
                List<String> header = new ArrayList();
                header.add("ID");
                header.add("Mã PIN");
                header.add("Ngày Tạo");
                file.setTitles(header);
                Calendar c = Calendar.getInstance();
                file.setFileName("Dach sach ma PIN");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
    }

    public Pair<Integer, String> insertPinSys(Integer count) {
        Session session = null;
        Transaction tran = null;
        Pair<Integer, String> result = new Pair(0, "Đã xảy ra lỗi. Vui lòng thử lại sau!");
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                tran = session.beginTransaction();
                Query q = session.createSQLQuery("Create_PinCode :count")
                        .addScalar("Result", IntegerType.INSTANCE).addScalar("Msg", StringType.INSTANCE);
                q.setParameter("count", count);
                Object[] row = (Object[]) q.uniqueResult();
                result = new Pair<>((Integer) row[0], (String) row[1]);
                tran.commit();
                session.flush();
            }
        } catch (Exception e) {
            if (tran != null) {
                tran.rollback();
            }
            e.printStackTrace();
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return result;
    }

    public Pair<Integer, String> usingPinSys(Integer customerID, String pinCode) {
        Session session = null;
        Transaction tran = null;
        Pair<Integer, String> result = new Pair(0, "Đã xảy ra lỗi. Vui lòng thử lại sau!");
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                tran = session.beginTransaction();
                Query q = session.createSQLQuery("Using_Pin :customerID,:pinCode")
                        .addScalar("Result", IntegerType.INSTANCE).addScalar("Msg", StringType.INSTANCE);
                q.setParameter("customerID", customerID).setParameter("pinCode", pinCode);
                Object[] row = (Object[]) q.uniqueResult();
                result = new Pair<>((Integer) row[0], (String) row[1]);
                tran.commit();
                session.flush();
            }
        } catch (Exception e) {
            if (tran != null) {
                tran.rollback();
            }
            e.printStackTrace();
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return result;
    }
}
