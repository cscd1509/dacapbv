package com.facade;

import com.bean.Pager;
import com.entity.MessageSystem;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;

public class MessageSystemFacade extends AbstractFacade {

    public MessageSystemFacade() {
        super(MessageSystem.class);
    }

    public int countMessageInDay(int cusId) {
        Session session = null;
        int result = 5;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            Criteria cr = session.createCriteria(entityClass);
            cr.createAlias("customerID", "customerID");
            cr.add(Restrictions.sqlRestriction("CAST(GETDATE() AS DATE)=CAST(this_.CreatedDate AS DATE)"));
            cr.add(Restrictions.eq("isDelete", false));
            cr.add(Restrictions.eq("customerID.id", cusId));
            cr.setProjection(Projections.rowCount());
            result = ((Long) cr.uniqueResult()).intValue();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return result;
    }

    public List<MessageSystem> pagerNotRead(Pager pager, boolean read) {
        Session session = null;
        List<MessageSystem> list = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Criteria cr = session.createCriteria(entityClass, "m");
                cr.add(Restrictions.eq("isDelete", false));
                cr.add(Restrictions.eq("isCheck", read));
                cr.setProjection(Projections.rowCount());
                pager.setTotalResult(((Long) cr.uniqueResult()).intValue());
                cr = session.createCriteria(entityClass);
                cr.createAlias("customerID", "customerID", JoinType.LEFT_OUTER_JOIN);
                cr.createAlias("categoryID", "categoryID", JoinType.LEFT_OUTER_JOIN);
                cr.setFetchMode("customerID", FetchMode.JOIN);
                cr.setFetchMode("categoryID", FetchMode.JOIN);
                cr.add(Restrictions.eq("isDelete", false));
                cr.add(Restrictions.eq("isCheck", read));
                cr.setFirstResult(pager.getFirstResult());
                cr.setMaxResults(pager.getDisplayPerPage());
                cr.addOrder(pager.getAsc() ? Order.asc(pager.getOrderColumn()) : Order.desc(pager.getOrderColumn()));
                list = cr.list();
            }
        } catch (Exception e) {
            Logger.getLogger(entityClass.getName()).log(Level.SEVERE, null, e);
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return list;
    }

    public MessageSystem findEager(int id) {
        Session session = null;
        MessageSystem obj = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            obj = (MessageSystem) session.get(entityClass, id);
            Hibernate.initialize(obj.getCustomerID());
            Hibernate.initialize(obj.getCategoryID());
        } catch (Exception e) {
            Logger.getLogger(entityClass.getName()).log(Level.SEVERE, null, e);
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return obj;
    }
}
