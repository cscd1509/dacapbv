package com.facade;

import com.bean.CustomerTree;
import com.bean.ExcelFile;
import com.bean.Pager;
import com.bean.PagerPlus;
import com.entity.Customer;
import com.utils.CustomFunction;
import com.utils.StringUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.util.Pair;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.type.StringType;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.hibernate.transform.Transformers;
import org.hibernate.type.BigDecimalType;
import org.hibernate.type.DateType;
import org.hibernate.type.IntegerType;

public class CustomerFacade extends AbstractFacade {

    public CustomerFacade() {
        super(Customer.class);
    }

    public int updateWalletAddress(int cusID, String walletID, String walletAddress) {
        Transaction trans = null;
        Session session = null;
        int rs = 0;
        try {
            session = com.facade.HibernateConfiguration.getInstance().openSession();
            trans = session.beginTransaction();
            Query q = session.createSQLQuery("Update Customer set WalletID=:walletID,WalletAddress=:walletAddress where ID=:cusID");
            q.setParameter("cusID", cusID)
                    .setParameter("walletID", walletID)
                    .setParameter("walletAddress", walletAddress);
            rs = q.executeUpdate();
            trans.commit();
        } catch (HibernateException e) {
            try {
                if (trans != null) {
                    trans.rollback();
                }
            } catch (Exception ex) {
                throw ex;
            }
            e.printStackTrace();
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return rs;
    }

    public Pair<Integer, String> createUser(String fullName, String password, String email, String refIDUserName,
            String mobile, Boolean gender, String username, String peoplesIdentity, String bankName, String bankNumber, String bankAgency) {
        Transaction trans = null;
        Session session = null;
        Pair<Integer, String> result = new Pair(0, "Đã xảy ra lỗi. Vui lòng thực hiện lại sau!");
        try {
            session = com.facade.HibernateConfiguration.getInstance().openSession();
            trans = session.beginTransaction();
            Query q = session.createSQLQuery("CreateUser :fullName,:password,:email,:refIDUserName,:mobile,:gender,:username"
                    + ",:peoplesIdentity,:bankName,:bankNumber,:bankAgency")
                    .addScalar("Result", IntegerType.INSTANCE).addScalar("Msg", StringType.INSTANCE);
            q.setParameter("fullName", fullName)
                    .setParameter("password", CustomFunction.md5(password))
                    .setParameter("email", email)
                    .setParameter("refIDUserName", refIDUserName)
                    .setParameter("mobile", mobile)
                    .setParameter("gender", gender)
                    .setParameter("username", username)
                    .setParameter("peoplesIdentity", peoplesIdentity)
                    .setParameter("bankName", bankName)
                    .setParameter("bankNumber", bankNumber)
                    .setParameter("bankAgency", bankAgency);
            Object[] row = (Object[]) q.uniqueResult();
            result = new Pair<>((Integer) row[0], (String) row[1]);
            trans.commit();
        } catch (HibernateException e) {
            try {
                if (trans != null) {
                    trans.rollback();
                }
            } catch (Exception ex) {
                throw ex;
            }
            e.printStackTrace();
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return result;
    }

    public Customer checkLogin(String username, String password) throws Exception {
        Session session = null;
        Customer result = null;
        try {
            session = com.facade.HibernateConfiguration.getInstance().openSession();
            Criteria cr = session.createCriteria(Customer.class);
            cr.createAlias("parentID", "parentID", JoinType.LEFT_OUTER_JOIN);
            cr.setFetchMode("parentID", FetchMode.JOIN);
            cr.createAlias("refID", "refID", JoinType.LEFT_OUTER_JOIN);
            cr.setFetchMode("refID", FetchMode.JOIN);
            cr.createAlias("rankCustomer", "rankCustomer", JoinType.LEFT_OUTER_JOIN);
            cr.setFetchMode("rankCustomer", FetchMode.JOIN);
            cr.add(Restrictions.eq("isDeleted", false));
            cr.add(Restrictions.eq("userName", username));
            cr.add(Restrictions.eq("password", CustomFunction.md5(password)));
            result = (Customer) cr.uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return result;
    }

    public void updateProfile(String fullName, String email, String mobile, String peoplesIdentity, String bankName, String bankNumber, String bankAgency,
            boolean gender, int cusId) throws Exception {
        Transaction trans = null;
        Session session = null;
        try {
            session = com.facade.HibernateConfiguration.getInstance().openSession();
            trans = session.beginTransaction();
            Query q = session.createSQLQuery("UpdateProfile :fullName,:email,:mobile,:peoplesIdentity,:bankName,:bankNumber,:bankAgency,:gender,:cusId");
            q.setParameter("fullName", fullName)
                    .setParameter("email", email)
                    .setParameter("mobile", mobile)
                    .setParameter("peoplesIdentity", peoplesIdentity)
                    .setParameter("bankName", bankName)
                    .setParameter("bankNumber", bankNumber)
                    .setParameter("bankAgency", bankAgency)
                    .setParameter("gender", gender)
                    .setParameter("cusId", cusId);
            q.executeUpdate();
            trans.commit();
            session.flush();
        } catch (HibernateException e) {
            try {
                if (trans != null) {
                    trans.rollback();
                }
            } catch (Exception ex) {
                throw ex;
            }
            throw e;
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
    }

    public Pair<Integer, String> updatePassword(String oldPassword, String newPassword, int cusId) {
        Transaction trans = null;
        Session session = null;
        Pair<Integer, String> result = new Pair(0, "Đã xảy ra lỗi. Vui lòng thực hiện lại sau!");
        try {
            session = com.facade.HibernateConfiguration.getInstance().openSession();
            trans = session.beginTransaction();
            Query q = session.createSQLQuery("UpdatePassword :oldPassword,:newPassword,:cusId")
                    .addScalar("Result", IntegerType.INSTANCE).addScalar("Msg", StringType.INSTANCE);
            q.setParameter("oldPassword", oldPassword)
                    .setParameter("newPassword", newPassword)
                    .setParameter("cusId", cusId);
            Object[] row = (Object[]) q.uniqueResult();
            result = new Pair<>((Integer) row[0], (String) row[1]);
            trans.commit();
            session.flush();
        } catch (HibernateException e) {
            try {
                if (trans != null) {
                    trans.rollback();
                }
            } catch (Exception ex) {
                throw ex;
            }
            e.printStackTrace();
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return result;
    }

    public Customer findEager(int id) {
        Session session = null;
        Customer c = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            Criteria cr = session.createCriteria(Customer.class);
            cr.createAlias("parentID", "parentID", JoinType.LEFT_OUTER_JOIN);
            cr.setFetchMode("parentID", FetchMode.JOIN);
            cr.add(Restrictions.eq("id", id));
            c = (Customer) cr.uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return c;
    }

    public List<Customer> pager(PagerPlus pager) {
        Session session = null;
        List<Customer> list = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Criteria cr = session.createCriteria(Customer.class);
                cr.createAlias("rankCustomer", "rankCustomer", JoinType.LEFT_OUTER_JOIN);
                cr.add(Restrictions.eq("isDeleted", false));
                cr.add(Restrictions.eq("isActive", true));
                Conjunction conj = Restrictions.conjunction();
                if (!StringUtils.isEmpty(pager.getKeyword())) {
                    conj.add(Restrictions.like("userName", pager.getKeyword(), MatchMode.ANYWHERE));
                }
                if (pager.getIsCheck() != null) {
                    conj.add(Restrictions.eq("rankCustomer.id", pager.getIsCheck()));
                }
                cr.add(conj);
                cr.setProjection(Projections.rowCount());
                pager.setTotalResult(((Long) cr.uniqueResult()).intValue());
                cr = session.createCriteria(Customer.class);
                cr.createAlias("rankCustomer", "rankCustomer", JoinType.LEFT_OUTER_JOIN);
                cr.setFetchMode("rankCustomer", FetchMode.JOIN);
                cr.add(Restrictions.eq("isDeleted", false));
                cr.add(Restrictions.eq("isActive", true));
                cr.add(conj);
                cr.setFirstResult(pager.getFirstResult());
                cr.setMaxResults(pager.getDisplayPerPage());
                cr.addOrder(pager.getAsc() ? Order.asc(pager.getOrderColumn()) : Order.desc(pager.getOrderColumn()));
                list = cr.list();
            }
        } catch (Exception e) {
            Logger.getLogger(entityClass.getName()).log(Level.SEVERE, null, e);
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return list;
    }

    public void export(Pager pager, ExcelFile file) {
        Session session = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                String queryStr = "select cus.FullName,cus.UserName,case when cus.Gender=1 then N'Nam' else N'Nữ' end as Gender,cus.Mobile,cus.Email,cus.PeoplesIdentity,"
                        + "ref.UserName as RefID,parent.UserName as ParentID, cus.TotalLeft,cus.TotalRight,cus.SystemAmount,cus.TotalAmount,rank.Name as RankName "
                        + "from Customer cus left join Customer ref on cus.RefID=ref.ID left join Customer parent on cus.ParentID=parent.ID left join RankName rank on cus.RankCustomer=rank.ID where cus.IsDeleted=0 and cus.IsActive=1";

                if (!StringUtils.isEmpty(pager.getKeyword())) {
                    queryStr += " cus.userName like '%" + pager.getKeyword() + "%'";
                }
                Query q = session.createSQLQuery(queryStr)
                        .addScalar("FullName", StringType.INSTANCE)
                        .addScalar("UserName", StringType.INSTANCE)
                        .addScalar("Gender", StringType.INSTANCE)
                        .addScalar("Mobile", StringType.INSTANCE)
                        .addScalar("Email", StringType.INSTANCE)
                        .addScalar("PeoplesIdentity", StringType.INSTANCE)
                        .addScalar("RefID", StringType.INSTANCE)
                        .addScalar("ParentID", StringType.INSTANCE)
                        .addScalar("TotalLeft", BigDecimalType.INSTANCE)
                        .addScalar("TotalRight", BigDecimalType.INSTANCE)
                        .addScalar("SystemAmount", BigDecimalType.INSTANCE)
                        .addScalar("TotalAmount", BigDecimalType.INSTANCE)
                        .addScalar("RankName", StringType.INSTANCE);
                file.setContents(q.list());
                List<String> header = new ArrayList();
                header.add("Họ và tên");
                header.add("Tên đăng nhập");
                header.add("Giới tính");
                header.add("SĐT");
                header.add("Email");
                header.add("Số CMND");
                header.add("Người giới thiệu");
                header.add("Người chỉ định");
                header.add("Tổng trái");
                header.add("Tổng phải");
                header.add("Tổng tuyến");
                header.add("Năng động");
                header.add("Cấp bậc");
                file.setTitles(header);
                file.setFileName("Dach sach thanh vien he thong");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
    }

    public List<Customer> pagerNotActivated(Pager pager) {
        Session session = null;
        List<Customer> list = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Criteria cr = session.createCriteria(Customer.class);
                cr.add(Restrictions.eq("isDeleted", false));
                cr.add(Restrictions.eq("isActive", false));
                if (!StringUtils.isEmpty(pager.getKeyword())) {
                    cr.add(Restrictions.like("userName", "%" + pager.getKeyword() + "%"));
                }
                cr.setProjection(Projections.rowCount());
                pager.setTotalResult(((Long) cr.uniqueResult()).intValue());
                cr = session.createCriteria(Customer.class);
                cr.add(Restrictions.eq("isDeleted", false));
                cr.add(Restrictions.eq("isActive", false));
                if (!StringUtils.isEmpty(pager.getKeyword())) {
                    cr.add(Restrictions.like("userName", "%" + pager.getKeyword() + "%"));
                }
                cr.setFirstResult(pager.getFirstResult());
                cr.setMaxResults(pager.getDisplayPerPage());
                cr.addOrder(pager.getAsc() ? Order.asc(pager.getOrderColumn()) : Order.desc(pager.getOrderColumn()));
                list = cr.list();
            }
        } catch (Exception e) {
            Logger.getLogger(entityClass.getName()).log(Level.SEVERE, null, e);
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return list;
    }

    public List<Customer> pagerBackup(Pager pager, Boolean isBackUp) {
        Session session = null;
        List<Customer> list = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Criteria cr = session.createCriteria(Customer.class);
                Conjunction conj = Restrictions.conjunction();
                conj.add(Restrictions.eq("isDeleted", false));
                if (!StringUtils.isEmpty(pager.getKeyword())) {
                    conj.add(Restrictions.like("userName", "%" + pager.getKeyword() + "%"));
                }
                if (isBackUp != null) {
                    conj.add(Restrictions.eq("isBackUp", isBackUp));
                }
                cr.add(conj);
                cr.setProjection(Projections.rowCount());
                pager.setTotalResult(((Long) cr.uniqueResult()).intValue());
                cr = session.createCriteria(Customer.class);
                cr.add(conj);
                cr.setFirstResult(pager.getFirstResult());
                cr.setMaxResults(pager.getDisplayPerPage());
                cr.addOrder(pager.getAsc() ? Order.asc(pager.getOrderColumn()) : Order.desc(pager.getOrderColumn()));
                list = cr.list();
            }
        } catch (Exception e) {
            Logger.getLogger(entityClass.getName()).log(Level.SEVERE, null, e);
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return list;
    }

    public int edit(String fullName, String email, String mobile, String peoplesIdentity, String bankName, String bankNumber, String bankAgency, boolean gender, int cusId) {
        Transaction trans = null;
        Session session = null;
        Integer result = -1;
        try {
            session = com.facade.HibernateConfiguration.getInstance().openSession();
            trans = session.beginTransaction();
            Query q = session.createSQLQuery("UpdateProfileByAdmin :fullName,:email,"
                    + ":mobile,:peoplesIdentity,:bankName,:bankNumber,:bankAgency,:gender,:cusId");
            q.setParameter("fullName", fullName)
                    .setParameter("email", email)
                    .setParameter("mobile", mobile)
                    .setParameter("peoplesIdentity", peoplesIdentity)
                    .setParameter("bankName", bankName)
                    .setParameter("bankNumber", bankNumber)
                    .setParameter("bankAgency", bankAgency)
                    .setParameter("gender", gender)
                    .setParameter("cusId", cusId);
            result = (Integer) q.uniqueResult();
            trans.commit();
            session.flush();
        } catch (HibernateException e) {
            try {
                if (trans != null) {
                    trans.rollback();
                }
            } catch (Exception ex) {
                throw ex;
            }
            e.printStackTrace();
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return result;
    }

    public Pair<Integer, String> upRank(int cusID, int rankID) throws Exception {
        Transaction trans = null;
        Session session = null;
        Pair<Integer, String> result = new Pair(0, "Đã xảy ra lỗi. Vui lòng thực hiện lại sau!");
        try {
            session = com.facade.HibernateConfiguration.getInstance().openSession();
            trans = session.beginTransaction();
            Query q = session.createSQLQuery("UpRank_Customer :cusID,:rankID")
                    .addScalar("Result", IntegerType.INSTANCE).addScalar("Msg", StringType.INSTANCE);
            q.setParameter("cusID", cusID)
                    .setParameter("rankID", rankID);
            Object[] row = (Object[]) q.uniqueResult();
            result = new Pair<>((Integer) row[0], (String) row[1]);
            trans.commit();
        } catch (HibernateException e) {
            try {
                if (trans != null) {
                    trans.rollback();
                }
            } catch (Exception ex) {
                throw ex;
            }
            e.printStackTrace();
            throw e;
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return result;
    }

    public Pair<Integer, String> backUp(String listCustomerID, Integer adminID) throws Exception {
        Transaction trans = null;
        Session session = null;
        Pair<Integer, String> result = new Pair(0, "Đã xảy ra lỗi. Vui lòng thực hiện lại sau!");
        try {
            session = com.facade.HibernateConfiguration.getInstance().openSession();
            trans = session.beginTransaction();
            Query q = session.createSQLQuery("Reset_customer :listCustomerID,:adminID")
                    .addScalar("Result", IntegerType.INSTANCE).addScalar("Msg", StringType.INSTANCE);
            q.setParameter("listCustomerID", listCustomerID)
                    .setParameter("adminID", adminID);
            Object[] row = (Object[]) q.uniqueResult();
            result = new Pair<>((Integer) row[0], (String) row[1]);
            trans.commit();
        } catch (HibernateException e) {
            try {
                if (trans != null) {
                    trans.rollback();
                }
            } catch (Exception ex) {
                throw ex;
            }
            e.printStackTrace();
            throw e;
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return result;
    }

    public List<CustomerTree> getTreeCustomer(Integer firstId, Integer max_level) {
        Session session = null;
        List list = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session == null) {
                return null;
            }
            Query q = session.createSQLQuery("TreeCustomer :first_id,:max_level").addEntity(CustomerTree.class);
            q.setParameter("first_id", firstId);
            q.setParameter("max_level", max_level);
            list = q.list();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return list;
    }

    public Customer findByUsername(String userName) {
        Session session = null;
        Customer c = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Criteria cr = session.createCriteria(Customer.class);
                cr.add(Restrictions.eq("isDeleted", false));
                cr.add(Restrictions.eq("userName", userName));
                c = (Customer) cr.uniqueResult();
            }
        } catch (Exception e) {
            Logger.getLogger(entityClass.getName()).log(Level.SEVERE, null, e);
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return c;
    }

    public int countChildrenLeaf(int cusId) {
        Session session = null;
        int result = 0;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Query q = session.createSQLQuery("Return_totalmember :cusId")
                        .setParameter("cusId", cusId);
                result = (int) q.uniqueResult();
            }
        } catch (Exception e) {
            result = 0;
            e.printStackTrace();
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return result;
    }

    public Object[] getHistoryChangeRank(int cusId) {
        Session session = null;
        Object[] result = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Query q = session.createSQLQuery("Show_Topinformation :cusId")
                        .addScalar("Level1", DateType.INSTANCE)
                        .addScalar("Level2", DateType.INSTANCE)
                        .addScalar("Level3", DateType.INSTANCE)
                        .addScalar("Level4", DateType.INSTANCE)
                        .setParameter("cusId", cusId);
                result = (Object[]) q.uniqueResult();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return result;
    }

    public List getBasicInfomation(int cusId) {
        Session session = null;
        List result = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Query q = session.createSQLQuery("Show_Dasboard :cusID")
                        .addScalar("ID", IntegerType.INSTANCE)
                        .addScalar("Name", StringType.INSTANCE)
                        .addScalar("ValuesName", StringType.INSTANCE)
                        .setParameter("cusID", cusId);
                q.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
                result = q.list();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return result;
    }

    public List<Customer> getTop5Children(int cusID) {
        Session session = null;
        List<Customer> list = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Criteria cr = session.createCriteria(Customer.class);
                cr.createAlias("rankCustomer", "rankCustomer", JoinType.LEFT_OUTER_JOIN);
                cr.setFetchMode("rankCustomer", FetchMode.JOIN);
                cr.add(Restrictions.eq("isDeleted", false));
                cr.add(Restrictions.eq("isActive", true));
                cr.add(
                        Restrictions.or(
                                Restrictions.like("listCustomerID", "%," + cusID + ",%"),
                                Restrictions.like("listCustomerID", "%," + cusID)
                        )
                );
                cr.setFirstResult(0);
                cr.setMaxResults(5);
                cr.addOrder(Order.desc("rankCustomer")).addOrder(Order.asc("createdDate"));
                list = cr.list();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return list;
    }

    public List<Customer> pagerReferral(PagerPlus pager, Customer cus) {
        Session session = null;
        List<Customer> list = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Criteria cr = session.createCriteria(Customer.class);
                Conjunction conj = Restrictions.conjunction();
                conj.add(Restrictions.eq("isDeleted", false));
                conj.add(Restrictions.eq("isActive", true));
                conj.add(
                        Restrictions.or(
                                Restrictions.like("listCustomerRef", "%," + cus.getId() + ",%"),
                                Restrictions.like("listCustomerRef", "%," + cus.getId())
                        )
                );
                if (pager.getIsCheck() != null) {
                    conj.add(Restrictions.eq("level", cus.getLevelCustomerRef() + pager.getIsCheck()));
                }
                if (!StringUtils.isEmpty(pager.getKeyword())) {
                    conj.add(
                            Restrictions.or(
                                    Restrictions.like("userName", "%" + pager.getKeyword() + "%"),
                                    Restrictions.like("fullName", "%" + pager.getKeyword() + "%")
                            )
                    );
                }
                cr.add(conj);
                cr.setProjection(Projections.rowCount());
                pager.setTotalResult(((Long) cr.uniqueResult()).intValue());
                cr = session.createCriteria(Customer.class);
                cr.add(conj);
                cr.setFirstResult(pager.getFirstResult());
                cr.setMaxResults(pager.getDisplayPerPage());
                cr.addOrder(Order.desc("rankCustomer")).addOrder(Order.asc("createdDate"));
                list = cr.list();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return list;
    }
}
