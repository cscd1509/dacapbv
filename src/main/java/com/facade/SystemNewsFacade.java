package com.facade;

import com.bean.Pager;
import com.entity.Customer;
import com.entity.SystemNews;
import com.utils.StringUtils;
import java.util.List;
import javafx.util.Pair;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;

public class SystemNewsFacade extends AbstractFacade {

    public SystemNewsFacade() {
        super(SystemNews.class);
    }

    public List pager(Pager pager, Customer cus) {
        Session session = null;
        List list = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Criteria cr = session.createCriteria(SystemNews.class);
                cr.createAlias("customerID", "customerID", JoinType.LEFT_OUTER_JOIN);
                Conjunction conj = Restrictions.conjunction();
                if (!StringUtils.isEmpty(pager.getKeyword())) {
                    conj.add(Restrictions.or(
                            Restrictions.like("title", pager.getKeyword(), MatchMode.ANYWHERE),
                            Restrictions.like("shortDescription", pager.getKeyword(), MatchMode.ANYWHERE),
                            Restrictions.like("content", pager.getKeyword(), MatchMode.ANYWHERE)
                    ));
                }
                conj.add(Restrictions.eq("isDeleted", Boolean.FALSE));
                if (cus != null) {
                    Query q = session.createSQLQuery("AvaiabledSystemNews :cusID").addScalar("ID", IntegerType.INSTANCE).setParameter("cusID", cus.getId());
                    conj.add(Restrictions.in("id", q.list()));
                }
                cr.add(conj);
                cr.setProjection(Projections.rowCount());
                pager.setTotalResult(((Long) cr.uniqueResult()).intValue());
                cr = session.createCriteria(SystemNews.class);
                cr.createAlias("createdAdminID", "createdAdminID", JoinType.LEFT_OUTER_JOIN);
                cr.setFetchMode("createdAdminID", FetchMode.JOIN);
                cr.createAlias("customerID", "customerID", JoinType.LEFT_OUTER_JOIN);
                cr.setFetchMode("customerID", FetchMode.JOIN);
                cr.add(conj);
                cr.setFirstResult(pager.getFirstResult());
                cr.setMaxResults(pager.getDisplayPerPage());
                cr.addOrder(pager.getAsc() ? Order.asc(pager.getOrderColumn()) : Order.desc(pager.getOrderColumn()));
                list = cr.list();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return list;
    }

    public SystemNews findEager(int id) {
        Session session = null;
        SystemNews result = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Criteria cr = session.createCriteria(SystemNews.class);
                cr.createAlias("createdAdminID", "createdAdminID", JoinType.LEFT_OUTER_JOIN);
                cr.setFetchMode("createdAdminID", FetchMode.JOIN);
                cr.createAlias("customerID", "customerID", JoinType.LEFT_OUTER_JOIN);
                cr.setFetchMode("customerID", FetchMode.JOIN);
                cr.add(Restrictions.eq("isDeleted", Boolean.FALSE));
                cr.add(Restrictions.eq("id", id));
                result = (SystemNews) cr.uniqueResult();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return result;
    }

    public Pair<Integer, String> create(int createdAdminID, String customerUsername, String title, String urlAvatar,
            String shortDescription, String content) throws Exception {
        Transaction trans = null;
        Session session = null;
        Pair<Integer, String> result = new Pair(0, "Đã xảy ra lỗi. Vui lòng thực hiện lại sau!");
        try {
            session = com.facade.HibernateConfiguration.getInstance().openSession();
            trans = session.beginTransaction();
            Query q = session.createSQLQuery("InsertSystemNews :createdAdminID,:customerUsername,:title,:urlAvatar,"
                    + ":shortDescription,:content")
                    .addScalar("Result", IntegerType.INSTANCE).addScalar("Msg", StringType.INSTANCE);
            q.setParameter("createdAdminID", createdAdminID)
                    .setParameter("customerUsername", customerUsername)
                    .setParameter("title", title)
                    .setParameter("urlAvatar", urlAvatar)
                    .setParameter("shortDescription", shortDescription)
                    .setParameter("content", content);
            Object[] row = (Object[]) q.uniqueResult();
            result = new Pair<>((Integer) row[0], (String) row[1]);
            trans.commit();
        } catch (HibernateException e) {
            try {
                if (trans != null) {
                    trans.rollback();
                }
            } catch (Exception ex) {
                throw ex;
            }
            e.printStackTrace();
            throw e;
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return result;
    }

    public Pair<Integer, String> edit(int id, String customerUsername, String title, String urlAvatar,
            String shortDescription, String content) throws Exception {
        Transaction trans = null;
        Session session = null;
        Pair<Integer, String> result = new Pair(0, "Đã xảy ra lỗi. Vui lòng thực hiện lại sau!");
        try {
            session = com.facade.HibernateConfiguration.getInstance().openSession();
            trans = session.beginTransaction();
            Query q = session.createSQLQuery("EditSystemNews :id,:customerUsername,:title,:urlAvatar,"
                    + ":shortDescription,:content")
                    .addScalar("Result", IntegerType.INSTANCE).addScalar("Msg", StringType.INSTANCE);
            q.setParameter("id", id)
                    .setParameter("customerUsername", customerUsername)
                    .setParameter("title", title)
                    .setParameter("urlAvatar", urlAvatar)
                    .setParameter("shortDescription", shortDescription)
                    .setParameter("content", content);
            Object[] row = (Object[]) q.uniqueResult();
            result = new Pair<>((Integer) row[0], (String) row[1]);
            trans.commit();
        } catch (HibernateException e) {
            try {
                if (trans != null) {
                    trans.rollback();
                }
            } catch (Exception ex) {
                throw ex;
            }
            e.printStackTrace();
            throw e;
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return result;
    }

    public int delete(int id) throws Exception {
        Transaction trans = null;
        Session session = null;
        int result = 0;
        try {
            session = com.facade.HibernateConfiguration.getInstance().openSession();
            trans = session.beginTransaction();
            Query q = session.createSQLQuery("Update SystemNews set IsDeleted=1 where ID=:id");
            q.setParameter("id", id);
            result = q.executeUpdate();
            trans.commit();
        } catch (HibernateException e) {
            try {
                if (trans != null) {
                    trans.rollback();
                }
            } catch (Exception ex) {
                throw ex;
            }
            e.printStackTrace();
            throw e;
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return result;
    }
}
