package com.facade;

import com.bean.Pager;
import com.entity.Agency;
import com.entity.HistoryTransfer;
import com.utils.StringUtils;
import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.util.Pair;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;

public class HistoryTransferFacade extends AbstractFacade {

    public HistoryTransferFacade() {
        super(Agency.class);
    }

    public List pager(Pager pager, Integer adminID) {
        Session session = null;
        List list = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Criteria cr = session.createCriteria(HistoryTransfer.class);
                Conjunction conj = Restrictions.conjunction();
                if (!StringUtils.isEmpty(pager.getKeyword())) {
                    cr.createAlias("sendindAdminID", "sendindAdminID", JoinType.LEFT_OUTER_JOIN);
                    cr.createAlias("receivingAdminID", "receivingAdminID", JoinType.LEFT_OUTER_JOIN);
                    conj.add(Restrictions.or(
                            Restrictions.like("sendindAdminID.userName", pager.getKeyword(), MatchMode.ANYWHERE),
                            Restrictions.like("receivingAdminID.userName", pager.getKeyword(), MatchMode.ANYWHERE)
                    ));
                }
                if (adminID != null) {
                    conj.add(Restrictions.or(
                            Restrictions.eq("sendindAdminID.id", adminID),
                            Restrictions.eq("receivingAdminID.id", adminID)
                    ));
                }
                cr.add(conj);
                cr.setProjection(Projections.rowCount());
                pager.setTotalResult(((Long) cr.uniqueResult()).intValue());
                cr = session.createCriteria(HistoryTransfer.class);
                cr.createAlias("sendindAdminID", "sendindAdminID", JoinType.LEFT_OUTER_JOIN);
                cr.setFetchMode("sendindAdminID", FetchMode.JOIN);
                cr.createAlias("receivingAdminID", "receivingAdminID", JoinType.LEFT_OUTER_JOIN);
                cr.setFetchMode("receivingAdminID", FetchMode.JOIN);
                cr.add(conj);
                cr.setFirstResult(pager.getFirstResult());
                cr.setMaxResults(pager.getDisplayPerPage());
                cr.addOrder(pager.getAsc() ? Order.asc(pager.getOrderColumn()) : Order.desc(pager.getOrderColumn()));
                list = cr.list();
            }
        } catch (Exception e) {
            Logger.getLogger(entityClass.getName()).log(Level.SEVERE, null, e);
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return list;
    }

    public Pair<Integer, String> transfer(int sendingAdminID, String receivingAdminUserName, BigDecimal amount) throws Exception {
        Transaction trans = null;
        Session session = null;
        Pair<Integer, String> result = new Pair(0, "Đã xảy ra lỗi. Vui lòng thực hiện lại sau!");
        try {
            session = com.facade.HibernateConfiguration.getInstance().openSession();
            trans = session.beginTransaction();
            Query q = session.createSQLQuery("TransferAdminToAdmin :sendingAdminID,:receivingAdminUserName,:amount")
                    .addScalar("Result", IntegerType.INSTANCE).addScalar("Msg", StringType.INSTANCE);
            q.setParameter("sendingAdminID", sendingAdminID)
                    .setParameter("receivingAdminUserName", receivingAdminUserName)
                    .setParameter("amount", amount);
            Object[] row = (Object[]) q.uniqueResult();
            result = new Pair<>((Integer) row[0], (String) row[1]);
            trans.commit();
        } catch (HibernateException e) {
            try {
                if (trans != null) {
                    trans.rollback();
                }
            } catch (Exception ex) {
                throw ex;
            }
            e.printStackTrace();
            throw e;
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return result;
    }
}
