package com.facade;

import com.bean.Pager;
import com.entity.Admin;
import com.entity.ModuleInRole;
import com.entity.RoleAdmin;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

public class RoleAdminFacade extends AbstractFacade {

    public RoleAdminFacade() {
        super(RoleAdmin.class);
    }

    @Override
    public List<RoleAdmin> findAll() {
        Session session = null;
        List<RoleAdmin> list = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {

                list = session.createCriteria(RoleAdmin.class).add(Restrictions.eq("isActive", true)).add(Restrictions.eq("isDelete", false)).list();
            }
        } catch (Exception e) {
            Logger.getLogger(RoleAdmin.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return list;
    }
    
    
    @Override
    public RoleAdmin find(int id) {
        Session session = null;
        RoleAdmin obj = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            obj = (RoleAdmin) session.get(RoleAdmin.class, id);
            Hibernate.initialize(obj.getModuleInRoles());
            for(ModuleInRole m:obj.getModuleInRoles()){
                Hibernate.initialize(m);
                Hibernate.initialize(m.getModuleID());
            }
        } catch (Exception e) {
            Logger.getLogger(RoleAdmin.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return obj;
    }
    
    public List pager(Pager pager) {
        Session session = null;
        List list = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Criteria cr = session.createCriteria(RoleAdmin.class);
                cr.add(Restrictions.eq("isDelete", false));
                cr.setProjection(Projections.rowCount());
                pager.setTotalResult(((Long) cr.uniqueResult()).intValue());
                cr = session.createCriteria(RoleAdmin.class);
                cr.add(Restrictions.eq("isDelete", false));
                cr.setFirstResult(pager.getFirstResult());
                cr.setMaxResults(pager.getDisplayPerPage());
                cr.addOrder(pager.getAsc() ? Order.asc(pager.getOrderColumn()) : Order.desc(pager.getOrderColumn()));
                list = cr.list();
            }
        } catch (Exception e) {
            Logger.getLogger(entityClass.getName()).log(Level.SEVERE, null, e);
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return list;
    }
    
    public int delete(int roleId) throws Exception {
        Transaction trans = null;
        Session session = null;
        int result = 0;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            trans = session.beginTransaction();
            result=session.createSQLQuery("update RoleAdmin set isDelete=1 where id=:roleId").setParameter("roleId", roleId).executeUpdate();
            trans.commit();
            session.flush();
        } catch (HibernateException e) {
            try {
                if (trans != null) {
                    trans.rollback();
                }
            } catch (Exception ex) {
                throw ex;
            }
            throw e;
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return result;
    }
}
