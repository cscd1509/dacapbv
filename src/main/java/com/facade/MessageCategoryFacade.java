package com.facade;

import com.entity.MessageCategory;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

public class MessageCategoryFacade extends AbstractFacade{
    
    public MessageCategoryFacade() {
        super(MessageCategory.class);
    }
    
    public List<MessageCategory> getAllAvailableActiomSms(){
        List<MessageCategory> rs=null;
        Session session=null;
        try {
            session=HibernateConfiguration.getInstance().openSession();
            Criteria cr=session.createCriteria(entityClass);
            cr.add(Restrictions.eq("isDelete", false));
            rs=cr.list();
        } catch (Exception e) {
            e.printStackTrace();
        }finally{
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return rs;
    }
}
