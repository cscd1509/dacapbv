package com.facade;

import com.entity.ReporterQuery;
import com.entity.ReporterQueryAlias;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;

public class ReporterQueryAliasFacade extends AbstractFacade {

    public ReporterQueryAliasFacade() {
        super(ReporterQuery.class);
    }

    public List findAllAliasByReportID(int reportID) {
        List list = null;
        Session session = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Criteria cr = session.createCriteria(ReporterQueryAlias.class);
                cr.createAlias("reporterQueryID", "reporterQueryID", JoinType.LEFT_OUTER_JOIN);
                cr.add(Restrictions.eq("reporterQueryID.id", reportID));
                cr.addOrder(Order.asc("id"));
                list = cr.list();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return list;
    }
}
