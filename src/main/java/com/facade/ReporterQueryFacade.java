package com.facade;

import com.bean.Pager;
import com.entity.ReporterQuery;
import com.entity.ReporterQueryAlias;
import com.utils.AliasType;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

public class ReporterQueryFacade extends AbstractFacade {

    public ReporterQueryFacade() {
        super(ReporterQuery.class);
    }

    public List pager(Pager pager, Boolean isActive) {
        List list = null;
        Session session = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Criteria cr = session.createCriteria(ReporterQuery.class);
                Conjunction conj = Restrictions.conjunction();
                conj.add(Restrictions.eq("isDeleted", false));
                if (isActive != null) {
                    conj.add(Restrictions.eq("isActive", isActive));
                }
                cr.add(conj);
                cr.setProjection(Projections.rowCount());
                pager.setTotalResult(((Long) cr.uniqueResult()).intValue());
                cr = session.createCriteria(ReporterQuery.class);
                cr.add(conj);
                cr.setFirstResult(pager.getFirstResult());
                cr.setMaxResults(pager.getDisplayPerPage());
                cr.addOrder(pager.getAsc() ? Order.asc(pager.getOrderColumn()) : Order.desc(pager.getOrderColumn()));
                list = cr.list();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return list;
    }

    public List<Object[]> run(ReporterQuery reporter) {
        List<Object[]> result = null;
        Session session = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                SQLQuery q = session.createSQLQuery(reporter.getsQlQuery());
                reporter.getReporterQueryAliases().forEach(alias -> {
                    q.addScalar(alias.getAliasName(), AliasType.getType(alias.getAliasType().toLowerCase()));
                });
                result = (List<Object[]>) q.list();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return result;
    }

    public void delete(int id) throws Exception {
        Session session = null;
        Transaction trans = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                trans = session.beginTransaction();
                Query query = session.createSQLQuery("update ReporterQuery set isDeleted=:isDeleted where id=:id")
                        .setParameter("isDeleted", true).setParameter("id", id);
                query.executeUpdate();
                trans.commit();
            }
        } catch (Exception e) {
            try {
                if (trans != null) {
                    trans.rollback();
                }
            } catch (Exception ex) {
                throw ex;
            }
            throw e;
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
    }

    public void create(ReporterQuery reporter) throws Exception {
        Transaction trans = null;
        Session session = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            trans = session.beginTransaction();
            session.save(reporter);
            for (ReporterQueryAlias alias : reporter.getReporterQueryAliases()) {
                alias.setReporterQueryID(new ReporterQuery(reporter.getId()));
                session.save(alias);
            }
            trans.commit();
            session.flush();
        } catch (Exception e) {
            try {
                if (trans != null) {
                    trans.rollback();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            e.printStackTrace();
            throw e;
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
    }

    public void edit(ReporterQuery reporter) throws Exception {
        Transaction trans = null;
        Session session = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            trans = session.beginTransaction();
            session.update(reporter);
            session.createSQLQuery("DELETE FROM ReporterQueryAlias WHERE ReporterQueryID=:reporterQueryID").setParameter("reporterQueryID", reporter.getId()).executeUpdate();
            for (ReporterQueryAlias alias : reporter.getReporterQueryAliases()) {
                alias.setReporterQueryID(reporter);
                session.save(alias);
            }
            trans.commit();
        } catch (Exception e) {
            try {
                if (trans != null) {
                    trans.rollback();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            e.printStackTrace();
            throw e;
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
    }
}
