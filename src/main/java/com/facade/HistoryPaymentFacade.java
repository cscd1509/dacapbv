package com.facade;

import com.bean.Pager;
import com.bean.PagerPlus;
import com.entity.Admin;
import com.entity.HistoryPayment;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;

public class HistoryPaymentFacade extends AbstractFacade {

    public HistoryPaymentFacade() {
        super(HistoryPayment.class);
    }

    public List pager(PagerPlus pager, Integer cusID) {
        Session session = null;
        List list = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Criteria cr = session.createCriteria(HistoryPayment.class);
                cr.createAlias("customerID", "customerID",JoinType.LEFT_OUTER_JOIN);
                Conjunction conj = Restrictions.conjunction();
                if (cusID != null) {
                    conj.add(Restrictions.eq("customerID.id", cusID));
                }
                if (pager.getIsCheck() != null) {
                    conj.add(Restrictions.eq("isPayment", pager.getIsCheck() == 0 || pager.getIsCheck() == null ? false : true));
                }
                cr.add(conj);
                cr.setProjection(Projections.rowCount());
                pager.setTotalResult(((Long) cr.uniqueResult()).intValue());
                cr = session.createCriteria(HistoryPayment.class);
                cr.createAlias("customerID", "customerID",JoinType.LEFT_OUTER_JOIN);
                cr.createAlias("circleWithdrawID", "circleWithdrawID",JoinType.LEFT_OUTER_JOIN);
                cr.setFetchMode("circleWithdrawID", FetchMode.JOIN);
                cr.add(conj);
                cr.setFirstResult(pager.getFirstResult());
                cr.setMaxResults(pager.getDisplayPerPage());
                cr.addOrder(pager.getAsc() ? Order.asc(pager.getOrderColumn()) : Order.desc(pager.getOrderColumn()));
                list = cr.list();
            }
        } catch (Exception e) {
            Logger.getLogger(entityClass.getName()).log(Level.SEVERE, null, e);
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return list;
    }
}
