package com.facade;

import com.bean.Pager;
import com.entity.HistoryChangeRank;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;

public class HistoryChangeRankFacade extends AbstractFacade {

    public HistoryChangeRankFacade() {
        super(HistoryChangeRank.class);
    }

    public List pager(Pager pager, Integer cusID) {
        Session session = null;
        List list = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Criteria cr = session.createCriteria(HistoryChangeRank.class);
                cr.createAlias("customerID", "customerID", JoinType.LEFT_OUTER_JOIN);
                Conjunction conj = Restrictions.conjunction();
                if (cusID != null) {
                    conj.add(Restrictions.eq("customerID.id", cusID));
                }
                cr.add(conj);
                cr.setProjection(Projections.rowCount());
                pager.setTotalResult(((Long) cr.uniqueResult()).intValue());
                cr = session.createCriteria(HistoryChangeRank.class);
                cr.createAlias("customerID", "customerID", JoinType.LEFT_OUTER_JOIN);
                cr.createAlias("rankID", "rankID", JoinType.LEFT_OUTER_JOIN);
                cr.setFetchMode("rankID", FetchMode.JOIN);
                if (cusID == null) {
                    cr.setFetchMode("customerID", FetchMode.JOIN);
                }
                cr.add(conj);
                cr.setFirstResult(pager.getFirstResult());
                cr.setMaxResults(pager.getDisplayPerPage());
                cr.addOrder(Order.desc("createdDate"));
                list = cr.list();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return list;
    }
}
