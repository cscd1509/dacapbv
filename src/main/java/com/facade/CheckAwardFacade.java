package com.facade;

import com.bean.Pager;
import com.entity.CheckAward;
import java.math.BigDecimal;
import java.util.List;
import javafx.util.Pair;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;

public class CheckAwardFacade extends AbstractFacade {

    public CheckAwardFacade() {
        super(CheckAward.class);
    }

    public List pager(Pager pager) {
        Session session = null;
        List list = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Criteria cr = session.createCriteria(CheckAward.class);
                cr.add(Restrictions.eq("isDeleted", false));
                cr.setProjection(Projections.rowCount());
                pager.setTotalResult(((Long) cr.uniqueResult()).intValue());
                cr = session.createCriteria(CheckAward.class);
                cr.add(Restrictions.eq("isDeleted", false));
                cr.setFirstResult(pager.getFirstResult());
                cr.setMaxResults(pager.getDisplayPerPage());
                cr.addOrder(pager.getAsc() ? Order.asc(pager.getOrderColumn()) : Order.desc(pager.getOrderColumn()));
                list = cr.list();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return list;
    }

    public List<CheckAward> getAvailableCheckAward() {
        List<CheckAward> rs = null;
        Session session = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            Criteria cr = session.createCriteria(CheckAward.class);
            cr.add(Restrictions.eq("isDeleted", false));
            cr.addOrder(Order.asc("name"));
            rs = cr.list();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return rs;
    }

    public Pair<Integer, String> updateAward(int checkAwardID, BigDecimal newPrice) throws Exception {
        Transaction trans = null;
        Session session = null;
        Pair<Integer, String> result = new Pair(0, "Đã xảy ra lỗi. Vui lòng thực hiện lại sau!");
        try {
            session = com.facade.HibernateConfiguration.getInstance().openSession();
            trans = session.beginTransaction();
            Query q = session.createSQLQuery("Update_Awards :checkAwardID,:newPrice")
                    .addScalar("Result", IntegerType.INSTANCE).addScalar("Msg", StringType.INSTANCE);
            q.setParameter("checkAwardID", checkAwardID)
                    .setParameter("newPrice", newPrice);
            Object[] row = (Object[]) q.uniqueResult();
            result = new Pair<>((Integer) row[0], (String) row[1]);
            trans.commit();
        } catch (HibernateException e) {
            try {
                if (trans != null) {
                    trans.rollback();
                }
            } catch (Exception ex) {
                throw ex;
            }
            e.printStackTrace();
            throw e;
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return result;
    }
}
