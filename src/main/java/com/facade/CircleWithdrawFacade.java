package com.facade;

import com.bean.Pager;
import com.entity.CircleWithdraw;
import com.entity.HistoryPayment;
import java.util.Date;
import java.util.List;
import javafx.util.Pair;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;

public class CircleWithdrawFacade extends AbstractFacade {

    public CircleWithdrawFacade() {
        super(CircleWithdraw.class);
    }

    public List pager(Pager pager) {
        Session session = null;
        List list = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Criteria cr = session.createCriteria(CircleWithdraw.class);
                cr.setProjection(Projections.rowCount());
                pager.setTotalResult(((Long) cr.uniqueResult()).intValue());
                cr = session.createCriteria(CircleWithdraw.class);
                cr.setFirstResult(pager.getFirstResult());
                cr.setMaxResults(pager.getDisplayPerPage());
                cr.addOrder(pager.getAsc() ? Order.asc(pager.getOrderColumn()) : Order.desc(pager.getOrderColumn()));
                list = cr.list();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return list;
    }

    public List circleDetail(int id, Boolean isPayment) {
        Session session = null;
        List result = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            Criteria cr = session.createCriteria(HistoryPayment.class);
            cr.createAlias("circleWithdrawID", "circleWithdrawID",JoinType.LEFT_OUTER_JOIN);
            cr.createAlias("customerID", "customerID",JoinType.LEFT_OUTER_JOIN);
            cr.setFetchMode("customerID", FetchMode.JOIN);
            if (isPayment != null) {
                cr.add(Restrictions.eq("isPayment", isPayment));
            }
            cr.add(Restrictions.eq("circleWithdrawID.id", id));
            cr.addOrder(Order.desc("amount"));
            result = cr.list();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return result;
    }

    public Pair<Integer, String> paid(int paymentID) {
        Transaction trans = null;
        Session session = null;
        Pair<Integer, String> result = new Pair(0, "Đã xảy ra lỗi. Vui lòng thực hiện lại sau!");
        try {
            session = com.facade.HibernateConfiguration.getInstance().openSession();
            trans = session.beginTransaction();
            Query q = session.createSQLQuery("PaidCircle :circleID")
                    .addScalar("Result", IntegerType.INSTANCE).addScalar("Msg", StringType.INSTANCE);
            q.setParameter("circleID", paymentID);
            Object[] row = (Object[]) q.uniqueResult();
            result = new Pair<>((Integer) row[0], (String) row[1]);
            trans.commit();
            session.flush();
        } catch (HibernateException e) {
            try {
                if (trans != null) {
                    trans.rollback();
                }
            } catch (Exception ex) {
                throw ex;
            }
            e.printStackTrace();
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return result;
    }

    public Pair<Integer, String> delete(int paymentID) {
        Transaction trans = null;
        Session session = null;
        Pair<Integer, String> result = new Pair(0, "Đã xảy ra lỗi. Vui lòng thực hiện lại sau!");
        try {
            session = com.facade.HibernateConfiguration.getInstance().openSession();
            trans = session.beginTransaction();
            Query q = session.createSQLQuery("Delete_CircleWithdrawal :circleID")
                    .addScalar("Result", IntegerType.INSTANCE).addScalar("Msg", StringType.INSTANCE);
            q.setParameter("circleID", paymentID);
            Object[] row = (Object[]) q.uniqueResult();
            result = new Pair<>((Integer) row[0], (String) row[1]);
            trans.commit();
            session.flush();
        } catch (HibernateException e) {
            try {
                if (trans != null) {
                    trans.rollback();
                }
            } catch (Exception ex) {
                throw ex;
            }
            e.printStackTrace();
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return result;
    }

    public Pair<Integer, String> createCircleWithdraw(Date fromdate, Date todate) {
        Transaction trans = null;
        Session session = null;
        Pair<Integer, String> result = new Pair(0, "Đã xảy ra lỗi. Vui lòng thực hiện lại sau!");
        try {
            session = com.facade.HibernateConfiguration.getInstance().openSession();
            trans = session.beginTransaction();
            Query q = session.createSQLQuery("Create_CircleWithdrawal :fromdate,:todate")
                    .addScalar("Result", IntegerType.INSTANCE).addScalar("Msg", StringType.INSTANCE);
            q.setParameter("fromdate", fromdate).setParameter("todate", todate);
            Object[] row = (Object[]) q.uniqueResult();
            result = new Pair<>((Integer) row[0], (String) row[1]);
            trans.commit();
            session.flush();
        } catch (HibernateException e) {
            try {
                if (trans != null) {
                    trans.rollback();
                }
            } catch (Exception ex) {
                throw ex;
            }
            e.printStackTrace();
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return result;
    }
}
