package com.listener;

import com.facade.HibernateConfiguration;
import com.utils.ConfigUtils;
import java.util.Properties;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class GeneralContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        String configPath = sce.getServletContext().getRealPath("/WEB-INF/config.support.account.properties");
        Properties props = new ConfigUtils().getProperties(configPath);
        Boolean on = Boolean.valueOf(props.getProperty("support.enable"));
        if (!on) {
            HibernateConfiguration.getInstance().shutdown();
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        HibernateConfiguration.getInstance().shutdown();
    }
}
