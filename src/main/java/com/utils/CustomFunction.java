package com.utils;

import com.bean.CustomerTree;
import com.bean.Pager;
import com.entity.Admin;
import com.entity.Customer;
import com.entity.ModuleInRole;
import com.facade.MessageCategoryFacade;
import com.facade.AdminFacade;
import com.facade.CheckAwardFacade;
import com.facade.CustomerFacade;
import com.facade.HistoryAwardFacade;
import com.facade.RoleAdminFacade;
import com.google.gson.Gson;
import com.warrenstrange.googleauth.GoogleAuthenticator;
import java.math.BigDecimal;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class CustomFunction {
    
    public static int getIntegerRandom() {
        try {
            Random r = new Random();
            return r.nextInt();
        } catch (Exception e) {
            return 0;
        }
    }
    
    public static String getBundle(String localeCode, String key) {
        try {
            Locale locale = localeCode == null ? new Locale("en") : new Locale(localeCode);
            ResourceBundle bundles = ResourceBundle.getBundle("com/bundle/Resources", locale);
            return bundles.getString(key);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public static String md5(String input) {
        MessageDigest md;
        StringBuffer sB;
        String output = null;
        try {
            md = MessageDigest.getInstance("MD5");
            md.update(input.getBytes());
            byte[] digest = md.digest();
            sB = new StringBuffer();
            for (byte b : digest) {
                sB.append(String.format("%02x", b & 0xff));
            }
            output = sB.toString();
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(CustomFunction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return output;
    }
    
    public static Customer findCustomerById(int id) {
        return (Customer) new CustomerFacade().find(id);
    }
    
    public static Customer findCustomerEagerById(int id) {
        return (Customer) new CustomerFacade().findEager(id);
    }
    
    public static List<ModuleInRole> filterModuleInRoleList(List<ModuleInRole> moduleInRoles, Integer parentId) {
        return moduleInRoles.stream().filter(m -> {
            return m.getModuleID().getModule().getId() == parentId;
        }).collect(Collectors.toList());
    }
    
    public static Admin findAdminById(Integer id) {
        return new AdminFacade().findAdminById(id);
    }
    
    public static int size(Collection c) {
        return c == null ? 0 : c.size();
    }
    
    public static String buildPager(Pager pager) {
        String url = "?currentPage=" + pager.getCurrentPage() + "&displayPerPage=" + pager.getDisplayPerPage()
                + "&orderColumn=" + pager.getOrderColumn() + "&asc=" + pager.getAsc() + "&keyword=" + pager.getKeyword();
        return url;
    }
    
    public static List findAllAvailableRoleAdmin() {
        return new RoleAdminFacade().findAll();
    }
    
    public static List getAvailableCheckAward() {
        return new CheckAwardFacade().getAvailableCheckAward();
    }
    
    public static boolean checkModuleInrole(List<ModuleInRole> moduleInRoles, int moduleId) {
        return moduleInRoles.stream().filter(m -> {
            return Objects.equals(m.getModuleID().getId(), moduleId);
        }).collect(Collectors.toList()).size() > 0;
    }
    
    public static List<CustomerTree> filterCustomerList(List<CustomerTree> customerList, Integer parentId) {
        return customerList.stream().filter(c -> {
            return Objects.equals(c.getBoss(), parentId);
        }).collect(Collectors.toList());
    }
    
    private static String generateRankColor(int rank) {
        StringBuilder sB = new StringBuilder();
        switch (rank) {
            case 1: {
                sB.append(" class='text-success'>");
                break;
            }
            case 2: {
                sB.append(" class='text-warning'>");
                break;
            }
            case 3: {
                sB.append(" class='text-info'>");
                break;
            }
            case 4: {
                sB.append(" class='text-danger'>");
                break;
            }
            default: {
                sB.append(">");
                break;
            }
        }
        return sB.toString();
    }
    
    public static String buildTreeCustomer(List<CustomerTree> customerList, Integer first_id, String rootController) {
        List<CustomerTree> list = filterCustomerList(customerList, first_id);
        StringBuilder sB = new StringBuilder();
        sB.append("<ul>");
        list.stream().forEach((c) -> {
            String popOver = "<b data-toggle='popover' data-placement='right' data-title='" + c.getName().toUpperCase() + "' ";
            popOver += "data-content='"
                    + "<b>Tiêu dùng trái:</b> " + customFormatDecimal("###,###,##0 bv", c.getTotalLeft()) + "<br/>"
                    + "<b>Tiêu dùng phải:</b> " + customFormatDecimal("###,###,##0 bv", c.getTotalRight()) + "<br/>"
                    + "<b>Chiết khấu trái:</b> " + customFormatDecimal("###,###,##0 bv", c.getDiscountLeft()) + "<br/>"
                    + "<b>Chiết khấu phải:</b> " + customFormatDecimal("###,###,##0 bv", c.getDiscountRight()) + "<br/>"
                    + "<b>Thành viên lên 5000 trái :</b> " + c.getListUser5000L() + "<br/>"
                    + "<b>Thành viên lên 5000 phải :</b> " + c.getListUser5000R() + "<br/>'>";
            if (c.getTotalChildren() > 0) {
                if (c.getRelativeLevel() == 3) {
                    sB.append("<li class='next-item'><a>");
                    sB.append("<span class='ti-plus' controller='")
                            .append(rootController).append("/Next/");
                    sB.append(c.getKey());
                    sB.append("/");
                    sB.append(c.getKey());
                    sB.append("'></span>&nbsp;");
                } else {
                    sB.append("<li><a>");
                    sB.append("<span class='ti-minus'></span>&nbsp;");
                }
            } else {
                sB.append("<li><a>");
                sB.append("<span class='ti-user'></span>&nbsp;");
            }
            sB.append("<span data-id='").append(c.getKey()).append("'>")
                    .append(popOver)
                    .append(c.getUserName()).append("</b>");
            sB.append("</span></a>");
            if (c.getRelativeLevel() != 3) {
                sB.append(buildTreeCustomer(customerList, c.getKey(), rootController));
            }
            sB.append("</li>");
        });
        sB.append("</ul>");
        return sB.toString();
    }
    
    public static String formatDate(Date date) {
        return new SimpleDateFormat("MMMM dd, yyyy").format(date);
    }
    
    public static String customFormatDate(String format, Date date) {
        return new SimpleDateFormat(format).format(date);
    }
    
    public static String customFormatDecimal(String format, BigDecimal num) {
        num = num == null ? BigDecimal.ZERO : num;
        return new DecimalFormat(format).format(num);
    }
    
    public static String subString(String input, int start, int end) {
        if (StringUtils.isEmpty(input)) {
            return null;
        }
        return input.length() > end ? input.substring(start, end) + "..." : input;
    }
    
    public static String getJSON(Object object) {
        Gson gson = new Gson();
        return gson.toJson(object);
    }
    
    public static List getAllAvailableActiomSms() {
        return new MessageCategoryFacade().getAllAvailableActiomSms();
    }
    
    public static int getDay(Date startDate) {
        Date endDate = new Date();
        long startTime = startDate.getTime();
        long endTime = endDate.getTime();
        long diffTime = endTime - startTime;
        long diffDays = diffTime / (1000 * 60 * 60 * 24);
        return ((Long) diffDays).intValue();
    }
    
    public static Date getCurrentTime() {
        return new Date();
    }
    
    public static int dateDiffToDays(Date startDate, Date enddate) {
        return (int) (enddate.getTime() - startDate.getTime()) / (1000 * 60 * 60 * 24) + 1;
    }
    
    public static boolean verify(String secret, String code) {
        try {
            int intCode = Integer.valueOf(code);
            return verify(secret, intCode);
        } catch (NumberFormatException e) {
        }
        return false;
    }
    
    public static boolean verify(String secret, int code) {
        GoogleAuthenticator gauth = new GoogleAuthenticator();
        return gauth.authorize(secret, code);
//        return 
    }
    
    public static boolean compareString(String str1, String str2) {
        return StringUtils.isEquals(str1, str2);
    }
    
    public static BigDecimal sumAwardByCusID(int cusId) {
        return new HistoryAwardFacade().sumAwardByCusID(cusId);
    }
}
