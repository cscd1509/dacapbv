/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utils;

import org.apache.commons.text.WordUtils;
import org.hibernate.type.AbstractSingleColumnStandardBasicType;
import org.hibernate.type.BigDecimalType;
import org.hibernate.type.BooleanType;
import org.hibernate.type.DateType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;

/**
 *
 * @author pc3-cellx
 */
public enum AliasType {
    ALIAS_TYPE_INT(IntegerType.INSTANCE.getName(), IntegerType.INSTANCE),
    ALIAS_TYPE_DECIMAL(BigDecimalType.INSTANCE.getName(), BigDecimalType.INSTANCE),
    ALIAS_TYPE_STRING(StringType.INSTANCE.getName(), StringType.INSTANCE),
    ALIAS_TYPE_DATE(DateType.INSTANCE.getName(), DateType.INSTANCE),
    ALIAS_TYPE_BOOLEAN(BooleanType.INSTANCE.getName(), BooleanType.INSTANCE);
    private final String name;
    private final AbstractSingleColumnStandardBasicType type;

    private AliasType(String typeName, AbstractSingleColumnStandardBasicType type) {
        this.name = typeName;
        this.type = type;
    }

    @Override
    public String toString() {
        return WordUtils.capitalize(this.name); //To change body of generated methods, choose Tools | Templates.
    }

    public String getName() {
        return WordUtils.capitalize(this.name);
    }

    public AbstractSingleColumnStandardBasicType getType() {
        return type;
    }

    public static AbstractSingleColumnStandardBasicType getType(String name) {
        for (AliasType alias : AliasType.values()) {
            if (alias.name.equals(name)) {
                return alias.type;
            }
        }
        return null;
    }
}
