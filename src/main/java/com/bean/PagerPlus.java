package com.bean;

import java.math.BigDecimal;
import java.util.Date;

public class PagerPlus extends Pager {

    private Integer isCheck = null;
    private BigDecimal amount = BigDecimal.ZERO;
    private Date startDate = null;
    private Date endDate = null;
    private Integer isAvailability = null;

    public Integer getIsCheck() {
        return isCheck;
    }

    public void setIsCheck(Integer isCheck) {
        this.isCheck = isCheck;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Integer getIsAvailability() {
        return isAvailability;
    }

    public void setIsAvailability(Integer isAvailability) {
        this.isAvailability = isAvailability;
    }

}
