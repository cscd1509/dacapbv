package com.bean;

public class Alert {
    private static final String TYPE_ERROR = "error";
    private static final String TYPE_SUCCESS = "success";
    private static final String TYPE_WARNING = "warning";
    private static final String TYPE_INFO = "information";

    public static String createErrorAlert(String text) {
        StringBuilder sB = new StringBuilder();
        sB.append("<script>")
                .append("noty({text: \"")
                .append(text)
                .append("\", layout: \"bottomCenter\", type: \"")
                .append(TYPE_ERROR)
                .append("\"});")
                .append("</script>");
        return sB.toString();
    }

    public static String createSuccessAlert(String text) {
        StringBuilder sB = new StringBuilder();
        sB.append("<script>")
                .append("noty({text: \"")
                .append(text)
                .append("\", layout: \"bottomCenter\", type: \"")
                .append(TYPE_SUCCESS)
                .append("\"});")
                .append("</script>");
        return sB.toString();
    }

    public static String createWarningAlert(String text) {
        StringBuilder sB = new StringBuilder();
        sB.append("<script>")
                .append("noty({text: \"")
                .append(text)
                .append("\", layout: \"bottomCenter\", type: \"")
                .append(TYPE_WARNING)
                .append("\"});")
                .append("</script>");
        return sB.toString();
    }

    public static String createInfoAlert(String text) {
        StringBuilder sB = new StringBuilder();
        sB.append("<script>")
                .append("noty({text: \"")
                .append(text)
                .append("\", layout: \"bottomCenter\", type: \"")
                .append(TYPE_INFO)
                .append("\"});")
                .append("</script>");
        return sB.toString();
    }

    public static String createReloadPage() {
        StringBuilder sB = new StringBuilder();
        sB.append("<script>location.reload();</script>");
        return sB.toString();
    }

    public static String createRedirectPage(String href) {
        StringBuilder sB = new StringBuilder();
        sB.append("<script>window.location.href='");
        sB.append(href);
        sB.append("';</script>");
        return sB.toString();
    }
}
