package com.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class CustomerTree implements Serializable {

    @Id
    private Integer key;
    private String name;
    private String userName;
    private Integer boss;
    private String parentName;
    private Integer level;
    private Integer relativeLevel;
    private Integer totalChildren;
    private BigDecimal totalLeft;
    private BigDecimal totalRight;
    private BigDecimal discountLeft;
    private BigDecimal discountRight;
    private String listUser5000L;
    private String listUser5000R;

    public CustomerTree() {
    }

    public Integer getKey() {
        return key;
    }

    public void setKey(Integer key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getBoss() {
        return boss;
    }

    public void setBoss(Integer boss) {
        this.boss = boss;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public Integer getTotalChildren() {
        return totalChildren;
    }

    public void setTotalChildren(Integer totalChildren) {
        this.totalChildren = totalChildren;
    }    

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getRelativeLevel() {
        return relativeLevel;
    }

    public void setRelativeLevel(Integer relativeLevel) {
        this.relativeLevel = relativeLevel;
    }

    public BigDecimal getTotalLeft() {
        return totalLeft;
    }

    public void setTotalLeft(BigDecimal totalLeft) {
        this.totalLeft = totalLeft;
    }

    public BigDecimal getTotalRight() {
        return totalRight;
    }

    public void setTotalRight(BigDecimal totalRight) {
        this.totalRight = totalRight;
    }

    public BigDecimal getDiscountLeft() {
        return discountLeft;
    }

    public void setDiscountLeft(BigDecimal discountLeft) {
        this.discountLeft = discountLeft;
    }

    public BigDecimal getDiscountRight() {
        return discountRight;
    }

    public void setDiscountRight(BigDecimal discountRight) {
        this.discountRight = discountRight;
    }

    public String getListUser5000L() {
        return listUser5000L;
    }

    public void setListUser5000L(String listUser5000L) {
        this.listUser5000L = listUser5000L;
    }

    public String getListUser5000R() {
        return listUser5000R;
    }

    public void setListUser5000R(String listUser5000R) {
        this.listUser5000R = listUser5000R;
    }
    
}
