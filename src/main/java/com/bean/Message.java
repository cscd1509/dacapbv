package com.bean;

public class Message {

    private String title;
    private String msg;
    private String msgClass;
    
    public static String INFO_MESSAGE_CLASS="message-info";
    public static String WARNING_MESSAGE_CLASS="message-warning";
    public static String SUCCESS_MESSAGE_CLASS="message-success";
    public static String ERROR_MESSAGE_CLASS="message-error";

    public Message(){}
    public Message(String title, String msg, String msgClass) {
        this.title = title;
        this.msg = msg;
        this.msgClass = msgClass;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getMsgClass() {
        return msgClass;
    }

    public void setMsgClass(String msgClass) {
        this.msgClass = msgClass;
    }

}
