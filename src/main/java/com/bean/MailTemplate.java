package com.bean;

import com.utils.CustomFunction;

public class MailTemplate {

    public static String createRsPassTemp(String contextPath, String lastName, String userName, String newPass) {
        StringBuilder sB = new StringBuilder();
        sB.append("<h1>Hi ")
                .append(lastName)
                .append(",</h1><br/>")
                .append("<b>We've received a request to reset your")
                .append(" password.</b><br/>")
                .append("You can reset your")
                .append(" password by clicking the link bellow:<br/>")
                .append("<a href='").append(contextPath)
                .append("/ConfirmResetPassword/")
                .append(userName)
                .append("/")
                .append(CustomFunction.md5(newPass))
                .append("'>")
                .append(contextPath)
                .append("/ConfirmResetPassword/")
                .append(userName)
                .append("/")
                .append(CustomFunction.md5(newPass))
                .append("</a>").append("<br/>")
                .append("<p style='color:red'>If you didn't make this request then you can safely ignore this email :)</p>")
                .append("Thanks, <br/>")
                .append("The BV company");
        return sB.toString();
    }
}
